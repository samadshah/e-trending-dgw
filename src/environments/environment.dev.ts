export const environment = {
    production: false,
    auth_url: 'http://dbu-export-03.systemsltd.local:1310/identity_v2/auth',
    base_api_url: 'http://dbu-export-03.systemsltd.local:1310/services_v4',
    log_level: 5, // OFF = 0,  ERROR = 1,  WARN = 2,  INFO = 3,  DEBUG = 4,  LOG = 5,
    root_path: '/portal_v4/',
    captcha_key: '6LdZnDkUAAAAAGCzAcsbxqs-eqWM9VbZPPC3VHWB',
    captcha_enable: 1,    // 0 disable greater then 0 enable
    use_cache: true,
    cache_expiry: 172800000 // 48 hours
};
