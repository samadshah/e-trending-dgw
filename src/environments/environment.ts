// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// export const environment = {
//   production: false,
//   auth_url: 'https://localhost:44333/auth',
//   base_api_url: 'http://localhost:12600',
//   log_level: 5, // OFF = 0,  ERROR = 1,  WARN = 2,  INFO = 3,  DEBUG = 4,  LOG = 5,
//   root_path: '/',
//   captcha_key: '6LdZnDkUAAAAAGCzAcsbxqs-eqWM9VbZPPC3VHWB',
//   captcha_enable: 1,    // 0 disable greater then 0 enable
//   use_cache: true,
//   cache_expiry: 172800000 // 48 hours
// };

// export const environment = {
//   production: true,
//   auth_url: 'http://10.100.101.134:8080/DGW-Identity/core',
//   base_api_url: 'http://10.100.101.134/DGW-WebAPI',
//   log_level: 5, // OFF = 0,  ERROR = 1,  WARN = 2,  INFO = 3,  DEBUG = 4,  LOG = 5,
//   root_path: '/',
//   captcha_key: '6LfYiJUUAAAAAEZNVqDLMvKvYgdqP1_j3D0YcUso',
//   captcha_enable: 0,    // 0 disable, greater then 0 enable
//   use_cache: true,
//   cache_expiry: 172800000 // 48 hours
// };


export const environment = {
  production: true,
  // auth_url: 'http://10.100.101.134:8080/DGW-Identity/core',
  auth_url: 'http://20.46.41.2:8080/EProcurement-Identity/core',
  base_api_url: 'http://20.46.41.2:8080/EProcurement-WebAPI',
  log_level: 5, // OFF = 0,  ERROR = 1,  WARN = 2,  INFO = 3,  DEBUG = 4,  LOG = 5,
  root_path: '/',
  // root_path: '/EProcurement-UI/',
  captcha_key: '6LfYiJUUAAAAAEZNVqDLMvKvYgdqP1_j3D0YcUso',
  captcha_enable: 0,    // 0 disable, greater then 0 enable
  use_cache: true,
  cache_expiry: 172800000 // 48 hours
};