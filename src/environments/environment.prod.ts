export const environment = {
    production: true,
    //  auth_url: 'http://192.168.128.86/EsaadIdentity/core',
    auth_url: 'http://10.100.101.134:8080/DGW-Identity/core',
    //  base_api_url: 'http://192.168.128.86/VendorPortal',
    base_api_url: 'http://10.100.101.134:8080/DGW-WebAPI',
    // base_api_url: 'http://10.100.101.134/DGW-WebAPI',
    //  root_path: '/portal_v1/',
    root_path: '/DGW-UI/',
    captcha_key: '6LfYiJUUAAAAAEZNVqDLMvKvYgdqP1_j3D0YcUso',
    log_level: 5, // OFF = 0,  ERROR = 1,  WARN = 2,  INFO = 3,  DEBUG = 4,  LOG = 5,
    captcha_enable: 0,    // 0 disable greater then 0 enable
    use_cache: true,
    cache_expiry: 172800000, // 48 hours
    version: '02/07/2018 - v1'     // MM/DD/YYYY - version
};
