export const environment = {
    production: true,
    auth_url: 'http://10.100.101.134:8080/DGW-Identity/core',
    base_api_url: 'http://10.100.101.134/DGW-WebAPI',
    log_level: 5, // OFF = 0,  ERROR = 1,  WARN = 2,  INFO = 3,  DEBUG = 4,  LOG = 5,
    root_path: '/',
    captcha_key: '6LfYiJUUAAAAAEZNVqDLMvKvYgdqP1_j3D0YcUso',
    captcha_enable: 0,    // 0 disable, greater then 0 enable
    use_cache: true,
    cache_expiry: 172800000 // 48 hours
};
