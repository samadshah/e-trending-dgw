export const environment = {
    production: true,
    // auth_url: 'http://khilt-2027:8031/EsaadIdentity/core',
    auth_url: 'http://192.168.128.86/EsaadIdentity/core',
    // base_api_url: 'http://pmo-vm-06.systemsltd.local:1310/services_v4',
    base_api_url: 'http://192.168.128.86/VendorPortal',
    log_level: 5, // OFF = 0,  ERROR = 1,  WARN = 2,  INFO = 3,  DEBUG = 4,  LOG = 5,
    root_path: '/',
    captcha_key: '6LfYiJUUAAAAAEZNVqDLMvKvYgdqP1_j3D0YcUso',
    captcha_enable: 0,    // 0 disable, greater then 0 enable
    use_cache: true,
    cache_expiry: 172800000 // 48 hours
};
