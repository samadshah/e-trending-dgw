
export class ResetPassword {
    oldPassword: string;
    newPassword: string;
    confirmPassword: string;
}

export class IResetPassword {
    oldPassword: string;
    newPassword: string;
    confirmPassword: string;
}
