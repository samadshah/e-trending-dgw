import { Component, OnInit, Inject } from "@angular/core";
import { PasswordService } from "./password.service";
import { ResetPassword } from "./models/reset-password.model";
import {
  HttpStatusCode,
  MessageText,
  RedirectUrl,
  storage
} from "../core/configuration/config";
import { OAuthService } from "angular-oauth2-oidc";
import { Router } from "@angular/router";
import { Logger } from "angular2-logger/core";
import { HttpErrorResponse } from "@angular/common/http";
import { APP_BASE_HREF } from "@angular/common";
import { AppInitial } from "../core/models/appInitials";
import { AppSettingService } from "../core/services/app-setting.service";
import { NotificationsService } from "angular2-notifications/dist";
import { DashBoardDataService } from "../main/dashboard/dashboard-data.service";
import { CompanyMeta } from "../main/dashboard/models/company-meta.model";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.css"],
  providers: [PasswordService, DashBoardDataService]
})
export class ResetPasswordComponent implements OnInit {
  input: ResetPassword = new ResetPassword();
  serverMessage: string;
  resetPwdBtnDisable = false;
  urls = RedirectUrl;
  logoPath = "";
  appInitial: AppInitial;
  companyData: CompanyMeta = new CompanyMeta();
  checkPassLength = true;
  constructor(
    private dashBoardDataService: DashBoardDataService,
    private notificationsService: NotificationsService,
    private resetPasswordService: PasswordService,
    private oauthService: OAuthService,
    private router: Router,
    private logger: Logger,
    private appSettingService: AppSettingService,
    @Inject(APP_BASE_HREF) private baseHref: string
  ) {
    this.logoPath =
      "../" +
      baseHref +
      "/assets/images/" +
      appSettingService.settings.logoName;
    this.appInitial = new AppInitial(
      appSettingService.settings.logoText,
      appSettingService.settings.appName
    );
  }

  ngOnInit() {
    this.initComponent();
  }

  private initComponent() {
    this.dashBoardDataService.getCompanyMeta().subscribe(data => {
      this.companyData = data;
    });
  }

  public resetPassword(isValid) {
    //!
    this.checkPassLength = true;
    if (!isValid) {
      return;
    }

    if (this.resetPwdBtnDisable === true) {
      return false;
    }

    if (this.input.newPassword !== this.input.confirmPassword) {
      return false;
    }

    //!
    if (this.input.newPassword.length < 12) {
      this.checkPassLength = false;
      return false;
    }

    this.resetPwdBtnDisable = true;
    //!
    this.resetPasswordService.resetPwd(this.input).subscribe(
      resp => {
        if (resp.isSuccess == true) {
          this.notificationsService.success(MessageText.passwordChangeSuccess);
          //!
          // this.refreshToken();
          this.router.navigate([RedirectUrl.root]);
          this.resetPwdBtnDisable = false;
        } else {
          this.resetPwdBtnDisable = false;
          this.notificationsService.error(resp.message);
        }
      },
      err => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === HttpStatusCode.badRequest) {
            this.resetPwdBtnDisable = false;
            const message = err.error.error || err.error.Message;
            this.setServerMessage(message);
          }
        }
        this.resetPwdBtnDisable = false;
      }
    );
  }

  private refreshToken() {
    this.oauthService
      .refreshToken()
      .then(() => {
        const item = localStorage.getItem(storage.key);
        const obj = JSON.parse(item);
        obj["PasswordChanged"] = new Date().getTime();
        localStorage.setItem(storage.key, JSON.stringify(obj));
        this.router.navigate([RedirectUrl.root]);
      })
      .catch(response => {
        this.logger.error(response);
      });
  }

  private setServerMessage(message) {
    this.serverMessage = message;
  }

  private logoutRedirect() {
    this.oauthService.logOut();
    this.router.navigate([RedirectUrl.login]);
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }
}
