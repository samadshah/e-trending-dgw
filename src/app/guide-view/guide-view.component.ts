import { Component, OnInit, Inject } from "@angular/core";
import { AttachmentService } from "../shared/services/attachment-service";
import { Guide, HttpStatusCode } from "../core/configuration/config";
import { HttpErrorResponse } from "@angular/common/http";
import { NotificationsService } from "angular2-notifications/dist";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-guide-view",
  templateUrl: "./guide-view.component.html",
  styleUrls: ["./guide-view.component.css"]
})
export class GuideViewComponent implements OnInit {
  guide = Guide;
  clicked = false;
  type = "";
  constructor(
    private route: ActivatedRoute,
    private attachmentService: AttachmentService,
    private notificationsService: NotificationsService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      // data is now an instance of type ItemsResponse, so you can do this:
      this.type = params["doc"];
      this.openGuideFoViewOrDownload(this.type);
    });
  }

  private openGuideFoViewOrDownload(type) {
    if (this.clicked === true) {
      return false;
    }
    this.clicked = true;
    this.attachmentService.downloadGuideAttachment(type).subscribe(
      resp => {
        this.clicked = false;
        const blob = new Blob([resp.content], { type: resp.content.type });
        const fileName = this.getFileName();
        const url = window.URL.createObjectURL(blob);
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(blob, fileName);
        } else {
          // const objectUrl = URL.createObjectURL(blob);
          window.location.href = url;
        }
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.message;
          this.notificationsService.error(message);
        }
        this.clicked = false;
      }
    );
  }

  getFileName(): string {
    if (this.type === this.guide.bidding) {
      return "RFQ Bidding Guide";
    }
    if (this.type === this.guide.contact) {
      return "contact Guide";
    }
    if (this.type === this.guide.registrationGuide) {
      return "Registration Guide";
    }

    if (this.type === this.guide.registerOrganisation) {
      return "Registration Organisation";
    }

    if (this.type === this.guide.registerPerson) {
      return "Registration Person";
    }
  }
}
