
import { APP_BASE_HREF } from '@angular/common';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgForageCache, NgForageModule } from '@ngforage/ngforage-ng5';
import { NgProgressModule } from '@ngx-progressbar/core';
import { OAuthModule } from 'angular-oauth2-oidc';
import { SimpleNotificationsModule } from 'angular2-notifications/dist';
import { RecaptchaModule } from 'ng-recaptcha';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LayoutModule } from './layout/layout.module';
import { LoginComponent } from './login/login.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SharedModule } from './shared/shared.module';
import { SupplierGuidComponent } from './supplier-guid/supplier-guid.component';
import { RfqPrintComponent } from './rfq-print/rfq-print.component';
import { BidResolve } from './main/rfq/bid-resolve.resolve';
import { BidDetailService } from './main/rfq/rfq-service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { AppSettingService } from './core/services/app-setting.service';
import { CountriesService } from './core/services/countries.service';
import { GuideViewComponent } from './guide-view/guide-view.component';
//!!
import { PaymentService } from './shared/services/payment-service';

export function appSettingInitFactory(appSettingsService: AppSettingService) {
  return () => appSettingsService.loadAppSettings();
}

export function countriesInitFactory(countriesService: CountriesService) {
  return () => countriesService.loadCountries();
}

import { LoginService } from './login/login.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent,
    SupplierGuidComponent,
    RfqPrintComponent,
    GuideViewComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    AppRoutingModule,
    SharedModule,
    CoreModule.forRoot(),
    LayoutModule,
    RecaptchaModule.forRoot(),
    SimpleNotificationsModule.forRoot(),
    OAuthModule.forRoot(),
    NgForageModule,
    NgProgressModule.forRoot(),
    NgbModule.forRoot(),
    LoadingBarHttpClientModule,
    LoadingBarRouterModule
  ],
  exports: [
    OAuthModule,
  ],
  providers: [
    AppSettingService,
    {
      provide: APP_INITIALIZER,
      useFactory: appSettingInitFactory,
      deps: [AppSettingService],
      multi: true
    },
    CountriesService,
    {
      provide: APP_INITIALIZER,
      useFactory: countriesInitFactory,
      deps: [CountriesService],
      multi: true
    },
    BidResolve,
    BidDetailService,
    { provide: APP_BASE_HREF, useValue: environment.root_path },
    NgForageCache,
    LoginService,
    //!!
    PaymentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
