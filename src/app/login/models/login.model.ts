export class Login {
    constructor(public userName: string,
        public password: string,
        public captcha: boolean) {
    }
}

export interface ILogin {
    userName: string;
    password: string;
    captcha: boolean;
}
