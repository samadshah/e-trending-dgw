import { AfterViewInit, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgProgress } from '@ngx-progressbar/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Logger } from 'angular2-logger/core';
import { environment } from '../../environments/environment';
import { HttpStatusCode, MessageText, RedirectUrl, Regex, storage } from '../core/configuration/config';
import { AppSettingService } from '../core/services/app-setting.service'
import { ResetPasswordService } from '../core/services/reset-password/reset-password.service';
import { VendorService } from '../core/services/vendor/vendor.service';
import { Login } from './models/login.model';
import { APP_BASE_HREF } from '@angular/common';
import { AppInitial } from '../core/models/appInitials';
import { NgForageCache } from '@ngforage/ngforage-ng5';
import { LoginService } from './login.service';

declare var captcha: any;

@Component({
    selector: 'app-home',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [LoginService]
})
export class LoginComponent implements OnInit, AfterViewInit {

    @ViewChild('captchaChild')
    captchaChild: any;

    serverMessage = '';
    public form: Login = new Login('', '', false);

    returnUrl: string;
    regex: any;
    inProgress = false;
    loginBtnDisable = false;
    timer: any;
    appbase = '';
    logoPath = '';
    appInitial: AppInitial;
    enableCaptacha = 1;

    constructor(
        private appSettingService: AppSettingService,
        private route: ActivatedRoute,
        private oauthService: OAuthService,
        private router: Router,
        private logger: Logger,
        private vendorService: VendorService,
        private resetPasswordService: ResetPasswordService,
        private loginService: LoginService,
        public progress: NgProgress,
        private readonly ngfc: NgForageCache,
        @Inject(APP_BASE_HREF) private baseHref: string, ) {
        this.enableCaptacha = environment.captcha_enable;
        this.appbase = baseHref;
        this.logoPath = '../' + baseHref + '/assets/images/' + appSettingService.settings.logoName;
        this.appInitial = new AppInitial(appSettingService.settings.logoText, appSettingService.settings.appName);
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.regex = Regex;
    }

    ngAfterViewInit(): void {
        // this.adjustCaptcha();
        captcha.realPerson('#realPersonUser', 'realPersonHash');
        this.form.captcha = false;

    }

    private autoLogin() {
        if (this.oauthService.getAccessToken()) {
            this.refreshToken();
        }
    }

    private refreshToken() {
        this.oauthService.refreshToken()
            .then(() => {
                this.logger.info('Token refreshed successfully.');
                this.router.navigate([this.returnUrl]);
            })
            .catch((response) => {
                this.logger.info('Error in Token refresing.');
                this.logger.error(response);
            });
    }

    public login(valid: boolean) {
        //!!
        this.checkRecaptchaValidity();

        this.serverMessage = '';
        if (this.enableCaptacha === 0) { // 0 means disable
            this.form.captcha = true;
        }
        if (!valid || !this.form.captcha) {
            return;
        }
        this.logger.debug('Loading Start');
        this.progress.start();
        this.inProgress = true;
        if (this.loginBtnDisable === true) {
            return false;
        }
        this.loginBtnDisable = true;
        this.tryClearingBoth();
        this.loginServerCall();
    }

    //!!
    checkRecaptchaValidity() {
      let text1 = $("#realPersonUser").val();
      let text1Hash = this.getHashCode(text1);
      let hash = captcha.getHash("#realPersonUser");

      if (text1 != "") {
        if (hash != text1Hash) {
            // console.log('no');
            this.form.captcha = false;
        } else {
            // console.log('yes');
            this.form.captcha = true;
        }
      }
    }

    //!!
    getHashCode(value) {
      var hash = 5381;
      if (value.length == 0) return hash;
      for (let i = 0; i < value.length; i++) {
        let char = value.charCodeAt(i);
        hash = (hash << 5) + hash + char;
        hash = hash & hash;
      }
      return hash;
    }

    //!
    public loginServerCall() {
        console.log(this.form);
      let pwd = encodeURIComponent(this.form.password);
        this
          .oauthService
          .fetchTokenUsingPasswordFlow(this.form.userName, pwd)
          // .fetchTokenUsingPasswordFlowAndLoadUserProfile(this.form.userName, this.form.password)
          .then((token) => {
              if (token) {
                  this.loginService.getUserClaim().subscribe(claim => {
                    //!
                    localStorage.setItem('claim', JSON.stringify(claim));

                    this.vendorService.initialLoad(claim);
                    if (this.resetPasswordService.isFirstLogin(claim)) {
                        this.router.navigate([RedirectUrl.passwordReset]);
                    } else {
                      //!
                      this.router.navigate([RedirectUrl.home]);
                    }
                  });
              }
              this.loginBtnDisable = false;
              // this.callTimeout();
          })
          .catch((response) => {
              this.loginBtnDisable = false;
              this.inProgress = false;
              this.logger.debug('Loading end');
              this.progress.done();

              if (response.status === HttpStatusCode.badRequest) {
                  this.serverMessage = MessageText.invalidCredentials;
              }
              this.clearLoginModel();
              this.resetCaptach();
          });
    }

    public resolved(captchaResponse: string) {
        if (captchaResponse) {
            this.form.captcha = true;
        } else {
            this.form.captcha = false;
        }
    }

    private resetCaptach() {
        this.captchaChild.reset();
    }

    public get captchaKey() {
        return environment.captcha_key;
    }

    private clearLoginModel() {
        this.form.password = '';
        this.form.captcha = false;
    }

    public redirectToForgotPwdScreen() {
        this.router.navigate([RedirectUrl.forgotPassword]);
    }

    public redirectToRegisterScreen() {
        this.router.navigate([RedirectUrl.register]);
    }

    public adjustCaptcha() {
        this.timer = setInterval(() => {
            const captcha = $('.captcha-name > div');
            if (captcha && captcha.length !== 0) {
                const x = $(".captcha-holder").width() / 304;
                const y = $(".captcha-holder").height() / 78;
                $('.captcha-name > div').css({
                    'transform': 'scale(' + x + ',' + y + ')'
                });
                window.clearInterval(this.timer);
            }
        }, 2000);
    }

    public tryClearingBoth() {
        try {
            this.ngfc.clear();
        } catch (error) { }
        try {
            localStorage.removeItem(storage.key);
        } catch (error) { }
    }

    public tryGetUserFromLocalUser() {
        let item: any = undefined;
        if (this.lsEnabled() === true) {
            item = localStorage.getItem(storage.key);
            if (item && item !== 'undefined' && item !== 'null') {
                const parseValue = JSON.parse(item);
                return parseValue['email'];
            }
            if(item){}else {
                item = localStorage.getItem('vendor-info');
                if (item && item !== 'undefined' && item !== 'null') {
                    const parseValue = JSON.parse(item);
                    return parseValue['loginUserEmail'];
                }
            }
        }
    }

    public lsEnabled() {
        let test = 'test';
        try {
            localStorage.setItem(test, test);
            localStorage.removeItem(test);
            return true;
        } catch (e) {
            return false;
        }
    }
}
