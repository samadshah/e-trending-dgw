import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { Logger } from 'angular2-logger/core';
import { RedirectUrl } from '../configuration/config';
import { ResetPasswordService } from '../services/reset-password/reset-password.service';

@Injectable()
export class NoAuthGuard implements CanActivate {

  params: any;
  constructor(private oauthService: OAuthService,
    private router: Router, private logger: Logger,
    private resetPasswordService: ResetPasswordService) {
     }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.oauthService.hasValidAccessToken()) {
      const returnUrl = route.queryParams['returnUrl'] || '/';
      // checking for first login --
      this.router.navigate([returnUrl]);
    }
    return true;
  }
}
