import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { Logger } from 'angular2-logger/core';
import { RedirectUrl } from '../configuration/config';
import { ResetPasswordService } from '../services/reset-password/reset-password.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private oauthService: OAuthService,
    private router: Router, private logger: Logger,
    private resetPasswordService: ResetPasswordService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if (this.oauthService.hasValidAccessToken()) {

      // checking for first login --
      if (state.url !== RedirectUrl.passwordReset && this.resetPasswordService.isFirstLogin(undefined)) {
        this.router.navigate([RedirectUrl.passwordReset]);
        return false;
      }

      return true;

    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
