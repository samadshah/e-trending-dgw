import { InjectionToken } from '@angular/core';
export const AUTH_URL = new InjectionToken('AUTH_URL');
export const BASE_URL = new InjectionToken('BASE_URL');
export const USE_CACHE = new InjectionToken<boolean>('USE_CACHE');
