import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

export interface SearchState {
  searchText: string;
}

@Injectable()
export class SearchSharingService {
  private loaderSubject = new Subject<SearchState>();
  searchState = this.loaderSubject.asObservable();
  constructor() {
  }

  public search(text) {
          this.loaderSubject.next(<SearchState>{searchText: text});
  }
}
