import { HttpRequest, HttpResponse } from '@angular/common/http';
import { CachedItem } from './CachedItem';
import { CachedItemImpl } from './CacheItemImpl';
import { Logger } from 'angular2-logger/core';
import { Injectable } from '@angular/core';

export abstract class HttpCache {
    /**
     * Returns a cached response, if any, or null if not present.
     */
    // abstract get(req: HttpRequest<any>): HttpResponse<any> | null;
    abstract get<T>(key: string): CachedItem<T> | null;

    /**
     * Adds or updates the response in the cache.
     */
    // abstract put(req: HttpRequest<any>, resp: HttpResponse<any>): void;
    abstract put<T>(key: string, value: T, cacheTime?: number): void;
}


@Injectable()
export class MemoryHttpCache extends HttpCache {
    constructor(private logger: Logger) {
        super();

        logger.debug('MemoryHttpCache initiated');
    }

    // private values: Map<string, CachedItem<T>>  = new Map<string, CachedItem<T>>();
    private values: Map<string, any> = new Map<string, any>();

    /**
  * Cache time in milliseconds
  * @default 300000
  */
    private cacheTime = 300000;
    private maxEntries = 20;

    // public get cacheTime(): number {
    //     return 'cacheTime' in this.config ? this.config.cacheTime : this.baseConfig.cacheTime;
    // }

    get<T>(key: string): CachedItem<T> {
        // throw new Error("Method not implemented.");

        return this.values.get(key);

    }

    put<T>(key: string, value: T, cacheTime?: number): void {
        if (this.values.size >= this.maxEntries) {
            this.cleanup()
                .then(deleted => {
                    // this.logger.debug('LocalStorageHttpCache clean up process deleted [' + deleted + '] items');
                })
                .catch(error => {
                    // this.logger.debug('LocalStorageHttpCache clean up process has some errors');
                    // this.logger.debug(error);
                });
        }

        const expiry = typeof cacheTime === 'number' ? cacheTime : this.cacheTime;
        const cache = new CachedItemImpl<T>(value, expiry);
        this.values.set(key, cache);
        // const dataPromise = this.setItem<T>(key, data);
        // const expiryPromise = this.setItem<number>(keys.expiry, Date.now() + expiry);

    }

    cleanup(): Promise<number> {

        return new Promise((resolve, reject) => {
            // this.values.forEach((value: CachedItem<T>, key: string) => {
            //     if()
            // });
            // this.logger.debug('LocalStorageHttpCache clean up process executed');
            let deleted = 0;
            for (const entry of Array.from(this.values.entries())) {
                const key = entry[0];
                const value = entry[1];
                if (value.expired) {
                    deleted += 1;
                    this.values.delete(key);
                }
            }
            resolve(deleted);
        });
    }
}
