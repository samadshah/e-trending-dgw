import { Injectable, Injector } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Logger } from 'angular2-logger/core';

export enum configColumns {

  locale = 'locale',
  currencyDecimal = 'currencyDecimal',
  dateFormate = 'dateFormate',
  TwentyFourHrTimeFormate = 'TwentyFourHrTimeFormate',
  TwelveHrTimeFormate = 'TwelveHrTimeFormate',
  dateTimeFormate = 'dateTimeFormate',
  datePicker = 'datePicker',
  dateFormateForAx = 'dateFormateForAx',
  headerPosition = 'headerPosition',
  layoutValidationScrollThreashold = 'layoutValidationScrollThreashold',
  searchEnableOnItem = 'searchEnableOnItem',
  showbreadcrumb = 'showbreadcrumb',
  expiredNoDaysBeforeNotification = 'expiredNoDaysBeforeNotification',
  pageNo = 'pageNo',
  pageSize = 'pageSize',
  pagingAppearence = 'pagingAppearence',
  filterLength = 'filterLength',
  activityFeeds = 'activityFeeds',
  notifications = 'notifications',
  logoText = 'logoText',
  appName = 'appName',
  logoName = 'logoName'
}

@Injectable()
export class AppSettingService {
  private servicePath: string;

  public settings: any = {
    // datetime changes
    locale: 'en',
    currencyDecimal: 2,
    dateFormate: 'DD/MM/YYYY',
    TwentyFourHrTimeFormate: 'HH:mm',
    TwelveHrTimeFormate: 'hh:mm:ss a',
    dateTimeFormate: 'DD/MM/YYYY HH:mm',
    datePicker: 'mm/dd/yyyy',
    dateFormateForAx: 'YYYY-MM-DD',

    // header layout fixed or
    headerPosition: 'fixedHeaderAndNav',   // 'fixedHeaderAndNav',  change " layoutValidationScrollThreashold " config also;
    layoutValidationScrollThreashold: 90,  // for fix layout it should be 90 - for moving layout it should be 30
    searchEnableOnItem: 10,
    showbreadcrumb: 0,
    // expiry marking
    expiredNoDaysBeforeNotification: 5,

    // paging info
    pageNo: 1,
    pageSize: 50,
    pagingAppearence: 5,
    filterLength: 10,

    // scroll paging info
    activityFeeds: 7,
    notifications: 15,
    logoText: 'DGW',
    appName: 'e - Procurement Portal',
    logoName: 'dgw-logo.png'
  };
  constructor(private injector: Injector, private logger: Logger) {
    this.servicePath = environment.base_api_url + '/api/vendors';
  }

  public loadAppSettings() {
     return new Promise((resolve, reject) => {

      const appSettingExist = this.FetchFromLocalStorage('appSettings');
      if (appSettingExist) {
        this.settings = appSettingExist;
        resolve(true);
      }
      else {
        this.SetLocalStorage('appSettings', this.settings);
        resolve(true);
      }
    });
  }

  public lsEnabled(){
    let test = 'test';
    try {
          localStorage.setItem(test, test);
          localStorage.removeItem(test);
          return true;
        } catch(e) {
          return false;
        }
  }

  public FetchFromLocalStorage(key) {
    if(this.lsEnabled() === true) {
      let item = localStorage.getItem(key);
      if (item && item !== 'undefined' && item !== 'null') {
        item = JSON.parse(item);
      }
      return item;
    }
    return null;
  }

  public SetLocalStorage(key, data) {
    if(this.lsEnabled() === true) {
      const parseData: any = JSON.stringify(data);
      localStorage.setItem(key, parseData);
    }
  }
}
