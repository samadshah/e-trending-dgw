import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

export interface CountState {
  count: string;
}

@Injectable()
export class NotificationCountSharingServiceService {

  private loaderSubject = new Subject<CountState>();
  countState = this.loaderSubject.asObservable();
  constructor() {
  }

  public sendCount(text) {
          this.loaderSubject.next(<CountState>{count: text});
  }
}
