export class Vendor {
        public vendorId: string;
        public vendorName: string;
        public loginUserName: string;
        public loginUserDomain: string;
        public loginUserEmail: string;
}

export interface IVendor {
        vendorId: string;
        vendorName: string;
        loginUserName: string;
        loginUserDomain: string;
        loginUserEmail: string;
}
