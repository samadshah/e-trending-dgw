import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';
import { Subject } from 'rxjs/Subject';

import { environment } from '../../../../environments/environment';
import { IVendor } from './models/vendor.model';

@Injectable()
export class VendorService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;
  public vendor = new Subject<any>();

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient, private oAuthService: OAuthService) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/vendors';
    this.servicePath2 = '/api';
    this.loadVendor();
  }

  //!
  public initialLoad(claims) {
    // const claims: any = this.oAuthService.getIdentityClaims();
    this.getVendorInfoByEmail(claims.email);
  }

  //!
  public loadVendor() {
    if (this.oAuthService.hasValidAccessToken()) {
      const item: any = this.FetchFromLocalStorage();
      if (item) {
        return;
      }
      //!
      const claims: any = localStorage.getItem('claim');
      if (claims) {
        this.getVendorInfoByEmail(claims.email);
      }
    }
  }

  public getVendor(): Observable<any> {
    return this.vendor.asObservable();
  }

  public FetchFromLocalStorage() {
    let item = localStorage.getItem('vendor-info');
    if (item) {
      item = JSON.parse(item);
      this.vendor.next(item);
    }
    return item;
  }

  //!
  private getVendorInfoByEmail(vendorEmail) {
    this.http.get<IVendor>(environment.base_api_url + this.servicePath2 + '/User/VendorInfo').subscribe(data => {
      const parseData: any = JSON.stringify(data);
      this.vendor.next(data);
      localStorage.setItem('vendor-info', parseData);
    });
  }

  //!
  public validateCompanyName(data): Observable<any> {
    return this.http.post(environment.base_api_url + this.servicePath2 + '/vendorcompanyverify', data);
  }

  //!
  public validateEmail(data): Observable<any> {
    return this.http.post(environment.base_api_url + this.servicePath2 + '/vendorverification', data);
  }
}
