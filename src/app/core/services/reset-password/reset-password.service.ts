import { PasswordService } from './../../../reset-password/password.service';
import { LoginService } from './../../../login/login.service';
import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable()
export class ResetPasswordService {

  constructor(private oauthService: OAuthService,
    private loginService: LoginService
    ) { }
  // checking for first login --
  //!
  public isFirstLogin(claims) {
      let result = false;
      let claim: any;
      if (claims) {
          claim = claims;
          if (claim.PasswordReset === false) {
            result = true;
          }
          else {
            result = false;
          }
      } else {
        // claim = JSON.parse(localStorage.getItem('claim'));
        this.loginService.getUserClaim().subscribe(gotClaim => {
            claim = gotClaim;
            if (claim.PasswordReset === false) {
              result = true;
            } else {
              result = false;
            }
        });
      }

      // if (claim) {
      //   if(claim.PasswordReset === false){
      //     result = true;
      //   }
      // }

      return result;
  }
}
