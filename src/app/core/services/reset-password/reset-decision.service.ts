import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { ResetState } from './reset-decision';
@Injectable()
export class ResetDecisionService {
        private loaderSubject = new Subject<ResetState>();
        decisionState = this.loaderSubject.asObservable();

        private isLoginSubject = new Subject<ResetState>();
        loginState = this.isLoginSubject.asObservable();
        constructor() { }
        show() {
                this.loaderSubject.next(<ResetState>{ show: true });
        }

        hide() {
                this.loaderSubject.next(<ResetState>{ show: false });
        }


        isLogin(value) {
                this.isLoginSubject.next(<ResetState>{ show: value });
        }
}
