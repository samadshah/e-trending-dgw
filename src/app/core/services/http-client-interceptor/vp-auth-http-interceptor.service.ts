import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { Logger } from 'angular2-logger/core';
import { NotificationsService } from 'angular2-notifications/dist';
import { Observable } from 'rxjs/Observable';
import { range } from 'rxjs/observable/range';
import { _throw } from 'rxjs/observable/throw';
import { timer } from 'rxjs/observable/timer';
import { mergeMap, retryWhen, zip } from 'rxjs/operators';

export class ToasterMessage {
  constructor(status) {
   this.status = status;
   this.occurredAt = new Date();
  }
  status: string;
  occurredAt: any;
}

@Injectable()
export class VpAuthHttpInterceptorService implements HttpInterceptor {

  neglectMessagesList = [];
  constructor(private logger: Logger,
    // private oauthService: OAuthService,
    private toaster: NotificationsService,
    private router: Router,
    private injector: Injector) { }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // current user access token

    // Get the auth header from the service.
    const oauthService = this.injector.get(OAuthService);

    req = this.addAuthHeader(req, oauthService);
    return next.handle(req)
      .pipe(
      retryWhen(attempts => {
        return attempts
          .pipe(
          zip(range(1, 3)),
          mergeMap(([error, i]) => {
            if (i > 2) {
              return _throw(error);
            }
            if (error.status === 401) {
              this.logger.debug('unauthorized .. refreshing by refreshToken');

              return oauthService.refreshToken()
                .then((result) => {

                  req = this.addAuthHeader(req, oauthService);
                  return next.handle(req);
                })
                .catch((tokenError) => {
                  const previousRefreshToken = oauthService.getAccessToken();

                  if (previousRefreshToken) {
                    oauthService.logOut();
                    this.logger.debug('refresh token issue in interceptor');
                    this.logger.debug('logout and navigate to /login');

                    this.router.navigateByUrl('/login')
                      .then(b => {
                        this.logger.debug('Showing toaster error 401 refresh');
                        this.toaster.error('Invalid or expired token.');
                      });
                  } else {
                    this.logger.debug('previousRefreshToken is null');
                  }
                  // return _throw(tokenError);
                  return _throw(new Error());
                  // return empty();
                });

            } else if (error.status !== 429 && error.status <= 500) {
              return _throw(error);
            } else {
              this.logger.debug('Wait ' + i + ' seconds, then retry!');
              if (this.alreadyShownInTime(12012)) {
                this.toaster.alert('Request failed please wait retrying ...');
                this.neglectMessagesList.push(new ToasterMessage(12012));
              }
              return timer(i * 1000);
            }
          })
          );
      })
      )
      .catch(response => {
        if (response instanceof HttpErrorResponse) {
          this.logger.debug('Processing http error');
          this.logger.debug(response);
          // show toaster for server error here.
            if (this.alreadyShownInTime(response.status)) {
            if (response.status === 503) {
              const defaultMessage = 'Service is unavailable please try again later. Please contact system administrator.';
              const message = response.error && response.error.message || defaultMessage;
              this.toaster.error(message);
              this.neglectMessagesList.push(new ToasterMessage(response.status));
            } else if (response.status >= 500) {
              this.toaster.error('something went wrong please try again later');
              this.neglectMessagesList.push(new ToasterMessage(response.status));
            } else if (response.status === 0) {
              this.toaster.error('please check your internet connection and try again.');
              this.neglectMessagesList.push(new ToasterMessage(response.status));
            } else if (response.status === 403) {
              this.toaster.error('you are not authorized for this action.');
              this.neglectMessagesList.push(new ToasterMessage(response.status));
            } else if (response.status === 429) {
              this.toaster.error('too many requests please wait and try again');
            } else { }
          }
          return _throw(response);
        }
        return _throw(new Error('Error in response.'));
      });

  }

  public alreadyShownInTime(status) {
     const exist = this.neglectMessagesList.filter(x => x.status === status);
     if (exist.length > 0) {
        return ((new Date().getTime() - exist[exist.length - 1].occurredAt.getTime()) / 1000) > 3 ? true : false;
     }
     return true;
  }

  addAuthHeader(request: HttpRequest<any>, oauthService: OAuthService): HttpRequest<any> {
    // current user access token
    const token: string = oauthService.authorizationHeader();

    // using token in every http request for authorization header
    if (token) {
      request = request.clone({ headers: request.headers.set('Authorization', token) });
    }
    return request;
  }
}

