import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Logger } from 'angular2-logger/core';
import { Observable } from 'rxjs/Observable';
import { concat } from 'rxjs/observable/concat';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';

import { environment } from '../../../../environments/environment';
import { HttpCache } from '../Cache/HttpCache';

@Injectable()
export class MemoryCacheHttpInterceptorService implements HttpInterceptor {

  constructor(private logger: Logger, private memoryCache: HttpCache) { }



  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Still skip non-GET requests.
    if (!environment.use_cache || req.method !== 'GET') {
      return next.handle(req);
    }

    // This will be an Observable of the cached value if there is one,
    // or an empty Observable otherwise. It starts out empty.
    let maybeCachedResponse: Observable<HttpEvent<any>> = empty();

    const cacheKey = req.urlWithParams;
    // Check the cache.
    const cachedResponse = this.memoryCache.get<HttpEvent<any>>(cacheKey);
    if (cachedResponse) {


      if (cachedResponse.expired) {
        this.logger.debug('cache is expired for : ' + cacheKey);
      } else {
        const expiryDate: Date = cachedResponse.expires;

        // tslint:disable-next-line:max-line-length
        // this.logger.debug('cache will expire in ' + Math.round(cachedResponse.expiresIn / 1000) + ' seconds, on ' + expiryDate.toLocaleString());


        maybeCachedResponse = of(cachedResponse.data);
        this.logger.debug('cache hit for : ' + cacheKey);

        req = req.clone({ params: req.params.append('cached', 'true') });
      }

    } else {
      this.logger.debug('cache not found for : ' + cacheKey);
    }

    // Create an Observable (but don't subscribe) that represents making
    // the network request and caching the value.
    const networkResponse = next.handle(req)
      .do(event => {
        // Just like before, check for the HttpResponse event and cache it.
        if (event instanceof HttpResponse) {

          this.memoryCache.put<HttpEvent<any>>(cacheKey, event);
          this.logger.debug('cache Set successfully for : ' + cacheKey);
        }
      });

    // Now, combine the two and send the cached response first (if there is
    // one), and the network response second.
    return concat(maybeCachedResponse, networkResponse);
  }
}

