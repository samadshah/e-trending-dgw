import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CachedItem, NgForageCache } from '@ngforage/ngforage-ng5';
import { Logger } from 'angular2-logger/core';
import { Observable } from 'rxjs/Observable';
import { concat } from 'rxjs/observable/concat';
import { empty } from 'rxjs/observable/empty';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { of } from 'rxjs/observable/of';
import { switchMap } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { MapUtils } from '../MapUtils';

@Injectable()
export class NgForageCacheHttpInterceptorService implements HttpInterceptor {

  // useCache = true;
  // multipleResponses = true;

  constructor(private logger: Logger,
    private readonly ngfc: NgForageCache,
    private route: ActivatedRoute) { }


  private validRequest(req: HttpRequest<any>): boolean {

    return environment.use_cache
      && (
        req.method === 'GET'
        || (req.method === 'POST' && req.body && req.body.pageNo));
  }

  private getCacheKey(req: HttpRequest<any>): string {
    if (req.method === 'GET') {
      return req.urlWithParams;
    } else {
      const bodyKey = req.body ? JSON.stringify(req.body) : '';
      return req.urlWithParams + bodyKey;
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // req = this.initCache(req);

    // Still skip non-GET requests.
    if (!this.validRequest(req)) {
      return next.handle(req);
    }

    const cacheKey = this.getCacheKey(req);

    // This will be an Observable of the cached value if there is one,
    // or an empty Observable otherwise. It starts out empty.
    let maybeCachedResponse: Observable<HttpEvent<any>> = empty();
    let networkResponse: Observable<HttpEvent<any>> = empty();

    // Check the cache.
    let cachedResponse: CachedItem<string>;

    return fromPromise(this.ngfc.getCached<string>(cacheKey))
      .pipe(
      switchMap(cacheItem => {
        cachedResponse = cacheItem;

        if (cachedResponse) {

          if (cachedResponse.expired) {
            this.logger.debug('cache is expired for : ' + cacheKey);
          } else {
            const expiryDate: Date = cachedResponse.expires;

            // tslint:disable-next-line:max-line-length
            this.logger.debug('cache will expire in ' + Math.round(cachedResponse.expiresIn / 1000) + ' seconds, on ' + expiryDate.toLocaleString());

            const jsonObj: any = JSON.parse(cachedResponse.data); // string to generic object first
            const event: HttpEvent<any> = MapUtils.deserialize(HttpResponse, jsonObj);

            maybeCachedResponse = of(event);
            this.logger.debug('cache hit for : ' + cacheKey);
          }

        } else {
          this.logger.debug('cache not found for : ' + cacheKey);
        }

        networkResponse = this.callService(req, next, cacheKey);
        return concat(maybeCachedResponse, networkResponse);

      })
      );
  }


  private callService(req: HttpRequest<any>, next: HttpHandler, cacheKey: string): Observable<HttpEvent<any>> {
    return next.handle(req)
      .do(event => {
        // Just like before, check for the HttpResponse event and cache it.
        if (event instanceof HttpResponse) {


          const event_cache = event.clone({ headers: event.headers.set('local_cache', 'true') });

          const cacheJson = JSON.stringify(event_cache);
          this.ngfc.setCached(cacheKey, cacheJson)
            .then(result => {
              this.logger.debug('cache Set successfully for : ' + cacheKey);
            });
        }
      });
  }

}

