import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgProgress } from '@ngx-progressbar/core';
import { Logger } from 'angular2-logger/core';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';

@Injectable()
export class LogHttpInterceptorService implements HttpInterceptor {

  constructor(private logger: Logger, public progress: NgProgress) { }
  inProgress = false;
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const started = Date.now();

    return next.handle(req)
      .do(event => {
        // Just like before, check for the HttpResponse event and cache it.
        if (event instanceof HttpResponse) {
          const elapsed = Date.now() - started;

          const msg = 'Request for [' + req.method + '] ' + req.urlWithParams + ' took ' + elapsed + ' ms.';
          this.logger.debug(msg);
        }
      },
      error => {
        return _throw(error);
      }
      );

  }
}
