// // import { Injectable } from '@angular/core';
// // import { HttpClient, HttpHeaders, HttpEvent, HttpInterceptor } from '@angular/common/http';
// // import { HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
// // // import { Observable } from 'rxjs/Observable';

// // import { OAuthService } from 'angular-oauth2-oidc';
// // import { Logger } from 'angular2-logger/core';
// // import { NotificationsService } from 'angular2-notifications/dist';
// // import { HttpStatusCode } from '../../Configuration/config';
// // import { Router } from '@angular/router';
// // import { environment } from '../../../../environments/environment';

// // // // // import { _______ } from 'rxjs/_________';
// // // // // Observable, Observer, BehaviorSubject, Subject, ReplaySubject
// // // // import { Observable } from 'rxjs/Observable';
// // // // // import { Observer } from 'rxjs/Observer';
// // // // // import { BehaviorSubject } from 'rxjs/BehaviorSubject';
// // // // // import { Subject } from 'rxjs/Subject';
// // // // // import { ReplaySubject } from 'rxjs/ReplaySubject';


// // // // // import 'rxjs/add/observable/__________';
// // // // // concat, defer, empty, forkJoin, from, fromPromise, if, interval, merge, of,
// // // // // range, throw, timer, using, zip
// // // // import 'rxjs/add/observable/of';
// // // // import 'rxjs/add/observable/throw';
// // // // import 'rxjs/add/observable/empty';
// // // // import 'rxjs/add/observable/interval';
// // // // import 'rxjs/add/observable/concat';

// // // // // import 'rxjs/add/operator/_________';
// // // // // audit, buffer, catch, combineAll, combineLatest, concat, count, debounce, delay,
// // // // // distinct, do, every, expand, filter, finally, find , first, groupBy,
// // // // // ignoreElements, isEmpty, last, let, map, max, merge, mergeMap, min, pluck,
// // // // // publish, race, reduce, repeat, scan, skip, startWith, switch, switchMap, take,
// // // // // takeUntil, throttle, timeout, toArray, toPromise, withLatestFrom, zip
// // // // import 'rxjs/add/operator/do';
// // // // import 'rxjs/add/operator/concat';
// // // // import 'rxjs/add/operator/toPromise';

// // import { Observable } from 'rxjs/Observable';

// // import { of } from 'rxjs/observable/of';
// // import { empty } from 'rxjs/observable/empty';
// // import { interval } from 'rxjs/observable/interval';
// // import { concat } from 'rxjs/observable/concat';
// // import { _throw } from 'rxjs/observable/throw';
// // import 'rxjs/add/operator/retryWhen';

// // import { catchError, map, tap, flatMap } from 'rxjs/operators';

// // import { CachedItem, NgForageCache } from '@ngforage/ngforage-ng5';
// // import { MapUtils } from '../MapUtils';

// // @Injectable()
// // export class CacheHttpInterceptorService implements HttpInterceptor {

// //   constructor(private logger: Logger, private oauthService: OAuthService,
// //     private toaster: NotificationsService, private readonly ngfc: NgForageCache,
// //     private router: Router) { }



// //   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
// //     const started = Date.now();

// //     // Still skip non-GET requests.
// //     if (environment.use_cache === false || req.method !== 'GET') {
// //       // return next.handle(req);
// //       // return this.httpServiceRequest(req, next);


// //       return next.handle(req)
// //         .do(event => {
// //           // Just like before, check for the HttpResponse event and cache it.
// //           if (event instanceof HttpResponse) {
// //             const elapsed = Date.now() - started;

// //             const msg = 'Request for [' + req.method + '] ' + req.urlWithParams + ' took ' + elapsed + ' ms.';
// //             this.logger.debug(msg);
// //           }
// //         });
// //     }

// //     // This will be an Observable of the cached value if there is one,
// //     // or an empty Observable otherwise. It starts out empty.
// //     let maybeCachedResponse: Observable<HttpEvent<any>> = empty();
// //     let networkResponse: Observable<HttpEvent<any>> = empty();

// //     // Check the cache.
// //     // const cachedResponse: HttpResponse<any> = this.getCacheJson(req); // this.cache.get(req);
// //     this.getCacheJsonAsync(req)
// //       .then(result => {
// //         const cachedResponse: HttpResponse<any> = result;

// //         if (cachedResponse) {
// //           maybeCachedResponse = of(cachedResponse);
// //         }

// //         // Create an Observable (but don't subscribe) that represents making
// //         // the network request and caching the value.
// //         // const networkResponse = next.handle(req)
// //         // .do(event => {
// //         //   // Just like before, check for the HttpResponse event and cache it.
// //         //   if (event instanceof HttpResponse) {
// //         //     this.cache.put(req, event);
// //         //   }
// //         // });

// //         // const networkResponse = this.httpServiceRequest(req, next, true);

// //         // Now, combine the two and send the cached response first (if there is
// //         // one), and the network response second.

// //       });

// //     networkResponse = next.handle(req)
// //       .do(event => {
// //         // Just like before, check for the HttpResponse event and cache it.
// //         if (event instanceof HttpResponse) {
// //           const elapsed = Date.now() - started;

// //           const msg = 'Request for [' + req.method + '] ' + req.urlWithParams + ' took ' + elapsed + ' ms.';
// //           this.logger.debug(msg);

// //           // Update Cache
// //           this.updateCacheJson(req, event);
// //           // this.updateCache(req, event);

// //         }
// //       });

// //     return concat(maybeCachedResponse, networkResponse);



// //     // // // Before doing anything, it's important to only cache GET requests.
// //     // // // Skip this interceptor if the request method isn't GET.
// //     // // // Skip this if use cache is false.
// //     // // if (environment.use_cache === false || req.method !== 'GET') {
// //     // //   return this.httpServiceRequest(req, next);
// //     // // }

// //     // // let maybeCachedResponse: Observable<HttpEvent<any>> = empty();
// //     // // maybeCachedResponse = this.getCacheJson(req);
// //     // // // maybeCachedResponse = this.getCache(req);


// //     // // // Create an Observable (but don't subscribe) that represents making
// //     // // // the network request and caching the value.
// //     // // const networkResponse: Observable<HttpEvent<any>> = this.httpServiceRequest(req, next, true);

// //     // // // Now, combine the two and send the cached response first (if there is
// //     // // // one), and the network response second.
// //     // // return concat(maybeCachedResponse, networkResponse);
// //   }

// //   // private httpServiceRequest(req: HttpRequest<any>, next: HttpHandler,
// //   //   cacheable: boolean = false)
// //   //   : Observable<HttpResponse<any>> {

// //   //   // handling some exception centeraly
// //   //   const started = Date.now();
// //   //   return next.handle(req)
// //   //     .do(event => {
// //   //       // Just like before, check for the HttpResponse event and cache it.
// //   //       if (event instanceof HttpResponse) {
// //   //         const elapsed = Date.now() - started;

// //   //         const msg = 'Request for [' + req.method + '] ' + req.urlWithParams + ' took ' + elapsed + ' ms.';
// //   //         this.logger.debug(msg);

// //   //         // Update Cache
// //   //         if (cacheable) {
// //   //           this.updateCacheJson(req, event);
// //   //           // this.updateCache(req, event);
// //   //         }
// //   //       }
// //   //     });
// //   // }


// //   updateCacheJson(req: HttpRequest<any>, event: HttpResponse<any>) {
// //     this.logger.debug('Cache set for url: ' + req.urlWithParams);

// //     const cacheJson = JSON.stringify(event);

// //     let cacheResponse: string = null;
// //     this.ngfc.setCached<string>(req.urlWithParams, cacheJson)
// //       .then(result => {
// //         cacheResponse = result;
// //         this.logger.debug('Cache Set Successfully : ' + result);
// //       })
// //       .catch(function (err) {
// //         // This code runs if there were any errors
// //         this.logger.debug('Error in this.ngfc.setCached<string>(req.urlWithParams, cacheJson)');
// //         this.logger.debug(err);
// //       });
// //   }

// //   updateCache(req: HttpRequest<any>, event: HttpEvent<any>) {
// //     this.logger.debug('Cache set for url: ' + req.urlWithParams);

// //     const cacheJson = JSON.stringify(event);

// //     this.ngfc.setCached<HttpEvent<any>>(req.urlWithParams, event)
// //       .then(result => {
// //         this.logger.debug('Cache Set Successfully [Type] : ' + result.type);
// //       })
// //       .catch(function (err) {
// //         // This code runs if there were any errors
// //         this.logger.debug('Error in this.ngfc.setCached<HttpEvent<any>>(req.urlWithParams, event)');
// //         this.logger.debug(err);
// //       });
// //   }

// //   getCache(req: HttpRequest<any>): Observable<HttpEvent<any>> {

// //     // This will be an Observable of the cached value if there is one,
// //     // or an empty Observable otherwise. It starts out empty.
// //     let maybeCachedResponse: Observable<HttpEvent<any>> = empty();


// //     // Check the cache.
// //     // HttpEvent<any>
// //     let cachedResponse: CachedItem<HttpEvent<any>>;

// //     this.ngfc.getCached<HttpEvent<any>>(req.urlWithParams)
// //       .then((result) => {
// //         cachedResponse = result;
// //         if (cachedResponse && cachedResponse.hasData) {
// //           this.logger.debug('The cache item is ' + cachedResponse.data + ' for url : ' + req.urlWithParams);

// //           if (cachedResponse.expired) {
// //             this.logger.debug('Cache Item has expired');
// //           } else {
// //             const expiryDate: Date = cachedResponse.expires;

            // tslint:disable-next-line:max-line-length
// //             this.logger.debug('The cache item will expire in ' + Math.round(cachedResponse.expiresIn / 1000) + ' seconds, on ' + expiryDate.toLocaleString());

// //             const parseJson: HttpEvent<any> = cachedResponse.data;

// //             maybeCachedResponse = of(parseJson);

// //             if (parseJson instanceof HttpResponse) {
// //               this.logger.debug('parseJson instanceof HttpResponse');
// //             } else {
// //               this.logger.debug('parseJson is not instanceof HttpResponse');
// //             }
// //           }
// //         } else {
// //           this.logger.debug('No cache data found for url : ' + req.urlWithParams);
// //         }
// //       });

// //     return maybeCachedResponse;
// //   }

// //   getCacheJson(req: HttpRequest<any>): HttpResponse<any> {

// //     // This will be an Observable of the cached value if there is one,
// //     // or an empty Observable otherwise. It starts out empty.
// //     let maybeCachedResponse: HttpResponse<any> = null;


// //     // Check the cache.
// //     // HttpEvent<any>
// //     let cachedResponse: CachedItem<string>;

// //     this.ngfc.getCached<string>(req.urlWithParams)
// //       .then((result) => {
// //         cachedResponse = result;
// //         if (cachedResponse && cachedResponse.hasData) {
// //           this.logger.debug('The cache item is ' + cachedResponse.data + ' for url : ' + req.urlWithParams);

// //           if (cachedResponse.expired) {
// //             this.logger.debug('Cache Item has expired');
// //           } else {
// //             const expiryDate: Date = cachedResponse.expires;


// tslint:disable-next-line:max-line-length
// //             this.logger.debug('The cache item will expire in ' + Math.round(cachedResponse.expiresIn / 1000) + ' seconds, on ' + expiryDate.toLocaleString());

// //             const jsonObj: any = JSON.parse(cachedResponse.data); // string to generic object first
// //             // const parseJson: HttpEvent<any> = <HttpEvent<any>>jsonObj;
// //             const parseJson: HttpResponse<any> = MapUtils.deserialize(HttpResponse, jsonObj);
// //             // const parseJson: HttpEvent<any> = <HttpEvent<any>>JSON.parse(cachedResponse.data);

// //             maybeCachedResponse = parseJson;

// //             if (parseJson instanceof HttpResponse) {
// //               this.logger.debug('parseJson instanceof HttpResponse');
// //             } else {
// //               this.logger.debug('parseJson is not instanceof HttpResponse');
// //             }
// //           }
// //         } else {
// //           this.logger.debug('No cache data found for url : ' + req.urlWithParams);
// //         }
// //       });

// //     return maybeCachedResponse;
// //   }

// //   getCacheJsonAsync(req: HttpRequest<any>): Promise<HttpResponse<any>> {

// //     return new Promise((resolve, reject) => {
// //       // This will be an Observable of the cached value if there is one,
// //       // or an empty Observable otherwise. It starts out empty.
// //       let maybeCachedResponse: HttpResponse<any> = null;


// //       // Check the cache.
// //       // HttpEvent<any>
// //       let cachedResponse: CachedItem<string>;

// //       this.ngfc.getCached<string>(req.urlWithParams)
// //         .then((result) => {
// //           cachedResponse = result;
// //           if (cachedResponse && cachedResponse.hasData) {
// //             this.logger.debug('The cache item is ' + cachedResponse.data + ' for url : ' + req.urlWithParams);

// //             if (cachedResponse.expired) {
// //               this.logger.debug('Cache Item has expired');
// //             } else {
// //               const expiryDate: Date = cachedResponse.expires;

// //               // tslint:disable-next-line:max-line-length
// tslint:disable-next-line:max-line-length
// //               this.logger.debug('The cache item will expire in ' + Math.round(cachedResponse.expiresIn / 1000) + ' seconds, on ' + expiryDate.toLocaleString());

// //               const jsonObj: any = JSON.parse(cachedResponse.data); // string to generic object first
// //               // const parseJson: HttpEvent<any> = <HttpEvent<any>>jsonObj;
// //               const parseJson: HttpResponse<any> = MapUtils.deserialize(HttpResponse, jsonObj);
// //               // const parseJson: HttpEvent<any> = <HttpEvent<any>>JSON.parse(cachedResponse.data);

// //               maybeCachedResponse = parseJson;

// //               if (parseJson instanceof HttpResponse) {
// //                 this.logger.debug('parseJson instanceof HttpResponse');
// //               } else {
// //                 this.logger.debug('parseJson is not instanceof HttpResponse');
// //               }
// //             }
// //           } else {
// //             this.logger.debug('No cache data found for url : ' + req.urlWithParams);
// //           }
// //         })
// //         .catch(error => {
// //           reject(error);
// //         });

// //       resolve(maybeCachedResponse);
// //       // return maybeCachedResponse;
// //     });


// //   }


// // }

