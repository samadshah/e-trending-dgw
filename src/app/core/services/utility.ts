import { Injectable } from '@angular/core';
@Injectable()
export class UtiltiyService {

    constructor() {

    }

    isBrowserIe() {
        const ua = window.navigator.userAgent;
        const msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            // return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            return true;
        }

        const trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            const rv = ua.indexOf('rv:');
            //  return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            return true;
        }

        const edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            // return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
            return true;
        }

        // other browser
        return false;


    }

    errorHandle(err) {
        const value = [];
        if (err.error.modelState) {
            const arr = Object.keys(err.error.modelState);
            for (let i = 0; i < arr.length; i++) {
                const text = err.error.modelState[arr[i]][0];
                value.push(text);
            }
            let msg = ''; // value.join(',');//.split('.');
            // if (msg[1]) {
            //     return msg[1].replace('\'' , '');
            // } else {
            //     return msg;
            // }
            for (let i = 0; i < value.length; i++) {
                const element = value[i];
                // if(element[1]) {
                    const splited = element.split('.');
                    const error = splited.length > 2 ? splited[splited.length - 2] : splited[0];
                    if (i < (value.length - 1)) {
                        msg += error + '</br>';
                    } else {
                        msg += error;
                    }
                // } else {
                //     msg += element[0];
                // }
            }
            return msg; // .replace('\'\g' , '');
        } else if (err.error.message) {
            return err.error.message;
        } else {
            return err.error.error;
        }
    }
}
