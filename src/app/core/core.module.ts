import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Optional, SkipSelf, APP_INITIALIZER } from '@angular/core';
import { NgForageCache, NgForageConfig, NgForageOptions } from '@ngforage/ngforage-ng5';
import { Logger, Options as LoggerOptions } from 'angular2-logger/core';
import { Observable } from 'rxjs/Observable';
import { interval } from 'rxjs/observable/interval';
import { first } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { AUTH_URL, USE_CACHE } from './app.tokens';
import { AuthGuard } from './guards/auth.guard';
import { NoAuthGuard } from './guards/noAuth.guard';
import { HttpCache, MemoryHttpCache } from './services/Cache/HttpCache';
import { LogHttpInterceptorService } from './services/http-client-interceptor/log-http-interceptor.service';
import { NgForageCacheHttpInterceptorService} from './services/http-client-interceptor/ngForageCache-http-interceptor.service';
import { VpAuthHttpInterceptorService } from './services/http-client-interceptor/vp-auth-http-interceptor.service';
import { ResetPasswordService } from './services/reset-password/reset-password.service';
import { SearchSharingService } from './services/search-sharing.service';
import { UtiltiyService } from './services/utility';
import { VendorService } from './services/vendor/vendor.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  declarations: [],
  providers: [
    AuthGuard,
    NoAuthGuard,
    Logger,
    { provide: AUTH_URL, useValue: environment.auth_url },
    { provide: USE_CACHE, useValue: environment.use_cache },
    { provide: LoggerOptions, useValue: { level: environment.log_level } },
    { provide: HttpCache, useClass: MemoryHttpCache },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: VpAuthHttpInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NgForageCacheHttpInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LogHttpInterceptorService,
      multi: true,
    },
    VendorService,
    ResetPasswordService,
    UtiltiyService,
    SearchSharingService,
  ]
})
export class CoreModule {

  private sub: Observable<number>;
  constructor( @Optional() @SkipSelf() parentModule: CoreModule,
    private logger: Logger,
    private conf: NgForageConfig,
    private readonly ngfc: NgForageCache) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }

    if (environment.use_cache) {
      this.configureCache(conf, ngfc);
    } else {
      this.logger.debug('Cache is disable.');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      providers: [
        AuthGuard,
        NoAuthGuard,
        Logger,
        { provide: AUTH_URL, useValue: environment.auth_url },
        // { provide: BASE_URL, useValue: environment.base_api_url },
        { provide: USE_CACHE, useValue: environment.use_cache },
        { provide: LoggerOptions, useValue: { level: environment.log_level } },
        VendorService
      ],
      ngModule: CoreModule
    };
  }

  private configureCache(conf: NgForageConfig, ngfc: NgForageCache) {

    // Set the database name
    conf.name = 'myDBVenorPortal';

    // Set the store name (e.g. in IndexedDB this is the dataStore)
    conf.storeName = 'my_store_vp';

    // Set default cache time to 5 minutes (300000)
    conf.cacheTime = environment.cache_expiry; // 48 hours

    // Set driver to local storage
    // conf.driver = NgForageConfig.DRIVER_LOCALSTORAGE;

    // Set the driver to indexed db if available,
    // falling back to websql
    // falling back to local storage
    conf.driver = [
      NgForageConfig.DRIVER_INDEXEDDB,
      NgForageConfig.DRIVER_WEBSQL,
      NgForageConfig.DRIVER_LOCALSTORAGE
    ];

    // Set websql database size
    conf.size = 1024 * 1024 * 4;

    // Set DB version. Currently unused.
    conf.version = 2.0;

    // Configure in bulk
    const bulk: NgForageOptions = {
      version: 3.0,
      name: 'newDBVp'
    };
    conf.configure(bulk);

    this.cacheCleanupProcess();
  }

  private cacheCleanupProcess() {
    this.sub = interval(600000);
    this.sub.subscribe((val) => {

      this.logger.debug('Running Cache Cleanup process.');
      this.ngfc.length()
        .then(size => {

          this.logger.debug('Total Cache items are : ' + size);
          const expiredKeys: string[] = [];
          const data = '_data';

          // Loop over each of the items.
          for (let i = 0; i < size; i += 2) {
            this.ngfc.key(i)
              .then(result => {

                const akey = result.replace(data, '');

                const cachedItem = this.ngfc.getCached(akey)
                  .then(cacheResult => {
                    if (cacheResult.expired) {
                      this.logger.debug('Cache is expired : ' + akey);
                      expiredKeys.push(akey);
                    }
                  });
              });
          }

          interval(10000)
            .pipe(
            first()
            ).subscribe(result2 => {
              const expireCacheSize = expiredKeys.length;
              this.logger.debug('Total expired cache items are ' + expireCacheSize);
              if (expireCacheSize > 0) {

                for (let i = 0; i < expireCacheSize; i++) {
                  this.ngfc.removeCached(expiredKeys[i])
                    .then(() => {
                      this.logger.debug('cache removed');
                    });
                }
              }
            });
        });
    });

    this.logger.debug('Cache Cleanup process scheduled');
  }


}
