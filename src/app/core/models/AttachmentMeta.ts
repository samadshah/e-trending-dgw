export class AttachmentMetaType {
  public  baseUrl: string;
  public  docName: string;
  public  docType: string;
  public  listName: string;
  public  repositoryPath: string;
  public  rootFolder: string;
}

export class Meta {
  public  docType: string;
  public  metaTags: Array<MetaData>;
}

export class MetaData {
  public  name: string;
  public  value: string;
}

export class  MetaPostModel {
  public  id: string;
  public  metaTypes: Array<AttachmentMetaType>;
  public  metaTags: Array<Meta>;
}

export class Attachment {
  public  id: string;
  public  name: string;
  public  docType: string;
  public  createdDateTime: Date;
  public  isDeleted: boolean;
  public  isUpdated: boolean;
  public  isNew: boolean;
}

export class UploadAttachmentMeta {
  public  Email: string;
  public  DocType: string;
  // public  baseUrl: string;
  // public  docName: string;
  // public  docType: string;
  // public  listName: string;
  // public  repositoryPath: string;
  // public  rootFolder: string;
  // public  metaList: Array<MetaData>;
}

export class AttachmentConfig {
  public  fileSize: string;
  public  fileTypes: Array<MetaData>;
  public  invoiceFileTypes: Array<MetaData>;
  public disallowedCharacters: string;
  public disallowedCharactersError: string;
}
