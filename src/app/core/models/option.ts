export class Option {
    public position: Array<any>;
    public timeOut: number;
    public lastOnBottom: boolean;
    public pauseOnHover: boolean;
    public clickToClose: boolean;
    public maxLength: number;
    public theClass: string;
    public icon: string;
    public showProgressBar: boolean;

    constructor() {
        this.position = ['top', 'center'];
        this.timeOut = 4000;
        this.lastOnBottom = false;
        this.pauseOnHover = true;
        this.clickToClose = true;
        this.showProgressBar = false;
    }
}
