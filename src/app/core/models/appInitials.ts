export class AppInitial {
    constructor(logoText: string , appName: string) {
        this.logoText = logoText;
        this.appName = appName
    }
    public logoText: string;
    public appName: string;
}