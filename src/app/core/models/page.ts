//!
export class PageQuery {
    // The number of elements in the page
    pageSize = 0;
    // The current page number
    pageNo = 0;
    // concatenated filter columns and values
    filters = '';
    // concatenated sorting columns and values
    sort = '';
    // Draw for async datatable calls
    draw = 0;

    searchTerm = '';
    withList = false;
    //!
    // vendorId = '1004'
}

export class Page {
    // The number of elements in the page
    size = 0;
    // The total number of elements
    totalElements = 0;
    // The total number of pages
    totalPages = 0;
    // The current page number
    pageNumber = 0;
}

export class PagingResponse {
    public  draw: number;
    public  recordsTotal: number;
    public  recordsFiltered: number;
    public  data: any;
}

