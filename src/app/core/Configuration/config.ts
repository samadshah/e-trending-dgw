export enum RedirectUrl {
  login = '../login',
  passwordReset = '/reset-password',
  forgotPassword = '/forgot-password',
  register = '/register',
  rfqs = '/vp/rfqs',
  contacts = '/vp/contacts',
  pOrders = '/vp/p-orders',
  notifications = '/vp/notifications',
  root = '/vp',
  home = '/vp/home',
  profile = '/vp/vendor-profile',
  profileEdit = '/vp/vendor-profile/edit',
  rfqSubmit = '/vp/rfqs/bid',
  rfqDetail = '/vp/rfqs/view',
  searchPage = '/vp/search',
  rfqPrint = 'print',
  guideView = 'guide-view',
  supplierGuide = '/supplier-guide',
  bAgreement = '/vp/blank-agreement',
  contracts = '/vp/contracts',
  rOrders = '/vp/rOrders',
}

export enum DocumentList {
  purchaseOrder = 'purchase order',
}

export enum Contains {
  login = 'login',
  passwordReset = 'reset-password',
  forgotPassword = 'forgot-password',
  searchRoute = 'search'
}

export enum CssClass {
  login = 'wrapper login-page',
  otherRoute = 'wrapper',
  loading = 'loading'
}

export enum MessageText {
  invalidCredentials = 'Invalid email or password',
  invalidFilters = 'kindly apply filters',
  passwordChangeSuccess = 'Password change succeed',
  passwordChangeSuccessRedirecting = 'Password change succeed redirecting to login',
  success = 'Success',
  invaliedId = 'Invalid id',
  serverOffline = 'Server is offline',
  unableToFetchData = 'Unable to fetch data',
  submitted = 'submitted',
  FormLeavingTitle = 'Are you sure ?',
  FormLeavingText = 'You have pending changes, do you want to leave?',
  FormUnsaveChangesText = 'No changes to save',
  FormLeaveingSAIcon = 'warning',
  gridEmpty = 'No record to display',
  fileNotAvailable = 'File not available',
  filterLimitExceed = 'Filter limit exceed',
  passcodeSent = 'An email has been sent with the generated/new passcode to your registered email address.',
  NoMoreRecord = 'No more records found',
  profileUpdated = 'Profile updated successfully',
  passwordChanged = 'Password changed successfully',
  contactAdded = 'Contact added successfully',
  contactDeleted = 'contact deleted successfully',
  contactUpdated = 'Contact updated successfully',
  bidSaved = 'Bid saved successfully',
  bidSubmitted = 'Bid submitted successfully',
  bidRecalled = 'The bid has been recalled. You can now modify it. To resubmit it, click Bid.',
  registerSubmit = 'Registration request submitted successfully',
  forgotPasswordSuccess= 'Password changed successfully redirecting to login page',
  //!
  documentsDeleteText = 'This will cause your attachments to be permanently deleted.',
  documentsDeleteTitle = 'Are you sure?'
}

export enum HttpStatusCode {
  success = 200,
  created = 201,
  badRequest = 400,
  forbidden = 403,
  notFound = 404,
  internalServerError = 500,
}

export enum RFQBidType {
  open = 'open',
  sealed = 'sealed',
}

export enum DataType {
  isString = 'String',
  isEnum = 'Enum',
  isDate = 'Date',
  isNumber = 'Int',
  Real = 'Real',
}

export enum YesNo {
  yes = 'Yes',
  no = 'No',
  none = 'None',
}

export enum Regex {
  email = '^\w+([\.-]?\w+)*@[a-zA-Z]+([\.-]?\[a-zA-Z]+)*(\.[a-zA-Z]{2,5})+$'
}

export enum GridParameters {
  draw = 'draw',
  columns = 'columns',
  start = 'start',
  length = 'length',
  order = 'order',
}

export enum GridType {
  RFQList = 1,
  POList = 2,
  ContactList = 6,
  NotificationList = 8,
}

export enum storage {
  key = 'id_token_claims_obj'
}

export enum rfQListIndication {
  status = 'new'
}

export enum QuestionType {
  text = 'Text',
  Integer = 'Integer',
  Real = 'Real',
  Date = 'Date',
  Time = 'Time',
  Note = 'Note',
  CheckBox = 'Check box',
  AlternativeButton = 'Alternative button',
  ComboBox = 'Combo box',
}

export enum FieldLength {
  value = 20
}

export enum GlobalSearchType {
  requestForQuotation = 'request for quotation',
  purchaseOrder = 'purchase order',
}

export enum DocTypeAttachments {
  tradeLicenseCopy = 'trade license copy',
  companyProfile = 'company profile',
  passportNo = 'passport no',
  cv = 'cv',
  incorporationDate = 'incorporation date',
  certificate = 'certificate',
  visaNo = 'trade license copy',
  emiratesid = 'emiratesid',
  financialDetails = 'financial details'
}

//! For docs
export enum TabAttachment {
  tradlicenseCertificate = 0,
  companyProfile = 1,
  passport = 2,
  cv = 3,
  incorporationCertificate = 4,
  vatCertificate = 5,
  visa = 6,
  emirateId = 7,
  financialDetails = 8
}

export enum Guide {
  registerOrganisation = 'roguide',
  registerPerson = 'rpguide',
  bidding = 'vpbguide',
  contact = 'vpcguide',
  login = 'vplguide',
  registrationGuide= 'rguide',
}
