import { Component, Inject, AfterViewInit, ViewChild, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart, NavigationCancel, NavigationEnd } from '@angular/router';
import { Logger } from 'angular2-logger/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { AUTH_URL } from './core/app.tokens';
import { CssClass, RedirectUrl, Contains, storage } from './core/configuration/config';
import { Option } from './core/models/option';
import { Title } from '@angular/platform-browser';
import { NgProgress } from '@ngx-progressbar/core';
import { authPasswordFlowConfig } from './auth-password-flow.config';
import { Location, PopStateEvent } from '@angular/common';
import * as $ from 'jquery';
import { SearchSharingService, SearchState } from './core/services/search-sharing.service';
import { NgForageCache } from '@ngforage/ngforage-ng5';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    // providers: [OAuthService],
})
export class AppComponent implements OnInit, AfterViewInit {
    title = 'Vendor Portal';
    loading: boolean;
    dynamicWrapCss: any = '';
    inProgress = false;
    options = new Option();
    private lastPoppedUrl: string;
    private yScrollStack: number[] = [];
    constructor(
        private location: Location,
        private oauthService: OAuthService,
        private router: Router,
        private logger: Logger,
        @Inject(AUTH_URL) private baseUrl: string,
        private titleService: Title,
        public progress: NgProgress,
        private searchSharingService: SearchSharingService,
        private readonly ngfc: NgForageCache) {
        this.oauthService.configure(authPasswordFlowConfig);
        this.oauthService.setStorage(localStorage);
        this.oauthService.loadDiscoveryDocument();
        this.logger.info('Auth Password Flow Configured.');

        this
            .oauthService
            .events
            .filter(e => e.type === 'token_expires')
            .subscribe(e => {
                logger.info('OIDC / OAUTH token_expires event called.');
                this.refreshToken();
            });

        this
            .oauthService
            .events
            .filter(e => e.type === 'token_received' || e.type === 'token_refreshed')
            .subscribe(e => {
                logger.info('OIDC / OAUTH token_received || token_refreshed event called.');
            });
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.location.subscribe((ev: PopStateEvent) => {
            this.lastPoppedUrl = ev.url;
        });
        setTimeout(() => {
        const previousRefreshToken = this.oauthService.getAccessToken();
        if (previousRefreshToken) {
            this.validateTokenExpiry();
            this.logger.info('first time token refresh successfully');
        }
        }, 2000);
        this.router.events
            .subscribe((event) => {
                if (event instanceof NavigationStart) {
                    if (event.url.toLocaleLowerCase().indexOf(Contains.login.toLocaleLowerCase()) !== -1 ||
                        event.url.toLocaleLowerCase().indexOf(Contains.forgotPassword.toLocaleLowerCase()) !== -1 ||
                        event.url.toLocaleLowerCase().indexOf(Contains.passwordReset.toLocaleLowerCase()) !== -1) {
                        this.dynamicWrapCss = CssClass.login;
                    } else {
                        this.dynamicWrapCss = CssClass.otherRoute;
                    }
                    // this.showLoading(event);
                    if (event.url !== this.lastPoppedUrl) {
                        this.yScrollStack.push(window.scrollY);
                    }
                } else if (
                    event instanceof NavigationEnd ||
                    event instanceof NavigationCancel
                ) {
                    if (event instanceof NavigationEnd) {
                        const title = this.getTitle(this.router.routerState, this.router.routerState.root).join('-');
                        if (title.length > 0) {
                            const value = title.split(',');
                            const currentTarget = value[value.length - 2];
                            this.titleService.setTitle(currentTarget);
                        }
                        if (event.url.toLocaleLowerCase().indexOf(Contains.searchRoute.toLocaleLowerCase()) === -1) {
                            this.searchSharingService.search('');
                        }
                    }
                    // this.closeLoading(event);
                    if (event.url === this.lastPoppedUrl) {
                        this.lastPoppedUrl = undefined;
                        window.scrollTo(0, this.yScrollStack.pop());
                    } else {
                        window.scrollTo(0, 0);
                    }
                }
            });
    }

    private refreshToken() {
        this.oauthService.refreshToken()
            .then(() => {
                this.logger.info('Token refreshed successfully.');
            })
            .catch((response) => {
                this.logger.error(response);
            });
    }

    private navigate() {
        const url = window.location.pathname;
        if (url === '/') {
            this.router.navigateByUrl('/dashboard');
        } else {
            this.router.navigateByUrl(url);
        }
    }

    public loggedOut(input) {
        this.oauthService.logOut(false);
        this.router.navigate([RedirectUrl.login]);
    }

    get hasValidAccessToken() {
        return this.oauthService.hasValidAccessToken();
    }

    public getTitle(state, parent) {
        const data = [];
        if (parent && parent.snapshot.data && parent.snapshot.data.breadcrumb) {
            data.push(parent.snapshot.data.breadcrumb);
        }
        if (state && parent) {
            data.push(this.getTitle(state, state.firstChild(parent)));
        }
        return data;
    }

    public validateTokenExpiry() {
        const tokenExpiry = this.oauthService.getAccessTokenExpiration();
        if(this.isExpired(tokenExpiry)) {
            this.oauthService.logOut();
            this.tryClearingBoth();
            this.logger.debug('access token expired');
            this.router.navigateByUrl('/login');
        } else {
            this.refreshToken();
        }
    }

    public isExpired(expiredAt): boolean {
        return new Date(expiredAt) < new Date();
    }

    public tryClearingBoth() {
        try {
            this.ngfc.clear();
        } catch (error) { }
        try {
            localStorage.removeItem(storage.key);
        } catch (error) { }
    }

}

