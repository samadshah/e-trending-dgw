import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  PrecurementCategoryRequest
} from "../../../models/registeration.model";
import { Router } from "@angular/router";
import {
  RedirectUrl
} from "../../../../core/configuration/config";
import { Lookup } from "../../../../core/models/lookup";
import { PrecurementCategories } from "../../../models/registeration.model";

@Component({
  selector: "app-precurement-categories",
  templateUrl: "./precurement-categories.component.html",
  styleUrls: ["./precurement-categories.component.css"]
})
export class PrecurementCategoriesComponent implements OnInit {
  // output methodes
  @Output() precurementCategorySubmit = new EventEmitter<any>();
  @Output() notifyServerMessage = new EventEmitter<any>();
  @Output() next = new EventEmitter<any>();
  @Output() previous = new EventEmitter<any>();
  @Output() conditionalBack = new EventEmitter<any>();
  @Input() form = new PrecurementCategoryRequest();

  categoryValidationFlag = false;
  serverMessage = "";
  precurementCategories = new Array<PrecurementCategories>();
  viewArea = [];

  constructor(private router: Router) {}

  ngOnInit() {}

  public loadProcurementCategories(categories) {
    this.precurementCategories = [];
    for (let i = 0; i < categories.length; i++) {
      this.precurementCategories.push(
        new PrecurementCategories(i, categories[i].name, categories[i].value)
      );
    }
  }

  public precurementCategory(precurementCategoryForm) {
    //!!
    // this.precurementCategorySubmit.emit(this.form);

    if (this.isCategorySelected()) {
      this.categoryValidationFlag = false;
      this.form.ProcurementCategories = this.getPrecurementCategories();
    } else {
      this.categoryValidationFlag = false;
    }
    this.precurementCategorySubmit.emit(this.form);
  }

  getPrecurementCategories(): Array<Lookup> {
    const categories = Array<Lookup>();
    for (let i = 0; i < this.precurementCategories.length; i++) {
      if (this.precurementCategories[i].isSelected) {
        const currentLookup = new Lookup();
        currentLookup.name = this.precurementCategories[i].name;
        currentLookup.value = this.precurementCategories[i].value;
        categories.push(currentLookup);
      }
    }
    return categories;
  }

  isCategorySelected() {
    for (let i = 0; i < this.precurementCategories.length; i++) {
      if (this.precurementCategories[i].isSelected) {
        return true;
      }
    }
    return false;
  }

  public specialCaseBackPrecurementCategories() {
    this.conditionalBack.emit();
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }

  public pushToView(evt, item) {
    if (evt) {
      const index = this.viewArea.indexOf(item);
      if (index === -1) {
        this.viewArea.push(item);
      }
    } else {
      const index = this.viewArea.indexOf(item);
      this.viewArea.splice(index, 1);
    }
  }

  public removeCategory(item) {
    const index = this.viewArea.indexOf(item);
    const id = this.getCategoryById(item);
    this.viewArea.splice(index, 1);
  }

  public getCategoryById(item) {
    item.isSelected = false;
    $("#" + item.id).prop("checked", false);
  }

  public loadExisiting(data) {
    this.mappModel(data);
  }

  public mappModel(data) {
    for (let index = 0; index < data.procurementCategories.length; index++) {
      this.findSelectedCategory(data.procurementCategories[index]);
    }
    this.form.otherCategory = data.otherCategory;
  }

  public findSelectedCategory(selected) {
    for (let index = 0; index < this.precurementCategories.length; index++) {
      if (this.precurementCategories[index].value === selected.value) {
        this.precurementCategories[index].isSelected = true;
        $("#" + this.precurementCategories[index].id).prop("checked", true);
        this.viewArea.push(this.precurementCategories[index]);
      }
    }
  }
}
