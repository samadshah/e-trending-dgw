import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  TermAndConditionTab
} from "../../../models/registeration.model";
import { Router } from "@angular/router";
import {
  RedirectUrl
} from "../../../../core/configuration/config";

@Component({
  selector: "app-term-and-condition",
  templateUrl: "./term-and-condition.component.html",
  styleUrls: ["./term-and-condition.component.css"]
})
export class TermAndConditionComponent implements OnInit {
  // output methodes
  @Output() termAndCondition = new EventEmitter<any>();
  @Output() notifyServerMessage = new EventEmitter<any>();
  @Output() next = new EventEmitter<any>();
  @Output() previous = new EventEmitter<any>();
  @Input() form: TermAndConditionTab = new TermAndConditionTab();

  termAndConditionValidationFlag = false;
  serverMessage = "";

  constructor(private router: Router) {}

  ngOnInit() {}

  public termAndConditionSubmit(termAndConditionForm) {

    if (this.form.termsAndConditionCheck === "No") {
      this.termAndConditionValidationFlag = true;
      return false;
    } else {
      this.termAndConditionValidationFlag = false;
    }
    this.termAndCondition.emit(this.form);
  }

  public onChangeTermAndCondition(value) {
    this.termAndConditionValidationFlag = false;
    if (value === true) {
      this.form.termsAndConditionCheck = "Yes";
    } else {
      this.form.termsAndConditionCheck = "No";
    }
  }

  public gotoPrevious() {
    this.previous.emit();
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }

  public loadExisiting(data) {
    this.mappModel(data);
  }

  public mappModel(data) {
    this.form.termsAndConditionCheck = data;
  }
}
