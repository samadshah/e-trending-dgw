import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  TermAndConditionTab
} from "../../../models/registeration.model";
import { Router } from "@angular/router";
import {
  RedirectUrl
} from "../../../../core/configuration/config";

@Component({
  selector: "app-non-disclosure",
  templateUrl: "./non-disclosure.component.html",
  styleUrls: ["./non-disclosure.component.css"]
})
export class NonDisclosureComponent implements OnInit {
  // output methodes
  @Output() termAndCondition = new EventEmitter<any>();
  @Output() notifyServerMessage = new EventEmitter<any>();
  @Output() next = new EventEmitter<any>();
  @Output() previous = new EventEmitter<any>();
  @Input() form: TermAndConditionTab = new TermAndConditionTab();

  termAndConditionValidationFlag = false;
  serverMessage = "";

  constructor(private router: Router) {}

  ngOnInit() {}

  public nonDisclosureTermAndConditionSubmit(termAndConditionForm) {

    if (this.form.nonDisclosureAgreementCheck === "No") {
      this.termAndConditionValidationFlag = true;
      return false;
    } else {
      this.termAndConditionValidationFlag = false;
    }
    this.termAndCondition.emit(this.form);
  }

  public onChangeNonDisclousreTermAndCondition(value) {
    this.termAndConditionValidationFlag = false;
    if (value === true) {
      this.form.nonDisclosureAgreementCheck = "Yes";
    } else {
      this.form.nonDisclosureAgreementCheck = "No";
    }
  }

  public gotoPrevious() {
    this.previous.emit();
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }

  public loadExisiting(data) {
    this.mappModel(data);
  }

  public mappModel(data) {
    this.form.nonDisclosureAgreementCheck = data;
  }
}
