// tslint:disable
import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  GeneralInformationRequest,
  OrganizationTypeRequest
} from "../../../models/registeration.model";
import { HttpErrorResponse } from "@angular/common/http";
import { IMyDpOptions } from "mydatepicker";
import * as Moment from "moment";
import { Router } from "@angular/router";
import { AppSettingService } from "../../../../core/services/app-setting.service";
import {
  HttpStatusCode,
  FieldLength,
  RedirectUrl
} from "../../../../core/configuration/config";
import { LookupService } from "../../../../shared/services/lookup-service";
import { NotificationsService } from "angular2-notifications/dist";

@Component({
  selector: "app-general-info",
  templateUrl: "./general-info.component.html",
  styleUrls: ["./general-info.component.css"]
})
export class GeneralInfoComponent implements OnInit {
  // output methodes
  @Output() generalInfoSumit = new EventEmitter<any>();
  @Output() notifyServerMessage = new EventEmitter<any>();
  @Output() next = new EventEmitter<any>();
  @Output() previous = new EventEmitter<any>();
  @Input() form: GeneralInformationRequest = new GeneralInformationRequest();
  @Input() isPerson: boolean;
  @Input() vatDisclamerText: string;
  //!!
  @Input() orgTypeData: OrganizationTypeRequest;

  placeHolderIssueDate = "Select Issue Date";
  placeHolderExpiryDate = "Select Expiry Date";

  public model1: any;
  public model2: any;
  dateDifference = 0;
  fieldConfig = FieldLength;
  generalFormTelephoneValidation = false;
  vatNumberValidation = false;

  vatReasonValidation = false;
  vatTypeValidation = false;
  numberValidation = false;
  vatTypes = [];
  options = { separateDialCode: true };
  serverMessage = "";
  datePickerSyntax = "";
  typeExpiryDateValidation = false;
  typeIssueDateValidation = false;
  issueDateGreaterThenCurrentDateValidation = false;

  vatSetting: any;
  titleSetting: any;
  languageSetting: any;
  currencySetting: any;
  issuingEmiratesSetting: any;
  regTypesSetting: any;

  languages = [];
  selectedLanguage = [];

  currencies = [];
  selectedCurrency = [];

  vats = [];
  selectedVat = [];

  personTitles = [];
  selectedTitle = [];

  issuingEmirates = [];
  selectedIssuingEmirate = [];

  registrationTypes = [];
  selectedRegistrationType = [];

  public myDatePickerOptions: IMyDpOptions = {};

  constructor(
    private lookupService: LookupService,
    private router: Router,
    private appSettingSrvc: AppSettingService,
    private notificationsService: NotificationsService
  ) {
    this.myDatePickerOptions = {
      dateFormat: this.appSettingSrvc.settings.datePicker.toString(),
      showClearDateBtn: false,
      alignSelectorRight: true,
      allowSelectionOnlyInCurrentMonth: false
    };
    this.datePickerSyntax = this.appSettingSrvc.settings.datePicker;
  }

  ngOnInit() {
    this.selectedIssuingEmirate = ["1"];
  }

  public generalInfo(generalInfoForm, formClass) {
    this.form.vatNotRegisteredType = "No";
    if (this.isPerson) {
      if (this.form.passportNumber == undefined) {
        this.form.passportNumber = "NA";
      }
    }

    if (!this.isPerson) {
      if (this.form.tradeLicenseNumber == undefined) {
        this.form.tradeLicenseNumber = "NA";
      }
    }

    let generalInfoSuccessFlag = true;
    if (!generalInfoForm.valid || this.dateDifference < 0) {
      if (!generalInfoForm.valid) {
        this.foucsValidation(formClass);
      } else {
        const cls = "." + formClass + " ." + "diff";
        this.initiateFocus(cls);
      }
      generalInfoSuccessFlag = false;
    }

    if (this.form.telephoneInput) {
      this.generalFormTelephoneValidation = false;
      if (this.form.telephoneInput.toString().length > this.fieldConfig.value) {
        generalInfoSuccessFlag = false;
      }
    } else {
      this.generalFormTelephoneValidation = true;
      generalInfoSuccessFlag = false;
    }

    //!
    if (this.form.vatRegistered.toLocaleLowerCase() === "yes") {
      if (this.form.vatNumber) {
        this.vatNumberValidation = false;
      } else {
        this.vatNumberValidation = true;
        generalInfoSuccessFlag = false;
      }
    } else if (this.form.vatRegistered.toLocaleLowerCase() === "no") {
      if (this.form.vatNotRegisteredType) {
        this.vatTypeValidation = false;
      } else {
        this.vatTypeValidation = true;
        generalInfoSuccessFlag = false;
      }
    }

    if (this.form.issueDate) {
      this.verifyIssueDateEqLessThenCurrentDate();
      if (
        this.issueDateGreaterThenCurrentDateValidation &&
        generalInfoForm.valid
      ) {
        generalInfoSuccessFlag = false;
        const cls = "." + formClass + " ." + "moveHere";
        this.initiateFocus(cls);
      }
    }

    if (generalInfoSuccessFlag === false) {
      return generalInfoSuccessFlag;
    }

    //!!
    if (this.selectedLanguage.length > 0) {
      if (this.selectedLanguage[0].id == "ar") {
        this.form.languagePreference = "1";
      } else if (this.selectedLanguage[0].id == "en-US" || this.selectedLanguage[0].id == "en-us") {
        this.form.languagePreference = "0";
      }
    } else {
      this.form.languagePreference = "0";
    }

    //!!
    if (this.orgTypeData.organizationPerson == "Organization") {
      this.validateTradeLicenseNumber();
    } else {
      this.generalInfoSumit.emit(this.form);
    }
  }

  //!!
  validateTradeLicenseNumber() {
    let payload = { _tradeLicenseNumber: this.form.tradeLicenseNumber };
    this.lookupService.validateTradeLicenseNumber(payload).subscribe(
      resp => {
        if (resp.isSuccess) {
          this.generalInfoSumit.emit(this.form);
        } else {
          this.notificationsService.error(resp.message);
        }
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.Message;
          this.notificationsService.error(message);
        }
      }
    );
  }

  //!
  onTextToNumberChanged(e) {
    if (e.target.name == "vatNumberN") {
      this.vatNumberValidation = false;
    }

    if (
      [46, 8, 9, 27, 13, 110, 16].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode == 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+V
      (e.keyCode == 86 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode == 88 && e.ctrlKey === true) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)
    ) {
      return;
    }

    // Ensure that it is a number and stop the keypress
    if (
      (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) &&
      (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }
  }

  onIssueDateChanged(event) {
    this.issueDateGreaterThenCurrentDateValidation = false;
    this.form.issueDate = Moment(event.jsdate).format(
      this.appSettingSrvc.settings.dateFormateForAx
    );
    this.verifyDate();
  }

  onExpiryDateChanged(event) {
    this.form.expiryDate = Moment(event.jsdate).format(
      this.appSettingSrvc.settings.dateFormateForAx
    );
    this.verifyDate();
  }

  public verifyIssueDateEqLessThenCurrentDate() {
    if (this.form.issueDate) {
      const ToDate = Moment().format(
        this.appSettingSrvc.settings.dateFormateForAx
      );
      const FromDate = Moment(this.form.issueDate).format(
        this.appSettingSrvc.settings.dateFormateForAx
      );
      if (Date.parse(FromDate) > Date.parse(ToDate)) {
        this.issueDateGreaterThenCurrentDateValidation = true;
      } else {
        this.issueDateGreaterThenCurrentDateValidation = false;
      }
    }
    return this.issueDateGreaterThenCurrentDateValidation;
  }

  public verifyDate() {
    if (this.form.issueDate && this.form.expiryDate) {
      if (Date.parse(this.form.expiryDate) <= Date.parse(this.form.issueDate)) {
        this.dateDifference = -1;
        return;
      }
      this.dateDifference = 1;
    }
    this.dateDifference = 1;
  }

  public fillLanguages(languages) {
    this.languages = [];
    for (let i = 0; i < languages.length; i++) {
      this.languages.push({
        id: languages[i].value,
        itemName: languages[i].name
      });
    }
    this.languageSetting = this.dropdownSetting(
      "Select Language Preference",
      this.languages.length > this.appSettingSrvc.settings.searchEnableOnItem
    );

    // make english default
    const lng = this.languages.filter(
      x => x.itemName === "English (United States)"
    );
    this.selectedLanguage =
      lng.length > 0 ? lng : languages.length > 0 ? languages[0] : [];
  }

  public fillCurrency(currencies) {
    this.currencies = [];
    for (let i = 0; i < currencies.length; i++) {
      this.currencies.push({
        id: currencies[i].value,
        itemName: currencies[i].name
      });
    }
    this.currencySetting = this.dropdownSetting(
      "Select Currency",
      this.currencies.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
    this.currencySetting["disabled"] = "true";
    //!  Default value for Currency dropdown
    this.selectedCurrency = [{ id: "AED", itemName: "UAE Dirhams" }];
    this.form.currency = "AED";
  }

  public fillVateRegistered(vats) {
    this.vats = [];
    for (let i = 0; i < vats.length; i++) {
      if (vats[i].value && vats[i].name) {
        this.vats.push({ id: vats[i].value, itemName: vats[i].name });
      }
    }
    this.vatSetting = this.dropdownSetting(
      "Select VAT",
      this.vats.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public fillPersonSalutation(titles) {
    this.personTitles = [];
    for (let i = 0; i < titles.length; i++) {
      this.personTitles.push({ id: titles[i].value, itemName: titles[i].name });
    }
    this.titleSetting = this.dropdownSetting(
      "Select Title",
      this.personTitles.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  //!!
  public fillIssuingEmirates(issuingEmirates) {
    this.issuingEmirates = [];
    for (let i = 0; i < issuingEmirates.length; i++) {
      this.issuingEmirates.push({
        id: issuingEmirates[i].value,
        itemName: issuingEmirates[i].name
      });
    }
    this.issuingEmiratesSetting = this.dropdownSetting(
      "Select Issuing Emirate",
      this.issuingEmirates.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  //!!
  public fillRegistrationTypes(regTypes) {
    this.registrationTypes = [];
    for (let i = 0; i < regTypes.length; i++) {
      this.registrationTypes.push({
        id: regTypes[i].value,
        itemName: regTypes[i].name
      });
    }
    this.regTypesSetting = this.dropdownSetting(
      "Select Registration Type",
      this.registrationTypes.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public onVatSelect(data) {
    this.form.vatRegistered = data.id;
  }

  public onLanguageSelect(data) {
    this.form.languagePreference = data.id;
  }

  public onCurrencySelect(data) {
    this.form.currency = data.id;
  }

  //!!
  public onIssuingEmirateSelect(data) {
    this.form.tlIssuingEmirate = data.id;
  }

  //!!
  public onRegistrationTypeSelect(data) {
    this.form.registrationType = data.id;
  }

  public onTitleSelect(data) {
    this.form.title = data.id;
  }

  private dropdownSetting(text, search = true) {
    return {
      singleSelection: true,
      text: text,
      showCheckbox: false,
      classes: "form-control custom-selectbox",
      enableSearchFilter: search,
      searchAutofocus: search
    };
  }

  public foucsValidation(formClass) {
    formClass =
      "." +
      formClass +
      " .ng-invalid:not(#disclaimer):not(.phoneControl):not(form):first";
    const el = $(formClass);
    if (el && el.length !== 0) {
      $("html,body").animate(
        {
          scrollTop:
            el.offset().top -
            this.appSettingSrvc.settings.layoutValidationScrollThreashold
        },
        "slow",
        () => {
          el.focus();
        }
      );
    }
  }

  public gotoPrevious() {
    this.previous.emit();
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }

  public fillVatTypes(types) {
    this.vatTypes = [];
    for (let i = 0; i < types.length; i++) {
      if (types[i].value && types[i].name) {
        this.vatTypes.push({ value: types[i].value, name: types[i].name });
      }
    }
  }

  public initiateFocus(id) {
    const ele = $(id);
    if (ele && ele.length !== 0) {
      $("html,body").animate(
        {
          scrollTop:
            ele.offset().top -
            this.appSettingSrvc.settings.layoutValidationScrollThreashold
        },
        "slow",
        () => {
          ele.focus();
        }
      );
    }
  }

  public onChangeTermAndCondition(value) {
    if (value === true) {
      this.form.vatDisclaimer = "Yes";
    } else {
      this.form.vatDisclaimer = "No";
    }
  }

  public loadExisiting(data) {
    this.mappModel(data);
    this.selectedLanguage = this.languages.filter(
      x => x.id === this.form.languagePreference
    );
    this.selectedCurrency = this.currencies.filter(
      x => x.id === this.form.currency
    );
    this.selectedVat = this.vats.filter(x => x.id === this.form.vatRegistered);
    this.selectedTitle = this.personTitles.filter(
      x => x.id === this.form.title
    );
    this.model1 = this.getFormattedDate(this.form.issueDate);
    this.model2 = this.getFormattedDate(this.form.expiryDate);
  }

  public mappModel(data) {
    this.form.arabicCompanyName = data.arabicCompanyName;
    this.form.companyName = data.companyName;
    this.form.firstName = data.firstName;
    this.form.lastName = data.lastName;
    this.form.currency = data.currency;
    this.form.emailAddress = data.emailAddress;
    this.form.internetAddress = data.internetAddress;
    this.form.passportNumber = data.passportNumber;
    this.form.issueDate = data.issueDate;
    this.form.secondaryEmail = data.secondaryEmail;
    this.form.expiryDate = data.expiryDate;
    this.form.languagePreference = data.languagePreference;
    this.form.title = data.title;
    this.form.tradeLicenseNumber = data.tradeLicenseNumber;
    this.form.vatDisclaimer = data.vatDisclaimer;

    if (this.form.vatDisclaimer === "Yes") {
      this.form.vatDisclaimerFlag = true;
    } else {
      this.form.vatDisclaimerFlag = false;
    }

    this.form.vatNotRegisteredType = data.vatNotRegisteredType;
    this.form.vatNumber = data.vatNumber;
    this.form.vatReason = data.vatReason;
    this.form.vatRegistered = data.vatRegistered;
    this.form.yearsCompanyOperatesInUAE = data.yearsCompanyOperatesInUAE;

    if (data.telephone) {
      const teleParts = data.telephone.split("-");
      if (teleParts.length > 1) {
        if (teleParts[0] && teleParts[1]) {
          this.form.telephoneInput = teleParts[1];
          this.form.telephoneCountryCode = teleParts[0];
        }
      }
    }
    if (data.fax) {
      const faxParts = data.fax.split("-");
      if (faxParts.length > 1) {
        if (faxParts[0] && faxParts[1]) {
          this.form.faxInput = faxParts[1];
          this.form.faxCountryCode = faxParts[0];
        }
      }
    }
  }

  private getFormattedDate(data) {
    if (data) {
    } else {
      data = Moment();
    }
    const mdate = Moment(data);
    const date = mdate.date();
    const month = mdate.month() + 1;
    return {
      date: {
        year: mdate.year(),
        day: date,
        month: month
      }
    };
  }
}
