// tslint:disable
import { Component, OnInit, Output, EventEmitter, ViewChild, Input } from '@angular/core';
import { Tab } from '../../../models/registeration.model';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppSettingService } from '../../../../core/services/app-setting.service';
import { MessageText, HttpStatusCode, FieldLength, RedirectUrl } from '../../../../core/configuration/config';
import { Attachment, MetaPostModel, AttachmentMetaType, Meta, UploadAttachmentMeta } from '../../../../core/models/AttachmentMeta';
import { AttachmentService } from '../../../../shared/services/attachment-service';

enum DocTypes {
  certificate = 'VAT Certificate',
  companyProfile = 'Company profile',
  cv = 'CV',
  emiratesId = 'Emirates ID',
  financialDetails = 'Financials',
  incorporationDate = 'Incorporation Certificate',
  passportNo = 'Passport',
  tradeLicenseCopy = 'Trade License',
  visaNo = 'Visa',
}

@Component({
  selector: 'app-document-attachment',
  templateUrl: './document-attachment.component.html',
  styleUrls: ['./document-attachment.component.css']
})
export class DocumentAttachmentComponent implements OnInit {

  @ViewChild('ngxCustomUploadTradeLicense')
  ngxCustomUploadTradeLicense: any;

  @ViewChild('ngxCustomUploadProfile')
  ngxCustomUploadProfile: any;

  @ViewChild('ngxCustomUploadPassport')
  ngxCustomUploadPassport: any;

  @ViewChild('ngxCustomUploadcv')
  ngxCustomUploadcv: any;

  @ViewChild('ngxCustomUploadInCorporation')
  ngxCustomUploadInCorporation: any;

  @ViewChild('ngxCustomUploadVatCert')
  ngxCustomUploadVatCert: any;

  @ViewChild('ngxCustomUploadVisa')
  ngxCustomUploadVisa: any;

  @ViewChild('ngxCustomUploadEmirateId')
  ngxCustomUploadEmirateId: any;

  @ViewChild('ngxCustomUploadFinancial')
  ngxCustomUploadFinancial: any;

  // output methodes
  @Output() attachment = new EventEmitter<any>();
  @Output() notifyServerMessage = new EventEmitter<any>();
  @Output() next = new EventEmitter<any>();
  @Output() previous = new EventEmitter<any>();
  @Input() isPerson = false;
  @Input() attachmentsTabs: Array<Tab> = [];
  uploadMetaList = new Array<UploadAttachmentMeta>();

  email = '';
  attachmentValidation = false;
  isTradeLicenseAttachmentMandatory = false;
  isInCorporationAttachmentMandatory = false;
  tradeLicenseTableId = '#data-table-trade-license';
  profileTableId = '#data-table-profile';
  financialTableId = '#data-table-financial';
  passportTableId = '#data-table-passport';
  cvTableId = '#data-table-cv';
  inCorporationTableId = '#data-table-inCorporation';
  vatCertTableId = '#data-table-vat-cert';
  visaTableId = '#data-table-visa';
  emirateIdTableId = '#data-table-emirate-id';
  certificates = '#data-table-certificates';
  serverMessage = '';
  attachmentsValidationMessages = '';
  attachmentsRequired = [];
  attachmentBin = [];

  constructor(private router: Router,
    private appSettingSrvc: AppSettingService,
    private attachmentService: AttachmentService) { }

  ngOnInit() {
  }

  public attachmentSubmit() {
    //!!
    // this.attachment.emit('');

    if (this.requiredAttachmentsUploaded()) {
    } else {
      this.attachmentValidation = true;
      return false;
    }
    this.attachment.emit('');
  }

  public writeMessage() {
    if (this.attachmentsRequired.length === 1) {
      this.attachmentsValidationMessages = 'Attachment ' + this.attachmentsRequired[0] + ' is required';
    } else {
      this.attachmentsValidationMessages = 'Attachments ' + this.attachmentsRequired.join(', ') + ' are required';
    }
  }

  public requiredAttachmentsUploaded() {
    let attachmentValidated = true;
    this.attachmentsRequired = [];
    for (let index = 0; index < this.attachmentsTabs.length; index++) {
      const element = this.attachmentsTabs[index];
      if (element.allowRender) {
        if (this.tableData(element.tableId) > 0) {

        } else {
          //!
          if (element.name === DocTypes.financialDetails || element.name === DocTypes.certificate || element.name === DocTypes.visaNo
              || element.name === DocTypes.passportNo || element.name === DocTypes.cv || element.name === DocTypes.incorporationDate
              || element.name === DocTypes.emiratesId || element.name === DocTypes.tradeLicenseCopy) {
          }
          else {
            attachmentValidated = false;
            this.attachmentsRequired.push(element.name);
          }
        }
      }
    }

    if (attachmentValidated === false) {
      this.writeMessage();
      return false;
    }
    return true;
  }

  //!
  public setUploaderConfigration(data) {
    this.ngxCustomUploadTradeLicense.fileExt = data.fileTypes;
    this.ngxCustomUploadTradeLicense.charactersNotAllowedMessage = data.disallowedCharactersError;

    this.ngxCustomUploadProfile.fileExt = data.fileTypes;
    this.ngxCustomUploadProfile.charactersNotAllowedMessage = data.disallowedCharactersError;

    this.ngxCustomUploadFinancial.fileExt = data.fileTypes;
    this.ngxCustomUploadFinancial.charactersNotAllowedMessage = data.disallowedCharactersError;

    this.ngxCustomUploadPassport.fileExt = data.fileTypes;
    this.ngxCustomUploadPassport.charactersNotAllowedMessage = data.disallowedCharactersError;

    this.ngxCustomUploadcv.fileExt = data.fileTypes;
    this.ngxCustomUploadcv.charactersNotAllowedMessage = data.disallowedCharactersError;

    this.ngxCustomUploadInCorporation.fileExt = data.fileTypes;
    this.ngxCustomUploadInCorporation.charactersNotAllowedMessage = data.disallowedCharactersError;

    this.ngxCustomUploadVatCert.fileExt = data.fileTypes;
    this.ngxCustomUploadVatCert.charactersNotAllowedMessage = data.disallowedCharactersError;

    this.ngxCustomUploadEmirateId.fileExt = data.fileTypes;
    this.ngxCustomUploadEmirateId.charactersNotAllowedMessage = data.disallowedCharactersError;

    this.ngxCustomUploadVisa.fileExt = data.fileTypes;
    this.ngxCustomUploadVisa.charactersNotAllowedMessage = data.disallowedCharactersError;
  }

  public assignUploadMeta(data) {
    this.uploadMetaList = [];
    this.uploadMetaList = data;
    for (let i = 0; i < data.length; i++) {
      switch (data[i].DocType) {
        case DocTypes.certificate:
          this.setMeta(this.ngxCustomUploadVatCert, data[i]);
          break;
        case DocTypes.companyProfile:
          this.setMeta(this.ngxCustomUploadProfile, data[i]);
          break;
        case DocTypes.cv:
          this.setMeta(this.ngxCustomUploadcv, data[i]);
          break;
        case DocTypes.financialDetails:
          this.setMeta(this.ngxCustomUploadFinancial, data[i]);
          break;
        case DocTypes.incorporationDate:
          this.setMeta(this.ngxCustomUploadInCorporation, data[i]);
          break;
        case DocTypes.passportNo:
          this.setMeta(this.ngxCustomUploadPassport, data[i]);
          break;
        case DocTypes.tradeLicenseCopy:
          this.setMeta(this.ngxCustomUploadTradeLicense, data[i]);
          break;
        case DocTypes.visaNo:
          this.setMeta(this.ngxCustomUploadVisa, data[i]);
          break;
        case DocTypes.emiratesId:
          this.setMeta(this.ngxCustomUploadEmirateId, data[i]);
          break;
        default:
          break;
      }
    }
  }

  public setMeta(control, data) {
    data.Email = this.email;
    control.updateMeta(data);
  }

  public bindAllAttachmentGrids() {
    this.bindAttachmentGrid(this.tradeLicenseTableId);
    this.bindAttachmentGrid(this.profileTableId);
    this.bindAttachmentGrid(this.passportTableId);
    this.bindAttachmentGrid(this.cvTableId);
    this.bindAttachmentGrid(this.inCorporationTableId);
    this.bindAttachmentGrid(this.vatCertTableId);
    this.bindAttachmentGrid(this.visaTableId);
    this.bindAttachmentGrid(this.emirateIdTableId);
    this.bindAttachmentGrid(this.financialTableId);
  }
  public bindAttachmentGrid(Id) {
    const dtId3: any = $(Id);
    const tableWidget1 = dtId3
      .DataTable({
        sorting: [],
        responsive: true,
        paging: false,
        pageLength: this.appSettingSrvc.settings.pagingAppearence,
        searching: false,
        info: false,
        lengthChange: false,
        sortable: false,
        ordering: false,
        autoWidth: false,
        language: {
          emptyTable: MessageText.gridEmpty,
        },
        columnDefs: [
          {
            targets: [2, 3, 4],
            visible: false
          },
          {
            targets: 0,
            render: function (data, type, row, index) {
              const result = data || row.id;
              return '<span id="downloadBtn" data-index=' + index.row +
                ' data-toggle="tooltip" data-placement="bottom" title="download" style="cursor: pointer">' + result + '</span>';
            }
          },
          {
            targets: 5,
            render: function (data, type, row, index) {
              const result = data || row.id;
              return ' <a id="deleteAttachment" style="cursor: pointer" class="delete" data-index='
                + index.row + '><i class="icon-delete"></i></a>';
            }
          },
        ],
        columns: [
          { data: 'name', title: 'Name', width: '39%' },
          { data: 'docType', title: 'Type', width: '24%' },
          { data: 'id', title: '' },
          { data: 'isDeleted', title: '' },
          { data: 'isUpdated', title: '' },
          { data: 'Delete', title: 'Delete', width: '5%' }],
        data: []
      });

    dtId3.on('click', '#deleteAttachment', (e) => {
      e.preventDefault();
      const dtId1: any = $(Id);
      const widget = dtId1.DataTable();
      const index = e.currentTarget.attributes['data-index'].nodeValue;
      const data = this.findDataByIndex(index, Id);
      if (data.isUpdated === false) {
        data.isDeleted = true;
        this.attachmentBin.push(data);
      }
      //!
      this.deleteAttachment(data.id);

      widget.row(index).remove();
      widget.rows().invalidate('data').draw(false);
    });

    dtId3.on('click', '#downloadBtn', (e) => {
      e.preventDefault();
      const index = e.currentTarget.attributes['data-index'].nodeValue;
      const data = this.findDataByIndex(index, Id);
      this.downloadAttachment(data);
    });
  }

  // download logic
  public downloadAttachment(item) {
    const payload = {
      Id : item.id,
      Purpose : 'Register'
    };

    this.attachmentService.downloadAttachment(payload).subscribe(
      resp => {
        const blob = new Blob([resp.content], { type: resp.content.type });
        const fileName = item.name;
        const objectUrl = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;

        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);

      },
      err => {
        if (err instanceof HttpErrorResponse
          && (err.status === HttpStatusCode.badRequest)) {
          const message = err.error.error || err.error.message;
          const msg = { isSuccess: true, message: message };
          this.notifyServerMessage.emit(msg);
        } else if (err instanceof HttpErrorResponse
          && (err.status === HttpStatusCode.notFound)) {
          const msg = { isSuccess: false, message: MessageText.fileNotAvailable };
          this.notifyServerMessage.emit(msg);
        }
      });
  }

  //!
  public deleteAttachment(docId) {
    const payload={ Id: docId }
    this.attachmentService.deleteAttachment(payload).subscribe(
      resp => { },
      err => {
        if (err instanceof HttpErrorResponse
          && (err.status === HttpStatusCode.badRequest)) {
          const message = err.error.error || err.error.Message;
          const msg = { isSuccess: true, message: message };
          this.notifyServerMessage.emit(msg);
        }
      });
  }

  // uploaderCallBacks
  public uploaderStatus(data) {
    this.attachmentValidation = false;
    const dtId1: any = $(data.viewId);
    const tableWidgetAttachment = dtId1.DataTable();
    for (let i = 0; i < data.length; i++) {
      const attach = new Attachment();
      attach.id = data[i].Id;
      attach.name = data[i].FileName;
      attach.docType = data[i].FileType;
      attach.isUpdated = true;
      attach.isDeleted = false;
      tableWidgetAttachment.row.add(attach).draw();
    }
    const controlContxt = this.getUploaderContext(data.viewId);
    controlContxt.progress = 0;
    controlContxt.isCompleted = false;
  }

  public conditionalAttachmentValid(): boolean {
    let validFlag = true;
    if (this.isTradeLicenseAttachmentMandatory) {
      validFlag = this.tableData(this.tradeLicenseTableId) > 0;
      if (validFlag === false) {
        this.attachmentsRequired.push('Trade License/Registration Certificate');
      }
    }

    if (this.isInCorporationAttachmentMandatory) {
      validFlag = this.tableData(this.inCorporationTableId) > 0;
      if (validFlag === false) {
        this.attachmentsRequired.push('Incorporation Certificate');
      }
    }
    return validFlag;
  }

  public tableData(id) {
    const dtId1: any = $(id);
    return dtId1.DataTable().data().length;
  }

  public gotoPrevious() {
    this.previous.emit();
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }

  public attachmentData(id) {
    const data = [];
    const dtId1: any = $(id);
    const table = dtId1.DataTable().data();
    for (let i = 0; i < table.length; i++) {
      data.push(table[i]);
    }
    return data;
  }

  public getUploaderContext(id) {
    switch (id) {
      case '#data-table-trade-license':
        return this.ngxCustomUploadTradeLicense;
      case '#data-table-profile':
        return this.ngxCustomUploadProfile;
      case '#data-table-passport':
        return this.ngxCustomUploadPassport;
      case '#data-table-cv':
        return this.ngxCustomUploadcv;
      case '#data-table-inCorporation':
        return this.ngxCustomUploadInCorporation;
      case '#data-table-vat-cert':
        return this.ngxCustomUploadVatCert;
      case '#data-table-visa':
        return this.ngxCustomUploadVisa;
      case '#data-table-emirate-id':
        return this.ngxCustomUploadEmirateId;
      case '#data-table-financial':
        return this.ngxCustomUploadFinancial;
      default:
        break;
    }
  }

  public findDataByIndex(index, grid) {
    const dtId: any = $(grid);
    const data = dtId.DataTable().row(index).data();
    return data;
  }

  public loadExisitingData(data) {
    data = this.fillData(data);
    this.mappData(data.tradeLicenseAttach, '#data-table-trade-license');
    this.mappData(data.companyProfileAttach, '#data-table-profile');
    this.mappData(data.passportAttach, '#data-table-passport');
    this.mappData(data.cvAttach, '#data-table-cv');
    this.mappData(data.incorporationAttach, '#data-table-inCorporation');
    this.mappData(data.vatCertificateAttach, '#data-table-vat-cert');
    this.mappData(data.visaNoAttach, '#data-table-visa');
    this.mappData(data.emirateIdAttach, '#data-table-emirate-id');
    this.mappData(data.financialAttach, '#data-table-financial');
  }

  public fillData(data) {
    data.tradeLicenseAttach = data.attachmentList.filter(x => x.docType.toLowerCase() === 'trade license copy');
    data.companyProfileAttach = data.attachmentList.filter(x => x.docType.toLowerCase() === 'company profile');
    data.financialAttach = data.attachmentList.filter(x => x.docType.toLowerCase() === 'financial details');
    data.passportAttach = data.attachmentList.filter(x => x.docType.toLowerCase() === 'passport no');
    data.cvAttach = data.attachmentList.filter(x => x.docType.toLowerCase() === 'cv');
    data.incorporationAttach = data.attachmentList.filter(x => x.docType.toLowerCase() === 'incorporation date');
    data.vatCertificateAttach = data.attachmentList.filter(x => x.docType.toLowerCase() === 'certificate');
    data.emirateIdAttach = data.attachmentList.filter(x => x.docType.toLowerCase() === 'emiratesid');
    data.visaNoAttach = data.attachmentList.filter(x => x.docType.toLowerCase() === 'visa no');
    return data;
  }

  public mappData(data, grid) {
    const dtId1: any = $(grid);
    const tableWidgetAttachment = dtId1.DataTable();
    for (let i = 0; i < data.length; i++) {
      const attach = new Attachment();
      attach.id = data[i].id;
      attach.name = data[i].name;
      attach.docType = data[i].docType;
      attach.isUpdated = false;
      attach.isDeleted = false;
      tableWidgetAttachment.row.add(attach).draw();
    }
  }

  private mappDeletedAttachments(docType) {
    switch (docType.toLowerCase()) {
      case 'certificate':
        return this.attachmentBin.filter(x => x.docType.toLowerCase() === docType.toLowerCase() && x.isDeleted === true && x.isUpdated === false);
      case 'company profile':
        return this.attachmentBin.filter(x => x.docType.toLowerCase() === docType.toLowerCase() && x.isDeleted === true && x.isUpdated === false);
      case 'cv':
        return this.attachmentBin.filter(x => x.docType.toLowerCase() === docType.toLowerCase() && x.isDeleted === true && x.isUpdated === false);
      case 'financial details':
        return this.attachmentBin.filter(x => x.docType.toLowerCase() === docType.toLowerCase() && x.isDeleted === true && x.isUpdated === false);
      case 'incorporation date':
        return this.attachmentBin.filter(x => x.docType.toLowerCase() === docType.toLowerCase() && x.isDeleted === true && x.isUpdated === false);
      case 'passport no':
        return this.attachmentBin.filter(x => x.docType.toLowerCase() === docType.toLowerCase() && x.isDeleted === true && x.isUpdated === false);
      case 'trade license copy':
        return this.attachmentBin.filter(x => x.docType.toLowerCase() === docType.toLowerCase() && x.isDeleted === true && x.isUpdated === false);
      case 'visa no':
        return this.attachmentBin.filter(x => x.docType.toLowerCase() === docType.toLowerCase() && x.isDeleted === true && x.isUpdated === false);
      case 'emiratesid':
        return this.attachmentBin.filter(x => x.docType.toLowerCase() === docType.toLowerCase() && x.isDeleted === true && x.isUpdated === false);
      default:
        break;
    }
  }
}
