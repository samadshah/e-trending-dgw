// tslint:disable
import { Component, OnInit, Output, EventEmitter, AfterViewInit, Input } from '@angular/core';
import { ServeMessageNotify, AddressDetailRequest, OrganizationTypeRequest } from '../../../models/registeration.model';
import { HttpErrorResponse } from '@angular/common/http';
import { IMyDpOptions } from 'mydatepicker';
import * as Moment from 'moment';
import { Router } from '@angular/router';
import { AppSettingService } from '../../../../core/services/app-setting.service';
import { MessageText, HttpStatusCode, FieldLength, RedirectUrl } from '../../../../core/configuration/config';
import { UtiltiyService } from '../../../../core/services/utility';
import { LookupService } from '../../../../shared/services/lookup-service';
import { Lookup } from '../../../../core/models/lookup';
import { RegisterService } from '../../../register.service';

@Component({
  selector: 'app-address-detail',
  templateUrl: './address-detail.component.html',
  styleUrls: ['./address-detail.component.css']
})
export class AddressDetailComponent implements OnInit, AfterViewInit {

  // output methodes
  @Output() addressDetailSumit = new EventEmitter<any>();
  @Output() notifyServerMessage = new EventEmitter<any>();
  @Output() next = new EventEmitter<any>();
  @Output() previous = new EventEmitter<any>();
  @Input() form = new AddressDetailRequest();
  //! For country auto-select
  @Input() orgTypeData: OrganizationTypeRequest;

  serverMessage = '';
  countrySetting: any = {};
  citySetting: any = this.dropdownSetting('Select City', false);

  countries = [];
  selectedCountry = [];

  selectedCity = [];
  cities = [];

  constructor(private utility: UtiltiyService,
    private lookupService: LookupService,
    private registerService: RegisterService,
    private router: Router,
    private appSettingSrvc: AppSettingService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  public companyAddress(companyAddressForm) {
    //!!
    // this.addressDetailSumit.emit(this.form);
    //hardcoding mandatory values for DB
    this.form.building = "1";
    this.form.area = "1";
    this.form.streetNo = "1";
    this.form.poBox = "1";
    if (!companyAddressForm.valid) {
      return false;
    }
    this.addressDetailSumit.emit(this.form);
  }

  public onCountrySelect(data, selected = undefined) {
    this.selectedCity = [];
    this.lookupService.VPCountryDependent(data.id).subscribe(res => {
      this.fillCities(res);
      if (selected) {
        this.selectedCity = this.cities.filter(x => x.id === this.form.city);
      }
    },
      err => {
        if (err instanceof HttpErrorResponse
          && (err.status === HttpStatusCode.badRequest)) {
          const message = err.error.error || err.error.message;
          const errorPaylaod = new ServeMessageNotify(message, false);
          this.notifyServerMessage.emit(errorPaylaod);
        }
      });
    this.form.countryRegion = data.id;
  }

  public onCitySelect(data) {
    this.form.city = data.id;
  }

  //!
  public fillCountries(countries, location = '') {
    this.countries = [];
    for (let i = 0; i < countries.length; i++) {
      this.countries.push({ 'id': countries[i].value, 'itemName': countries[i].name });
    }
    this.countrySetting = this.dropdownSetting('Select Country/Region', this.countries.length > this.appSettingSrvc.settings.searchEnableOnItem);
    //!!
    // if (location == 'Within UAE' || location == '') {
    //   this.countrySetting["disabled"] = true;
    // }
    // else if (location == 'Outside UAE') {
    //   this.countrySetting["disabled"] = false;
    // }
  }

  //! Country auto-select UAE if Org/type location is Within UAE
  public mapCountryFromOrgType(location){
    // if (location === 'Within UAE') {
    //   this.selectedCountry = [{ 'id': 'ARE', 'itemName': 'United Arab Emirates' }];
    //   const selectedCountry = { 'id': 'ARE', 'itemName': 'United Arab Emirates' };
    //   this.onCountrySelect(selectedCountry);
    // }
    // else if (location === 'Outside UAE') {
    //   this.selectedCountry = [];
    //   this.form.countryRegion = '';
    //   this.cities = [];
    // }
  }

  public fillCities(cities) {
    this.cities = [];
    this.selectedCity = [];
    for (let i = 0; i < cities.length; i++) {
      this.cities.push({ 'id': cities[i].value, 'itemName': cities[i].name });
    }
    this.citySetting = this.dropdownSetting('Select City', this.cities.length > this.appSettingSrvc.settings.searchEnableOnItem);
  }

  private dropdownSetting(text, search = true) {
    return {
      singleSelection: true,
      text: text,
      showCheckbox: false,
      classes: 'form-control custom-selectbox',
      enableSearchFilter: search,
      searchAutofocus: search
    };
  }


  //!!
  onTextToNumberChanged(e){
    if ([46, 8, 9, 27, 13, 110, 16].indexOf(e.keyCode) !== -1 ||
          // Allow: Ctrl+A
          (e.keyCode == 65 && e.ctrlKey === true) ||
          // Allow: Ctrl+C
          (e.keyCode == 67 && e.ctrlKey === true) ||
          // Allow: Ctrl+V
          (e.keyCode == 86 && e.ctrlKey === true) ||
          // Allow: Ctrl+X
          (e.keyCode == 88 && e.ctrlKey === true) ||
          // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)
        ) {
            return;
          }

    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
  }


  public gotoPrevious() {
    this.previous.emit();
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }

  public loadExisiting(data) {
    this.mappModel(data);
    this.selectedCountry = this.countries.filter(x => x.id === this.form.countryRegion);
    const ob = { id: this.form.countryRegion };
    this.onCountrySelect(ob, this.form.city);
  }

  public mappModel(data) {
    this.form.countryRegion = data.countryRegion;
    this.form.city = data.city;
    this.form.poBox = data.poBox;
    this.form.street = data.street;
    this.form.building = data.building;
    this.form.area = data.area;
    this.form.streetNo = data.unitNo;
  }
}
