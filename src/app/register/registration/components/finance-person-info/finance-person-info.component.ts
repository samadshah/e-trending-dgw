// tslint:disable
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ServeMessageNotify, PersonInfoRequest, FinancePersonInfoRequest } from '../../../models/registeration.model';
import { HttpErrorResponse } from '@angular/common/http';
import { IMyDpOptions } from 'mydatepicker';
import * as Moment from 'moment';
import { Router } from '@angular/router';
import { AppSettingService } from '../../../../core/services/app-setting.service';
import { MessageText, HttpStatusCode, FieldLength, RedirectUrl } from '../../../../core/configuration/config';
import { VendorService } from '../../../../core/services/vendor/vendor.service';
import { NotificationsService } from 'angular2-notifications/dist';

@Component({
  selector: 'app-finance-person-info',
  templateUrl: './finance-person-info.component.html',
  styleUrls: ['./finance-person-info.component.css']
})
export class FinancePersonInfoComponent implements OnInit {

  // output methodes
  @Output() financePersonSubmit = new EventEmitter<any>();
  @Output() notifyServerMessage = new EventEmitter<any>();
  @Output() next = new EventEmitter<any>();
  @Output() previous = new EventEmitter<any>();
  //!
  @Input() form: FinancePersonInfoRequest = new FinancePersonInfoRequest();
  @Input() contactPersonData: PersonInfoRequest;

  fieldConfig = FieldLength;
  financeTelephoneValidation = false;
  financeJobTitleValidation = false;
  serverMessage = '';

  financePersonSetting: any;
  jobTitleSetting: any;

  selectedFinancePersonTitle = [];
  personTitles = [];


  selectedFinancePersonJobTitle = [];
  jobTitles = [];

  options = { separateDialCode: true };

  emailServerIssueMessage = '';
  forbiddenEmail = false;
  sameAsContact = false;

  constructor(private router: Router, private appSettingSrvc: AppSettingService,
    private vendorService: VendorService,
    private notificationsService: NotificationsService) {
      //!
    this.form.sameAsContactPerson = "No";
  }

  ngOnInit() {
  }

  public financePerson(financePersonForm) {
    //!!
    // this.financePersonSubmit.emit(this.form);

    let financePersonSuccessFlag = true;
    if (!financePersonForm.valid) {
      financePersonSuccessFlag = false;
    }

    if (this.form.telephoneInput) {
      this.financeTelephoneValidation = false;
      if (this.form.telephoneInput.toString().length > this.fieldConfig.value) {
        financePersonSuccessFlag = false;
      }
    } else {
      this.financeTelephoneValidation = true;
      financePersonSuccessFlag = false;
    }

    if (this.form.secondaryContactNumberInput) {
      if (this.form.secondaryContactNumberInput.toString().length > this.fieldConfig.value) {
        financePersonSuccessFlag = false;
      }
    }

    //!
    if (this.form.jobTitle) {
      this.financeJobTitleValidation = false;
    } else {
        this.financeJobTitleValidation = true;
        financePersonSuccessFlag = false;
    }

    if (financePersonSuccessFlag === false) {
      return financePersonSuccessFlag;
    }

    this.financePersonSubmit.emit(this.form);
  }

  public fillJobTitle(jobTitles) {
    this.jobTitles = [];
    for (let i = 0; i < jobTitles.length; i++) {
      this.jobTitles.push({ 'id': jobTitles[i].value, 'itemName': jobTitles[i].name });
    }
    this.jobTitleSetting = this.dropdownSetting('Select Job Title', this.jobTitles.length > this.appSettingSrvc.settings.searchEnableOnItem);
  }

  public fillPersonSalutation(titles) {
    this.personTitles = [];
    for (let i = 0; i < titles.length; i++) {
      this.personTitles.push({ 'id': titles[i].value, 'itemName': titles[i].name });
    }
    this.financePersonSetting = this.dropdownSetting('Select Title', this.personTitles.length > this.appSettingSrvc.settings.searchEnableOnItem);
  }


  public onfinancePersonSelect(data) {
    this.form.title = data.id;
  }

  public onFinancePersonJobTitleSelect(data) {
    this.financeJobTitleValidation = false;
    this.form.jobTitle = data.id;
  }

  private dropdownSetting(text, search = true) {
    return {
      singleSelection: true,
      text: text,
      showCheckbox: false,
      classes: 'form-control custom-selectbox',
      enableSearchFilter: search,
      searchAutofocus: search
    };
  }

  public gotoPrevious() {
    this.previous.emit();
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }

  public loadExisiting(data) {
    this.mappModel(data);
    this.selectedFinancePersonTitle = this.personTitles.filter(x => x.id === this.form.title);
    this.selectedFinancePersonJobTitle = this.jobTitles.filter(x => x.id === this.form.jobTitle);
  }

  public mappModel(data) {
    this.form.arabicFirstName = data.arabicFirstName;
    this.form.arabicLastName = data.arabicLastName;
    this.form.email = data.email;
    this.form.firstName = data.firstName;
    this.form.lastName = data.lastName;
    this.form.jobTitle = data.jobTitle;
    this.form.otherJobTitle = data.otherJobTitle;
    this.form.secondaryContactEmail = data.secondaryContactEmail;
    this.form.title = data.title;

    if (data.telephone) {
      const teleParts = data.telephone.split('-');
      if (teleParts.length > 1) {
        if (teleParts[0] && teleParts[1]) {
          this.form.telephoneInput = teleParts[1]
          this.form.telephoneCountryCode = teleParts[0];;
        }
      }
    }
    if (data.contactFax) {
      const faxParts = data.contactFax.split('-');
      if (faxParts.length > 1) {
        if (faxParts[0] && faxParts[1]) {
          this.form.contactFaxInput = faxParts[1];
          this.form.contactFaxCountryCode = faxParts[0];
        }
      }
    }
    if (data.secondaryContactNumber) {
      const faxParts = data.secondaryContactNumber.split('-');
      if (faxParts.length > 1) {
        if (faxParts[0] && faxParts[1]) {
          this.form.secondaryContactNumberInput = faxParts[1];
          this.form.secondaryContactNumberCode = faxParts[0];
        }
      }
    }
  }

  //!
  public onChangeSameAsContactPerson(value) {
    if (value === true) {
      this.fillContactPersonData(this.contactPersonData);
      this.form.sameAsContactPerson = "Yes";
      this.sameAsContact = true;
      this.resetDropdownSettings(true);
    } else {
      this.fillContactPersonData();
      this.form.sameAsContactPerson = "No";
      this.sameAsContact = false;
      this.resetDropdownSettings(false);
    }
  }

  private resetDropdownSettings(check){
    this.financePersonSetting = this.dropdownSetting('Select Title', this.personTitles.length > this.appSettingSrvc.settings.searchEnableOnItem);
    this.financePersonSetting["disabled"] = check;
    this.jobTitleSetting = this.dropdownSetting('Select Job Title', this.jobTitles.length > this.appSettingSrvc.settings.searchEnableOnItem);
    this.jobTitleSetting["disabled"] = check;

  }

  public fillContactPersonData(data = null){
    this.mapContactPerson(data);
    if(data == null){
      this.selectedFinancePersonTitle = [];
      this.selectedFinancePersonJobTitle = [];
    }
    else{
      this.selectedFinancePersonTitle = this.personTitles.filter(x => x.id === this.form.title);
      this.selectedFinancePersonJobTitle = this.jobTitles.filter(x => x.id === this.form.jobTitle);
    }
  }

  public mapContactPerson(data){
    this.form.arabicFirstName = data == null ? '' : data.arabicFirstName;
    this.form.arabicLastName = data == null ? '' : data.arabicLastName;
    this.form.email = data == null ? '' : data.email;
    this.form.firstName = data == null ? '' : data.firstName;
    this.form.lastName = data == null ? '' : data.lastName;
    this.form.jobTitle = data == null ? '' : data.jobTitle;
    this.form.otherJobTitle = data == null ? '' : data.otherJobTitle;
    this.form.secondaryContactEmail = data == null ? '' : data.secondaryContactEmail;
    this.form.title = data == null ? '' : data.title;

    this.form.telephoneInput = data == null ? '' : data.telephoneInput;
    this.form.telephoneCountryCode = data == null ? '971' : data.telephoneCountryCode;

    this.form.contactFaxInput = data == null ? '' : data.contactFaxInput;
    if(data == null){
      this.form.contactFaxCountryCode = '971';
    }
    else{
      if(data.contactFaxCountryCode){
        this.form.contactFaxCountryCode = data.contactFaxCountryCode;
      }
    }

    this.form.secondaryContactNumberInput = data == null ? '' : data.secondaryContactNumberInput;
    this.form.secondaryContactNumberCode = data == null ? '971' : data.secondaryContactNumberCode;
  }

}
