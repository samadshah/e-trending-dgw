import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  AfterViewInit,
  Input
} from "@angular/core";
import {
  ServeMessageNotify,
  OrganizationTypeRequest
} from "../../../models/registeration.model";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { AppSettingService } from "../../../../core/services/app-setting.service";
import {
  HttpStatusCode,
  RedirectUrl
} from "../../../../core/configuration/config";
import { UtiltiyService } from "../../../../core/services/utility";
import { LookupService } from "../../../../shared/services/lookup-service";

@Component({
  selector: "app-org-type",
  templateUrl: "./org-type.component.html",
  styleUrls: ["./org-type.component.css"]
})
export class OrgTypeComponent implements OnInit, AfterViewInit {
  // output methodes
  @Output() organizationSumit = new EventEmitter<any>();
  @Output() notifyServerMessage = new EventEmitter<any>();
  @Output() next = new EventEmitter<any>();
  @Output() previous = new EventEmitter<any>();
  @Input() form = new OrganizationTypeRequest();
  @Output() changeCountryInCAD = new EventEmitter<any>();

  orgSetting: any;
  locationSetting: any;
  orgSectorSetting: any = this.dropdownSetting(
    "Select Organization Sectors",
    false
  );
  serverMessage = "";

  orgPersons = [];
  selectedOrgPerson = [];

  Locations = [];
  selectedLocation = [];

  orgSectors = [];
  selectedOrgSector = [];

  constructor(
    private utility: UtiltiyService,
    private lookupService: LookupService,
    private router: Router,
    private appSettingSrvc: AppSettingService
  ) {}

  ngOnInit() {}

  ngAfterViewInit() {}

  public organizationType(orgTypeForm) {
    //!!
    // this.organizationSumit.emit(this.form);

    if (!orgTypeForm.valid) {
      return false;
    }
    this.organizationSumit.emit(this.form);
  }

  private dropdownSetting(text, search = true) {
    return {
      singleSelection: true,
      text: text,
      showCheckbox: false,
      classes: "form-control custom-selectbox",
      enableSearchFilter: search,
      searchAutofocus: search
    };
  }

  public onOrgPersonSelect(data) {
    this.form.organizationPerson = data.id;
  }

  public onLocationSelect(data, selected = undefined) {
    this.selectedOrgSector = [];
    this.lookupService.VPLocationDependent(data.id).subscribe(
      res => {
        this.fillOrgSectors(res);
        if (selected) {
          this.selectedOrgSector = this.orgSectors.filter(
            x => x.id === selected
          );
        }
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.message;
          const errorPayload = new ServeMessageNotify(message, false);
          this.notifyServerMessage.emit(errorPayload);
        }
      }
    );
    this.form.location = data.id;
    this.changeCountryInCAD.emit(data.id);
  }

  public onOrgSectorSelect(data) {
    this.form.organizationSector = data.id;
  }

  public fillOrgSectors(orgSectors) {
    this.orgSectors = [];
    for (let i = 0; i < orgSectors.length; i++) {
      this.orgSectors.push({
        id: orgSectors[i].value,
        itemName: orgSectors[i].name
      });
    }
    this.orgSectorSetting = this.dropdownSetting(
      "Select Organization Sectors",
      this.orgSectors.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public fillOrgTypes(orgPersons) {
    this.orgPersons = [];
    for (let i = 0; i < orgPersons.length; i++) {
      this.orgPersons.push({
        id: orgPersons[i].value,
        itemName: orgPersons[i].name
      });
    }
    this.orgSetting = this.dropdownSetting(
      "Select Organization/Person",
      this.orgPersons.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
    //! Default value for Org Type dropdown
    this.selectedOrgPerson = [{ id: "Organization", itemName: "Organization" }];
    this.form.organizationPerson = "Organization";
  }

  public fillOrgLocations(Locations) {
    this.Locations = [];
    for (let i = 0; i < Locations.length; i++) {
      this.Locations.push({
        id: Locations[i].value,
        itemName: Locations[i].name
      });
    }
    this.locationSetting = this.dropdownSetting(
      "Select Location",
      this.Locations.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
    //! Default value for Location dropdown
    // this.selectedLocation = [{id: "Within UAE", itemName: "Within UAE"}];
    // const selectedLocation = {id: "Within UAE", itemName: "Within UAE"};
    this.selectedLocation = [{ id: "Outside UAE", itemName: "Outside UAE" }];
    const selectedLocation = { id: "Outside UAE", itemName: "Outside UAE" };
    this.onLocationSelect(selectedLocation);
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }

  public loadExisiting(data) {
    this.mappModel(data);
    this.selectedOrgPerson = this.orgPersons.filter(
      x => x.id === this.form.organizationPerson
    );
    this.selectedLocation = this.Locations.filter(
      x => x.id === this.form.location
    );
    const ob = { id: this.form.location };
    this.onLocationSelect(ob, this.form.organizationSector);
  }

  public mappModel(data) {
    this.form.organizationPerson = data.organizationPerson;
    this.form.organizationSector = data.organizationSector;
    this.form.location = data.location;
  }
}
