import { HttpErrorResponse } from "@angular/common/http";
import {
  Component,
  EventEmitter,
  OnInit,
  Input,
  Output,
  ViewChild
} from "@angular/core";
import { Router } from "@angular/router";
import { interval } from "rxjs/observable/interval";
import { Subscription } from "rxjs/Subscription";

import {
  HttpStatusCode,
  MessageText,
  RedirectUrl
} from "../../../../core/configuration/config";
import { UtiltiyService } from "../../../../core/services/utility";
import {
  ServeMessageNotify,
  UserVerficationRequest
} from "../../../models/registeration.model";
import { RegisterService } from "../../../register.service";
import { NotificationsService } from "angular2-notifications/dist";

// services
@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit {
  @ViewChild("signUpForm")
  signUpForm: any;

  // output methods
  @Output() signup = new EventEmitter<any>();
  @Output() notifyServerMessage = new EventEmitter<any>();
  @Output() next = new EventEmitter<any>();
  @Output() previous = new EventEmitter<any>();
  @Output() loadProfile = new EventEmitter<any>();
  @Input() userVerfication = new UserVerficationRequest();

  // class level variables
  sendPassCodeConditionalValidation = false;
  sendPassCodeBtnPress = false;
  passCodeIsValidFlag = false;
  passCodeBtnDisable = false;
  verifyPasscodeBtnFlag = false;
  serverMessage = "";
  isSendBack = false;

  // timer work
  private _trialEndsAt;
  private _diff: number;
  private _minutes: number;
  private _seconds: number;
  subscription: Subscription;
  passcodeButtonValue = "REQUEST PASSCODE";

  constructor(
    private router: Router,
    private registerService: RegisterService,
    private notificationsService: NotificationsService,
    private utility: UtiltiyService
  ) {}

  ngOnInit() {}

  public signUp(signUpForm, emailPatter, namePattern) {
    //!!
    // this.signup.emit();
    // this.next.emit(0);

    this.serverMessage = "";
    this.sendPassCodeConditionalValidation = false;
    if (this.sendPassCodeBtnPress) {
      this.sendPassCodeBtnPress = false;
      if (emailPatter.errors && emailPatter.errors.pattern) {
        return false;
      }
      //!!
      else if (namePattern.errors && namePattern.errors.pattern) {
        return false;
      } else {
        if (this.userVerfication) {
          if (this.userVerfication.name) {
            if (this.userVerfication.email) {
              this.sendPasscode();
            } else {
              const errorMessage = new ServeMessageNotify(
                "Email is required.",
                false
              );
              this.notifyServerMessage.error(errorMessage);
            }
          } else {
            const errorMessage = new ServeMessageNotify(
              "Name is required.",
              false
            );
            this.notifyServerMessage.error(errorMessage);
          }
        } else {
          return false;
        }
      }
    } else {
      this.sendPassCodeConditionalValidation = true;
      if (!signUpForm.valid) {
        return false;
      }
      if (this.passCodeIsValidFlag === false) {
        this.isPassCodeValid();
      }
    }
  }

  public sendPasscode() {
    this.serverMessage = "";
    this.passCodeBtnDisable = true;
    this.registerService
      .sendPassCode(this.userVerfication.name, this.userVerfication.email)
      .subscribe(
        res => {
          let resp = JSON.parse(res.data);
          if (resp.isSuccess) {
            this.notificationsService.success(MessageText.passcodeSent);
            this.startTimer();
          } else {
            this.notificationsService.error(resp.message);
          }
        },
        err => {
          if (
            err instanceof HttpErrorResponse &&
            err.status === HttpStatusCode.badRequest
          ) {
            //!
            const errorText = err.error.Message;
            const errorPayload = new ServeMessageNotify(errorText, false);
            this.notifyServerMessage.error(errorPayload);
            this.serverMessage = errorText;
          }
          this.passCodeBtnDisable = false;
        }
      );
  }

  public isPassCodeValid() {
    if (this.verifyPasscodeBtnFlag === true) {
      return false;
    }
    this.verifyPasscodeBtnFlag = true;
    this.registerService
      .validateAndloadProfile(
        this.userVerfication.name,
        this.userVerfication.email,
        this.userVerfication.passCode
      )
      .subscribe(
        res => {
          this.passCodeIsValidFlag = false; //  already valid when true;
          this.verifyPasscodeBtnFlag = false;
          this.signup.emit();
          this.next.emit(0);
        },
        err => {
          if (
            err instanceof HttpErrorResponse &&
            err.status === HttpStatusCode.badRequest
          ) {
            const errorText = err.error.Message;
            const errorPayload = new ServeMessageNotify(errorText, false);
            this.notifyServerMessage.emit(errorPayload);
            this.serverMessage = errorText;
          }
          this.passCodeIsValidFlag = false;
          this.verifyPasscodeBtnFlag = false;
        }
      );
  }

  startTimer() {
    //! For passcode trial ends at 30 secs
    const twoMinute = new Date();
    twoMinute.setSeconds(twoMinute.getSeconds() + 30);
    this._trialEndsAt = twoMinute;
    this.subscription = interval(1000)
      .map(x => {
        this._diff =
          Date.parse(this._trialEndsAt) - Date.parse(new Date().toString());
      })
      .subscribe(x => {
        // this._minutes = this.getMinutes(this._diff);
        this._seconds = this.getSeconds(this._diff);
        const minutes = "00";
        const seconds =
          this._seconds < 10 ? "0" + this._seconds : this._seconds;
        this.passcodeButtonValue = minutes + ":" + seconds;
        if (this._diff < 1) {
          this.subscription.unsubscribe();
          this.passcodeButtonValue = "Resend Passcode";
          this.passCodeBtnDisable = false;
        }
      });
  }

  getMinutes(t) {
    return Math.floor((t / 1000 / 60) % 60);
  }

  getSeconds(t) {
    return Math.floor((t / 1000) % 60);
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }
}
