// tslint:disable
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { PersonInfoRequest } from '../../../models/registeration.model';
import { Router } from '@angular/router';
import { AppSettingService } from '../../../../core/services/app-setting.service';
import { FieldLength, RedirectUrl } from '../../../../core/configuration/config';

@Component({
  selector: 'app-contact-person-info',
  templateUrl: './contact-person-info.component.html',
  styleUrls: ['./contact-person-info.component.css']
})
export class ContactPersonInfoComponent implements OnInit {

  // output methodes
  @Output() contactPersonSumit = new EventEmitter<any>();
  @Output() notifyServerMessage = new EventEmitter<any>();
  @Output() next = new EventEmitter<any>();
  @Output() previous = new EventEmitter<any>();
  @Input() form: PersonInfoRequest = new PersonInfoRequest();

  serverMessage = '';
  fieldConfig = FieldLength;
  contactPersonTelephoneValidation = false;
  secondaryContactNumberValidation = false;
  contactJobTitleValidation = false;
  contactPersonTitleSetting: any;
  jobTitleSetting: any;

  selectedContactPersonTitle = [];
  selectedContactJobTitle = [];
  personTitles = [];
  jobTitles = [];

  options = { separateDialCode: true };

  constructor(private router: Router,
    private appSettingSrvc: AppSettingService) { }

  ngOnInit() {
  }

  public contactPerson(contactPersonForm) {
    //!!
    // this.contactPersonSumit.emit(this.form);

    let contactPersonSuccessFlag = true;
    if (!contactPersonForm.valid) {
      contactPersonSuccessFlag = false;
    }

    if (this.form.telephoneInput) {
      this.contactPersonTelephoneValidation = false;
      if (this.form.telephoneInput.toString().length > this.fieldConfig.value) {
        contactPersonSuccessFlag = false;
      }
    } else {
      this.contactPersonTelephoneValidation = true;
      contactPersonSuccessFlag = false;
    }

    if (this.form.secondaryContactNumberInput) {
      if (this.form.secondaryContactNumberInput.toString().length > this.fieldConfig.value) {
        contactPersonSuccessFlag = false;
      }
    }

    //!
    if (this.form.jobTitle) {
      this.contactJobTitleValidation = false;
    } else {
        this.contactJobTitleValidation = true;
        contactPersonSuccessFlag = false;
    }

    if (contactPersonSuccessFlag === false) {
      return contactPersonSuccessFlag;
    }
    this.contactPersonSumit.emit(this.form);
  }

  public onContactPersonTitleSelect(data) {
    this.form.title = data.id;
  }

  public onContactJobTitleSelect(data) {
    this.contactJobTitleValidation = false;
    this.form.jobTitle = data.id;
  }

  public fillJobTitle(jobTitles) {
    this.jobTitles = [];
    for (let i = 0; i < jobTitles.length; i++) {
      this.jobTitles.push({ 'id': jobTitles[i].value, 'itemName': jobTitles[i].name });
    }
    this.jobTitleSetting = this.dropdownSetting('Select Job Title', this.jobTitles.length > this.appSettingSrvc.settings.searchEnableOnItem);
  }

  public fillPersonSalutation(titles) {
    this.personTitles = [];
    for (let i = 0; i < titles.length; i++) {
      this.personTitles.push({ 'id': titles[i].value, 'itemName': titles[i].name });
    }
    this.contactPersonTitleSetting = this.dropdownSetting('Select Title', this.personTitles.length > this.appSettingSrvc.settings.searchEnableOnItem);
  }

  private dropdownSetting(text, search = true) {
    return {
      singleSelection: true,
      text: text,
      showCheckbox: false,
      classes: 'form-control custom-selectbox',
      enableSearchFilter: search,
      searchAutofocus: search
    };
  }

  public gotoPrevious() {
    this.previous.emit();
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }

  public loadExisiting(data) {
    this.mappModel(data);
    this.selectedContactPersonTitle = this.personTitles.filter(x => x.id === this.form.title);
    this.selectedContactJobTitle = this.jobTitles.filter(x => x.id === this.form.jobTitle);
  }

  public mappModel(data) {
    this.form.arabicFirstName = data.arabicFirstName;
    this.form.arabicLastName = data.arabicLastName;
    this.form.email = data.email;
    this.form.firstName = data.firstName;
    this.form.lastName = data.lastName;
    this.form.jobTitle = data.jobTitle;
    this.form.otherJobTitle = data.otherJobTitle;
    this.form.secondaryContactEmail = data.secondaryContactEmail;
    this.form.title = data.title;

    if (data.telephone) {
      const teleParts = data.telephone.split('-');
      if (teleParts.length > 1) {
          if(teleParts[0] && teleParts[1]) {
            this.form.telephoneInput = teleParts[1]
            this.form.telephoneCountryCode = teleParts[0];
          }
      }
    }
    if (data.contactFax) {
      const faxParts = data.contactFax.split('-');
      if (faxParts.length > 1) {
         if(faxParts[0] && faxParts[1]) {
            this.form.contactFaxInput = faxParts[1];
            this.form.contactFaxCountryCode = faxParts[0];
         }
      }
    }
    if (data.secondaryContactNumber) {
      const faxParts = data.secondaryContactNumber.split('-');
      if (faxParts.length > 1) {
        if(faxParts[0] && faxParts[1]) {
          this.form.secondaryContactNumberInput = faxParts[1];
          this.form.secondaryContactNumberCode = faxParts[0];
        }
      }
    }
  }
}
