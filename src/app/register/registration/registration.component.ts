/* tslint:disable */
import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications/dist';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import swal from 'sweetalert2';

import { HttpStatusCode, MessageText, RedirectUrl, DocTypeAttachments, TabAttachment } from '../../core/configuration/config';
import { AttachmentMetaType, Meta, UploadAttachmentMeta } from '../../core/models/AttachmentMeta';
import { AttachmentService } from '../../shared/services/attachment-service';
import { LookupService } from '../../shared/services/lookup-service';
//!!
import { PaymentService } from '../../shared/services/payment-service';

import {
  RegisterationLookup,
  RegisterVendorRequestModel,
  Tab,
  UserVerficationRequest,
  OrganizationTypeRequest,
  GeneralInformationRequest,
  AddressDetailRequest,
  PersonInfoRequest,
  FinancePersonInfoRequest,
  PrecurementCategoryRequest,
  TermAndConditionTab,
} from '../models/registeration.model';
import { RegisterService } from '../register.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit, AfterViewInit, OnDestroy {

  // components
  @ViewChild('appsignup')
  signUpCompnent: any;

  @ViewChild('apporgtype')
  orgTypeCompnent: any;

  @ViewChild('appgeneralinfo')
  generalInfoComponent: any;

  @ViewChild('appaddressdetail')
  addressDetailComponent: any;

  @ViewChild('appcontactpersoninfo')
  contactPersonInfoComponent: any;

  @ViewChild('appfinancepersoninfo')
  financePersonInfoComponent: any;

  @ViewChild('appprecurementcategories')
  PrecurementCategoriesComponent: any;

  @ViewChild('apptermandcondition')
  termAndConditionComponent: any;

  @ViewChild('appnondisclosure')
  NonDisclosureComponent: any;

  @ViewChild('appdocumentattachment')
  documentAttachmentComponent: any;

  form = new RegisterVendorRequestModel();
  tabs: Array<Tab> = new Array<Tab>();
  currentTab: Tab = new Tab('', '', '');
  currentTabIndex = 0;
  registerationLookup = new RegisterationLookup();
  vendorRegisterBtnDisable = false;
  canGo: Subject<boolean> = new Subject<boolean>();
  isFormSubmitted = false;
  isPerson = false;
  metaTypeList: any;
  metaTags: any;
  serverMessage = '';
  uploadMetaList = new Array<UploadAttachmentMeta>();
  vatDisclamerText = '';

  // tabs models
  verificationRequest: UserVerficationRequest = new UserVerficationRequest();
  orgRequest: OrganizationTypeRequest = new OrganizationTypeRequest();
  generalInfoRequest: GeneralInformationRequest = new GeneralInformationRequest();
  addressDetailRequest: AddressDetailRequest = new AddressDetailRequest();
  contactPersonRequest: PersonInfoRequest = new PersonInfoRequest();
  //!
  financePersonRequest: FinancePersonInfoRequest = new FinancePersonInfoRequest();
  precurementCategoryRequest: PrecurementCategoryRequest = new PrecurementCategoryRequest();
  termAndCondition: TermAndConditionTab = new TermAndConditionTab();
  attachmentTabs: Array<Tab> = [];
  tabsIndex = TabAttachment;
  constructor(
    private router: Router,
    private notificationsService: NotificationsService,
    private registerService: RegisterService,
    private lookupService: LookupService,
    private attachmentService: AttachmentService,
    //!!
    private paymentService: PaymentService
  ) {
    this.initModelValues();
  }

  ngOnInit() {
    this.uploadMetaList = this.registerService.uploadMeta();
    this.documentAttachmentComponent.setUploaderConfigration(this.attachmentService.getConfig());


    this.applyScrollBarLight3();
    this.laodTabs();

    this.lookupService.registerationLookup().subscribe(resp => {
      this.registerationLookup = resp;
      this.fillOrgTypeFormDropdown();
      this.fillGeneralInfoFormDropdown();
      this.fillAddressDetailFormDropdown();
      this.fillPersonFormDropdown();
      this.loadProcurementCategories(this.registerationLookup.procurementCategories);
    },
    err => {
      if (err instanceof HttpErrorResponse
        && (err.status === HttpStatusCode.badRequest)) {
        const message = err.error.error || err.error.message;
        this.notificationsService.error(message);
      }
    });

    //!!
    this.lookupService.getIssuingEmirates().subscribe(resp => {
      this.fillIssuingEmiratesDropdown(resp.vendorLookup);
    },
    err => {
      if (err instanceof HttpErrorResponse
        && (err.status === HttpStatusCode.badRequest)) {
        const message = err.error.error || err.error.message;
        this.notificationsService.error(message);
      }
    });

    //!!
    this.lookupService.getRegistrationTypes().subscribe(resp => {
      this.fillRegistrationTypes(resp.vendorLookup);
    },
    err => {
      if (err instanceof HttpErrorResponse
        && (err.status === HttpStatusCode.badRequest)) {
        const message = err.error.error || err.error.message;
        this.notificationsService.error(message);
      }
    });

    this.registerService.termAndCondition().subscribe(res => {
      const resp = JSON.parse(res.data);
      this.vatDisclamerText = resp.VATDisclaimer;
      this.termAndCondition.termAndCondition = resp.TermsAndCondition;
      this.termAndCondition.nonDisclosureAgreement = resp.NonDisclosureAgreement;
    },
      err => {
        if (err instanceof HttpErrorResponse
          && (err.status === HttpStatusCode.badRequest)) {
          const message = err.error.error || err.error.message;
          this.notificationsService.error(message);
        }
      });

    this.documentAttachmentComponent.bindAllAttachmentGrids();
    setTimeout(() => {
      $(':input[type=number]').on('mousewheel', function (e) { $(this).blur(); });

      $(':input[type=number]').on('keydown', function (e) {
        if (e.which === 38 || e.which === 40 || e.which === 69) {
          e.preventDefault();
        }
      });
    }, 0);
  }

  initModelValues() {
    this.generalInfoRequest.vatRegistered = '';
    this.termAndCondition.termsAndConditionCheck = 'No';
    this.termAndCondition.nonDisclosureAgreementCheck = 'No';
    this.termAndCondition.nonDisclosureAgreement = '';

    // push attachments tabs
    this.attachmentTabs.push(new Tab('Company Profile', 'DocumentTab01', 'DocumentTab01-1', '', false, false, '#data-table-profile'));
    this.attachmentTabs.push(new Tab('Trade License', 'DocumentTab02', 'DocumentTab02-2', '', false, false, '#data-table-trade-license'));
    this.attachmentTabs.push(new Tab('Passport', 'DocumentTab03', 'DocumentTab03-3', '', false, false, '#data-table-passport'));
    this.attachmentTabs.push(new Tab('CV', 'DocumentTab04', 'DocumentTab04-4', '', false, false, '#data-table-cv'));
    this.attachmentTabs.push(new Tab('Incorporation Certificate', 'DocumentTab05', 'DocumentTab05-5', '', false, false, '#data-table-inCorporation'));
    this.attachmentTabs.push(new Tab('VAT Certificate', 'DocumentTab06', 'DocumentTab06-6', '', false, false, '#data-table-vat-cert'));
    this.attachmentTabs.push(new Tab('Visa', 'DocumentTab07', 'DocumentTab07-7', '', false, false, '#data-table-visa'));
    this.attachmentTabs.push(new Tab('Emirates ID', 'DocumentTab08', 'DocumentTab08-8', '', false, false, '#data-table-emirate-id'));
    this.attachmentTabs.push(new Tab('Financials', 'DocumentTab09', 'DocumentTab09-9', '', false, false, '#data-table-financial'));
  }

  public applyScrollBarLight3() {
    const customScrollBar: any = $('.scrollCont');
    customScrollBar.mCustomScrollbar({
      theme: 'light-3'
      , axis: 'y'
    });
  }

  ngAfterViewInit() {
    const collaspe: any = $('#regNav');
    collaspe.tabCollapse({
      tabsClass: 'hidden-sm hidden-xs'
      , accordionClass: 'visible-sm visible-xs'
    });
  }

  public laodTabs() {
    this.tabs.push(new Tab('User Verification', 'in active', 'active', 'Tab01', true));
    this.tabs.push(new Tab('Organization Type', '', '', 'Tab02'));
    this.tabs.push(new Tab('General  Information', '', '', 'Tab03'));
    this.tabs.push(new Tab('Company Address Details', '', '', 'Tab04'));
    this.tabs.push(new Tab('Contact Person Information', '', '', 'Tab05'));
    // this.tabs.push(new Tab('Finance Person Details', '', '', 'Tab06'));
    this.tabs.push(new Tab('Procurement Categories', '', '', 'Tab07'));
    this.tabs.push(new Tab('Terms and Conditions', '', '', 'Tab08'));
    this.tabs.push(new Tab('Non Disclosure Agreement', '', '', 'Tab11'));
    this.tabs.push(new Tab('Document Attachment(s)', '', '', 'Tab09'));
    //!!
    // this.tabs.push(new Tab('Payment', '', '', 'Tab12'));

    this.tabs.push(new Tab('Done', '', '', 'Tab10'));
    this.currentTab = this.tabs[0];
  }

  public loadProcurementCategories(categories) {
    this.PrecurementCategoriesComponent.loadProcurementCategories(categories);
  }

  public next(value = 0) {
    if (this.currentTabIndex < (this.tabs.length - 1)) {
      this.currentTabIndex = this.currentTabIndex + 1 + value;
      this.currentTab.isActive = '';
      this.currentTab.expanded = false;
      this.currentTab.activated = 'active is-done';
      const prevTabId = '#' + this.currentTab.hrefTab + '-collapse';
      const preTab = $(prevTabId);
      if (preTab) {
        preTab.removeClass('in active');
      }
      this.currentTab = this.tabs[this.currentTabIndex];
      this.currentTab.isActive = 'in active';
      this.currentTab.activated = 'active';
      const currentTabId = '#' + this.currentTab.hrefTab + '-collapse';
      const currentTab = $(currentTabId);
      if (currentTab) {
        currentTab.addClass('in active');
      }
      this.currentTab.expanded = true;
    }
  }

  public previous(value = 0) {
    if (this.currentTabIndex > 0) {
      this.currentTabIndex = this.currentTabIndex - 1 - value;
      this.currentTab.isActive = '';
      this.currentTab.activated = '';
      this.currentTab.expanded = false;
      const prevTabId = '#' + this.currentTab.hrefTab + '-collapse';
      const preTab = $(prevTabId);
      if (preTab) {
        preTab.removeClass('in active');
      }
      this.currentTab = this.tabs[this.currentTabIndex];
      this.currentTab.isActive = 'in active';
      this.currentTab.activated = 'active';
      const currentTabId = '#' + this.currentTab.hrefTab + '-collapse';
      const currentTab = $(currentTabId);
      if (currentTab) {
        currentTab.addClass('in active');
      }
      this.currentTab.expanded = true;
    }
  }

  // submit methods
  public signUp(signupModel) {
    if (this.signUpCompnent.isSendBack === false) {
      this.contactPersonRequest.email = this.verificationRequest.email;
      this.generalInfoRequest.emailAddress = this.verificationRequest.email;
      this.financePersonRequest.forbiddenEmail = this.verificationRequest.email;
    }
    this.documentAttachmentComponent.email = this.verificationRequest.email;
    this.financePersonRequest.forbiddenEmail = this.verificationRequest.email;
    this.documentAttachmentComponent.assignUploadMeta(this.uploadMetaList);
    this.showServerMessage('');
    this.scrollToTop();
  }

  public organizationType(orgModel) {
    if (orgModel.organizationPerson.toLowerCase() === 'person') {
      this.tabs[3].name = 'Address Details';
      this.tabs[5].allowRender = false;
      this.isPerson = true;
    } else {
      this.tabs[3].name = 'Company Address Details';
      this.tabs[5].allowRender = true;
      this.isPerson = false;
    }
    this.isPerson = this.isPerson;
    this.conditionalAttachmentMarkMandatory();
    this.next();
    this.scrollToTop();
    //! For docs
    this.conditionalEmiratedIdAttachment();
  }

  public generalInfo(generalInfoModel) {
    this.next();
    this.scrollToTop();
    //! For docs
    this.conditionalVatAttachment();
  }

  public companyAddress(addressModel) {
    this.next();
    this.scrollToTop();
  }

  public contactPerson(contactPersonModel) {
    if (this.isPerson) {
      this.next(1);
    } else {
      this.next();
    }
    this.scrollToTop();
  }

  public financePerson(financePersonModel) {
    this.next();
    this.scrollToTop();
  }

  public precurementCategory(precurementCategoryModel) {
    this.next();
    this.scrollToTop();
  }

  public conditionalAttachmentMarkMandatory() {

    for (let index = 0; index < this.attachmentTabs.length; index++) {
      const element = this.attachmentTabs[index];
      element.allowRender = false;
    }
    $('#docNav li').removeClass('active');
    $('.tab-document div').removeClass('active').removeClass('in');

    //! For docs
    if (this.isPerson) {
      $('#' + this.attachmentTabs[this.tabsIndex.passport].isActive).addClass('active in');
      $('#' + this.attachmentTabs[this.tabsIndex.passport].activated).addClass('active');
        this.attachmentTabs[this.tabsIndex.passport].allowRender = true;
        this.attachmentTabs[this.tabsIndex.cv].allowRender = true;
        // this.attachmentTabs[this.tabsIndex.visa].allowRender = true;
        this.attachmentTabs[this.tabsIndex.financialDetails].allowRender = true;
    } else {
          $('#' + this.attachmentTabs[this.tabsIndex.tradlicenseCertificate].isActive).addClass('active in');
          $('#' + this.attachmentTabs[this.tabsIndex.tradlicenseCertificate].activated).addClass('active');
          this.attachmentTabs[this.tabsIndex.tradlicenseCertificate].allowRender = true;
          this.attachmentTabs[this.tabsIndex.companyProfile].allowRender = true;
          this.attachmentTabs[this.tabsIndex.incorporationCertificate].allowRender = true;
          this.attachmentTabs[this.tabsIndex.financialDetails].allowRender = true;
    }

    this.attachmentTabs = this.attachmentTabs;
  }

  //! For docs
  public conditionalVatAttachment(){
    if (this.generalInfoRequest.vatRegistered === 'Yes') {
      this.attachmentTabs[this.tabsIndex.vatCertificate].allowRender = true;
    }
    else {
      this.attachmentTabs[this.tabsIndex.vatCertificate].allowRender = false;
    }
  }

  //! For docs
  public conditionalEmiratedIdAttachment(){
    // if (this.orgRequest.organizationPerson === 'Person' && this.orgRequest.location === 'Within UAE') {
    //   this.attachmentTabs[this.tabsIndex.emirateId].allowRender = true;
    // }
    if (this.orgRequest.location === 'Within UAE') {
      // this.attachmentTabs[this.tabsIndex.emirateId].allowRender = true;
    }
    else {
      this.attachmentTabs[this.tabsIndex.emirateId].allowRender = false;
    }
  }

  public specialCaseBackPrecurementCategories(event) {
    if (this.isPerson) {
      this.previous(1);
    } else {
      this.previous();
    }
  }

  public termAndConditionSubmit(termAndConditionModel) {
    this.form.termsAndConditionCheck = termAndConditionModel.termsAndConditionCheck;
    this.next();
    this.scrollToTop();
  }

  public nonDisclosuretermAndConditionSubmit(nonDisclousreModel) {
    this.form.NonDisclosureAgreementCheck = nonDisclousreModel.termsAndConditionCheck;    // TODO
    this.next();
    this.scrollToTop();
  }

  public attachmentSubmit(attachmentModel) {
    this.submitForm();
  }

  public submitForm() {
    this.serverMessage = '';
    this.showServerMessage('');
    let postModel = new RegisterVendorRequestModel();
    postModel = this.fillServerModel();
    if (this.vendorRegisterBtnDisable === true) {
      return false;
    }
    this.vendorRegisterBtnDisable = true;

    this.registerService.register(postModel).subscribe(res => {
      this.isFormSubmitted = true;
      this.notificationsService.success(MessageText.registerSubmit);
      this.next();
      this.scrollToTop();
    },
      err => {
        if (err instanceof HttpErrorResponse
          && (err.status === HttpStatusCode.badRequest)) {
          const errorText = err.error.Message;
          this.notificationsService.error(errorText);
          this.serverMessage = errorText;
        }
        //!
        if (err instanceof HttpErrorResponse
          && (err.status === HttpStatusCode.internalServerError)) {
          const errorText = err.error.Message;
          this.notificationsService.error(errorText);
          this.serverMessage = errorText;
        }
        this.vendorRegisterBtnDisable = false;
      });
  }

  public fillServerModel(): RegisterVendorRequestModel {
    // org type
    this.form.organizationType = this.orgRequest;

    // general info
    this.form.generalInformation = this.generalInfoRequest;
    this.form.generalInformation.vatDisclaimer = this.form.generalInformation.vatDisclaimerFlag &&
      this.form.generalInformation.vatDisclaimerFlag === true ? 'Yes' : 'No';
    this.form.generalInformation.fax = this.form.generalInformation.faxCountryCode &&
      this.form.generalInformation.faxInput ?
      this.form.generalInformation.faxCountryCode + '-' + this.form.generalInformation.faxInput : '';

    this.form.generalInformation.telephone = this.form.generalInformation.telephoneCountryCode &&
      this.form.generalInformation.telephoneInput ?
      this.form.generalInformation.telephoneCountryCode + '-' + this.form.generalInformation.telephoneInput : '';

    if (this.form.generalInformation.vatRegistered.toLowerCase() === 'yes' && this.form.generalInformation.vatReason) {
      this.form.generalInformation.vatReason = '';
    } else if (this.form.generalInformation.vatRegistered.toLowerCase() === 'no' && this.form.generalInformation.vatNumber) {
      this.form.generalInformation.vatNumber = '';
    }

    // company address details
    this.form.companyAddressDetails = this.addressDetailRequest;

    // contact person
    this.form.contactPersonInformation = this.contactPersonRequest;
    this.form.contactPersonInformation.contactFax = this.form.contactPersonInformation.contactFaxCountryCode && this.form.contactPersonInformation.contactFaxInput
      ? this.form.contactPersonInformation.contactFaxCountryCode + '-' + this.form.contactPersonInformation.contactFaxInput : '';
    this.form.contactPersonInformation.secondaryContactNumber = this.form.contactPersonInformation.secondaryContactNumberCode && this.form.contactPersonInformation.secondaryContactNumberInput
      ? this.form.contactPersonInformation.secondaryContactNumberCode + '-' + this.form.contactPersonInformation.secondaryContactNumberInput : '';
    this.form.contactPersonInformation.telephone = this.form.contactPersonInformation.telephoneCountryCode && this.form.contactPersonInformation.telephoneInput
      ? this.form.contactPersonInformation.telephoneCountryCode + '-' + this.form.contactPersonInformation.telephoneInput : '';

    //! finance person
    // if(!this.isPerson){
    //   this.form.financePersonDetails = this.financePersonRequest;
    //   this.form.financePersonDetails.contactFax = this.form.financePersonDetails.contactFaxCountryCode && this.form.financePersonDetails.contactFaxInput
    //     ? this.form.financePersonDetails.contactFaxCountryCode + '-' + this.form.financePersonDetails.contactFaxInput : '';
    //   this.form.financePersonDetails.secondaryContactNumber = this.form.financePersonDetails.secondaryContactNumberCode && this.form.financePersonDetails.secondaryContactNumberInput
    //     ? this.form.financePersonDetails.secondaryContactNumberCode + '-' + this.form.financePersonDetails.secondaryContactNumberInput : '';
    //   this.form.financePersonDetails.telephone = this.form.financePersonDetails.telephoneCountryCode && this.form.financePersonDetails.telephoneInput
    //     ? this.form.financePersonDetails.telephoneCountryCode + '-' + this.form.financePersonDetails.telephoneInput : '';
    // }

    // precurement categories
    this.form.procurementCategories = this.precurementCategoryRequest;

    // term and condition
    this.form.termsAndConditionCheck = this.form.termsAndConditionCheck;
    this.form.NonDisclosureAgreementCheck = this.form.NonDisclosureAgreementCheck;

    //!!
    if(this.form.generalInformation.vatRegistered === 'Yes'){
      this.form.generalInformation.vatNumber = "TR-" + this.form.generalInformation.vatNumber;
    }

    return this.form;
  }

  public fillDeletedItems(items, docType) {
    const binAttachments = this.documentAttachmentComponent.mappDeletedAttachments(docType);
    for (let index = 0; index < binAttachments.length; index++) {
      const element = binAttachments[index];
      items.push(element);
    }
  }

  canDeactivate(): boolean | Observable<boolean> | Promise<boolean> {
    this.canGo = new Subject<boolean>();
    // if form is submitted
    if (this.isFormSubmitted === true) {
      return true;
    }

    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    if (this.signUpCompnent.signUpForm.dirty === false) {
      return true;
    }

    // Otherwise ask the user with the dialog service and return its
    // observable which resolves to true or false when the user decides
    swal({
      title: MessageText.FormLeavingTitle,
      text: MessageText.FormLeavingText,
      // type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-default',
      cancelButtonClass: 'btn btn-info',
      buttonsStyling: false,
      reverseButtons: true,
      width : 360
    }).then((discardChanges) => {
      if (discardChanges.value) {
        this.canGo.next(true);
      } else if (discardChanges.dismiss.toString() === 'cancel') {
        if (window.history.state) {
          const url = window.history.state.url;
          window.history.pushState({ url: url }, '', url);
        } else {
          const url = '/login';
          window.history.pushState({ url: url }, '', url);
        }
        this.canGo.next(false);
      }
    });
    return this.canGo;
  }

  ngOnDestroy(): void {
    if (this.signUpCompnent.subscription) {
      this.signUpCompnent.subscription.unsubscribe();
    }
  }

  public notifyMessage(event) {
    if (event.isSuccess === true && event.clear === false) {
      this.notificationsService.success(event.message);
    } else if (event.isSuccess === false && event.clear === false) {
      this.showServerMessage(event.message);
      this.notificationsService.error(event.message);
    } else {
      this.showServerMessage('');
    }
  }

  public showServerMessage(msg) {
    this.signUpCompnent.serverMessage = msg;
    this.orgTypeCompnent.serverMessage = msg;
    this.generalInfoComponent.serverMessage = msg;
    this.addressDetailComponent.serverMessage = msg;
    this.contactPersonInfoComponent.serverMessage = msg;
    this.financePersonInfoComponent.serverMessage = msg;
    this.PrecurementCategoriesComponent.serverMessage = msg;
    this.termAndConditionComponent.serverMessage = msg;
    this.documentAttachmentComponent.serverMessage = msg;
  }

  public fillOrgTypeFormDropdown() {
    this.orgTypeCompnent.fillOrgTypes(this.registerationLookup.organizationTypes);
    this.orgTypeCompnent.fillOrgLocations(this.registerationLookup.organizationLocation);
  }

  public fillGeneralInfoFormDropdown() {
    this.generalInfoComponent.fillPersonSalutation(this.registerationLookup.personSalutation);
    this.generalInfoComponent.fillLanguages(this.registerationLookup.languages);
    this.generalInfoComponent.fillCurrency(this.registerationLookup.currency);
    this.generalInfoComponent.fillVateRegistered(this.registerationLookup.vatRegistered);
    this.generalInfoComponent.fillVatTypes(this.registerationLookup.vatType);
  }

  //!!
  public fillIssuingEmiratesDropdown(issuingEmirates) {
    this.generalInfoComponent.fillIssuingEmirates(issuingEmirates);
  }

  //!!
  public fillRegistrationTypes(regTypes) {
    this.generalInfoComponent.fillRegistrationTypes(regTypes);
  }

  public fillPersonFormDropdown() {
    this.contactPersonInfoComponent.fillJobTitle(this.registerationLookup.jobTitles);
    this.contactPersonInfoComponent.fillPersonSalutation(this.registerationLookup.personSalutation);
    this.financePersonInfoComponent.fillJobTitle(this.registerationLookup.jobTitles);
    this.financePersonInfoComponent.fillPersonSalutation(this.registerationLookup.personSalutation);
  }

  public fillAddressDetailFormDropdown() {
    this.addressDetailComponent.fillCountries(this.registerationLookup.countries);
  }

  //!!
  public fillCountryInCAD(location){
    this.addressDetailComponent.fillCountries(this.registerationLookup.countries, location);
    this.addressDetailComponent.mapCountryFromOrgType(location);
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }

  public scrollToTop() {
    window.scrollTo(0, 0);
  }

  public loadProfile(data) {
    this.fillorgType(data.organizationType);
    this.fillGeneralInfo(data.generalInformation);
    this.filladdressDetail(data.companyAddressDetails);
    this.fillContactPersonInfo(data.contactPersonInformation);
    // this.fillFinancePersonInfo(data.financePersonDetails);
    this.fillProcurementCategories(data.procurementCategories);
    this.fillTermAndConditionTab(data.termsAndConditionCheck);
    this.fillNonDisclosureTab(data.nonDisclosureAgreementCheck);
    this.fillattachmentTab(data);
  }

  private fillorgType(org) {
    this.orgTypeCompnent.loadExisiting(org);
  }

  private fillGeneralInfo(generalInfo) {
    this.generalInfoComponent.loadExisiting(generalInfo);
  }

  private filladdressDetail(companyAddressDetails) {
    this.addressDetailComponent.loadExisiting(companyAddressDetails);
  }

  private fillContactPersonInfo(personInfo) {
    this.contactPersonInfoComponent.loadExisiting(personInfo);
  }

  private fillFinancePersonInfo(personInfo) {
    this.financePersonInfoComponent.loadExisiting(personInfo);
  }

  private fillProcurementCategories(personInfo) {
    this.PrecurementCategoriesComponent.loadExisiting(personInfo);
  }

  private fillTermAndConditionTab(termAndCondition) {
    this.termAndConditionComponent.loadExisiting(termAndCondition);
  }

  private fillNonDisclosureTab(termAndCondition) {
    this.NonDisclosureComponent.loadExisiting(termAndCondition);
  }

  private fillattachmentTab(data) {
    this.documentAttachmentComponent.loadExisitingData(data);
  }


  //!!
  private paymentOnline(){
    let vendorName;
    if (this.form.organizationType.organizationPerson == 'Person') {
      vendorName = this.form.generalInformation.firstName +' '+ this.form.generalInformation.lastName;
    } else {
      vendorName = this.form.generalInformation.companyName;
    }

    let payload = {
      email : this.form.contactPersonInformation.email,
      name : vendorName,
      phone : this.form.generalInformation.telephone,
      vendorType : this.form.organizationType.organizationPerson
    };

    this.paymentService.paymentOnline(payload).subscribe((resp => {
      if(resp.isSuccess){
          window.location.href = resp.message;
      } else {
          this.notificationsService.error(resp.message);
      }
    }),
      err => {
        if (err instanceof HttpErrorResponse
          && (err.status === HttpStatusCode.badRequest)) {
          const message = err.error.error || err.error.Message;
          this.notificationsService.error(message);
        }
      });
  }


  //!!
  // private paymentDGW(){
  //   let vendorName;
  //   if (this.form.organizationType.organizationPerson == 'Person') {
  //     vendorName = this.form.generalInformation.firstName +' '+ this.form.generalInformation.lastName;
  //   } else {
  //     vendorName = this.form.generalInformation.companyName;
  //   }

  //   let payload = {
  //     email : this.form.contactPersonInformation.email,
  //     name : vendorName,
  //     phone : this.form.generalInformation.telephone,
  //     vendorType : this.form.organizationType.organizationPerson
  //   };

  //   this.paymentService.paymentDGW(payload).subscribe((resp => {
  //     if(resp.isSuccess){
  //         this.next();
  //     } else {
  //         this.notificationsService.error(resp.message);
  //     }
  //   }),
  //     err => {
  //       if (err instanceof HttpErrorResponse
  //         && (err.status === HttpStatusCode.badRequest)) {
  //         const message = err.error.error || err.error.Message;
  //         this.notificationsService.error(message);
  //       }
  //     });
  // }


}
