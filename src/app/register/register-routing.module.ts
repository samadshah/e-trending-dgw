import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register.component';
import { RegistrationComponent } from './registration/registration.component';
import { CanDeactivateGuard } from '../core/guards/can-deactivate-guard.service';

const routes: Routes = [
  {
    path: '',
    component: RegisterComponent,
    children: [
      {
        path: '',
        component: RegistrationComponent,
        canDeactivate: [CanDeactivateGuard],
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CanDeactivateGuard]
})


export class RegisterRoutingModule { }
