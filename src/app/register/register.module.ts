import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { RegistrationComponent } from './registration/registration.component';
import { SharedModule } from '../shared/shared.module';
import { RegisterService } from './register.service';
import { SignupComponent } from './registration/components/signup/signup.component';
import { OrgTypeComponent } from './registration/components/org-type/org-type.component';
import { GeneralInfoComponent } from './registration/components/general-info/general-info.component';
import { AddressDetailComponent } from './registration/components/address-detail/address-detail.component';
import { ContactPersonInfoComponent } from './registration/components/contact-person-info/contact-person-info.component';
import { FinancePersonInfoComponent } from './registration/components/finance-person-info/finance-person-info.component';
import { PrecurementCategoriesComponent } from './registration/components/precurement-categories/precurement-categories.component';
import { TermAndConditionComponent } from './registration/components/term-and-condition/term-and-condition.component';
import { DocumentAttachmentComponent } from './registration/components/document-attachment/document-attachment.component';
import { NonDisclosureComponent } from './registration/components/non-disclosure/non-disclosure.component';


@NgModule({
  imports: [
    CommonModule,
    RegisterRoutingModule,
    SharedModule,
  ],
  declarations: [RegisterComponent,
                 RegistrationComponent,
                 SignupComponent,
                 OrgTypeComponent,
                 GeneralInfoComponent,
                 AddressDetailComponent,
                 ContactPersonInfoComponent,
                 FinancePersonInfoComponent,
                 PrecurementCategoriesComponent,
                 TermAndConditionComponent,
                 DocumentAttachmentComponent,
                 NonDisclosureComponent],
  providers: [RegisterService],
  exports: [SharedModule]
})
export class RegisterModule { }
