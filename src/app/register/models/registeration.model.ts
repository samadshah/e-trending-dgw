import { Lookup } from '../../core/models/lookup';
import { MetaPostModel, Attachment, AttachmentMetaType, Meta } from '../../core/models/AttachmentMeta';

export class Tab {
    constructor(name: string,
                isActive: string,
                activated: string,
                hrefTab: string = '',
                expanded = false,
                allowRender = true,
                tableId: string = '') {
        this.name = name;
        this.isActive = isActive;
        this.activated = activated;
        this.allowRender = allowRender;
        this.hrefTab = hrefTab;
        this.expanded = expanded;
        this.tableId = tableId;
    }
    public name: string;
    public isActive: string;
    public activated: string;
    public allowRender: boolean;
    public hrefTab: string;
    public expanded: boolean;
    public tableId: string;
}

export class UserVerficationRequest {
    public name: string;
    public email: string;
    public passCode: string;
}

export class OrganizationTypeRequest {
    public organizationPerson: string;
    public organizationSector: string;
    public location: string;
}

export class GeneralInformationRequest {
    public arabicCompanyName: string;
    public companyName: string;
    //!
    // public companyRegisteredInDED: string;
    public currency: string;
    public emailAddress: string;
    public expiryDate: string;
    public fax: string;
    public firstName: string;
    public internetAddress: string;
    public issueDate: string;
    public languagePreference: string;
    public lastName: string;
    public passportNumber: string;
    public secondaryEmail: string;
    public telephone: string;
    public title: string;
    public tradeLicenseNumber: string;

    public vatDisclaimer: string;
    public vatNotRegisteredType: string;
    public vatNumber: string;
    public vatReason: string;
    public vatRegistered: string;
    public yearsCompanyOperatesInUAE: string;

    // front usage end propertiess
    public vatDisclaimerFlag: boolean;
    public telephoneCountryCode: string;
    public faxCountryCode: string;

    public faxInput: string;
    public telephoneInput: string;
    //!
    public tlIssuingEmirate: string;
    public registrationType: string;
}

export class AddressDetailRequest {
    public countryRegion: string;
    public city: string;
    public poBox: string;
    public street: string;
    public building: string;
    public area: string;
    public streetNo: string;
    public makaniNo: string;
}

export class PersonInfoRequest {
    public arabicFirstName: string;
    public arabicLastName: string;
    public contactFax: string;
    public email: string;
    public firstName: string;
    public jobTitle: string;
    public lastName: string;
    public otherJobTitle: string;
    public secondaryContactEmail: string;
    public secondaryContactNumber: string;
    public telephone: string;
    public title: string;
    public telephoneCountryCode: string;
    public secondaryContactNumberCode: string;
    public contactFaxCountryCode: string;
    public contactFaxInput: string;
    public secondaryContactNumberInput: string;
    public telephoneInput: string;
    public forbiddenEmail: string;
}

//!!
export class FinancePersonInfoRequest {
  public arabicFirstName: string;
  public arabicLastName: string;
  public contactFax: string;
  public email: string;
  public firstName: string;
  public jobTitle: string;
  public lastName: string;
  public otherJobTitle: string;
  public secondaryContactEmail: string;
  public secondaryContactNumber: string;
  public telephone: string;
  public title: string;
  public telephoneCountryCode: string;
  public secondaryContactNumberCode: string;
  public contactFaxCountryCode: string;
  public contactFaxInput: string;
  public secondaryContactNumberInput: string;
  public telephoneInput: string;
  public forbiddenEmail: string;
  public sameAsContactPerson: string;
}

export class PrecurementCategoryRequest {
    public otherCategory: string;
    public ProcurementCategories: Array<Lookup>;
}

// UI purpose model

export class TermAndConditionTab {
    public termAndCondition: string;
    public termsAndConditionCheck: string;

    public nonDisclosureAgreement: string;
    public nonDisclosureAgreementCheck: string;
}

export class RegisterationLookup {
    public organizationTypes: Array<Lookup>;
    public organizationLocation: Array<Lookup>;
    public organizationSectors: Array<Lookup>;
    public languages: Array<Lookup>;
    public currency: Array<Lookup>;
    public countries: Array<Lookup>;
    public vatRegistered: Array<Lookup>;
    public vatType: Array<Lookup>;
    public personSalutation: Array<Lookup>;
    public procurementCategories: Array<Lookup>;
    public jobTitles: Array<Lookup>;
}

export class PrecurementCategories {

    constructor(id, name, value, selected = false) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.isSelected = selected;
    }
    public id: number;
    public name: string;
    public value: string;
    public isSelected: boolean;
}

// serve model post
export class RegisterVendorRequestModel {
    public organizationType: OrganizationTypeRequest;
    public generalInformation: GeneralInformationRequest;
    //!
    public financePersonDetails: FinancePersonInfoRequest;
    public contactPersonInformation: PersonInfoRequest;
    public companyAddressDetails: AddressDetailRequest;
    public procurementCategories: PrecurementCategoryRequest;
    public termsAndConditionCheck: string;

    // ---------
    // attachment
    public companyProfileAttach: Array<Attachment>;
    public passportAttach: Array<Attachment>;
    public cvAttach: Array<Attachment>;
    public incorporationAttach: Array<Attachment>;
    public tradeLicenseAttach: Array<Attachment>;
    public emirateIdAttach: Array<Attachment>;
    public visaNoAttach: Array<Attachment>;
    public vatCertificateAttach: Array<Attachment>;
    public financialAttach: Array<Attachment>; //! optional

    // upload meta
    public metaTagTypeList: Array<AttachmentMetaType>;
    public metaTags: Array<Meta>;

    public isSendBack: boolean;
    public NonDisclosureAgreementCheck: string;

    public attachmentList: Array<Attachment>;
}

export class ServeMessageNotify {
    constructor(message, isSuccess = true, clear = false) {
        this.message = message;
        this.isSuccess = isSuccess;
        this.clear = clear;
    }
    public isSuccess: boolean;
    public message: string;
    public clear: boolean;
}


