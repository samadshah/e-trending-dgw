import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';

import { environment } from '../../environments/environment';
import { RegisterVendorRequestModel } from './models/registeration.model';

enum DocTypes {
  certificate = 'VAT Certificate',
  companyProfile = 'Company profile',
  cv = 'CV',
  emiratesId = 'Emirates ID',
  financialDetails = 'Financials',
  incorporationDate = 'Incorporation Certificate',
  passportNo = 'Passport',
  tradeLicenseCopy = 'Trade License',
  visaNo = 'Visa',
}

@Injectable()
export class RegisterService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;

  DocumentAttachmemts=[
    {
      Email: '',
      DocType: DocTypes.tradeLicenseCopy
    },
    {
      Email: '',
      DocType: DocTypes.companyProfile
    },
    {
      Email: '',
      DocType: DocTypes.passportNo
    },
    {
      Email: '',
      DocType: DocTypes.cv
    },
    {
      Email: '',
      DocType: DocTypes.incorporationDate
    },
    {
      Email: '',
      DocType: DocTypes.certificate
    },
    {
      Email: '',
      DocType: DocTypes.visaNo
    },
    {
      Email: '',
      DocType: DocTypes.emiratesId
    },
    {
      Email: '',
      DocType: DocTypes.financialDetails
    }
  ];


  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/vendors';
    this.servicePath2 = '/api';
  }

  sendPassCode(name, email): Observable<any> {
    const request = { name: name, email: email };
    return this.http.post(environment.base_api_url + this.servicePath2 + '/vendorregistration', request);
  }

  termAndCondition(): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/termsandconditions');
  }

  uploadMeta(){
    return this.DocumentAttachmemts;
  }

  register(model): Observable<any> {
    return this.http.post(environment.base_api_url + this.servicePath2 + '/vendorrequest', model);
  }

  validateAndloadProfile(name, email, passcode): Observable<RegisterVendorRequestModel> {
    const request = { Name: name, Email: email, Passcode: passcode };
    return this.http.post<RegisterVendorRequestModel>(environment.base_api_url + this.servicePath2 + '/vendorpasscodeverification', request);
  }
}
