import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BidDetail } from "../main/rfq/models/bid";
import { HttpStatusCode, RedirectUrl } from "../core/configuration/config";
import { AttachmentService } from "../shared/services/attachment-service";
import {
  MetaPostModel,
  AttachmentMetaType,
  Meta,
  Attachment
} from "../core/models/AttachmentMeta";
import { NotificationsService } from "angular2-notifications/dist";
import { HttpErrorResponse, HttpResponse } from "@angular/common/http";

@Component({
  selector: "app-rfq-print",
  templateUrl: "./rfq-print.component.html",
  styleUrls: ["./rfq-print.component.css"]
})
export class RfqPrintComponent implements OnInit {
  message = " Loading document";
  currentItem: BidDetail = new BidDetail();
  printDoc = new Array<Attachment>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private attachmentService: AttachmentService,
    private notificationsService: NotificationsService
  ) {}

  ngOnInit() {
    this.initComponent();
  }
  initComponent() {
    this.route.data.subscribe(
      (data: { bid: HttpResponse<BidDetail> }) => {
        // data is now an instance of type ItemsResponse, so you can do this:
        this.currentItem = data.bid.body;
        this.openForPrint();
      },
      error => {
        if (
          error instanceof HttpErrorResponse &&
          error.status === HttpStatusCode.badRequest
        ) {
          const message = error.error.error || error.error.message;
          this.notificationsService.error(message);
        }
        this.router.navigate([RedirectUrl.rfqs]);
        this.message = "";
      }
    );
  }

  private fetchPrintdocMeta(): MetaPostModel {
    const postmeta = new MetaPostModel();
    postmeta.metaTypes = new Array<AttachmentMetaType>();
    postmeta.metaTags = new Array<Meta>();
    for (const entry of this.currentItem.metaTagTypeList) {
      if (entry.docType === "RFQPrintForm") {
        postmeta.metaTypes.push(entry);
      }
    }
    for (const entry of this.currentItem.metaTags) {
      if (entry.docType === "RFQPrintForm") {
        postmeta.metaTags.push(entry);
      }
    }
    return postmeta;
  }

  private openForPrint() {
    const postMeta = this.fetchPrintdocMeta();
    const value = this.currentItem.attachments.find(
      x => x.docType === "RFQPrintForm"
    );
    if (value) {
      this.attachmentService.downloadAttachment(postMeta).subscribe(
        resp => {
          this.message = "";
          const blob = new Blob([resp.content], { type: resp.content.type });
          const fileName = "RFQPrintForm";
          const url = window.URL.createObjectURL(blob);
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, fileName);
          } else {
            const objectUrl = URL.createObjectURL(blob);
            window.location.href = url;
          }
        },
        err => {
          if (
            err instanceof HttpErrorResponse &&
            err.status === HttpStatusCode.badRequest
          ) {
            const message = err.error.error || err.error.message;
            this.notificationsService.error(message);
          }
        }
      );
    }
  }
}
