import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './core/guards/auth.guard';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { NoAuthGuard } from './core/guards/noAuth.guard';
import { SupplierGuidComponent } from './supplier-guid/supplier-guid.component';
import { RfqPrintComponent } from './rfq-print/rfq-print.component';
import { BidResolve } from './main/rfq/bid-resolve.resolve';
import { GuideViewComponent } from './guide-view/guide-view.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'vp',
    pathMatch: 'full'
  },
  {
    path: 'vp',
    loadChildren: './main/main.module#MainModule',
    canActivate: [AuthGuard],
    data: {
      breadcrumb: 'Home'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [NoAuthGuard],
    data: {
      breadcrumb: 'login'
    }
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent,
    canActivate: [AuthGuard],
    data: {
      breadcrumb: 'Reset-Password'
    }
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
    canActivate: [NoAuthGuard],
    data: {
      breadcrumb: 'Forgot Password'
    }
  },
  {
    path: 'register',
    loadChildren: './register/register.module#RegisterModule',
    canActivate: [NoAuthGuard],
    data: {
      breadcrumb: 'Register',
    }
  },
  {
    path: 'supplier-guide',
    component: SupplierGuidComponent,
    data: {
      breadcrumb: 'Supplier Guide'
    }
  },
  {
    path: 'print',
    component: RfqPrintComponent,
    data: {
      breadcrumb: 'print'
    },
    resolve: {
      bid: BidResolve
    }
  },
  {
    path: 'guide-view',
    component: GuideViewComponent,
    data: {
      breadcrumb: 'Guide View'
    },
  },
  {
    path: '**',
    redirectTo: ''
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }


