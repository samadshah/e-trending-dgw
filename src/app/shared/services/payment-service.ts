import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';

import { environment } from '../../../environments/environment';

@Injectable()
export class PaymentService {
  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath2: string;

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath2 = '/api';
  }


  paymentOnline(payload): Observable<any> {
    return this.http.post<any>(environment.base_api_url + this.servicePath2 + '/PaymentOnline' , payload);
  }

  paymentDGW(payload): Observable<any> {
    return this.http.post<any>(environment.base_api_url + this.servicePath2 + '/PaymentDGW', payload);
  }

}
