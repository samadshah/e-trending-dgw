import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';

import { environment } from '../../../environments/environment';
// import { Attachment } from '../../core/models/AttachmentMeta';

@Injectable()
export class AttachmentService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;

  private attachmentApiData = {"fileSize":2048,"fileTypes":"xls,xlsx,doc,docx,jpg,jpeg,png,gif,pdf","invoiceFileTypes":"jpg,jpeg,png,gif,pdf","disallowedCharacters":"#|$|%|&|(|)|*|+|-|/|^|_|\\\\|,|.","disallowedCharactersError":"File cannot contain following special character(s) #,$,%,&,(,),*,+,-,.,/,^,_,\\"}


  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/attachments';
    this.servicePath2 = '/api';
  }

  downloadAttachment(docId) {
    return this.http.post(environment.base_api_url + this.servicePath2 + '/vendorregistration/downloaddoc',
      docId,
      {
        responseType: 'blob',
        observe: 'response'
      })
      .map(res => ({
        content: res.body,
        type: res.type
      }));
  }

  deleteAttachment(docId) {
    return this.http.post(environment.base_api_url + this.servicePath2 + '/vendorregistration/deletedoc', docId);
  }

  get(rfqId): Observable<any> {
    return this.http.get<any>(environment.base_api_url + this.servicePath2 + '/RFQs/DocInfo?RfqID=' + rfqId);
  }

  getConfig(){
    return this.attachmentApiData;
  }

  // downloadRFQAttachment(meta, isTemporary = false) {
  //   return this.http.post(environment.base_api_url + this.servicePath + '/download-rfq-doc?isTemporary=' + isTemporary,
  //     meta,
  //     {
  //       responseType: 'blob',
  //       observe: 'response'
  //     })
  //     .map(res => ({
  //       content: res.body,
  //       type: res.type
  //     }));
  // }

  downloadGuideAttachment(type) {
    let payload = {"_meta" : type};
    return this.http.post(environment.base_api_url + this.servicePath2 + '/vendorregistration/downloadSupplierGuide',
      payload,
      {
        responseType: 'blob',
        observe: 'response'
      })
      .map(res => ({
        content: res.body,
        type: res.type
      }));
  }
}
