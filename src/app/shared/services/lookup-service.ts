import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';
import { environment } from '../../../environments/environment';
import { Lookup } from '../../core/models/lookup';
import { VendorProfileLookup } from '../../main/vendor-profile/models/vendor-profile-lookup';
import { RegisterationLookup } from '../../register/models/registeration.model';

@Injectable()
export class LookupService {
  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/lookups';
    this.servicePath2 = '/api';
  }

  getVendorProfileLookups(): Observable<VendorProfileLookup> {
    return this.http.get<VendorProfileLookup>(environment.base_api_url + this.servicePath2 + '/LookUp/VendorProfile');
  }

  VPCountryDependent(countryId): Observable<VendorProfileLookup> {
    return this.http.get<VendorProfileLookup>(environment.base_api_url + this.servicePath2 + '/vendorlookup/city/' + countryId);
  }

  registerationLookup(): Observable<RegisterationLookup> {
    return this.http.get<RegisterationLookup>(environment.base_api_url + this.servicePath2 + '/vendorlookup');
  }

  VPLocationDependent(location): Observable<Array<Lookup>> {
    return this.http.get<Array<Lookup>>(environment.base_api_url + this.servicePath2 + '/vendorlookup/organizationsector/' + location);
  }

  vatTypes(): Observable<Array<Lookup>> {
    return this.http.get<Array<Lookup>>(environment.base_api_url + this.servicePath2 + '/LookUp/VatType');
  }

  getIssuingEmirates(): Observable<any> {
    return this.http.get<any>(environment.base_api_url + this.servicePath2 + '/LookUp/GetLicenseissuingEmirates');
  }

  getRegistrationTypes(): Observable<any> {
    return this.http.get<any>(environment.base_api_url + this.servicePath2 + '/LookUp/GetResgistrationTypes');
  }

  //!!
  validateTradeLicenseNumber(payload): Observable<any> {
    return this.http.post<any>(environment.base_api_url + this.servicePath2 + '/VerifyTradeLicenseNumber', payload);
  }
}
