import { Component, OnInit, AfterViewInit, ElementRef, Input, Output, EventEmitter, SimpleChanges, OnChanges, HostListener } from '@angular/core';
import 'intl-tel-input';
import { CountriesService } from '../../core/services/countries.service'
import { FieldLength } from '../../core/configuration/config';

@Component({
  selector: 'app-ngx-phone',
  templateUrl: './ngx-phone.component.html',
  styleUrls: ['./ngx-phone.component.css']
})
export class NgxPhoneComponent implements OnInit, AfterViewInit, OnChanges {

  constructor(private ele: ElementRef,
    private countryService: CountriesService) { }

  @Input() options = { 'separateDialCode': true, 'excludeCountries': ['il'] };
  @Input() selectedValue;
  @Output() selectedValueChange: EventEmitter<String> = new EventEmitter<String>();
  @Input() countryCode;
  @Output() countryCodeChange: EventEmitter<String> = new EventEmitter<String>();
  // @Input() defaultCountry = 'ae';
  @Input() defaultCountry;
  @Input() placeholder = 'phone';
  @Input() disableCheck = false;

  @Input() clearValidation: boolean;
  @Output() clearValidationChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.countryCode && !changes.countryCode.firstChange) {
      const element: any = $(this.ele.nativeElement);
      //if(this.countryCode !== changes.countryCode.currentValue) {
      const ctry = this.countryService.countries.filter(x => x.dialCode === this.countryCode);
      if (ctry.length > 0) {
        element.intlTelInput('setCountry', ctry[0].iso2);
      } else {
        // this.countryCode = 971;
        this.countryCode = 1;
        this.selectedValue = '';
        // element.intlTelInput('setCountry', this.defaultCountry);
        element.intlTelInput('setCountry', 'us');
      }
      //}
    }
  }

  ngAfterViewInit(): void {
    const element: any = $(this.ele.nativeElement);
    element.intlTelInput(this.options);
    // element.intlTelInput('setCountry', this.defaultCountry);
    element.intlTelInput('setCountry', 'us');
    element.intlTelInput('setPlaceholderNumberType', 'FIXED_LINE');

    element.on('countrychange', (e, countryData) => {
      this.countryChange(countryData);
    });
    setTimeout(() => {
      // this.countryCodeChange.emit('971');
      this.countryCodeChange.emit('1');
    }, 0);


    $(this.ele.nativeElement).on('paste', () => {
      setTimeout(() => {
        let _value = this.selectedValue;
        if (_value) {
          _value = _value.match(/[0-9]+/gi);
          if (_value != null) {
            if (_value[0] != null) {
              _value = _value[0];
            } else {
              _value = '';
            }
          } else {
            _value = '';
          }

          if (_value == undefined || _value == null) {
            _value = '';
          }
          this.selectedValue = _value;
          this.selectedValueChange.emit(this.selectedValue);
        }
      }, 0);

    });
  }

  public countryChange(data) {
    this.countryCodeChange.emit(data.dialCode);
  }

  public validationChange() {
    this.clearValidationChange.emit(false);
  }

  public verifyRestrication(e) {
        if ([46, 8, 9, 27, 13, 110, 16].indexOf(e.keyCode) !== -1 ||
          // Allow: Ctrl+A
          (e.keyCode == 65 && e.ctrlKey === true) ||
          // Allow: Ctrl+C
          (e.keyCode == 67 && e.ctrlKey === true) ||
          // Allow: Ctrl+V
          (e.keyCode == 86 && e.ctrlKey === true) ||
          // Allow: Ctrl+X
          (e.keyCode == 88 && e.ctrlKey === true) ||
          // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
              e.preventDefault();
          }
  }
  // @HostListener('paste', ['$event']) blockPaste(e: any) {
  //   // e.preventDefault();
  //   setTimeout(() => {
  //     let value = this.ele.nativeElement.value;
  //   }, 400);
  // }
}
