import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MyDatePickerModule } from 'mydatepicker';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormsModule } from '@angular/forms';
/** Polyfill
 * Need to import at least one locale-data with intl.
 */
import 'intl/locale-data/jsonp/en';

// components
import { BreadcrumComponent } from './components/breadcrum/breadcrum.component';
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';
import { NotificationComponent } from './components/notification/notification.component';
import { NgxCustomUploadComponent } from './components/ngx-custom-upload/ngx-custom-upload.component';
import { AttachmentService } from './services/attachment-service';
import { NgSysGridComponent } from './components/ng-sys-grid/ng-sys-grid.component';
import { CustomLoaderComponent } from './components/custom-loader/custom-loader.component';
import { LoaderService } from './components/custom-loader/custom-loader.service';
import { LookupService } from './services/lookup-service';
// import { NgxQuestionareComponent } from './components/ngx-questionare/ngx-questionare.component';
import { NgxBidUploadComponent } from './components/ngx-bid-upload/ngx-bid-upload.component';
import { ClickOutsideModule } from 'ng-click-outside';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPhoneComponent } from './ngx-phone/ngx-phone.component';
import { NumberMaxLengthDirective } from './directives/number-max-length.directive';
// import { SearchGridComponent } from './components/global-search/search-grid/search-grid.component';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';
import { CustomRequiredDirective } from './directives/custom-required.directive';
//!
import { NoWhitespaceDirective } from './directives/noWhiteSpaceDirective';

import { SelectDropDownModule } from 'ngx-select-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MyDatePickerModule,
    AngularMultiSelectModule,
    TooltipModule.forRoot(),
    ClickOutsideModule,
    Ng2SearchPipeModule,
    FormsModule,
    TextMaskModule,
    NgbModule,
    SelectDropDownModule,
    NgSelectModule
  ],
  declarations: [
    // components
    LoadingIndicatorComponent,
    NotificationComponent,
    NgxCustomUploadComponent,
    NgSysGridComponent,
    CustomLoaderComponent,
    BreadcrumComponent,
    // NgxQuestionareComponent,
    NgxBidUploadComponent,
    NgxPhoneComponent,
    NumberMaxLengthDirective,
    // SearchGridComponent,
    SafeHtmlPipe,
    CustomRequiredDirective,
    //!!
    NoWhitespaceDirective
  ],
  providers: [
    AttachmentService,
    LoaderService,
    LookupService,
  ],
  exports: [
    CommonModule,
    // components
    BreadcrumComponent,
    LoadingIndicatorComponent,
    NotificationComponent,
    NgxCustomUploadComponent,
    NgSysGridComponent,
    CustomLoaderComponent,
    MyDatePickerModule,
    AngularMultiSelectModule,
    NgxBidUploadComponent,
    // NgxQuestionareComponent,
    TooltipModule,
    ClickOutsideModule,
    Ng2SearchPipeModule,
    FormsModule,
    NgxPhoneComponent,
    NumberMaxLengthDirective,
    // SearchGridComponent,
    SafeHtmlPipe,
    TextMaskModule,
    CustomRequiredDirective,
    SelectDropDownModule,
    NgSelectModule,
    //!!
    NoWhitespaceDirective,
  ]

})

export class SharedModule { }
