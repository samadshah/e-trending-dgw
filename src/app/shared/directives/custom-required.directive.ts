import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, ValidatorFn, Validator, FormControl } from '@angular/forms';


// validation function
function validateCustomDropdownFactory(): ValidatorFn {
  return (c: AbstractControl) => {

    const isValid = c.value;

    if (isValid) {
      return null;
    } else {
      return {
        dropdown: {
          valid: false
        }
      };
    }

  }
}

@Directive({
  selector: '[appCustomRequired]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: CustomRequiredDirective, multi: true }
  ]
})
export class CustomRequiredDirective implements Validator {

  validator: ValidatorFn;

  constructor() {
    this.validator = validateCustomDropdownFactory();
  }

  validate(c: FormControl) {
    return this.validator(c);
  }

}
