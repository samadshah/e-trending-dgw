import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';


@Directive({
  selector: '[appNumberMaxLength]',
  providers: [{provide: NG_VALIDATORS, useExisting: NumberMaxLengthDirective, multi: true}]
})
export class NumberMaxLengthDirective implements Validator {

  @Input() appNumberMaxLength: number;

    constructor() { }

    validate(control: AbstractControl): ValidationErrors {
        return control.value > this.appNumberMaxLength ? { maxNumber: { valid: false } } : null;
    }
}
