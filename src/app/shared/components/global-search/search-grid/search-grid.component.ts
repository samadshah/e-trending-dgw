import { HttpErrorResponse } from '@angular/common/http';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { Logger } from 'angular2-logger/core';
import { NotificationsService } from 'angular2-notifications/dist';
import * as Moment from 'moment';
import { IMyDpOptions } from 'mydatepicker';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { AppSettingService } from '../../../../core/services/app-setting.service';
import {
  DataType,
  GridParameters,
  HttpStatusCode,
  MessageText,
  RFQBidType,
  YesNo,
} from '../../../../core/configuration/config';
import { Utils } from '../../../../core/Helpers/utils';
import { Lookup } from '../../../../core/models/lookup';
import { PageQuery, PagingResponse } from '../../../../core/models/page';
import { SearchSharingService, SearchState } from '../../../../core/services/search-sharing.service';
import { UtiltiyService } from '../../../../core/services/utility';
import { KeyedCollection } from '../../ng-sys-grid/models/keyValue.model';
import { SearchGridServiceService } from './search-grid-service.service';

export class Item {
  constructor(name: string, lookup: Array<Lookup>, isLookup: boolean, data?: Object) {
    this.name = name;
    this.data = data;
    this.lookup = lookup;
    this.isLookup = isLookup;
  }
  name: string;
  data: Object;
  isLookup: boolean;
  lookup: Array<Lookup>;
  dropdowToggler: string;
  currentSelected: string;
}

@Component({
  selector: 'app-search-grid',
  templateUrl: './search-grid.component.html',
  styleUrls: ['./search-grid.component.css'],
  providers: [SearchGridServiceService]
})

export class SearchGridComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('filterBtn')
  filterButton: any;
  platformLocation: any;
  private subscription: Subscription;

  // rfqlist input methods
  @Output() bidRecall = new EventEmitter<any>();
  @Output() bid = new EventEmitter<any>();
  @Output() attachment = new EventEmitter<any>();
  @Output() viewDetail = new EventEmitter<any>();
  @Output() popUp = new EventEmitter<any>();
  @Output() loadTooltip = new EventEmitter<any>();

  @Output() edit = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();

  // input parameters
  @Input() hideFilter = false;
  @Input() url: boolean;
  @Input() appbase: boolean;
  @Input() gridType: any;
  @Input() columnsDefination: Array<any>;
  @Input() inputColumns: Array<any>;
  @Input() title: Array<any>;
  @Input() addUrl: any;
  datatableProcessing = false;
  public isBaseFilterAllowed = true;
  public tableWidgetG: any;
  private authToken: string;
  private body = 'body';
  columns: Array<any> = [];
  columnsDrpDown: Item[] = [];
  sortFilters: Array<any> = [];
  selectedColumn: Item;
  defaultSelectText = 'Select column';
  filterControls: Array<any> = [];
  serverColumns = new KeyedCollection<string>();
  tableId = '#list-table';
  pageQueryModel: PageQuery = new PageQuery();
  gridParams = GridParameters;
  customPage = [0];
  pageValue = 1;
  baseParams: any;
  readParamSubscription: any;
  filterValidation: string;
  filterDropdownToggle = 'none';
  searchTerm = '';
  canGo: Subject<boolean> = new Subject<boolean>();

  private gridData: any;

  public myDatePickerOptions: IMyDpOptions = {};

  constructor(private gridService: SearchGridServiceService,
    private oauthService: OAuthService,
    private router: Router,
    private route: ActivatedRoute,
    private notificationsService: NotificationsService,
    private utiltiy: UtiltiyService,
    private logger: Logger,
    private _renderer: Renderer,
    private searchSharingService: SearchSharingService,
    private appSettingSrvc: AppSettingService) {

    this.myDatePickerOptions = {
      dateFormat: this.appSettingSrvc.settings.datePicker.toString(),
      showClearDateBtn: false,
      alignSelectorRight: true,
      allowSelectionOnlyInCurrentMonth: false,
    }
  }

  ngOnInit() {
    this.readParamSubscription = this.route
      .queryParams
      .subscribe(params => {
        this.logger.debug('ng-sys-grid >> ngonInit >> this.route.queryParams.subscribe');
        this.baseParams = params || null;
        this.searchTerm = this.baseParams['term'] || '';
      });

    this.subscription = this.searchSharingService.searchState
      .subscribe((state: SearchState) => {
        if (this.searchTerm.toLowerCase() !== state.searchText.toLowerCase()) {
          this.searchTerm = state.searchText;
          this.reloadGrid();
        }
      });
  }

  ngAfterViewInit() {
    let pageNo = this.appSettingSrvc.settings.pageNo;
    const pageSize = this.appSettingSrvc.settings.pageSize;
    const order = [];
    pageNo = (pageNo - 1) * pageSize;
    this.initDatatable(pageNo, pageSize, order);
  }

  private initDatatable(pageNoParam, pageSizeParam, orderParam): void {

    const dtId: any = $(this.tableId);
    const tableWidget = dtId
      .DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        deferRender: true,
        paging: true,
        searching: false,
        displayStart: pageNoParam,
        pageLength: pageSizeParam,
        sorting: orderParam,
        autoWidth: false,
        language: {
          emptyTable: MessageText.gridEmpty,
          info: '_START_ - _END_ of _TOTAL_ records',
          processing: '',
        },
        sDom: '<"clear">t<"F"p><"F"ilr>',
        columnDefs: this.columnsDefination,
        columns: this.inputColumns,
        fnServerData: (sSource, aoData, fnCallback, osettings) => {
          this.serverCall(sSource, aoData, fnCallback, osettings);
        },
        rowCallback: (row: Node, data: any | Object, index: number) => {
          $('.actionView', row).unbind('click');
          $('.actionView', row).bind('click', (e) => {
            if (e.currentTarget.attributes['data-id'] && e.currentTarget.attributes['data-url']) {
              e.preventDefault();
              e.stopPropagation();
              const id = e.currentTarget.attributes['data-id'].nodeValue;
              const url = e.currentTarget.attributes['data-url'].nodeValue;
              let id2 = '';
              if (e.currentTarget.attributes['data-id2']) {
                id2 = e.currentTarget.attributes['data-id2'].nodeValue;
                this.router.navigate([url], { queryParams: { id1: id, id2: id2 } });
              } else {
                this.router.navigate([url], { queryParams: { id: id } });
              }

            }
          });
          return row;
        }
      });

    dtId.on('click', '#ViewBtn', (e) => {
      e.preventDefault();
      const result = e.currentTarget.attributes['data-id'].nodeValue;
      const prop = e.currentTarget.attributes['data-prop'].nodeValue;
      const row = this.findDataById(prop, result);
      this.viewDetail.emit(row);
    });

    tableWidget.on('draw', function () {
      if (tableWidget.page.info().pages < 2) {
        $(tableWidget.table().container())
          .find('a.previous, a.next, #list-table_paginate span')
          .css('display', tableWidget.page.info().pages < 2 ?
            'none' :
            ''
          );
      } else {
        $(tableWidget.table().container())
          .find('a.previous')
          .css('display', tableWidget.page.info().page === 0 ?
            'none' :
            ''
          );

        $(tableWidget.table().container())
          .find('a.next')
          .css('display', tableWidget.page.info().page === (tableWidget.page.info().pages - 1) ?
            'none' :
            ''
          );
      }
    });
  }

  public serverCall(sSource, aoData, fnCallback, osettings) {
    const isBaseFilterAllowed = this.isBaseFilterAllowed;
    this.logger.debug('serverCall');
    const draw = this.fetchServerParams(this.gridParams.draw, aoData);
    const start = this.fetchServerParams(this.gridParams.start, aoData);
    const size = this.fetchServerParams(this.gridParams.length, aoData);
    this.pageQueryModel.pageNo = (start.value / size.value) + 1;
    this.pageQueryModel.pageSize = size.value;
    this.pageQueryModel.draw = draw.value;
    this.pageQueryModel.searchTerm = this.searchTerm;
    if (this.datatableProcessing === true) {
      return false;
    }
    this.datatableProcessing = true;
    this.gridService.getGridData(this.pageQueryModel, this.url)
      .subscribe((data) => {
        this.updateURL();
        if (draw) {
          if (this.gridData) {
            this.gridData.draw = data.draw;
          }
          // if (!Utils.isEqual(data, this.gridData)) {
          this.gridData = data;
          const response = new PagingResponse();
          response.data = data.data; // this.getGridList(data.data);
          response.draw = data.draw;
          response.recordsFiltered = data.recordsFiltered;
          response.recordsTotal = data.recordsTotal;
          const pages = Math.ceil(response.recordsTotal / this.pageQueryModel.pageSize);
          this.customPage = Array(pages).fill(1).map((x, i) => i + 1);
          this.pageValue = this.pageQueryModel.pageNo;
          fnCallback(response);
          this.creatTooltip();
          this.logger.debug('Grid data UI refreshed.');
          // } else {
          //   this.logger.debug('Grid data is same. skipping UI refresh.');
          // }
        }
        this.isBaseFilterAllowed = false;
        this.datatableProcessing = false;
      },
        err => {
          if (err instanceof HttpErrorResponse
            && (err.status === HttpStatusCode.badRequest)) {
            const message = this.utiltiy.errorHandle(err);
            this.notificationsService.error(message);
          }
          this.datatableProcessing = false;
        });
  }

  private updateURL() {
    const url = this.appbase + '?term=' + encodeURIComponent(this.searchTerm);
    window.history.replaceState({ url: url }, '', url);
  }

  public enums() {
    return YesNo;
  }

  public types() {
    return DataType;
  }

  public bidType() {
    return RFQBidType;
  }

  public search() {
    this.filterValidation = '';
    if (this.filterControls.length === 1) {
      if (!this.filterControls[0].value) {
        this.filterValidation = MessageText.invalidFilters;
        return false;
      }
    }
    this.reloadGrid();
  }

  public refreshGrid() {
    this.pageQueryModel.filters = '';
    this.pageQueryModel.sort = '';
    this.filterValidation = '';

    const item = this.columnsDrpDown[0];
    this.filterControls = [];
    this.filterControls.push({
      name: item, value: '',
      filterColumnDropdowToggler: 'none',
      dropdowToggler: 'none',
      currentSelected: '',
      searchTextColumn: '',
      searchTextFilter: ''
    });

    const dtId: any = $(this.tableId);
    dtId.DataTable().order([]).ajax.reload();
  }

  public reloadGrid() {
    this.pageQueryModel.filters = '';
    this.pageQueryModel.sort = '';
    const dtId: any = $(this.tableId);
    dtId.DataTable().ajax.reload();
  }

  public reloadGridWithOrWithoutFilters() {
    this.pageQueryModel.filters = '';
    this.pageQueryModel.sort = '';
    this.filterValidationFailed();
    const dtId: any = $(this.tableId);
    dtId.DataTable().ajax.reload(
      null, false);
  }

  public filterValidationFailed() {
    if (this.filterControls.length === 1) {
      if (!this.filterControls[0].name && !this.filterControls[0].value) {
        return false;
      }
    }
    for (let i = 0; i < this.filterControls.length; i++) {
      if (!this.filterControls[i].name || !this.filterControls[i].value) {
        return true;
      }
    }
    this.updatePageQueryModel();
    return false;
  }

  public updatePageQueryModel() {
    let filter = '';
    for (let i = 0; i < this.filterControls.length; i++) {
      if (this.filterControls[i].value.hasOwnProperty('jsdate')) {
        const axdate = Moment(this.filterControls[i].value['jsdate']).format(this.appSettingSrvc.settings.dateFormateForAx);
        filter = filter + '' + this.filterControls[i].name.data.column + '='
          + axdate + ',';
      } else {
        filter = filter + '' + this.filterControls[i].name.data.column + '=' + this.filterControls[i].value + ',';
      }
    }
    this.pageQueryModel.filters = filter;
  }

  public updateSortFilter() {
    let sortFiler = '';
    for (let i = 0; i < this.sortFilters.length; i++) {
      sortFiler = sortFiler + '' + this.serverColumns.Item(this.sortFilters[i].prop) +
        '=' + this.getsortTerm(this.sortFilters[i].dir) + ',';
    }
    this.pageQueryModel.sort = sortFiler;
  }

  // helper functions
  private pascalToCamelCase(item) {
    const result = '';
    this.serverColumns.Add(item.toLowerCase(), item);
    return result;
  }

  private splitValue(value, index) {
    return value.substring(0, index).toLowerCase() + value.substring(index);
  }

  private getsortTerm(term) {
    return term === 'asc' ? 0 : 1;
  }

  public fetchServerParams(prop, data) {
    for (let i = 0; i < data.length; i++) {
      if (data[i].name === prop) {
        return data[i];
      }
    }
  }

  public fillSort(data) {
    const sortInfo = this.fetchServerParams(this.gridParams.order, data);
    if (sortInfo.value.length > 0) {
      const sortColumnIndex = sortInfo.value[0].column;
      const sortValue = sortInfo.value[0].dir;

      const sortColumn = this.fetchServerParams(this.gridParams.columns, data).value[sortColumnIndex];
      if (this.serverColumns.ContainsKey(sortColumn.data.toLowerCase())) {
        this.pageQueryModel.sort = this.serverColumns.Item(sortColumn.data.toLowerCase()) + '=' + this.getsortTerm(sortValue);
      }
    }
  }

  public reloadByPage() {
    const dtId: any = $(this.tableId);
    dtId.DataTable().page(this.pageValue - 1).draw(false);
  }

  public findDataById(prop, value) {
    const dtId: any = $(this.tableId);
    const data = dtId.DataTable().data();
    for (let i = 0; i < data.length; i++) {
      if (data[i][prop] === value) {
        return data[i];
      }
    }
  }

  public findDataByIndex(index) {
    const dtId: any = $(this.tableId);
    const data = dtId.DataTable().row(index).data();
    return data;
  }

  ngOnDestroy() {
    const dtId: any = $(this.tableId);
    dtId.DataTable().destroy();
    this.readParamSubscription.unsubscribe();
    this.subscription.unsubscribe();
  }

  /**
 * loadToolTip
 */
  public creatTooltip() {
    this.loadTooltip.emit();
  }

  private updateSortTerm(term) {
    return term === 0 ? 'asc' : 'desc';
  }
}


