// import 'rxjs/add/operator/filter';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

import { VendorService } from '../../../core/services/vendor/vendor.service';


@Component({
  selector: 'app-breadcrum',
  templateUrl: './breadcrum.component.html',
  styleUrls: ['./breadcrum.component.css']
})
export class BreadcrumComponent implements OnInit {

  public breadcrumbs: Array<any>;
  public vendor: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private vendorService: VendorService
  ) {
  }

  ngOnInit() {
    this.vendor = this.vendorService.FetchFromLocalStorage();
    const ROUTE_DATA_BREADCRUMB = 'breadcrum';
    this.getBreadCrumb();
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(event => {
        this.getBreadCrumb();
      });
  }

  private getBreadCrumb() {
    this.breadcrumbs = [];
    let currentRoute = this.route.root, url = '';
    do {
      const childrenRoutes = currentRoute.children;
      currentRoute = null;
      childrenRoutes.forEach(route => {
        if (route.outlet === 'primary') {
          const routeSnapshot = route.snapshot;
          url += '/' + routeSnapshot.url.map(segment => segment.path).join('/');
          const param = route.snapshot.paramMap.get('id');
          let label = '';
          if (param) {
            label = route.snapshot.data.breadcrumb + ': ' + decodeURIComponent(param);
          } else {
            label = route.snapshot.data.breadcrumb;
          }
          this.breadcrumbs.push({ label: label, url: url });
          currentRoute = route;
        }
      });
    } while (currentRoute);

    const output = [];
    for (let i = 0; i < this.breadcrumbs.length; i++) {
      if (this.breadcrumbs[i].label === 'Home') {
        continue;
      }
      let flag = false;
      for (let j = 0; j < output.length; j++) {
        if (output[j].label === this.breadcrumbs[i].label) {
          flag = true;
        }
      }
      if (!flag) {
        output.push(this.breadcrumbs[i]);
      }
    }
    this.breadcrumbs = output;
  }
}

