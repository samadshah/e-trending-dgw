import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { NotificationsService } from 'angular2-notifications/dist';
import { FileModel } from '../ngx-custom-upload/models/file.model';
import { NgxFileUploadService } from '../ngx-custom-upload/ngx-file-upload/ngx-file-upload.service';

enum FileType {
  error = 1,
  file = 2,
}

@Component({
  selector: 'app-ngx-bid-upload',
  templateUrl: './ngx-bid-upload.component.html',
  styleUrls: ['./ngx-bid-upload.component.css'],
  providers: [NgxFileUploadService]
})
export class NgxBidUploadComponent implements OnInit {

  filesData: Array<FileModel> = [];
  progress = 0;
  isCompleted = false;
  dragAreaClass = 'dragarea';
  hide = false;
  uploader: any;
  @Input() projectId = 0;
  @Input() sectionId = 0;
  @Input() fileExt = 'jpg, jpeg, gif, png, docx';
  @Input() maxFiles = 5;
  @Input() maxSize = 1; // 5MB
  @Input() overAllSize = 10; // 5MB
  @Output() uploadStatus = new EventEmitter();
  @Output() documentDeleteStatus = new EventEmitter();
  @Input() charactersNotAllowedFileName = '#,$,%,&,(,),*,+,-,.,/,^,\,_';
  @Input() charactersNotAllowedMessage = 'Special characters are not allowed in file Name';
  meta: any;
  constructor(private fileService: NgxFileUploadService, private notificationsService: NotificationsService) { }

  ngOnInit() {
  }

  onFileChange(event) {
    const files = event.target.files;
    this.saveFiles(files);
  }

  @HostListener('dragover', ['$event']) onDragOver(event) {
    this.dragAreaClass = 'droparea';
    event.preventDefault();
  }

  @HostListener('dragenter', ['$event']) onDragEnter(event) {
    this.dragAreaClass = 'droparea';
    event.preventDefault();
  }

  @HostListener('dragend', ['$event']) onDragEnd(event) {
    this.dragAreaClass = 'dragarea';
    event.preventDefault();
  }

  @HostListener('dragleave', ['$event']) onDragLeave(event) {
    this.dragAreaClass = 'dragarea';
    event.preventDefault();
  }

  @HostListener('drop', ['$event']) onDrop(event) {
    this.dragAreaClass = 'dragarea';
    event.preventDefault();
    event.stopPropagation();
    const files = event.dataTransfer.files;
    this.saveFiles(files);
  }

  saveFiles(files) {
    this.isCompleted = false;
    this.progress = 0;
    if (!this.meta) {
      this.notificationsService.error('select meta');
      return;
    }

    if (this.errorExist()) {
      this.notificationsService.error('Resovle attachment error by removing from queue');
      return;
    }

    // Validate file size and allowed extensions
    if (files.length > 0 && (!this.isValidFiles(files))) {
      return;
    }
    if (files.length > 0) {
      for (let j = 0; j < files.length; j++) {
        const fileDataOnly: Array<FileModel> = this.filesData.filter(x => x.type === FileType.file);
        const exist: boolean = fileDataOnly.some(x => x.file.name === files[j].name && x.file.name === files[j].size);
        if (exist) {
          this.notificationsService.error('already added ' + files[j].name);
        } else {
          this.filesData.push(new FileModel(files[j], FileType.file));
        }
      }
      this.uploadFiles();
    }
  }

  uploadFiles() {
    this.hide = true;
    if (this.errorExist()) {
      this.notificationsService.error('Clear Attachment errors then upload again');
      return;
    }
    const allFiles: Array<any> = this.filesData;
    const filesCount: number = this.filesData.length;
    const formData = new FormData();
    if (filesCount > 0) { // a file was selected
      for (let i = 0; i < filesCount; i++) {
        formData.append('file', allFiles[i].file);
      }
      // subscribe to observable to listen for response
      const jsonstring = JSON.stringify(this.meta);
      formData.append('data', jsonstring);
      this.uploader = this.fileService.upload(formData)
        .subscribe(
          event => {
            // Via this API, you get access to the raw event stream.
            // Look for upload progress events.
            if (event.type === HttpEventType.UploadProgress) {
              // This is an upload progress event. Compute and show the % done:
              const percentDone = Math.round(100 * event.loaded / event.total);
              this.progress = percentDone;

            } else if (event instanceof HttpResponse) {
              // File is completely uploaded!
              this.filesData = [];
              this.uploadStatus.emit(event.body);
              this.isCompleted = true;
              setTimeout(() => {
                this.hide = false;
              }, 900);
            }
          },
          error => {
            this.isCompleted = false;
            this.hide = false;
            const index = this.filesData.length - 1;
            const file = this.filesData[index];
            this.filesData[index].type = 1;
            this.filesData[index].file = error.error.message;
            this.uploadStatus.emit(error);
          });

    } else {
      this.notificationsService.error('no file attached');
    }
  }

  cancelUpload() {
    this.uploader.unsubscribe();
    this.filesData.splice(0, 1);
    this.hide = false;
  }

  private isValidFiles(files) {
    // Check Number of files
    const totalFiles: number = this.filesData.length + files.length;
    if (totalFiles > this.maxFiles) {
      this.filesData.push(new FileModel('Error: you can upload only ' + this.maxFiles + ' files', FileType.error));
      return;
    }
    this.isValidFileExtension(files);
    this.isValidFileName(files);
    return !this.errorExist();
  }

  private isValidFileExtension(files) {
    // Make array of file extensions
    const extensions = (this.fileExt.split(','))
      .map(function (x) { return x.toLocaleLowerCase().trim(); });
    for (let i = 0; i < files.length; i++) {
      // Get file extension
      const ext = files[i].name.toLowerCase().split('.').pop() || files[i].name;
      // Check the extension exists
      const exists = extensions.indexOf(ext);
      if (exists === -1) {
        // tslint:disable-next-line:max-line-length
        this.filesData.push(new FileModel('Error (Extension): ' + files[i].name + ' supported extensions are ' + this.fileExt + ' ', FileType.error));
      }
      // Check individual file size
      this.isValidFileSize(files[i]);
    }
    // Check individual file size  overall file size
    const validFiles = this.filesData.filter(x => x.type === FileType.file).map(x => x.file);
    const FilesSize = this.getSizes(validFiles) + this.getSizes(files);
    // this.isValidOverAllSize(FilesSize);
  }
  private getSizes(files) {
    let sum = 0;
    for (let i = 0; i < files.length; i++) {
      const Item: number = files[i].size / (1024 * 1000);
      sum += Math.round(Item * 100) / 100;
    }
    return sum;
  }

  private isValidFileSize(file) {
    const fileSizeinMB = file.size / (1024 * 1000);
    const size = Math.round(fileSizeinMB * 100) / 100; // convert upto 2 decimal place
    const maxFileSizeinMB = +this.maxSize / 1024;
    if (size > maxFileSizeinMB) {
      // tslint:disable-next-line:max-line-length
      this.filesData.push(new FileModel('Error (File Size): ' + file.name + ': exceed file size limit of ' + maxFileSizeinMB + 'MB ( ' + size + 'MB )', FileType.error));
    }
  }

  private isValidOverAllSize(size) {
    if (size > +this.overAllSize) {
      // tslint:disable-next-line:max-line-length
      this.filesData.push(new FileModel('Error (Over all File Size) : exceed file size limit of ' + this.overAllSize + 'MB ( ' + size + 'MB )', FileType.error));
    }
  }

  private isValidFileName(files) {
    // Make array of file extensions
    const disAllowedCharacters = this.charactersNotAllowedFileName.split('|');
    for (let i = 0; i < files.length; i++) {
      // Get file extension
      const name = files[i].name;
      // Check the extension exists
      for (let index = 0; index < disAllowedCharacters.length; index++) {
        const element = disAllowedCharacters[index];
        const exists = name.indexOf(element);
        if (exists !== -1) {
          this.filesData.push(new FileModel(this.charactersNotAllowedMessage, FileType.error));
          break;
        }
      }
    }
  }

  private errorExist() {
    return this.filesData.filter(x => x.type === FileType.error).length > 0;
  }

  public remove(index) {
    this.filesData.splice(index, 1);
  }

  get numberOfFiles() {
    return this.filesData.filter(x => x.type === FileType.file).length;
  }

  get numberOfErrors() {
    return this.filesData.filter(x => x.type === FileType.error).length;
  }

  public deleteDocument(data) {
    this.fileService.delete(data).subscribe(resp => {
      this.documentDeleteStatus.emit(true);
    });
  }
  public updateMeta(data) {
    this.meta = data;
  }

}
