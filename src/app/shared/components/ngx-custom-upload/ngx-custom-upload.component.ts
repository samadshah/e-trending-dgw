/* tslint:disable */
import { HttpErrorResponse, HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, HostListener, Input, OnInit, Output, ViewChild, AfterViewInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications/dist';
import { HttpStatusCode } from '../../../core/configuration/config';
import { FileModel, UploadEvent, UploadProgressModel } from './models/file.model';
import { NgxFileUploadService } from './ngx-file-upload/ngx-file-upload.service';

// file model
enum FileType {
    error = 1,
    file = 2,
}

@Component({
  selector: 'app-ngx-custom-upload',
  templateUrl: './ngx-custom-upload.component.html',
  styleUrls: ['./ngx-custom-upload.component.css'],
  providers: [NgxFileUploadService]
})
export class NgxCustomUploadComponent implements OnInit, AfterViewInit {

    @ViewChild('file') fileInput: any;

    uploadProgressEvents: Array<UploadProgressModel> = [];
    uploadEvents: Array<UploadEvent> = [];
    filesData: Array<FileModel> = [];
    progress = 0;
    isCompleted = false;
    dragAreaClass = 'dragarea';
    @Input() projectId = 0;
    @Input() sectionId = 0;
    @Input() fileExt = 'jpg, jpeg, gif, png, docx';
    @Input() maxFiles = 5;
    //!
    maxSize = 10236;

    @Input() overAllSize = 10; // 5MB
    @Input() viewId = '';
    @Output() uploadStatus = new EventEmitter();
    @Output() documentDeleteStatus = new EventEmitter();
    //!
    // @Input() charactersNotAllowedFileName = '#|$|%|&|(|)|*|+|-|.|/|^|\\|_|,';
    @Input() charactersNotAllowedFileName = '';

    //!
    charactersNotAllowedMessage = 'Special characters are not allowed in file Name';
    @Input() showRemoveBtn = false;
    counterType = 0;
    meta: any;
    constructor(private fileService: NgxFileUploadService, private notificationsService: NotificationsService) { }

    ngOnInit() {
    }

    onFileChange(event) {
        const files = event.target.files;
        this.saveFiles(files);
    }

    @HostListener('dragover', ['$event']) onDragOver(event) {
        this.dragAreaClass = 'droparea';
        event.preventDefault();
    }

    @HostListener('dragenter', ['$event']) onDragEnter(event) {
        this.dragAreaClass = 'droparea';
        event.preventDefault();
    }

    @HostListener('dragend', ['$event']) onDragEnd(event) {
        this.dragAreaClass = 'dragarea';
        event.preventDefault();
    }

    @HostListener('dragleave', ['$event']) onDragLeave(event) {
        this.dragAreaClass = 'dragarea';
        event.preventDefault();
    }

    @HostListener('drop', ['$event']) onDrop(event) {
        this.dragAreaClass = 'dragarea';
        event.preventDefault();
        event.stopPropagation();
        const files = event.dataTransfer.files;
        this.saveFiles(files);
    }

    saveFiles(files) {
        this.filesData = [];
        this.isCompleted = false;
        this.progress = 0;
        if (!this.meta) {
            this.notificationsService.error('select type');
           return;
        }

        if (this.errorExist()) {
           this.notificationsService.error('Resovle attachment error by removing from queue');
           return;
        }

        // Validate file size and allowed extensions
        if (files.length > 0 && (!this.isValidFiles(files))) {
            return;
        }
        if (files.length > 0) {
            for (let j = 0; j < files.length; j++) {
                const fileDataOnly: Array<FileModel> = this.filesData.filter(x => x.type === FileType.file);
                const exist: boolean = fileDataOnly.some(x => x.file.name === files[j].name && x.file.name === files[j].size);
                if (exist) {
                    this.notificationsService.error('already added ' + files[j].name);
                } else {
                    this.filesData.push(new FileModel(files[j], FileType.file));
                }
            }
            this.uploadFiles();
        }
    }

    uploadFiles() {
        if (this.errorExist()) {
            this.notificationsService.error('Clear Attachment errors then upload again');
            return;
        }
        const allFiles: Array<any> = this.filesData;
        const filesCount: number = this.filesData.length;
        const formData = new FormData();
        let fileNames = '';
        if (filesCount > 0) { // a file was selected
            for (let i = 0; i < filesCount; i++) {
                formData.append('Files', allFiles[i].file);
                fileNames = fileNames + ' ' +allFiles[i].file.name;
            }
            // subscribe to observable to listen for response
            const jsonstring = JSON.stringify(this.meta);
            formData.append('Meta', jsonstring);
            this.counterType = this.counterType + 1;
            const progress = new UploadProgressModel(0, false,false ,this.counterType, fileNames);
            this.uploadProgressEvents.push(progress);
            const eve = new UploadEvent();
            eve.type = progress.type;
            eve.event = this.serverCall(formData, progress);
            this.uploadEvents.push(eve);
        } else {
            this.notificationsService.error('no file attached');
        }
        this.fileInput.nativeElement.value = '';
    }
    private serverCall(formData, progressEvent) {
      return   this.fileService.upload(formData)
        .subscribe(
         event => {
             // Via this API, you get access to the raw event stream.
             // Look for upload progress events.
             if (event.type === HttpEventType.UploadProgress) {
                 const percentDone = Math.round(100 * event.loaded / event.total);
                 progressEvent.progress = percentDone;

             } else if (event instanceof HttpResponse) {
                 if (this.viewId) {
                    event.body['viewId'] = this.viewId;
                 }
                 this.uploadStatus.emit(event.body);
                 progressEvent.isCompleted = true;
                 const index =  this.uploadProgressEvents.indexOf(progressEvent);
                 this.uploadProgressEvents.splice(index, 1);
                 const eventToRemove = this.uploadEvents.filter(x => x.type === progressEvent.type)[0];
                 const eventIndex =  this.uploadEvents.indexOf(progressEvent);
                 this.uploadEvents.splice(eventIndex, 1);
             }
         },
         err => {
             if (err instanceof HttpErrorResponse) {
                 if (err.status === HttpStatusCode.badRequest) {
                     const message = err.error.error || err.error.message;
                     this.notificationsService.error(message);
                     progressEvent.errorMessage = message;
                 }
             }
             progressEvent.isFailed = true;
             progressEvent.progress = 0;
             this.uploadStatus.emit(false);
         });
    }
    private isValidFiles(files) {
        // Check Number of files
        const totalFiles: number = this.filesData.length + files.length;
        this.isValidFileExtension(files);
        return !this.errorExist();
    }

    private isValidFileExtension(files) {
        // Make array of file extensions
        const extensions = (this.fileExt.split(','))
            .map(function (x) { return x.toLocaleLowerCase().trim(); });
        for (let i = 0; i < files.length; i++) {
            // Get file extension
            const ext = files[i].name.toLowerCase().split('.').pop() || files[i].name;
            // Check the extension exists
            const exists = extensions.indexOf(ext);
            if (exists === -1) {
                this.filesData.push(new FileModel('Error in uploading ' + files[i].name + ' due to invalid file type. Supported file types are '  + this.fileExt.toUpperCase().split(',').join(', ') + '.', FileType.error));
            }
            // Check individual file size
            this.isValidFileSize(files[i]);
        }
         // Check individual file size  overall file size
         const validFiles = this.filesData.filter(x => x.type === FileType.file).map(x => x.file);
         const FilesSize = this.getSizes(validFiles) + this.getSizes(files);
    }
    private getSizes(files) {
        let sum = 0;
        for (let i = 0; i < files.length; i++) {
            // const Item: number = files[i].size / (1024 * 1000);
            sum += files[i].size / 1024; // Math.round(Item * 100) / 100;
        }
        return sum;
    }

    private isValidFileSize(file) {
        const fileSizeinKB = file.size / 1024; // this.formatBytes(file.size, 2); // file.size / (1024 * 1000);
        // const size = Math.round(fileSizeinMB * 100) / 100; // convert upto 2 decimal place
        if (fileSizeinKB > this.maxSize) {
            this.filesData.push(new FileModel('Error (File Size): ' + file.name + ' ( ' + (fileSizeinKB / 1024).toFixed(2) + 'MB )' + ' exceed file size limit of ' + (this.maxSize / 1024).toFixed(2) + 'MB.', FileType.error));
        }
    }

    private  errorExist() {
       return this.filesData.filter(x => x.type === FileType.error).length > 0;
    }

    public remove(index) {
        this.filesData.splice(index, 1);
    }

    get numberOfFiles() {
        return  this.filesData.filter(x => x.type === FileType.file).length;
    }

    get numberOfErrors() {
        return  this.filesData.filter(x => x.type === FileType.error).length;
    }

    public deleteDocument(data) {
        this.fileService.delete(data).subscribe(resp => {
               this.documentDeleteStatus.emit(true);
        });
    }
    public updateMeta(data) {
        this.meta = data;
    }

    public  cancelupload(type) {
        const remove =   this.uploadEvents.filter(x => x.type === type)[0];
        const result = remove.event.unsubscribe();
        const progressToRemove = this.uploadProgressEvents.filter(x => x.type === type)[0];
        const eventToRemove = this.uploadEvents.filter(x => x.type === type)[0];
        const index =  this.uploadProgressEvents.indexOf(progressToRemove);
        const eventIndex =  this.uploadEvents.indexOf(eventToRemove);
        this.uploadProgressEvents.splice(index, 1);
        this.uploadEvents.splice(eventIndex, 1);
    }

    ngAfterViewInit(): void {
     this.loadTooltip();
    }

    public loadTooltip() {
        $('[data-toggle="tooltip"]').tooltip({
          trigger: 'hover'
        });
      }
}
