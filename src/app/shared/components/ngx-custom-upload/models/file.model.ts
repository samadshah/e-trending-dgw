export class FileModel {
    constructor(public file: any,
        public type: number
       ) {
    }
}

export class UploadProgressModel {
    constructor(progress, isCompleted, isFailed, type, fileName) {
      this.progress = progress;
      this.isCompleted = isCompleted;
      this.isFailed = isFailed;
      this.type = type;
      this.fileName = fileName;
      this.errorMessage = '';
    }
    public type: number;
    public progress: number;
    public isCompleted: boolean;
    public isFailed: boolean;
    public fileName: string;
    public errorMessage: string;
}

export class UploadEvent {
    public type: number;
    public event: any;
}
