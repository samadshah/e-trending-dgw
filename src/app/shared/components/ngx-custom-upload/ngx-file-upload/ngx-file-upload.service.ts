import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { _throw } from 'rxjs/observable/throw';

import { environment } from '../../../../../environments/environment';

@Injectable()
export class NgxFileUploadService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/attachments';
    this.servicePath2 = '/api';
  }

  upload(files) {
    const headers = new HttpHeaders().set('ignoreLoadingBar', '');
    const req = new HttpRequest('POST', environment.base_api_url + this.servicePath2 + '/vendorregistration/uploaddoc', files, {
      reportProgress: true,
      headers,
      // withCredentials : false
    });
    return this.http.request(req);
  }

  delete(meta) {
    return this.http.post(environment.base_api_url + this.servicePath + '/remove-doc', meta);
  }
}
