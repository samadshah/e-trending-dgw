import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

// import * as $ from 'jquery';
// import 'datatables.net';
// import 'datatables.net-responsive';
import { Question, Option, Answer, PostQuestionare } from '../../../main/rfq/models/Questionare';
// import { formats } from '../../../../assets/scripts/config-file.js';
import { AppSettingService } from '../../../core/services/app-setting.service';
import { QuestionType } from '../../../core/configuration/config';
import * as Moment from 'moment';
import { IMyDpOptions } from 'mydatepicker';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@Component({
  selector: 'app-ngx-questionare',
  templateUrl: './ngx-questionare.component.html',
  styleUrls: ['./ngx-questionare.component.css']
})
export class NgxQuestionareComponent implements OnInit {


  @Output() FetchAnswers = new EventEmitter<any>();
  @Input() questionareType: string;
  @Input() leftThreshold = 0;
  @Input() questionareHeading = 0;

  left = '0%';
  top = '0%';
  display = 'inline';
  questionareFlag = 'none';
  questionareNoteFlag = false;
  dropdownToggle = 'none';
  searchTextColumn = '';

  private model: Object = { date: { year: 2018, month: 10, day: 9 } };

  currentQuestion: Question = new Question();
  currentIndex = 0;
  lowestIndex = 0;
  highestIndex = 0;
  questionareProgress = 0;
  questions: Array<Question> = [];
  options: Array<Option> = [];
  answers: Array<Answer> = [];
  textAnswer = '';
  hours = Array(12).fill(1).map((x, i) => i + 1);
  hour = 0;
  minute = 0;
  second = 0;
  AMPM = 'am';
  questoinareNote = '';
  questionaireRFQId = '';
  questionareError = '';

  public myDatePickerOptions: IMyDpOptions = {};

  constructor(private appSettingSrvc: AppSettingService) {
    this.myDatePickerOptions = {
        dateFormat: this.appSettingSrvc.settings.datePicker.toString(),
        showClearDateBtn: false,
        alignSelectorRight: true,
        allowSelectionOnlyInCurrentMonth: false,
    };
  }

  ngOnInit() {
    const answer = new Answer();
    answer.answer = '';
    this.currentQuestion.answer = answer;
  }

  public loadQuestionare(data) {
    this.clearQuestionare();
    this.questions = this.mapQuestionare(data);
    this.highestIndex = (this.questions.length - 1);
    this.updateBar();
    this.currentQuestion = this.questions[this.currentIndex];
  }

  public questionForward() {
    if (this.currentIndex < (this.questions.length - 1)) {
      this.currentIndex = this.currentIndex + 1;
      this.currentQuestion = this.questions[this.currentIndex];
      this.updateBar();
    }
  }

  public questionBackward() {
    if (this.currentIndex > 0) {
      this.currentIndex = this.currentIndex - 1;
      this.currentQuestion = this.questions[this.currentIndex];
      this.updateBar();
    }
  }

  public updateBar() {
    this.questionareProgress = (this.currentIndex + 1) * (100 / this.questions.length);
  }

  public updateAnswer() {
    const currQuestion = this.currentQuestion;
    const answerExist = this.answers.filter(x => x.questionId === currQuestion.questionId);
    if (answerExist.length > 0) {
      this.textAnswer = answerExist[0].answer;
    }
  }

  public updateRadioButton(event) {
    this.currentQuestion.answer.answer = event.currentTarget.id;
  }

  public submitAnswer() {
    this.questionareError = '';
    if (this.isValidQuestionare()) {
      const postModel = new PostQuestionare();
      postModel.questionnaireType = this.questionareType;
      postModel.rfqId = this.questionaireRFQId;
      postModel.answers = this.getAnswers();
      this.FetchAnswers.emit(postModel);
      //  this.bidDetailService.saveQuestionare(postModel).subscribe(data => {
      //    console.log('sumitted');
      //  },
      //  err => {
      //      this.questionareError = 'Unable to submit questionare';
      //  });
    } else {
      this.questionareError = 'Kindly answer all mandatory question marked with *';
      return false;
    }

  }

  public isValidQuestionare() {
    for (let i = 0; i < this.questions.length; i++) {
      if (this.questions[i].isMandatory === 'Yes') {
        if (this.questions[i].questionType === 'Check box') {
          for (let j = 0; j < this.questions[i].options.length; j++) {
            if (this.questions[i].options[j].selected && this.questions[i].options[j].selected === false) {
              return false;
            }
          }
        } else {
          if (this.questions[i].answer.answer) { } else { return false; }
        }
      }
    }
    return true;
  }

  public onChange(event, time = "") {
    if (time === 'hr') {
      this.currentQuestion.answer.answer = this.getFormattedNumber(event) +
        ':' + this.getFormattedNumber(this.minute);
    } else if (time === 'min') {
      this.currentQuestion.answer.answer = this.getFormattedNumber(this.hour) +
        ':' + this.getFormattedNumber(event);
    }
  }

  public getFormattedNumber(number) {
    return number < 10 ? '0' + number : number;
  }

 public getAnswers(): Array<Answer> {
   const answers = new Array<Answer>();
         for (let i = 0; i < this.questions.length; i++) {
                 if (this.questions[i].questionType === 'Check box') {
                       for (let j = 0; j < this.questions[i].options.length; j++) {
                           if (this.questions[i].options[j].selected === true) {
                             const answer = new Answer();
                             answer.answer = this.questions[i].options[j].text;
                             answer.questionId = this.questions[i].questionId;
                             answers.push(answer);
                           }
                       }
                 } else {
                   if (this.questions[i].answer.answer) {
                      if (this.questions[i].questionType === 'Date') {
                          const answer = new Answer();
                          answer.answer = Moment(this.questions[i].answer.answer['jsdate']).format(this.appSettingSrvc.settings.dateFormateForAx);
                          answer.questionId = this.questions[i].questionId;
                          answers.push(answer);
                      } else if (this.questions[i].questionType === 'Time') {
                          const answer = new Answer();
                          const fetchAnswer = this.getFormattedAnswer(this.questions[i].answer.answer);
                          answer.answer = Moment(fetchAnswer, this.appSettingSrvc.settings.TwentyFourHrTimeFormate)
                                          .format(this.appSettingSrvc.settings.TwelveHrTimeFormate);
                          answer.questionId = this.questions[i].questionId;
                          answers.push(answer);
                      } else {
                          const answer = new Answer();
                          answer.answer = this.questions[i].answer.answer;
                          answer.questionId = this.questions[i].questionId;
                          answers.push(answer);
                      }
                   }
                 }
     }
     return answers;
 }

 public getFormattedAnswer(timeObject) {
   return this.getFormattedNumber(timeObject.hour) + ':' + this.getFormattedNumber(timeObject.minute);
 }

  public mapQuestionare(data): Array<Question> {
    this.questoinareNote = this.questoinareNote + data.notes;
    this.questionaireRFQId = data.questionnaireForRFQId;
    let question = new Array<Question>();

    for (let i = 0; i < data.qaList.length; i++) {
      for (let j = 0; j < data.qaList[i].questions.length; j++) {
        if (data.qaList[i].questions[j].questionType === 'Text' ||
          data.qaList[i].questions[j].questionType === 'Note' ||
          data.qaList[i].questions[j].questionType === 'Integer' ||
          data.qaList[i].questions[j].questionType === 'Real' ||
          data.qaList[i].questions[j].questionType === 'Date' ||
          data.qaList[i].questions[j].questionType === 'Time') {
          const questionWithoutOptions: Question = new Question();
          questionWithoutOptions.question = data.qaList[i].questions[j].question;
          questionWithoutOptions.questionId = data.qaList[i].questions[j].questionId;
          questionWithoutOptions.questionType = data.qaList[i].questions[j].questionType;
          questionWithoutOptions.sequenceNo = data.qaList[i].questions[j].sequence;
          questionWithoutOptions.isMandatory = data.qaList[i].questions[j].isMandatory;
          questionWithoutOptions.answer = new Answer();
          question.push(questionWithoutOptions);
        } else {

          const questionWithOptions: Question = new Question();
          questionWithOptions.question = data.qaList[i].questions[j].question;
          questionWithOptions.questionId = data.qaList[i].questions[j].questionId;
          questionWithOptions.questionType = data.qaList[i].questions[j].questionType;
          questionWithOptions.sequenceNo = data.qaList[i].questions[j].sequence;
          questionWithOptions.isMandatory = data.qaList[i].questions[j].isMandatory;
          questionWithOptions.answer = new Answer();
          questionWithOptions.options = new Array<Option>();

          for (let k = 0; k < data.qaList[i].answers.length; k++) {
            const option = new Option();
            option.text = data.qaList[i].answers[k].answer;
            option.sequenceNo = data.qaList[i].answers[k].sequence;
            option.selected = false;
            questionWithOptions.options.push(option);
          }
          question.push(questionWithOptions);
        }
      }
    }
    if (question.length > 0) {
      question = question.sort((a, b) => a.sequenceNo - b.sequenceNo);
    }
    return question;
  }

  public toggleQuestinoarePopup(event, flag) {
    this.popUpPosition(event);
    this.questionareFlag = flag;
  }

  public popUpPosition(event) {
    // Calculate pageX/Y if missing and clientX/Y available
    if (event.pageX == null && event.clientX != null) {
      const doc = document.documentElement,
        body = document.body;
      event.pageX = event.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0)
        - (doc && doc.clientLeft || body && body.clientLeft || 0);
      event.pageY = event.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0)
        - (doc && doc.clientTop || body && body.clientTop || 0);
    }
    this.left = event.pageX + this.leftThreshold + 'px';
    this.top = event.pageY - 300 + 'px';
    const bodyWidth = document.body.offsetWidth;
    if (bodyWidth < 768) {
      this.left = 'auto';
    }
  }

  public cancelQuestionare() {
    this.questionareFlag = 'none';
    this.clearQuestionare();
  }

  public clearQuestionare() {
    this.currentQuestion = new Question();
    this.questions = new Array<Question>();
    this.options = new Array<Option>();
    this.answers = new Array<Answer>();

    this.currentIndex = 0;
    this.lowestIndex = 0;
    this.highestIndex = 0;
    this.questionareProgress = 0;

    const answer = new Answer();
    answer.answer = '';
    this.currentQuestion.answer = answer;

    this.questionareError = '';
  }
}
