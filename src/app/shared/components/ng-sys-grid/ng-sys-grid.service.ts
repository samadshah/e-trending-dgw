import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';

import { environment } from '../../../../environments/environment';

@Injectable()
export class NgSysGridService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/';
    this.servicePath2 = '/api';
  }

  getGridData(pageModel, url): Observable<any> {
    return this.http.post(environment.base_api_url + this.servicePath2 + url, pageModel);
  }
}
