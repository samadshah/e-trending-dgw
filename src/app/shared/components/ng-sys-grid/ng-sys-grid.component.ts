/* tslint:disable */
import { HttpErrorResponse } from '@angular/common/http';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { Logger } from 'angular2-logger/core';
import { NotificationsService } from 'angular2-notifications/dist';
import * as Moment from 'moment';
import { IMyDpOptions } from 'mydatepicker';
import { Subject } from 'rxjs/Subject';
import { AppSettingService } from '../../../core/services/app-setting.service';

import {
  DataType,
  GridParameters,
  GridType,
  HttpStatusCode,
  MessageText,
  RFQBidType,
  rfQListIndication,
  YesNo,
} from '../../../core/configuration/config';
import { Utils } from '../../../core/Helpers/utils';
import { AttachmentMetaType, MetaPostModel } from '../../../core/models/AttachmentMeta';
import { Lookup } from '../../../core/models/lookup';
import { PageQuery, PagingResponse } from '../../../core/models/page';
import { UtiltiyService } from '../../../core/services/utility';
import { KeyedCollection } from './models/keyValue.model';
import { NgSysGridService } from './ng-sys-grid.service';

export class Item {
  constructor(name: string, lookup: Array<Lookup>, isLookup: boolean, data?: Object) {
    this.name = name;
    this.data = data;
    this.lookup = lookup;
    this.isLookup = isLookup;
  }
  name: string;
  data: Object;
  isLookup: boolean;
  lookup: Array<Lookup>;
  dropdowToggler: string;
  currentSelected: string;
}

@Component({
  selector: 'app-ng-sys-grid',
  templateUrl: './ng-sys-grid.component.html',
  styleUrls: ['./ng-sys-grid.component.css'],
  providers: [NgSysGridService]
})

export class NgSysGridComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('filterBtn')
  filterButton: any;
  platformLocation: any;

  testCols = [
    { id: 1, name: 'Vilnius' },
    { id: 2, name: 'Kaunas' },
    { id: 3, name: 'Pabradė' }
  ];
  selectedTest: any = 3;
  peopleTypeahead = { id: 1, name: 'Vilnius' };
  placeHolderDateSelect = 'Select Date';

  // rfqlist input methods
  @Output() bidRecall = new EventEmitter<any>();
  @Output() bid = new EventEmitter<any>();
  @Output() attachment = new EventEmitter<any>();
  @Output() viewDetail = new EventEmitter<any>();
  @Output() popUp = new EventEmitter<any>();
  @Output() loadTooltip = new EventEmitter<any>();

  @Output() edit = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();

  // input parameters
  @Input() hideFilter = false;
  @Input() url: boolean;
  @Input() appbase: boolean;
  @Input() gridType: any;
  @Input() columnsDefination: Array<any>;
  @Input() inputColumns: Array<any>;
  @Input() title: Array<any>;
  @Input() addUrl: any;
  datatableProcessing = false;
  public isBaseFilterAllowed = true;
  public tableWidgetG: any;
  private authToken: string;
  private body = 'body';
  columns: Array<any> = [];
  columnsDrpDown: Item[] = [];
  sortFilters: Array<any> = [];
  selectedColumn: Item;
  defaultSelectText = 'Select column';
  filterControls: Array<any> = [];
  serverColumns = new KeyedCollection<string>();
  tableId = '#list-table';
  pageQueryModel: PageQuery = new PageQuery();
  gridParams = GridParameters;
  customPage = [0];
  pageValue = 1;
  //!
  // metaTypeInfo: Array<AttachmentMetaType>;
  baseParams: any;
  readParamSubscription: any;
  filterValidation: string;
  filterDropdownToggle = 'none';
  canGo: Subject<boolean> = new Subject<boolean>();
  private gridData: any;

  public myDatePickerOptions: IMyDpOptions = {};
  public calls: Array<any> = [];

  //!
  public newDraw = 0;


  constructor(
    private gridService: NgSysGridService,
    private oauthService: OAuthService,
    private router: Router,
    private route: ActivatedRoute,
    private notificationsService: NotificationsService,
    private utiltiy: UtiltiyService,
    private logger: Logger,
    private _renderer: Renderer,
    private appSettingSrvc: AppSettingService) {

      this.myDatePickerOptions = {
        dateFormat: this.appSettingSrvc.settings.datePicker.toString(),
        showClearDateBtn: false,
        alignSelectorRight: true,
        allowSelectionOnlyInCurrentMonth: false,
      }
     }

  ngOnInit() {
    this.readParamSubscription = this.route
      .queryParams
      .subscribe(params => {
        this.logger.debug('ng-sys-grid >> ngonInit >> this.route.queryParams.subscribe');
        this.baseParams = params || null;
      });
      $('#list-table').removeClass('expiryHeader');
      if(GridType.RFQList === this.gridType) {
        $('#list-table').addClass('expiryHeader');
      }
  }

  ngAfterViewInit() {
    let pageNo = this.getParam('pageNo') || this.appSettingSrvc.settings.pageNo;
    const pageSize = this.getParam('pageSize') || this.appSettingSrvc.settings.pageSize;
    const order = this.getParam('sort') || [];
    pageNo = (pageNo - 1) * pageSize;
    this.initDatatable(pageNo, pageSize, order);
  }

  private initDatatable(pageNoParam, pageSizeParam, orderParam): void {

    const dtId: any = $(this.tableId);
    const tableWidget = dtId
      .DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        deferRender: true,
        paging: true,
        searching: false,
        displayStart: pageNoParam,
        pageLength: pageSizeParam,
        sorting: orderParam,
        autoWidth: false,
        language: {
          emptyTable: MessageText.gridEmpty,
          processing: '',
          info: '_START_ - _END_ of _TOTAL_ records'
        },
        sDom: '<"clear">t<"F"p><"F"ilr>',
        columnDefs: this.columnsDefination,
        columns: this.inputColumns,
        fnServerData: (sSource, aoData, fnCallback, osettings) => {
          this.serverCall(sSource, aoData, fnCallback, osettings);
        },
        createdRow: (row, data, dataIndex) => {
            if (this.gridType === GridType.RFQList) {
              const Cdate = new Date();
              const date = Moment(data.expirationDateAndTime).toString(); // new Date(data.expirationDateAndTime).toLocaleDateString();
              const expDate = new Date(date);
              const timeDiff = expDate.getTime() - Cdate.getTime();
              const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
              if (diffDays > -1
                && diffDays < this.appSettingSrvc.settings.expiredNoDaysBeforeNotification
                && data.rfqStatus.toLowerCase() === rfQListIndication.status) {
                    $(row).addClass('expire-data');
                }
              }
        },
        rowCallback: (row: Node, data: any | Object, index: number) => {
          $('.actionView', row).unbind('click');
          $('.actionView', row).bind('click', (e) => {
            if (e.currentTarget.attributes['data-id'] && e.currentTarget.attributes['data-url']) {
              e.preventDefault();
              e.stopPropagation();
              const id = e.currentTarget.attributes['data-id'].nodeValue;
              const url = e.currentTarget.attributes['data-url'].nodeValue;
              let id2 = '';
              if (e.currentTarget.attributes['data-id2']) {
                id2 = e.currentTarget.attributes['data-id2'].nodeValue;
                this.router.navigate([url], { queryParams: { id1: id, id2: id2 }});
              } else {
                this.router.navigate([url], { queryParams: { id: id } });
              }
            }
          });
          return row;
        },
      });

    $(this.body).on('click', '#bidBtn', (e) => {
      e.preventDefault();
      const requestforquotation = e.currentTarget.attributes['data-rfq'].nodeValue;
      const rfqId = e.currentTarget.attributes['data-rfqId'].nodeValue;
      const obj = { requestforquotation: requestforquotation, rfqId: rfqId, event: e };
      this.bid.emit(obj);
    });

    $(this.body).on('click', '#bidRecallBtn', (e) => {
      e.preventDefault();
      const result = e.currentTarget.attributes['data-id'].nodeValue;
      const prop = e.currentTarget.attributes['data-prop'].nodeValue;
      const row = this.findDataById(prop, result);
      this.bidRecall.emit(row);
    });

    dtId.on('click', '#attachBtn', (e) => {
      e.preventDefault();
      const result = e.currentTarget.attributes['data-id'].nodeValue;
      const prop = e.currentTarget.attributes['data-prop'].nodeValue;
      const row = this.findDataById(prop, result);
      const pModel: MetaPostModel = new MetaPostModel();
      //!
      // pModel.metaTypes = this.metaTypeInfo;
      // pModel.metaTags = row.metaTags;
      pModel.id = row.requestforquotation;
      this.attachment.emit(pModel);
    });

    dtId.on('click', '.popUp', (e) => {
      e.preventDefault();
      const indx = e.currentTarget.attributes['data-id'].nodeValue;
      const data = this.findDataByIndex(indx);

      const pModel: MetaPostModel = new MetaPostModel();
      //!
      // pModel.metaTypes = this.metaTypeInfo;
      // pModel.metaTags = data.metaTags;

      const obj = { subject: data.subject, message: data.message, model: pModel, event: e };
      this.popUp.emit(obj);
    });

    dtId.on('click', '.popUpViewRfq', (e) => {
      e.preventDefault();
      const result = e.currentTarget.attributes['data-id'].nodeValue;
      const prop = e.currentTarget.attributes['data-prop'].nodeValue;
      const row = this.findDataById(prop, result);
      const pModel: MetaPostModel = new MetaPostModel();
      //!
      // pModel.metaTypes = this.metaTypeInfo;
      // pModel.metaTags = row.metaTags;
      pModel.id = row.requestforquotation;
      this.attachment.emit(pModel);

      const obj = { model: pModel, event: e };
      this.popUp.emit(obj);
    });

    dtId.on('click', '#ViewBtn', (e) => {
      e.preventDefault();
      const result = e.currentTarget.attributes['data-id'].nodeValue;
      const prop = e.currentTarget.attributes['data-prop'].nodeValue;
      const row = this.findDataById(prop, result);
      this.viewDetail.emit(row);
    });

    dtId.on('click', '#editBtn', (e) => {
      e.preventDefault();
      const result = e.currentTarget.attributes['data-id'].nodeValue;
      const prop = e.currentTarget.attributes['data-prop'].nodeValue;
      const row = this.findDataById(prop, result);
      this.edit.emit(row.attachmentMeta);
    });

    dtId.on('click', '#deleteBtn', (e) => {
      e.preventDefault();
      const result = e.currentTarget.attributes['data-id'].nodeValue;
      const prop = e.currentTarget.attributes['data-prop'].nodeValue;
      const row = this.findDataById(prop, result);
      this.delete.emit(row);
    });

    tableWidget.on('draw', function () {
      if (tableWidget.page.info().pages < 2) {
          $(tableWidget.table().container())
          .find('a.previous, a.next, #list-table_paginate span')
          .css('display', tableWidget.page.info().pages < 2 ?
            'none' :
            ''
          );
      } else {
          $(tableWidget.table().container())
          .find('a.previous')
          .css('display', tableWidget.page.info().page === 0 ?
            'none' :
            ''
          );

          $(tableWidget.table().container())
          .find('a.next')
          .css('display', tableWidget.page.info().page === (tableWidget.page.info().pages - 1) ?
            'none' :
            ''
          );
      }
      // tooltip
      $('.paginate_button').attr('data-toggle', 'tooltip');
      $('.paginate_button').not('.next').not('.previous').attr('title', 'View Page');
      $('.next').attr('title', 'Next Page');
      $('.previous').attr('title', 'Previous Page');

      $('.dataTables_length').attr('data-toggle', 'tooltip');
      $('.dataTables_length').attr('title', 'Number of records visible in this form');

      setTimeout(() => {
        $('.paginate_button').tooltip();
        $('.dataTables_length').tooltip();
      }, 0);
    });
  }

  public serverCall(sSource, aoData, fnCallback, osettings) {
    const isBaseFilterAllowed = this.isBaseFilterAllowed;
    this.logger.debug('serverCall');
    const draw = this.fetchServerParams(this.gridParams.draw, aoData);
    const start = this.fetchServerParams(this.gridParams.start, aoData);
    const size = this.fetchServerParams(this.gridParams.length, aoData);
    this.pageQueryModel.pageNo = (start.value / size.value) + 1;
    this.pageQueryModel.pageSize = size.value;
    this.pageQueryModel.draw = draw.value;
    this.fillSort(aoData);
    this.fillbaseFilter(this.pageQueryModel);
    this.datatableProcessing = true;
    const subscribe = this.gridService.getGridData(this.pageQueryModel, this.url)
      .subscribe((data) => {
        if (draw) {
            //!
            this.newDraw++;

            // if (this.gridData) {
            //   this.gridData.draw = data.draw;
            // }
            // if (!Utils.isEqual(data, this.gridData)) {
            // this.gridData = data;
            const response = new PagingResponse();
            response.data = this.getGridList(data.data);
            //!
            // response.draw = data.draw;
            // response.draw = data.data.pagination.pageNo;
            response.draw = this.newDraw;
            //!
            // response.recordsFiltered = data.recordsFiltered;
            response.recordsFiltered = data.data.pagination.pageCount;
            //!
            // response.recordsTotal = data.recordsTotal;
            response.recordsTotal = data.data.pagination.pageCount;
            //!
            // this.metaTypeInfo = data.data.metaTagTypeList;
            const pages = Math.ceil(response.recordsTotal / this.pageQueryModel.pageSize);
            this.customPage = Array(pages).fill(1).map((x, i) => i + 1);
            this.pageValue = this.pageQueryModel.pageNo;
            fnCallback(response);
            this.fillFilterDropdown(data.data.columns);
            this.fillServerColumns(data.data.columns);
            this.creatTooltip();
            this.logger.debug('Grid data UI refreshed.');
          // } else {
          //   this.logger.debug('Grid data is same. skipping UI refresh.');
          // }
        }
        this.scrollToTop();
        this.datatableProcessing = false;
      },
      err => {
        if (err instanceof HttpErrorResponse
          && (err.status === HttpStatusCode.badRequest)) {
          const message = this.utiltiy.errorHandle(err);
          this.notificationsService.error(message);
        }
        this.datatableProcessing = false;
      });
    if (isBaseFilterAllowed === false) {
      this.updateURL();
    }
    this.calls.push(subscribe);
  }

  private updateURL() {
    const url = this.appbase + '?pageNo=' + this.pageQueryModel.pageNo + '&pageSize=' + this.pageQueryModel.pageSize +
      '&filter=' + encodeURIComponent(this.pageQueryModel.filters) + '&sort=' + encodeURIComponent(this.pageQueryModel.sort);
    window.history.replaceState({ url: url }, '', url);
  }

  public fillbaseFilter(pageQueryModel: PageQuery) {
    const key = Object.keys(this.baseParams);
    if (key.length === 1 && this.isBaseFilterAllowed) {
      const baseFilter = this.baseParams['filter'];
      if (pageQueryModel.filters.indexOf(baseFilter) === -1) {
        pageQueryModel.filters = pageQueryModel.filters + baseFilter;
      }
    } else if (key.length > 1 && this.isBaseFilterAllowed) {
      const filters = this.getParam('filter') || [];
      for (let i = 0; i < filters.length; i++) {
        const baseFilter = ',' + filters[i].key + '=' + filters[i].value;
        if (pageQueryModel.filters.indexOf(baseFilter) === -1) {
          pageQueryModel.filters = pageQueryModel.filters + baseFilter;
        }
      }
    }
    if (this.isBaseFilterAllowed === true) {
      this.isBaseFilterAllowed = false;
    }
    if (pageQueryModel.filters) {
      pageQueryModel.filters = pageQueryModel.filters.trim();
      if (pageQueryModel.filters.startsWith(',')) {
        pageQueryModel.filters = pageQueryModel.filters.substr(1);
      }
      if (pageQueryModel.filters.endsWith(',')) {
        pageQueryModel.filters = pageQueryModel.filters.slice(0, -1);
      }
    }
  }

  public fillFilterDropdown(cols) {
    if (this.columnsDrpDown.length === 0) {
      const itemsSorted = cols.sort(function (obj1, obj2) {
        // Ascending: first order less than the previous
        return obj1.displayOrder - obj2.displayOrder;
      });
      for (let i = 0; i < itemsSorted.length; i++) {
        if (itemsSorted[i].allowFilter) { // itemsSorted[i].allowDisplay &&
          const isLookup = itemsSorted[i].lookup && itemsSorted[i].lookup.length > 0;
          this.columnsDrpDown.push(new Item(itemsSorted[i].name,
            itemsSorted[i].lookup,
            isLookup,
            { column: itemsSorted[i].value, dataType: itemsSorted[i].dataType }));
        }
      }
      const key = Object.keys(this.baseParams);
      if (key.length === 1) {
        const baseFilter = this.baseParams['filter'].split('=');
        const value = baseFilter[1];
        const item = this.columnsDrpDown.filter(x => x.data['column'] === baseFilter[0]);
        const index = this.columnsDrpDown.indexOf(item[0]);
        const lookupItem = item[0].lookup.filter(x => x.value.toLowerCase() === value.toLowerCase());
        this.filterControls.push({
          name: this.columnsDrpDown[index],
          value: lookupItem.length > 0 ? lookupItem[0].value : value,
          filterColumnDropdowToggler: 'none',
          dropdowToggler: 'none',
          currentSelected: lookupItem.length > 0 ? lookupItem[0].name : '',
          searchTextColumn: '', searchTextFilter: ''
        });
      } else if (key.length > 1) {
        const allFilterParams = this.getParam('filter') || [];
        if (allFilterParams && allFilterParams.length > 0) {
          for (let i = 0; i < allFilterParams.length; i++) {
            const item = this.columnsDrpDown.filter(x => x.data['column'] === allFilterParams[i].key);
            const index = this.columnsDrpDown.indexOf(item[0]);
            const lookupItem = item[0].lookup.filter(x => x.value.toLowerCase() === allFilterParams[i].value.toLowerCase());
            this.filterControls.push({
              name: this.columnsDrpDown[index],
              value: lookupItem.length > 0 ? lookupItem[0].value : allFilterParams[i].value,
              filterColumnDropdowToggler: 'none',
              dropdowToggler: 'none',
              currentSelected: lookupItem.length > 0 ? lookupItem[0].name : '',
              searchTextColumn: '', searchTextFilter: ''
            });
          }
        } else {
          this.filterControls.push({
            name: this.columnsDrpDown[0], value: '',
            filterColumnDropdowToggler: 'none',
            dropdowToggler: 'none',
            currentSelected: '',
            searchTextColumn: '', searchTextFilter: ''
          });
        }
      } else {
        this.filterControls.push({
          name: this.columnsDrpDown[0], value: '',
          filterColumnDropdowToggler: 'none',
          dropdowToggler: 'none',
          currentSelected: '',
          searchTextColumn: '', searchTextFilter: ''
        });
      }
    }
  }

  private fillServerColumns(data) {
    let cols: any = [];
    if (this.columns.length === 0) {
      cols = data.map(o => {
        return {
          name: o.name,
          prop: this.pascalToCamelCase(o.value),
        };
      });
    }
  }

  public addFilter() {
    const item = this.columnsDrpDown[0];
    if (this.filterControls.length < this.appSettingSrvc.settings.filterLength) {
      this.filterControls.push({
        name: item, value: '',
        filterColumnDropdowToggler: 'none',
        dropdowToggler: 'none',
        currentSelected: '',
        searchTextColumn: '',
        searchTextFilter: ''
      });
    } else {
      this.notificationsService.info(MessageText.filterLimitExceed);
    }
  }

  public removeFilter(index) {
    this.filterControls.splice(index, 1);
  }

  public enums() {
    return YesNo;
  }

  public types() {
    return DataType;
  }

  public bidType() {
    return RFQBidType;
  }

  public search() {
    this.filterValidation = '';
    if (this.filterControls.length === 1) {
      if (!this.filterControls[0].value) {
        // this.filterValidation = MessageText.invalidFilters;
        this.filterValidation = "Kindly enter " + this.filterControls[0].name.name;
        return false;
      }
    }
    if (this.datatableProcessing === true) {
      return false;
    }
    this.reloadGrid();
  }

  public jusReloadData() {
    const dtId: any = $(this.tableId);
    dtId.DataTable().order([]).ajax.reload();
  }

  public refreshGrid() {
    if (this.datatableProcessing === true) {
      return false;
    }
    this.pageQueryModel.filters = '';
    this.pageQueryModel.sort = '';
    this.filterValidation = '';

    const item = this.columnsDrpDown[0];
    this.filterControls = [];
    this.filterControls.push({
      name: item, value: '',
      filterColumnDropdowToggler: 'none',
      dropdowToggler: 'none',
      currentSelected: '',
      searchTextColumn: '',
      searchTextFilter: ''
    });

    const dtId: any = $(this.tableId);
    dtId.DataTable().order([]).ajax.reload();
  }

  public reloadGrid() {
    this.pageQueryModel.filters = '';
    this.pageQueryModel.sort = '';
    let filterResp = this.filterValidationFailed();
    if (filterResp.success) {
      // this.filterValidation = MessageText.invalidFilters;
      this.filterValidation = "Kindly enter " + filterResp.filter.name;
      return;
    }
    const dtId: any = $(this.tableId);
    dtId.DataTable().ajax.reload();
  }

  public reloadGridWithOrWithoutFilters() {
    this.pageQueryModel.filters = '';
    this.pageQueryModel.sort = '';
    this.filterValidationFailed();
    const dtId: any = $(this.tableId);
    dtId.DataTable().ajax.reload(
      null, false);
  }

  public filterValidationFailed() {
    if (this.filterControls.length === 1) {
      if (!this.filterControls[0].name && !this.filterControls[0].value) {
        let resp = { success : true, filter : this.filterControls[0].name }
        return resp;
      }
    }
    for (let i = 0; i < this.filterControls.length; i++) {
      if (!this.filterControls[i].name || !this.filterControls[i].value) {
        let resp = { success : true, filter : this.filterControls[i].name }
        return resp;
      }
    }
    this.updatePageQueryModel();
    let resp = { success : false, filter : '' }
    return resp;
  }

  public updatePageQueryModel() {
    let filter = '';
    for (let i = 0; i < this.filterControls.length; i++) {
      if (this.filterControls[i].value.hasOwnProperty('jsdate')) {
        const axdate = Moment(this.filterControls[i].value['jsdate']).format(this.appSettingSrvc.settings.dateFormateForAx);
        filter = filter + '' + this.filterControls[i].name.data.column + '='
          + axdate + ',';
      } else {
        filter = filter + '' + this.filterControls[i].name.data.column + '=' + this.filterControls[i].value + ',';
      }
    }
    this.pageQueryModel.filters = filter;
  }

  public updateSortFilter() {
    let sortFiler = '';
    for (let i = 0; i < this.sortFilters.length; i++) {
      sortFiler = sortFiler + '' + this.serverColumns.Item(this.sortFilters[i].prop) +
        '=' + this.getsortTerm(this.sortFilters[i].dir) + ',';
    }
    this.pageQueryModel.sort = sortFiler;
  }

  // helper functions
  private pascalToCamelCase(item) {
    const result = '';
    this.serverColumns.Add(item.toLowerCase(), item);
    return result;
  }

  private splitValue(value, index) {
    return value.substring(0, index).toLowerCase() + value.substring(index);
  }

  private getsortTerm(term) {
    return term === 'asc' ? 0 : 1;
  }

  public fetchServerParams(prop, data) {
    for (let i = 0; i < data.length; i++) {
      if (data[i].name === prop) {
        return data[i];
      }
    }
  }

  public fillSort(data) {
    const sortInfo = this.fetchServerParams(this.gridParams.order, data);
    if (sortInfo.value.length > 0) {
      const sortColumnIndex = sortInfo.value[0].column;
      const sortValue = sortInfo.value[0].dir;

      const sortColumn = this.fetchServerParams(this.gridParams.columns, data).value[sortColumnIndex];
      if (this.serverColumns.ContainsKey(sortColumn.data.toLowerCase())) {
        this.pageQueryModel.sort = this.serverColumns.Item(sortColumn.data.toLowerCase()) + '=' + this.getsortTerm(sortValue);
      }
    }
  }

  public reloadByPage() {
    const dtId: any = $(this.tableId);
    dtId.DataTable().page(this.pageValue - 1).draw(false);
  }

  public findDataById(prop, value) {
    const dtId: any = $(this.tableId);
    const data = dtId.DataTable().data();
    for (let i = 0; i < data.length; i++) {
      if (data[i][prop] === value) {
        return data[i];
      }
    }
  }

  public findDataByIndex(index) {
    const dtId: any = $(this.tableId);
    const data = dtId.DataTable().row(index).data();
    return data;
  }

  ngOnDestroy() {
    const dtId: any = $(this.tableId);
    dtId.DataTable().destroy();
    this.readParamSubscription.unsubscribe();

    for (let index = 0; index < this.calls.length; index++) {
      if(this.calls[index]){
        this.calls[index].unsubscribe();
      }
    }
  }

  // gird data
  public getGridList(data) {
    if (GridType.RFQList === this.gridType) {
      return data.rfqList;
    } else if (GridType.POList === this.gridType) {
      return data.poList;
    }
    //else if (GridType.ROList === this.gridType) {
      //return data.roList;
    // }
    else if (GridType.NotificationList === this.gridType) {
      return data.notificationList;
    }
    // else if (GridType.InvoiceList === this.gridType) {
    //   return data.vendorInvoiceList;
    // }
    else if (GridType.ContactList === this.gridType) {
      return data.contactList;
    }
    // else if (GridType.CAList === this.gridType) {
    //   return data.contractList;
    // }
    // else if (GridType.BAList === this.gridType) {
    //   return data.purchAgreementList;
    // }
    else {
      return [];
    }
  }

  /**
 * loadToolTip
 */
  public creatTooltip() {
    this.loadTooltip.emit();
  }

  public getParam(prop) {
    const keys = Object.keys(this.baseParams);
    if (keys && keys.length > 1) {
      const sortValue = this.baseParams[prop].toString();
      if (prop === 'sort') {
        if (sortValue) {
          const result = sortValue.split('=');
          if (result.length > 0) {
            const item = this.inputColumns.filter(x => x['data'].toLowerCase() === result[0].toLowerCase());
            const index = this.inputColumns.indexOf(item[0]);
            const sVal = this.updateSortTerm(result[1]);
            return [[index, sVal]];
          }
        }
      } else if (prop === 'filter') {
        const arr = [];
        const splitValue = sortValue.split(',');
        for (let i = 0; i < splitValue.length; i++) {
          if (splitValue[i]) {
            const currentItem = splitValue[i].split('=');
            if (currentItem[0] && currentItem[1]) {
              arr.push({ key: currentItem[0], value: currentItem[1] });
            }
          }
        }
        return arr;
      }
      return this.baseParams[prop] || null;
    }
    return null;
  }

  private updateSortTerm(term) {
    return term === 0 ? 'asc' : 'desc';
  }

  public keyDownColumns(event, control, option?: any) {
      let ele: any;
    switch (event.keyCode) {
        case 38: // this is the ascii of arrow up
                 ele = event.target;
                 if (event.srcElement.previousElementSibling) {
                  if (event.srcElement.previousElementSibling.children.length > 1) {
                    event.srcElement.previousElementSibling.children[1].focus();
                  }else {
                    event.srcElement.previousElementSibling.focus();
                  }
                 }
                 break;
        case 40: // this is the ascii of arrow down
                 ele = event.target;
                 if (control.filterColumnDropdowToggler === 'none') {
                  event.srcElement.nextElementSibling.focus();
                 }
                 control.filterColumnDropdowToggler = 'block';
                 event.preventDefault();
                 if (event.srcElement && event.srcElement.tagName.toLowerCase() === 'button') {
                      event.srcElement.nextElementSibling.children[0].children[1].focus();
                  } else if (event.srcElement.nextElementSibling && event.srcElement.nextElementSibling.tagName.toLowerCase() === 'li') {
                      event.srcElement.nextElementSibling.focus();
                 } else {
                 if (event.srcElement && event.srcElement.tagName.toLowerCase() === 'input') {
                        event.srcElement.parentElement.nextElementSibling.focus();
                    }
                 }
                 break;
        case 27: // enter button
                 ele = event.target;
                 control.filterColumnDropdowToggler = 'none';
                 break;
        case 13: // enter button
                if (control && option) {
                  control.name = option;
                  control.value = '';
                  control.currentSelected = '';
                  control.searchTextColumn = '';
                  control.filterColumnDropdowToggler = 'none';
                }
                 break;
    }
  }

  public keyDownFilter(event, control, option?: any) {
    let ele: any;
  switch (event.keyCode) {
      case 38: // this is the ascii of arrow up
               ele = event.target;
               if (event.srcElement.previousElementSibling) {
                if (event.srcElement.previousElementSibling.children.length > 1) {
                  event.srcElement.previousElementSibling.children[1].focus();
                }else {
                  event.srcElement.previousElementSibling.focus();
                }
               }
               break;
      case 40: // this is the ascii of arrow down
               ele = event.target;
               if (control.dropdowToggler === 'none') {
                event.srcElement.nextElementSibling.focus();
               }
               control.dropdowToggler = 'block';
               event.preventDefault();
               if (event.srcElement && event.srcElement.tagName.toLowerCase() === 'button') {
                    event.srcElement.nextElementSibling.children[0].children[1].focus();
                } else if (event.srcElement.nextElementSibling && event.srcElement.nextElementSibling.tagName.toLowerCase() === 'li') {
                    event.srcElement.nextElementSibling.focus();
               } else {
               if (event.srcElement && event.srcElement.tagName.toLowerCase() === 'input') {
                      event.srcElement.parentElement.nextElementSibling.focus();
                  }
               }
               break;
      case 27: // enter button
               ele = event.target;
               control.dropdowToggler = 'none';
               break;
      case 13: // enter button
              if (control && option) {
                control.value = option.value;
                control.currentSelected = option.name;
                control.searchTextFilter = '';
                control.dropdowToggler = 'none';
              }
               break;
    }
  }

  public enterKeyDown(event) {
        switch (event.keyCode) {
        case 13: // enter button
              this.filterButton.nativeElement.click();
                 break;
      }
  }

  public toggleFilterColumnsDropdown(event, control) {
    control.filterColumnDropdowToggler = control.filterColumnDropdowToggler === 'none' ? 'block' : 'none';
    if (control.filterColumnDropdowToggler === 'block') {
      setTimeout(function () {
         event.srcElement.nextElementSibling.children[0].children[1].focus();
      }, 1000);
    }
  }

  public toggleFilterDropdown(event, control) {
    control.dropdowToggler = control.dropdowToggler === 'none' ? 'block' : 'none';
    event.preventDefault();
    if (control.dropdowToggler === 'block') {
      setTimeout(function () {
        event.srcElement.nextElementSibling.children[0].children[1].focus();
     }, 1000);
    }
  }

  public scrollToTop() {
    window.scroll(0, 0);
  }
}


