import { environment } from '../environments/environment';
import { AuthConfig } from 'angular-oauth2-oidc';

export const authPasswordFlowConfig: AuthConfig = {
    issuer: environment.auth_url,
    clientId: 'etisalat.portal.ro',
    dummyClientSecret: 'secret',
    scope: 'etisalat_web_api',
    showDebugInformation: true,
    oidc: false,
    requireHttps: false
};

