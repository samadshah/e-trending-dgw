// tslint:disable
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import { finalize } from 'rxjs/operators';
import { APP_BASE_HREF } from '@angular/common';

import { environment } from '../../environments/environment';
import { HttpStatusCode, MessageText, RedirectUrl, Regex } from '../core/configuration/config';
import { ForgotPasswordService } from './forgot-password.service';
import { FrogotPassword } from './models/forgot-password.model';
import { AppInitial } from '../core/models/appInitials';
import { AppSettingService } from '../core/services/app-setting.service'
import { NotificationsService } from 'angular2-notifications/dist';
import { DashBoardDataService } from '../main/dashboard/dashboard-data.service';
import { CompanyMeta } from '../main/dashboard/models/company-meta.model';
import * as $ from 'jquery';
declare var captcha: any;

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  providers: [ForgotPasswordService, DashBoardDataService]
})

export class ForgotPasswordComponent implements OnInit {

  @ViewChild('captchaChild')
  captchaChild: any;

  public form: FrogotPassword = new FrogotPassword('', false);
  serverMessage: any = '';
  forgotPwdBtnDisable = false;
  timer: any;
  urls = RedirectUrl;
  appbase = '';
  logoPath = '';
  appInitial: AppInitial;
  enableCaptacha;
  companyData: CompanyMeta = new CompanyMeta();
  notifyMsg = false;

  constructor(
    private dashBoardDataService: DashBoardDataService,
    private notificationsService: NotificationsService,
    private appSettingService: AppSettingService,
    private forgotPasswordService: ForgotPasswordService,
    private route: ActivatedRoute,
    private router: Router,
    @Inject(APP_BASE_HREF) private baseHref: string) {
      this.enableCaptacha = environment.captcha_enable;
      this.appbase = baseHref;
      this.logoPath = '../' + baseHref + '/assets/images/' + appSettingService.settings.logoName;
      this.appInitial = new AppInitial(appSettingService.settings.logoText, appSettingService.settings.appName);
     }

  ngOnInit() {
    this.initComponent();
  }

  ngAfterViewInit(): void {
    //!!
    captcha.realPerson('#realPersonUser', 'realPersonHash');
    this.form.captcha = false;
  }

  private initComponent() {
    this.dashBoardDataService.getCompanyMeta()
    .subscribe(data => {
      this.companyData = data;
    });
  }

  public forgotPwd(valid: boolean) {
    //!!
    this.checkRecaptchaValidity();

    this.serverMessage = '';

    if (this.enableCaptacha === 0) {
      this.form.captcha = true;
    }

    if (!valid || !this.form.captcha) {
      return;
    }
    if (this.forgotPwdBtnDisable === true) {
      return false;
    }
    this.forgotPwdBtnDisable = true;

    //!
    this.forgotPasswordService
      .forgotPwd(this.form)
      .subscribe(
      res => {
        if(res.isSuccess){
          let successMsg = "Password change request submitted to "+ this.form.userName +" successfully.";
          if (!this.notifyMsg) {
            this.notificationsService.success(successMsg);
          }
          this.notifyMsg = true;
          setTimeout(() => {
            this.redirectToLoginScreen();
          }, 1000);
          this.forgotPwdBtnDisable = false;
          this.clearFormModel();
        } else {
          if (!this.notifyMsg) {
            this.notificationsService.error(res.message);
          }
          this.notifyMsg = true;
          this.forgotPwdBtnDisable = false;
          this.clearFormModel();
        }
      },
      err => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === HttpStatusCode.badRequest) {
            const message = err.error.error || err.error.Message;
            this.serverMessage = message;
          }
        }
        this.forgotPwdBtnDisable = false;
      }
      );

  }

  //!!
  checkRecaptchaValidity() {
    let text1 = $("#realPersonUser").val();
    let hash = captcha.getHash("#realPersonUser");
    let text1Hash = this.getHashCode(text1);

    if (text1 != "") {
      if (hash != text1Hash) {
        // console.log('no');
          this.form.captcha = false;
      } else {
        // console.log('yes');
        this.form.captcha = true;
      }
    }
  }

  //!!
  getHashCode(value) {
    var hash = 5381;
    if (value.length == 0) return hash;
    for (let i = 0; i < value.length; i++) {
      let char = value.charCodeAt(i);
      hash = (hash << 5) + hash + char;
      hash = hash & hash;
    }
    return hash;
  }


  public resolved(captchaResponse: string) {
    if (captchaResponse) {
      this.form.captcha = true;
    } else {
      this.form.captcha = false;
    }
  }

  private resetCaptach() {
    this.form.captcha = false;
    this.captchaChild.reset();
  }

  public get captchaKey() {
    return environment.captcha_key;
  }

  public redirectToLoginScreen() {
    this.router.navigate([RedirectUrl.login]);
  }

  public regexx() {
    return Regex;
  }

  private clearFormModel() {
    this.form.captcha = false;
  }

  public redirectToRegisterScreen() {
    this.router.navigate([RedirectUrl.register]);
  }

  public adjustCaptcha() {
    this.timer =  setInterval(() => {
    const captcha = $('.captcha-name > div');
    if (captcha && captcha.length !== 0) {
        const x = $('.captcha-holder').width() / 304;
        const y = $('.captcha-holder').height() / 78;
    $('.captcha-name > div').css({
        'transform': 'scale(' + x + ',' + y + ')'
      });
      window.clearInterval(this.timer);
    }
  }, 2000);
  }
}
