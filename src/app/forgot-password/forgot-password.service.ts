import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { _throw } from 'rxjs/observable/throw';

import { environment } from '../../environments/environment';
import { FrogotPassword } from './models/forgot-password.model';

@Injectable()
export class ForgotPasswordService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/auth';
    this.servicePath2 = '/api';
  }

  //!
  public forgotPwd(input: FrogotPassword): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/User/ResetPassword?userName=' + input.userName);
  }

}
