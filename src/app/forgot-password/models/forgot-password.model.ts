export class FrogotPassword {
    constructor(public userName: string,
        public captcha: boolean) {
    }
}

export interface IFrogotPassword {
    userName: string;
    captcha: boolean;
}
