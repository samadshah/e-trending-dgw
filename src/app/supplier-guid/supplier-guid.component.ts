import { Component, OnInit, Inject } from "@angular/core";
import { APP_BASE_HREF, PlatformLocation } from "@angular/common";
import { AttachmentService } from "../shared/services/attachment-service";
import { Guide, RedirectUrl } from "../core/configuration/config";

@Component({
  selector: "app-supplier-guid",
  templateUrl: "./supplier-guid.component.html",
  styleUrls: ["./supplier-guid.component.css"]
})
export class SupplierGuidComponent implements OnInit {
  icon1 = "";
  icon3 = "";
  icon5 = "";
  appbase = "";
  guide = Guide;

  regPath = "";
  biddingPath = "";
  contactPath = "";
  host = "";
  urls = RedirectUrl;
  constructor(@Inject(APP_BASE_HREF) private baseHref: string) {
    this.icon1 = "../" + baseHref + "assets/images/guide-icon01.png";
    this.icon3 = "../" + baseHref + "assets/images/guide-icon03.png";
    this.icon5 = "../" + baseHref + "assets/images/guide-icon05.png";
  }

  ngOnInit() {
    this.regPath =
      this.baseHref +
      this.urls.guideView +
      "?doc=" +
      this.guide.registrationGuide;
    this.biddingPath =
      this.baseHref + this.urls.guideView + "?doc=" + this.guide.bidding;
    this.contactPath =
      this.baseHref + this.urls.guideView + "?doc=" + this.guide.contact;
  }
}
