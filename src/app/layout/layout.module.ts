import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ProfileDropdownComponent } from './header/profile-dropdown/profile-dropdown.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    NavigationComponent,
    ProfileDropdownComponent,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    NavigationComponent,
  ]
})
export class LayoutModule { }
