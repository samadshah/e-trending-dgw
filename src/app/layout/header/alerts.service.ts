import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';

import { environment } from '../../../environments/environment';
import { NotificationAlert } from './models/alert';

@Injectable()
export class AlertsService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/dashboard';
    this.servicePath2 = '/api';
  }

  //!
  get(model): Observable<NotificationAlert> {
    return this.http.post<NotificationAlert>(environment.base_api_url + this.servicePath2 + '/Alerts/List', model);
  }

  //!
  // getCount(model): Observable<NotificationAlert> {
  //   return this.http.post<NotificationAlert>(environment.base_api_url + this.servicePath +
  //     '/alertCount', model, { headers: { ignoreLoadingBar: '' } });
  // }

  //!
  dismiss(model): Observable<any> {
    return this.http.post(environment.base_api_url + this.servicePath2 + '/Alerts/Dismis', model);
  }
}
