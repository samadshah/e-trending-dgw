// tslint:disable
import { Component, OnInit, EventEmitter, Output, ViewChild, AfterViewInit, Inject } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Logger } from 'angular2-logger/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import * as Moment from 'moment';
import { Contains, RedirectUrl, MessageText, HttpStatusCode, YesNo } from '../../core/configuration/config';
import { VendorService } from '../../core/services/vendor/vendor.service';
import { NotificationsService } from 'angular2-notifications/dist';
import { AlertsService } from './alerts.service';
import { APP_BASE_HREF } from '@angular/common';
import { Observable } from 'rxjs/Rx';
// models
import { Vendor } from '../../core/services/vendor/models/vendor.model';
import { NotificationAlert, Alert, PostDismiss } from './models/alert';
import { HttpErrorResponse } from '@angular/common/http';
import { PageQuery } from '../../core/models/page';
import { AppSettingService } from '../../core/services/app-setting.service';
import { AppInitial } from '../../core/models/appInitials';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [AlertsService]
})

export class HeaderComponent implements OnInit, AfterViewInit {
  @ViewChild('searchForm')
  searchForm: any;

  @Output() loggedOut = new EventEmitter<any>();
  vendor: Vendor = new Vendor();
  vendorSubscription: any;
  countSubscription: any;
  notificationALertDropdownToggle = 'none';
  notificationCount = 0;
  // pageNo = 1;
  pageModel = new PageQuery();
  firstLoad = false;
  waitToComplete = false;
  notifications = new Array<Alert>();
  recordCount = 0;
  notificationPaging = 0;
  dismissInProcess = false;
  currentDismissIndex = -1;
  currentIsDismiss = false;
  timer: any;
  moreRecords = true;
  logoPath = '';
  messageText = 'Loading...';
  appInitial: AppInitial;
  constructor(private oauthService: OAuthService,
    private logger: Logger,
    private vendorService: VendorService,
    private router: Router,
    private route: ActivatedRoute,
    private notificationsService: NotificationsService,
    private alertsService: AlertsService,
    private appSettingSrvc: AppSettingService,
    @Inject(APP_BASE_HREF) private baseHref: string
  ) {
    this.logoPath = '../' + baseHref + '/assets/images/' + appSettingSrvc.settings.logoName;
    this.appInitial = new AppInitial(appSettingSrvc.settings.logoText, appSettingSrvc.settings.appName);

    this.notificationPaging = 1; // this.appSettingSrvc.settings.notifications;
    this.pageModel.pageNo = 1;
    this.pageModel.pageSize = this.appSettingSrvc.settings.notifications;
    this.loadVendor();
    this.vendorSubscription = this.vendorService.getVendor().subscribe(data => {
      this.vendor = data;
    });
  }

  ngOnInit() {
    this.refreshCount();
    this.loadCount();
    this.apply();
  }

  public minimizeSideNav() {
    const outerDiv = $('#navMinimize');
    const btnMin = $('.btn-minimalize')
    outerDiv.toggleClass('nav-minimize');
    btnMin.toggleClass('active');
  }
  private loadVendor() {
    // try getting vendor from cahe
    this.vendorService.loadVendor();
    if (this.isEmpty(this.vendor)) {
      const hasVendor: any = JSON.parse(localStorage.getItem('vendor-info'));
      if (hasVendor) {
        this.vendor = hasVendor;
      }
    }
  }

  get hasValidAccessToken() {
    return this.oauthService.hasValidAccessToken();
  }

  public logout() {
    if (this.countSubscription) {
      this.countSubscription.unsubscribe();
      this.timer.unsubscribe();
    }

    this.oauthService.logOut();
    localStorage.clear();
    this.router.navigate([RedirectUrl.login], { relativeTo: this.route });
  }

  private isEmpty(obj) {
    return Object.keys(obj).length === 0 ? true : false;
  }

  public hideDropdown() {
    this.notificationALertDropdownToggle = 'none';
    $('#notification').removeClass('js-open');
  }

  public toggleALertDropdown() {
    if (this.notificationALertDropdownToggle === 'none') {
      $('#notification').addClass('js-open');
      this.messageText = 'Loading...';
      this.notificationALertDropdownToggle = 'block';
      this.pageModel.pageNo = 1;
      this.pageModel.pageSize = this.appSettingSrvc.settings.notifications;
      this.pageModel.withList = true;
      this.notifications = new Array<Alert>();
      this.getNotificationData();
      this.firstLoad = false;
      this.moreRecords = true;
    } else {
      $('#notification').removeClass('js-open');
      this.notificationALertDropdownToggle = 'none';
    }
  }

  public getNotificationData() {
    this.alertsService.get(this.pageModel).last().subscribe(data => {
      if (this.pageModel.pageNo > 1 && data.alerts.length === 0) {
        this.pageModel.pageNo = this.pageModel.pageNo - 1;
        this.moreRecords = false;
      }
      //  this.notifications = data.alerts;
      this.recordCount = data.pagination.pageCount;
      this.mappData(data.alerts);
      if (this.firstLoad === false) {
        this.firstLoad = true;
      }
      this.waitToComplete = false;
      this.messageText = MessageText.gridEmpty;
    },
      err => {
        if (err instanceof HttpErrorResponse
          && (err.status === HttpStatusCode.badRequest)) {
          const message = err.error.error || err.error.message;
          this.notificationsService.error(message);
        }
        if (this.pageModel.pageNo > 1) {
          this.pageModel.pageNo = this.pageModel.pageNo - 1;
        }
        this.waitToComplete = false;
      });
  }


  public refreshCount() {
    this.timer = Observable.interval(1000 * 60).subscribe(x => {
      this.loadCount();
    });
  }

  public loadCount() {
    if (this.countSubscription) {
      this.countSubscription.unsubscribe();
    }
    const model = new PageQuery();
    model.pageNo = 1;
    model.pageSize = this.appSettingSrvc.settings.notifications;
    model.withList = false;
    //!
    this.countSubscription = this.alertsService.get(model).subscribe(data => {
      this.notificationCount = data.pagination.pageCount;
    },
      err => {
      });
  }


  public apply() {
    $('.notification-alert .notification-list').css('max-height', '330px');
    $('.notification-alert .notification-list').css('overflow-x', 'auto');
    const customScrollBar: any = $('.notification-list');
    customScrollBar.mCustomScrollbar({
      axis: 'y', // horizontal scrollbar
      theme: 'dark',
      scrollInertia: 300,
    });
  }

  dismissSingle(item, index) {
    const mdl = new PostDismiss();
    mdl.dismissAll = false;
    mdl.isDismiss = true;
    mdl.transactionId = item.transactionId;
    //!
    mdl.LastAlertTime = new Date().toDateString();
    this.dismissAlert(mdl, index);
  }

  readNotification(item, index) {
    if (item.isRead === YesNo.yes) {
      return false;
    }
    const mdl = new PostDismiss();
    mdl.dismissAll = false;
    mdl.transactionId = item.transactionId;
    mdl.isDismiss = false;
    //!
    mdl.LastAlertTime = new Date().toDateString();
    this.dismissAlert(mdl, index);
  }

  dismissAll() {
    const mdl = new PostDismiss();
    mdl.dismissAll = true;
    mdl.LastAlertTime = this.fetchLatestTime();
    mdl.isDismiss = true;
    this.dismissAlert(mdl);
    this.notificationALertDropdownToggle = 'none';
  }

  public dismissAlert(requestModel, index = -1) {
    if (this.dismissInProcess === true) {
      return false;
    }
    this.dismissInProcess = true;
    this.currentIsDismiss = requestModel.isDismiss;
    if (index !== -1) {
      this.notifications.splice(index, 0);
      this.currentDismissIndex = index;
      if (this.currentIsDismiss) {
        this.notifications[this.currentDismissIndex].isActive = false;
      } else {
        this.notifications[this.currentDismissIndex].isRead = YesNo.yes;
      }
    }
    this.alertsService.dismiss(requestModel).subscribe(data => {
      //!
      if (data.isSuccess){
        this.dismissInProcess = false;
        this.loadCount();
      }
      else {
        this.notificationsService.error(data.message);
      }
    },
      err => {
        if (this.currentDismissIndex !== -1 && this.currentIsDismiss === true) {
          this.notifications[this.currentDismissIndex].isActive = true;
        } else if (this.currentDismissIndex !== -1 && this.currentIsDismiss === false) {
          //!
          // this.notifications[this.currentDismissIndex].isRead = YesNo.no;
          this.notifications[this.currentDismissIndex].isRead = '0';
        }
        if (err instanceof HttpErrorResponse
          && (err.status === HttpStatusCode.badRequest)) {
          const message = err.error.error || err.error.message;
          this.notificationsService.error(message);
        }
        this.dismissInProcess = false;
      });
  }

  public loadAlertScrollEnd() {
    if (this.waitToComplete === false) {
      this.waitToComplete = true;
      this.pageModel.pageNo = this.pageModel.pageNo + 1;
      this.getNotificationData();
    }
  }

  public mappData(data) {
    const dataToUpdate = data;
    for (let i = 0; i < dataToUpdate.length; i++) {
      dataToUpdate[i].documentDate = Moment(dataToUpdate[i].documentDate).format(this.appSettingSrvc.settings.dateFormate);
      dataToUpdate[i]['isActive'] = true;
      this.notifications.push(dataToUpdate[i]);
    }
  }

  public fetchLatestTime() {
    return this.notifications[0].alertTime;
  }

  ngAfterViewInit() {
    const search = $('.search-link');
    search.click(function () {
      const holder = $('.search-holder');
      holder.slideToggle();
    });

    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          if (event.url.toLocaleLowerCase().indexOf(Contains.searchRoute.toLocaleLowerCase()) === -1) {
            const bodyWidth = document.body.offsetWidth;
            const holder = $('.search-holder');
            if (holder && bodyWidth < 768) {
              holder.slideUp();
            }
          }
        }
      });
  }
}

