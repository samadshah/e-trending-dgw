import { LazyLoadingPageModel } from '../../../main/dashboard/models/activity-feed.model';

export class NotificationAlert {
    public alerts: Array<Alert>;
    public pagination: LazyLoadingPageModel;
}

export class Alert {
    public action: string;
    public alertTime: string;
    public documentId: string;
    public documentTitle: string;
    public alertHeader: string;
    public documentDate: string;
    public documentDateLabel: string;
    public message: string;
    public transactionId: number;
    public isActive: boolean;
    public isRead: string;
}


export class PostDismiss {
    public dismissAll: boolean;
    public LastAlertTime: string;
    public transactionId: number;
    public isDismiss: boolean;
}

