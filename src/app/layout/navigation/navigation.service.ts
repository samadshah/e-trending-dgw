import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';

import { environment } from '../../../environments/environment';

@Injectable()
export class NavigationService {
  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/Documents';

  }

  getCount(model): Observable<number> {
    return this.http.post<number>(environment.base_api_url + this.servicePath +
      '/notification-count', model, { headers: { ignoreLoadingBar: '' } });
  }

  dismiss(model) {
    const  payload = { lastAlertTime: model};
    return this.http.post(environment.base_api_url + this.servicePath + '/dismis-notification-count', payload);
  }
}
