import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications/dist';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { first } from 'rxjs/operators';

import { MessageText, RedirectUrl } from '../../../core/configuration/config';
import { PurcahseOrderResponse } from '../model/purchase-order-detail';
import { PurhaseOrderService } from './purchase-order-detail.service';


@Injectable()
export class PurchaseOrderDetailResolve implements Resolve<HttpResponse<PurcahseOrderResponse>> {

  constructor(private purhaseOrderService: PurhaseOrderService,
    private router: Router, private notificationsService: NotificationsService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<HttpResponse<PurcahseOrderResponse>> {
    const id = route.queryParams['id']; // route.paramMap.get('id');
    if (id) {
      return this.purhaseOrderService.getDetailData(id)
        // .pipe(first())
        .catch(() => {
          this.notificationsService.error(MessageText.unableToFetchData);
          this.router.navigate([RedirectUrl.pOrders]);
          return of(null);
        });
    } else {
      this.notificationsService.alert(MessageText.invaliedId);
      this.router.navigate([RedirectUrl.pOrders]);
    }
  }
}
