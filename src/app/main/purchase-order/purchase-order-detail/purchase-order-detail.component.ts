// tslint:disable
import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { PurcahseOrderResponse } from "../model/purchase-order-detail";
import { AttachmentService } from "../../../shared/services/attachment-service";
import {
  MetaPostModel
} from "../../../core/models/AttachmentMeta";
import { AppSettingService } from "../../../core/services/app-setting.service";
import {
  MessageText,
  RedirectUrl,
  HttpStatusCode
} from "../../../core/configuration/config";
import { PurhaseOrderService } from "./purchase-order-detail.service";
import { NotificationsService } from "angular2-notifications/dist";
import {
  HttpErrorResponse,
  HttpResponse,
  HttpHeaders
} from "@angular/common/http";
import { Location } from "@angular/common";
import * as Moment from "moment";

@Component({
  selector: "app-purchase-order-detail",
  templateUrl: "./purchase-order-detail.component.html",
  styleUrls: ["./purchase-order-detail.component.css"]
})
export class PurchaseOrderDetailComponent implements OnInit, OnDestroy {
  private id: any;
  private sub: any;
  model: PurcahseOrderResponse = new PurcahseOrderResponse();
  postAttachmentMeta: MetaPostModel = new MetaPostModel();
  tableId = "#data-table";
  gridInitialized = false;
  constructor(
    private route: ActivatedRoute,
    private attachmentService: AttachmentService,
    private purhaseOrderService: PurhaseOrderService,
    private notificationsService: NotificationsService,
    private router: Router,
    private location: Location,
    private appSettingSrvc: AppSettingService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(
      (data: { purchase: HttpResponse<PurcahseOrderResponse> }) => {
        if (data && data.purchase) {
          if (data.purchase.body.isSuccess === true) {
            this.refreshUI(data.purchase);
          } else {
            this.notificationsService.error(data.purchase.body.message);
            this.router.navigate([RedirectUrl.pOrders]);
          }
        }

        if (this.isCached(data.purchase)) {
          this.refreshData();
        }
      },
      error => {
        if (
          error instanceof HttpErrorResponse &&
          error.status === HttpStatusCode.badRequest
        ) {
          const message = error.error.error || error.error.message;
          this.notificationsService.error(message);
        }
        this.router.navigate([RedirectUrl.pOrders]);
      }
    );
  }

  refreshUI(data: HttpResponse<PurcahseOrderResponse>) {
    this.model = data.body;
    this.model.createdDateTime = Moment(this.model.createdDateTime).format(
      this.appSettingSrvc.settings.dateTimeFormate
    );
    this.model.total = this.setTwoNumberDecimal(this.model.total, 2);
    this.InitComponent();
  }

  refreshData() {
    this.route.queryParams.subscribe(result => {
      const id = result["id"];

      if (id) {
        this.purhaseOrderService
          .getDetailData(id)
          .last()
          .subscribe(
            data => {
              if (data) {
                this.refreshUI(data);
              }
            },
            err => {
              if (
                err instanceof HttpErrorResponse &&
                err.status === HttpStatusCode.badRequest
              ) {
                const message = err.error.error || err.error.message;
                this.notificationsService.error(message);
              }
            }
          );
      } else {
        this.notificationsService.alert(MessageText.invaliedId);
        this.router.navigate([RedirectUrl.pOrders]);
      }
    });
  }

  isCached(res: HttpResponse<PurcahseOrderResponse>): boolean {
    if (!res || res.headers instanceof HttpHeaders) {
      return false;
    }
    return true;
  }

  InitComponent() {
    const dtId1: any = $(this.tableId);
    if (this.gridInitialized) {
      this.tableDataRefresh(dtId1, this.model.line);
    } else {
      this.gridInitialized = true;
      this.bindGrid(this.model.line);
    }
  }

  tableDataRefresh(table, model) {
    table.DataTable().clear();
    table.DataTable().rows.add(model);
    table.DataTable().draw();
  }

  //!
  public downloadAttachment(item) {
    const payload = {
      Id: item.id,
      Purpose: "DOC"
    };

    this.attachmentService.downloadAttachment(payload).subscribe(
      resp => {
        const blob = new Blob([resp.content], { type: resp.content.type });
        const fileName = item.filename;

        const objectUrl = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement(
          "a"
        ) as HTMLAnchorElement;

        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.message;
          const msg = { isSuccess: true, message: message };
          // this.notifyServerMessage.emit(msg);
        } else if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.notFound
        ) {
          const msg = {
            isSuccess: false,
            message: MessageText.fileNotAvailable
          };
        }
      }
    );
  }

  public bindGrid(data) {
    const dtId: any = $(this.tableId);
    const tableWidget = dtId.DataTable({
      responsive: true,
      paging: data.length > this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: false,
      sDom: '<"clear">t<"F"p>',
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      order: [],
      columnDefs: [
        {
          className: "text-center",
          targets: [0, 1, 2],
          render: (value, type, row, index) => {
            return value;
          }
        },
        {
          className: "text-center",
          targets: 3,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(value, 2);
          }
        },
        {
          className: "text-center",
          targets: 4,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(value, 2);
          }
        },
        {
          className: "text-center",
          targets: 5,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(value, 2);
          }
        },
        {
          className: "text-center",
          targets: 6,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(row.lineTotal, 2);
          }
        },
        {
          className: "text-center",
          targets: 7,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(value, 2);
          }
        },
        {
          className: "text-center",
          targets: 8,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(row.lineTotal + row.vatAmount, 2);
          }
        }
      ],
      columns: [
        { data: "itemName", title: "Item Name", width: "25%" },
        {
          data: "deliveryDate",
          title: "Delivery Date",
          width: "10%",
          render: function(d) {
            return new Date(d).toLocaleDateString("en-US");
          }
        },
        { data: "quantity", title: "Quantity", width: "5%" },
        { data: "unitPrice", title: "Unit Price", width: "10%" },
        { data: "discountAmount", title: "Discount Amount", width: "10%" },
        { data: "discountPercent", title: "Discount Percentage", width: "10%" },
        { data: "amountExclVat", title: "Amount Excl. VAT", width: "10%" },
        { data: "vatAmount", title: "VAT Amount", width: "10%" },
        { data: "amountInclVat", title: "Amount Incl. VAT", width: "9%" }
      ],
      data: data
    });
  }

  ngOnDestroy() {
    const dtId: any = $(this.tableId);
    dtId.DataTable().destroy();
  }

  public getBackLink() {
    this.location.back();
  }

  public setTwoNumberDecimal(value, param) {
    value = value.toFixed(param);
    const data = value.split(".");
    value = parseFloat(data[0]).toLocaleString() + "." + data[1];
    return value;
  }
}
