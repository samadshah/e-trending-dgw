import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { PurcahseOrderResponse } from '../model/purchase-order-detail';

@Injectable()
export class PurhaseOrderService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/Documents';
    this.servicePath2 = '/api';
  }

  getDetailData(id): Observable<HttpResponse<PurcahseOrderResponse>> {
    return this.http.get<PurcahseOrderResponse>(environment.base_api_url + this.servicePath2 + '/PurchaseOrder/GetPurchaseOrderDetail?PurchaseOrderID=' + id, { observe: 'response' });
  }
}
