import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PurchaseOrderComponent } from './purchase-order.component';
import { PurchaseOrderListComponent } from './purchase-order-list/purchase-order-list.component';
import { PurchaseOrderDetailComponent } from './purchase-order-detail/purchase-order-detail.component';
import { PurchaseOrderDetailResolve } from './purchase-order-detail/purchase-order-detail.resolve';
import { CanDeactivateGuard } from '../../core/guards/can-deactivate-guard.service';

const routes: Routes = [
  {
    path: '',
    component: PurchaseOrderComponent,
    children: [
      {
        path: '',
        component: PurchaseOrderListComponent,
        canDeactivate: [CanDeactivateGuard],
      },
      {
        path: ':id',
        component: PurchaseOrderDetailComponent,
        data: {
          breadcrumb: 'Purchase Order'
        },
        resolve: {
          purchase: PurchaseOrderDetailResolve
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseOrderRoutingModule { }
