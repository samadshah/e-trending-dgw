// tslint:disable
import { PlatformLocation } from "@angular/common";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import * as Moment from "moment";
import { GridType, RedirectUrl } from "../../../core/configuration/config";
import { AppSettingService } from "../../../core/services/app-setting.service";

@Component({
  selector: "app-purchase-order-list",
  templateUrl: "./purchase-order-list.component.html",
  styleUrls: ["./purchase-order-list.component.css"]
})
export class PurchaseOrderListComponent implements OnInit {
  @ViewChild("purchaseOrderList")
  purchaseOrderList: any;

  columnsDefination: Array<any>;
  columns: Array<any>;
  serviceUrl = "/PurchaseOrder/GetPurchaseOrder";
  gridType = GridType.POList;
  title = "Purchase Orders";
  appbase;

  constructor(
    private router: Router,
    private platformLocation: PlatformLocation,
    private appSettingSrvc: AppSettingService
  ) {}

  ngOnInit() {
    this.appbase = (this.platformLocation as any).location.pathname;
    this.columnsDefination = [
      {
        targets: 2,
        render: (data, type, row) => {
          return Moment(data).format(this.appSettingSrvc.settings.dateFormate);
        }
      },
      {
        targets: 3,
        render: (data, type, row) => {
          return Moment(data).format(this.appSettingSrvc.settings.dateFormate);
        }
      },
      {
        className: "text-center",
        targets: 5,
        render: (data, type, row) => {
          return (
            `<a href="` +
            this.appbase +
            "/view?id=" +
            encodeURIComponent(row.purchaseOrder) +
            `"data-Id="` +
            encodeURIComponent(row.purchaseOrder) +
            `"data-url="` +
            RedirectUrl.pOrders +
            "/view" + // tslint:disable-next-line:max-line-length
            `" class="cursor-pointer actionView"  data-prop="purchaseOrder" data-toggle="tooltip" data-placement="bottom" title="View attached file"><i class="icon-view"></i></a>`
          );
        }
      }
    ];

    this.columns = [
      { data: "purchaseOrder", title: "Purchase Order", width: "20%" },
      { data: "documentTitle", title: "Document Title", width: "25%" },
      { data: "startDate", title: "Created Date", width: "15%" },
      { data: "endDate", title: "Delivery Date", width: "15%" },
      { data: "status", title: "Status", width: "15%" },
      { data: "view", title: "View", orderable: false, width: "10%" }
    ];
  }

  public viewDetail(data) {
    this.router.navigate([RedirectUrl.pOrders, data.purchaseOrder]);
  }

  public loadTooltip() {
    $('[data-toggle="tooltip"]').tooltip({
      trigger: "hover"
    });
  }
}
