import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaseOrderRoutingModule } from './purchase-order-routing.module';
import { PurchaseOrderListComponent } from './purchase-order-list/purchase-order-list.component';
import { PurchaseOrderDetailComponent } from './purchase-order-detail/purchase-order-detail.component';
import { PurchaseOrderComponent } from './purchase-order.component';
import { SharedModule } from '../../shared/shared.module';
import { PurchaseOrderDetailResolve } from './purchase-order-detail/purchase-order-detail.resolve';
import { PurhaseOrderService } from './purchase-order-detail/purchase-order-detail.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PurchaseOrderRoutingModule
  ],
  declarations: [PurchaseOrderListComponent, PurchaseOrderDetailComponent, PurchaseOrderComponent],
  providers: [PurhaseOrderService, PurchaseOrderDetailResolve]
})
export class PurchaseOrderModule {  }
