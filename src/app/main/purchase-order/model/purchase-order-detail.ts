import { Attachments } from '../../../core/models/attachments';
// import { AttachmentMetaType, MetaData, Meta } from '../../../core/models/AttachmentMeta';

export class PurcahseOrderResponse {
    createdDateTime: string;
    currency: string;
    documentTitle: string;
    purchaseOrder: string;
    status: string;
    total: string;
    attachments: Array<Attachments>;
    line: Array<Line>;
    isSuccess: boolean;
    message: string;
    //!
    // metaTagTypeList: Array<AttachmentMetaType>;
    // metaTags: Array<Meta>;
}


export class Line {
    public deliveryDate: Date;
    public discountAmount: number;
    public discountPercent: number;
    public itemName: string;
    public lineTotal: number;
    public quantity: number;
    public unitPrice: number;
    //!
    public vatAmountEntered: number;
    public vatAmountCalculated: number;
}
