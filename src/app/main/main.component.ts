import { Component, OnInit } from '@angular/core';
import { AppSettingService } from '../core/services/app-setting.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  headerPosition = '';
  showBreadcrumb = 0;
  constructor(private appSettingSrvc: AppSettingService) {
    this.showBreadcrumb = this.appSettingSrvc.settings.showbreadcrumb;
  }

  ngOnInit() {
    this.headerPosition =  this.appSettingSrvc.settings.headerPosition;
  }

}
