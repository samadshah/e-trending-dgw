import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReleaseOrderComponent } from './release-order.component';
import { ReleaseOrderListComponent } from './release-order-list/release-order-list.component';
import { ReleaseOrderDetailComponent } from './release-order-detail/release-order-detail.component';
import { ReleaseOrderDetailResolve } from './release-order-detail/resolve-order-detail.resolve';
import { CanDeactivateGuard } from '../../core/guards/can-deactivate-guard.service';

const routes: Routes = [{
  path: '',
  component: ReleaseOrderComponent,
  children: [
    {
      path: '',
      component: ReleaseOrderListComponent,
      canDeactivate: [CanDeactivateGuard],
    },
    {
      path: 'view',
      component: ReleaseOrderDetailComponent,
      data: {
        breadcrumb: 'Release Order'
      },
      resolve: {
        release: ReleaseOrderDetailResolve
      }
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReleaseOrderRoutingModule { }
