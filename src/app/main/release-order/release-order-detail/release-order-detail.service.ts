import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../../environments/environment';
import { ReleaseOrderResponse } from '../model/release-order-detail';


@Injectable()
export class ReleaseOrderService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/Documents';

  }

  getDetailData(id): Observable<HttpResponse<ReleaseOrderResponse>> {
    return this.http.get<ReleaseOrderResponse>(environment.base_api_url
      + this.servicePath + '/ReleaseOrders/' + id, { observe: 'response' });
  }
}
