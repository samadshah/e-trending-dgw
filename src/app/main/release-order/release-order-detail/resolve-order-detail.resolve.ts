import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications/dist';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { first } from 'rxjs/operators';

import { MessageText, RedirectUrl } from '../../../core/configuration/config';
import { ReleaseOrderResponse } from '../model/release-order-detail';
import { ReleaseOrderService } from './release-order-detail.service';



@Injectable()
export class ReleaseOrderDetailResolve implements Resolve<HttpResponse<ReleaseOrderResponse>> {

  constructor(private releaseOrderService: ReleaseOrderService, private router: Router,
    private notificationsService: NotificationsService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<HttpResponse<ReleaseOrderResponse>> {
    const id = route.queryParams['id']; // route.paramMap.get('id');
    if (id) {
      return this.releaseOrderService.getDetailData(id)
        // .pipe(
        // first()
        // )
        .catch(() => {
          this.notificationsService.error(MessageText.unableToFetchData);
          this.router.navigate([RedirectUrl.rOrders]);
          return of(null);
        });
    } else {
      alert(MessageText.invaliedId);
      this.router.navigate([RedirectUrl.rOrders]);
    }
  }
}
