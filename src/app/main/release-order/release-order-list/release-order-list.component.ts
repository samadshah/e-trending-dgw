import { PlatformLocation } from "@angular/common";
import { Component, OnInit, ViewChild } from "@angular/core";
import * as Moment from "moment";

import { RedirectUrl } from "../../../core/configuration/config";
import { AppSettingService } from "../../../core/services/app-setting.service";
@Component({
  selector: "app-release-order-list",
  templateUrl: "./release-order-list.component.html",
  styleUrls: ["./release-order-list.component.css"]
})
export class ReleaseOrderListComponent implements OnInit {
  @ViewChild("releaseOrderList")
  releaseOrderList: any;

  columnsDefination: Array<any>;
  columns: Array<any>;
  serviceUrl = "Documents/ReleaseOrders";
  gridType = 3;
  title = "Release Orders";
  appbase;

  constructor(
    private platformLocation: PlatformLocation,
    private appSettingSrvc: AppSettingService
  ) {}

  ngOnInit() {
    this.appbase = (this.platformLocation as any).location.pathname;
    this.columnsDefination = [
      {
        targets: 2,
        render: (data, type, row) => {
          return Moment(data).format(this.appSettingSrvc.settings.dateFormate);
          // return '' + new Date(data).toLocaleDateString();
        }
      },
      {
        targets: 3,
        render: (data, type, row) => {
          return Moment(data).format(this.appSettingSrvc.settings.dateFormate);
          // return '' + new Date(data).toLocaleDateString();
        }
      },
      {
        className: "text-center",
        targets: 5,
        render: (data, type, row) => {
          // return '<a href="' + this.appbase + '/' + row.releaseOrder +
          // tslint:disable-next-line:max-line-length
          //           '" data-prop="releaseOrder" data-toggle="tooltip" data-placement="bottom" title="View"><i class="icon-view"></i></a>';
          return (
            `<a href="` +
            this.appbase +
            "/view?id=" +
            encodeURIComponent(row.releaseOrder) +
            `"data-Id="` +
            encodeURIComponent(row.releaseOrder) +
            `"data-url="` +
            RedirectUrl.rOrders +
            "/view" +
            // tslint:disable-next-line:max-line-length
            `" class="cursor-pointer actionView"  data-prop="releaseOrder" data-toggle="tooltip" data-placement="bottom" title="View attached file"><i class="icon-view"></i></a>`
          );
        }
      }
    ];
    this.columns = [
      { data: "releaseOrder", title: "Release Order" },
      { data: "documentTitle", title: "Document Title" },
      { data: "createdDate", title: "Created Date" },
      { data: "deliveryDate", title: "Delivery Date" },
      { data: "status", title: "Status" },
      { data: "view", title: "View", orderable: false }
    ];
  }

  public viewDetail(data) {}

  public loadTooltip() {
    $('[data-toggle="tooltip"]').tooltip({
      trigger: "hover"
    });
  }
}
