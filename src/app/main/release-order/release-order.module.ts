import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ReleaseOrderRoutingModule } from './release-order-routing.module';
import { ReleaseOrderComponent } from './release-order.component';
import { ReleaseOrderListComponent } from './release-order-list/release-order-list.component';
import { ReleaseOrderDetailComponent } from './release-order-detail/release-order-detail.component';
import { ReleaseOrderService } from './release-order-detail/release-order-detail.service';
import { ReleaseOrderDetailResolve } from './release-order-detail/resolve-order-detail.resolve';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReleaseOrderRoutingModule
  ],
  declarations: [ReleaseOrderComponent, ReleaseOrderListComponent, ReleaseOrderDetailComponent],
  providers: [ReleaseOrderService, ReleaseOrderDetailResolve]
})
export class ReleaseOrderModule { }
