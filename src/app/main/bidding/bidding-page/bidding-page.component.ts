import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-bidding-page",
  templateUrl: "./bidding-page.component.html",
  styleUrls: ["./bidding-page.component.css"]
})
export class BiddingPageComponent implements OnInit {
  DD = [
    { id: 2, name: "Id Two here" },
    { id: 3, name: "Id Three here" },
    { id: 4, name: "Id Four here" },
    { id: 5, name: "Id Five here" }
  ];

  TechPropFiles = null;
  TechProp2Files = null;
  TechPropError = "";
  TechProp2Error = "";

  private model: Object = { date: { year: 2018, month: 10, day: 9 } };

  biddingForm = {
    rfeId: "",
    rfqTitle: "",
    expiryDateNTime: "",
    notes: ""
  };

  constructor() {}
  ngOnInit() {
    function getTimeRemaining(endtime) {
      const t = endtime.getTime() - new Date().getTime();
      const seconds = Math.floor((t / 1000) % 60);
      const minutes = Math.floor((t / 1000 / 60) % 60);
      const hours = Math.floor((t / (1000 * 60 * 60)) % 24);
      const days = Math.floor(t / (1000 * 60 * 60 * 24));
      return {
        total: t,
        days: days,
        hours: hours,
        minutes: minutes,
        seconds: seconds
      };
    }

    function initializeClock(id, endtime) {
      const clock = document.getElementById(id);
      const daysSpan = clock.querySelector(".days");
      const hoursSpan = clock.querySelector(".hours");
      const minutesSpan = clock.querySelector(".minutes");
      const secondsSpan = clock.querySelector(".seconds");

      function updateClock() {
        const t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days.toString();
        hoursSpan.innerHTML = ("0" + t.hours).slice(-2);
        minutesSpan.innerHTML = ("0" + t.minutes).slice(-2);
        secondsSpan.innerHTML = ("0" + t.seconds).slice(-2);

        if (t.total <= 0) {
          clearInterval(timeinterval);
        }
      }

      updateClock();
      const timeinterval = setInterval(updateClock, 1000);
    }
    const deadline = new Date(new Date().getTime() + 15 * 24 * 60 * 60 * 1000);
    initializeClock("clockdiv", deadline);
  }
  onSubmit() {
    if (!this.TechPropFiles) {
      this.TechPropError = "Required Files";
      // Stop submitting
    }
    if (!this.TechProp2Files) {
      this.TechProp2Error = "Required Files";
      // Stop submitting
    }

    // else submit biddingform and files
  }

  onTechPropChange(event) {
    const files = [].slice.call(event.target.files);
    this.TechPropError = "";
    for (const file of files) {
      const domain = file.name.split(".")[1];
      if (domain !== "txt") {
        this.TechPropError =
          "One or more File type not supported, Please upload again.";
        this.TechPropFiles = null;
      }
    }

    if (this.TechPropError === "") {
      this.TechPropFiles = files;
    }
  }

  onTechProp2Change(event) {
    const files = [].slice.call(event.target.files);
    this.TechProp2Error = "";
    for (const file of files) {
      const domain = file.name.split(".")[1];
      if (domain !== "txt") {
        this.TechProp2Error =
          "One or more File type not supported, Please upload again.";
        this.TechProp2Files = null;
      }
    }

    if (this.TechProp2Error === "") {
      this.TechProp2Files = files;
    }
  }
}
