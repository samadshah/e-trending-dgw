import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// import { MatSelectModule, MatInputModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
// import { NgxMyDatePickerModule } from 'ngx-mydatepicker';

import { SharedModule } from '../../shared/shared.module';

import { BiddingRoutingModule } from './bidding-routing.module';
import { BiddingComponent } from './bidding.component';
import { BiddingPageComponent } from './bidding-page/bidding-page.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BiddingRoutingModule,
    FormsModule,
    // NgxMyDatePickerModule,
    // MatSelectModule,
    // MatInputModule,
    // MatDatepickerModule,
    // MatNativeDateModule
  ],
  declarations: [BiddingComponent, BiddingPageComponent]
})
export class BiddingModule { }
