import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BiddingComponent } from './bidding.component';
import { BiddingPageComponent } from './bidding-page/bidding-page.component';

const routes: Routes = [
  {
    path: '',
    component: BiddingComponent,
    children: [
      {
        path: 'biddingpage',
        component: BiddingPageComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BiddingRoutingModule { }
