import { Location } from "@angular/common";
import {
  HttpErrorResponse,
  HttpHeaders,
  HttpResponse
} from "@angular/common/http";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationsService } from "angular2-notifications/dist";
import * as Moment from "moment";
import { last } from "rxjs/operators";
import { AppSettingService } from "../../../core/services/app-setting.service";
import {
  HttpStatusCode,
  MessageText,
  RedirectUrl
} from "../../../core/configuration/config";
import {
  AttachmentMetaType,
  Meta,
  MetaPostModel
} from "../../../core/models/AttachmentMeta";
import { UtiltiyService } from "../../../core/services/utility";
import { AttachmentService } from "../../../shared/services/attachment-service";
import { ContractResponse } from "../model/contract-detail";
import { ContractService } from "./contract-detail.service";

@Component({
  selector: "app-contract-detail",
  templateUrl: "./contract-detail.component.html",
  styleUrls: ["./contract-detail.component.css"]
})
export class ContractDetailComponent implements OnInit, OnDestroy {
  private id: any;
  private sub: any;
  model: ContractResponse = new ContractResponse();
  postAttachmentMeta: MetaPostModel = new MetaPostModel();
  tableId = "#data-table";
  gridInitialized = false;
  constructor(
    private route: ActivatedRoute,
    private attachmentService: AttachmentService,
    private utiltiy: UtiltiyService,
    private contractService: ContractService,
    private notificationsService: NotificationsService,
    private router: Router,
    private location: Location,
    private appSettingSrvc: AppSettingService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(
      (data: { contract: HttpResponse<ContractResponse> }) => {
        if (data && data.contract) {
          this.refreshUI(data.contract);
        }

        if (this.isCached(data.contract)) {
          this.refreshData();
        }
      },
      error => {
        if (
          error instanceof HttpErrorResponse &&
          error.status === HttpStatusCode.badRequest
        ) {
          const message = error.error.error || error.error.message;
          this.notificationsService.error(message);
        }
        this.router.navigate([RedirectUrl.contracts]);
      }
    );
  }

  refreshUI(data: HttpResponse<ContractResponse>) {
    this.model = data.body;
    this.model.startDate = Moment(this.model.startDate).format(
      this.appSettingSrvc.settings.dateTimeFormate
    );
    this.model.endDate = Moment(this.model.endDate).format(
      this.appSettingSrvc.settings.dateTimeFormate
    );
    this.model.total = this.setTwoNumberDecimal(this.model.total, 2);
    this.InitComponent();
  }

  refreshData() {
    this.route.queryParams.subscribe(result => {
      const id = result["id"];

      if (id) {
        this.contractService
          .getDetailData(id)
          .pipe(last())
          .subscribe(
            data => {
              if (data) {
                this.refreshUI(data);
              }
            },
            err => {
              if (
                err instanceof HttpErrorResponse &&
                err.status === HttpStatusCode.badRequest
              ) {
                const message = err.error.error || err.error.message;
                this.notificationsService.error(message);
              }
            }
          );
      } else {
        this.notificationsService.alert(MessageText.invaliedId);
        this.router.navigate([RedirectUrl.contracts]);
      }
    });
  }

  isCached(res: HttpResponse<ContractResponse>): boolean {
    if (!res || res.headers instanceof HttpHeaders) {
      return false;
    }
    return true;
  }

  InitComponent() {
    const dtId1: any = $(this.tableId);
    if (this.gridInitialized) {
      this.tableDataRefresh(dtId1, this.model.lines);
    } else {
      this.gridInitialized = true;
      this.bindGrid(this.model.lines);
    }
  }

  tableDataRefresh(table, model) {
    table.DataTable().clear();
    table.DataTable().rows.add(model);
    table.DataTable().draw();
  }

  ngOnDestroy() {
    const dtId: any = $(this.tableId);
    dtId.DataTable().destroy();
  }

  public downloadAttachment(item) {
    this.postAttachmentMeta.id = item.id;
    this.postAttachmentMeta.metaTypes = new Array<AttachmentMetaType>();
    this.postAttachmentMeta.metaTags = new Array<Meta>();
    for (const entry of this.model.metaTagTypeList) {
      if (entry.docType === item.docType) {
        this.postAttachmentMeta.metaTypes.push(entry);
      }
    }
    for (const entry of this.model.metaTags) {
      if (entry.docType === item.docType) {
        this.postAttachmentMeta.metaTags.push(entry);
      }
    }

    this.attachmentService
      .downloadAttachment(this.postAttachmentMeta)
      .subscribe(resp => {
        const blob = new Blob([resp.content], { type: resp.content.type });
        const fileName = item.name;

        if (this.utiltiy.isBrowserIe() === false) {
          const objectUrl = URL.createObjectURL(blob);
          const a: HTMLAnchorElement = document.createElement(
            "a"
          ) as HTMLAnchorElement;

          a.href = objectUrl;
          a.download = fileName;
          document.body.appendChild(a);
          a.click();

          document.body.removeChild(a);
          URL.revokeObjectURL(objectUrl);
        } else {
          window.navigator.msSaveBlob(blob, fileName);
        }
      });
  }

  public bindGrid(data) {
    const dtId: any = $(this.tableId);
    const tableWidget = dtId.DataTable({
      responsive: true,
      paging: data.length > this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: false,
      sDom: '<"clear">t<"F"p>',
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      order: [],
      columnDefs: [
        {
          className: "text-center",
          targets: 2,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(value, 2);
          }
        },
        {
          className: "text-center",
          targets: 3,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(value, 2);
          }
        },
        {
          className: "text-center",
          targets: 7,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(value, 2);
          }
        }
      ],
      columns: [
        { data: "itemName", title: "Item Name" },
        { data: "quantity", title: "Quantity" },
        { data: "unitPrice", title: "Unit Price" },
        { data: "discountAmount", title: "Discount Amount" },
        { data: "discountPercent", title: "Discount Percentage" },
        { data: "type", title: "Type" },
        { data: "amendmentNumber", title: "Amendment Number" },
        { data: "lineTotal", title: "Line Total" }
      ],
      data: data
    });
  }
  public getBackLink() {
    this.location.back();
  }

  public setTwoNumberDecimal(value, param) {
    value = value.toFixed(param);
    const data = value.split(".");
    value = parseFloat(data[0]).toLocaleString() + "." + data[1];
    return value;
  }
}
