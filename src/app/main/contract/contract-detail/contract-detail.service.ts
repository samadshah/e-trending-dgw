import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';

import { environment } from '../../../../environments/environment';
import { ContractResponse } from '../model/contract-detail';

@Injectable()
export class ContractService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/Documents';

  }

  getDetailData(id): Observable<HttpResponse<ContractResponse>> {
    return this.http.get<ContractResponse>(environment.base_api_url
      + this.servicePath + '/Contracts/' + id, { observe: 'response' });
  }
}
