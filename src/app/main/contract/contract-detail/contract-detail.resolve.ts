import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications/dist';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { first } from 'rxjs/operators';

import { MessageText, RedirectUrl } from '../../../core/configuration/config';
import { ContractResponse } from '../model/contract-detail';
import { ContractService } from './contract-detail.service';

@Injectable()
export class ContractDetailResolve implements Resolve<HttpResponse<ContractResponse>> {

  constructor(private contractService: ContractService, private router: Router,
    private notificationsService: NotificationsService) { }
  resolve(route: ActivatedRouteSnapshot): Observable<HttpResponse<ContractResponse>> {
    const id = route.queryParams['id']; //route.paramMap.get('id');
    if (id) {
      return this.contractService.getDetailData(id)
        // .pipe(
        // first()
        // )
        .catch(() => {
          this.notificationsService.error(MessageText.unableToFetchData);
          this.router.navigate([RedirectUrl.contracts]);
          return of(null);
        });
    } else {
      this.notificationsService.alert(MessageText.invaliedId);
      this.router.navigate([RedirectUrl.contracts]);
    }
  }
}
