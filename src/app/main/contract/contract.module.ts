import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ContractRoutingModule } from './contract-routing.module';
import { ContractComponent } from './contract.component';
import { ContractListComponent } from './contract-list/contract-list.component';
import { ContractDetailComponent } from './contract-detail/contract-detail.component';
import { ContractService } from './contract-detail/contract-detail.service';
import { ContractDetailResolve } from './contract-detail/contract-detail.resolve';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ContractRoutingModule
  ],
  declarations: [ContractComponent, ContractListComponent, ContractDetailComponent],
  providers: [ContractService, ContractDetailResolve]
})
export class ContractModule { }
