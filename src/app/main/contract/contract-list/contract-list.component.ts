import { PlatformLocation } from "@angular/common";
import { Component, OnInit, ViewChild } from "@angular/core";
import * as Moment from "moment";
import { AppSettingService } from "../../../core/services/app-setting.service";
import { RedirectUrl } from "../../../core/configuration/config";

@Component({
  selector: "app-contract-list",
  templateUrl: "./contract-list.component.html",
  styleUrls: ["./contract-list.component.css"]
})
export class ContractListComponent implements OnInit {
  @ViewChild("contractList")
  contractList: any;

  columnsDefination: Array<any>;
  columns: Array<any>;
  serviceUrl = "Documents/Contracts";
  gridType = 5;
  title = "Contracts";
  appbase;

  constructor(
    private platformLocation: PlatformLocation,
    private appSettingSrvc: AppSettingService
  ) {}

  ngOnInit() {
    this.appbase = (this.platformLocation as any).location.pathname;
    this.columnsDefination = [
      {
        targets: 2,
        render: (data, type, row) => {
          return Moment(data).format(this.appSettingSrvc.settings.dateFormate);
        }
      },
      {
        targets: 3,
        render: (data, type, row) => {
          return Moment(data).format(this.appSettingSrvc.settings.dateFormate);
        }
      },
      {
        className: "text-center",
        targets: 5,
        render: (data, type, row) => {
          // tslint:disable-next-line:max-line-length
          return (
            `<a href="` +
            this.appbase +
            "/view?id=" +
            encodeURIComponent(row.contractId) +
            `"data-Id="` +
            encodeURIComponent(row.contractId) +
            `"data-url="` +
            RedirectUrl.contracts +
            "/view" +
            // tslint:disable-next-line:max-line-length
            `" class="cursor-pointer actionView"  data-prop="contractId" data-toggle="tooltip" data-placement="bottom" title="View attached file"><i class="icon-view"></i></a>`
          );
        }
      }
    ];
    this.columns = [
      { data: "contractId", title: "Contract Id" },
      { data: "documentTitle", title: "Document Title" },
      { data: "startDate", title: "Start Date" },
      { data: "endDate", title: "End Date" },
      { data: "status", title: "Status" },
      { data: "view", title: "View", orderable: false }
    ];
  }

  public viewDetail(data) {}

  public loadTooltip() {
    $('[data-toggle="tooltip"]').tooltip({
      trigger: "hover"
    });
  }
}
