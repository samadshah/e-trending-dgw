import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractComponent } from './contract.component';
import { ContractListComponent } from './contract-list/contract-list.component';
import { ContractDetailComponent } from './contract-detail/contract-detail.component';
import { ContractDetailResolve } from './contract-detail/contract-detail.resolve';
import { CanDeactivateGuard } from '../../core/guards/can-deactivate-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ContractComponent,
    children: [
      {
        path: '',
        component: ContractListComponent,
        canDeactivate: [CanDeactivateGuard],
      },
      {
        path: 'view',
        component: ContractDetailComponent,
        data: {
          breadcrumb: 'Contract'
        },
        resolve: {
          contract: ContractDetailResolve
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractRoutingModule { }
