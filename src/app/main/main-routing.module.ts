import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AuthGuard } from '../core/guards/auth.guard';
import { MainComponent } from './main.component';
import { CanDeactivateGuard } from '../core/guards/can-deactivate-guard.service';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
     children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        loadChildren: 'app/main/dashboard/dashboard.module#DashboardModule',
        data: {
          breadcrumb: 'Home'
        }
      },
      {
        path: 'contacts',
        loadChildren: 'app/main/contact/contact.module#ContactModule',
        data: {
          breadcrumb: 'Contacts'
        }
      },
      {
        path: 'p-orders',
        loadChildren: 'app/main/purchase-order/purchase-order.module#PurchaseOrderModule',
        data: {
          breadcrumb: 'Purchase Orders'
        }
      },
      {
        path: 'rfqs',
        loadChildren: 'app/main/rfq/rfq.module#RfqModule',
        data: {
          breadcrumb: 'Request For Quotations'
        }
      },
      {
        path: 'vendor-profile',
        loadChildren: 'app/main/vendor-profile/vendor-profile.module#VendorProfileModule',
        data: {
          breadcrumb: 'Vendor Profile'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CanDeactivateGuard]
})

export class MainRoutingModule { }
