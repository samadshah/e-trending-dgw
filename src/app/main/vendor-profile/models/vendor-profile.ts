// tslint:disable
import { AttachmentMetaType, MetaData, Meta, Attachment, UploadAttachmentMeta } from '../../../core/models/AttachmentMeta';
import { Lookup } from '../../../core/models/lookup';

//!
export class VendorProfile {
    public  address: Array<VendorAddress>;
    public  categoryInformation: Array<VendorCategory>;
    public  contactInformation: Array<VendorContact>;
    public  organizationDetail: OrganizationDetail;
    public  attachments: Array<Attachment>;
    // public  metaTagTypeList: Array<AttachmentMetaType>;
    // public  metaTags: Array<Meta>;
    // public  uploadAttachmentMeta: Array<UploadAttachmentMeta>;
}

export class VendorAddress {
    constructor() {
      this.transactionId = 0;
      this.primary = 'No';
      this.canEdit = '';
      this.canDelete = '';
      this.isDeleted = false;
      this.isUpdated = false;
    }
    public  nameorDescription: string;
    public  address: string;
    public  purpose: Array<Lookup>;
    public  primary: string;
    public  building: string;
    public  city: string;
    public  countryRegion: string;
    public  county: string;
    public  state: string;
    public  street: string;
    public  streetNo: string;
    public  transactionId: number;
    public  zipPostalCode: string;
    public  canEdit: string;
    public  canDelete: string;
    public  isDeleted: boolean;
    public  isUpdated: boolean;
}

export class VendorCategory {
    public  categoryHierarchy: string;
    public  categoryName: string;
    public  categoryStatus: string;
    public  effectiveDate: string;
    public  expirationDate: string;
}

export class VendorContact {
    constructor() {
        this.transactionId = 0;
        this.primary = 'No';
        this.canEdit = '';
        this.canDelete = '';
        this.isDeleted = false;
        this.isUpdated = false;
      }
    public  contactNumberAddress: string;
    public  transactionId: number;
    public  description: string;
    public  primary: string;
    public  type: string;
    public  canEdit: string;
    public  canDelete: string;
    public  isDeleted: boolean;
    public  isUpdated: boolean;
}

export class OrganizationDetail {
    public  address: string;
    public  addressRecId: string;
    public  arabicCompanyName: string;
    public  city: string;
    public  country: string;
    public  currency: string;
    public  effectiveDate: string;
    public  email: string;
    public  emailRecId: string;
    public  expirationDate: string;
    public  fax: string;
    public  faxRecId: number;
    public  internetAddress: string;
    public  languagePreference: string;
    public  name: string;
    public  phoneRecId: number;
    public  telephone: string;
    public  tradeLicenseNo: string;
    public  passportNumber: string;
    public  urlRecId: number;
    public  vendorAccount: string;
    public  yearsCompanyOperatesinUAE: string;
    public  vatDisclaimer: string;
    public  vatNotRegisteredType: string;
    public  vatNumber: string;
    public  vatReason: string;
    public  vatRegistered: string;
    public  language: string;
    public  organizationType: number;
    public  countryRegionName: string;
    public  currencyName: string;
    public  website: string;
    //!!
    public  tlIssuingEmirate: string;
    public  registrationType: string;
    public  makaniNo: string;
}


export class PostVendorProfile {
    public  addresses: Array<PostVendorAddress>;
    public  contactInformation: Array<PostVendorContact>;
    public  organizationDetail: PostOrganizationDetail;
}

export class PostVendorAddress {
    public  address: string;
    public  building: string;
    public  city: string;
    public  countryRegion: string;
    public  county: string;
    public  nameorDescription: string;
    public  primary: string;
    public  purpose: Array<Lookup>;
    public  state: string;
    public  street: string;
    public  streetNo: string;
    public  transactionId: number;
    public  zipPostalCode: string;
}

export class PostVendorContact {
    public  contactNumberAddress: string;
    public  transactionId: number;
    public  description: string;
    public  primary: string;
    public  type: string;
}

export class PostOrganizationDetail {
    public  companyRegisteredinDED: string;
    public  currency: string;
    public  effectiveDate: string;
    public  expirationDate: string;
    public  languagePreference: string;
    public  tradeLicenseNo: string;
    public  yearsCompanyOperatesinUAE: string;
}
