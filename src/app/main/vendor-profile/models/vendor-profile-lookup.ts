import { Lookup } from '../../../core/models/lookup';

export class VendorProfileLookup {
  public countries: Array<Lookup>;
  public addressPurpose: Array<Lookup>;
  public companyOperateUAE: Array<Lookup>;
  public currency: Array<Lookup>;
  public languages: Array<Lookup>;
  public zipCode: Array<Lookup>;
  public cities: Array<Lookup>;
  //!
  public logisticsTypes: Array<Lookup>;
  public vatRegistered: Array<Lookup>;
  public vatType: Array<Lookup>;
}
