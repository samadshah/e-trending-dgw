import { Location } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications/dist';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { last } from 'rxjs/operators';

import { MessageText, RedirectUrl } from '../../../core/configuration/config';
import { VendorProfileLookup } from '../models/vendor-profile-lookup';
import { VendorProfileService } from '../vendor-profile.service';

@Injectable()
export class VendorProfileEditResolve implements Resolve<HttpResponse<VendorProfileLookup>> {

  constructor(private profileService: VendorProfileService,
    private router: Router,
    private location: Location,
    private notificationsService: NotificationsService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<HttpResponse<VendorProfileLookup>> {
    return this.profileService.getVendorProfile()
      // .pipe(last())
      .catch(() => {
        this.notificationsService.error(MessageText.unableToFetchData);
        this.router.navigate([RedirectUrl.profile]);
        return of(null);
      });
  }
}
