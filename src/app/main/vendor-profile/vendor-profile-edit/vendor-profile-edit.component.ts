import { async } from "@angular/core/testing";
// tslint:disable
import { Location } from "@angular/common";
import { HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { NotificationsService } from "angular2-notifications/dist";
import * as Moment from "moment";
import { IMyDpOptions } from "mydatepicker";
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { Subject } from "rxjs/Subject";
import swal from "sweetalert2";
import { AppSettingService } from "../../../core/services/app-setting.service";
import {
  HttpStatusCode,
  MessageText,
  YesNo
} from "../../../core/configuration/config";
import {
  Attachment,
  AttachmentMetaType,
  Meta,
  MetaPostModel
} from "../../../core/models/AttachmentMeta";
import { Lookup } from "../../../core/models/lookup";
import { UtiltiyService } from "../../../core/services/utility";
import { AttachmentService } from "../../../shared/services/attachment-service";
import { LookupService } from "../../../shared/services/lookup-service";
import {
  OrganizationDetail,
  VendorAddress,
  VendorContact,
  VendorProfile
} from "../models/vendor-profile";
import { VendorProfileLookup } from "../models/vendor-profile-lookup";
import { VendorProfileService } from "../vendor-profile.service";

@Component({
  selector: "app-vendor-profile-edit",
  templateUrl: "./vendor-profile-edit.component.html",
  styleUrls: ["./vendor-profile-edit.component.css"]
})
export class VendorProfileEditComponent implements OnInit, OnDestroy {
  @ViewChild("ngxCustomUploadOne")
  ngxCustomUploadOne: any;

  @ViewChild("ContactInfoEdit") ContactInfoEditForm;
  @ViewChild("addAddressEditForm") addAddressEditForm;
  @ViewChild("vendorProfileForm") vendorProfileForm;

  profile: VendorProfile = new VendorProfile();
  tableIdAdress = "#data-table-Adress";
  tableIdContactInfo = "#data-table-ContactInfo";
  tableIdAttachment = "#data-table-Attachment";
  tableIdCategoryInfo = "#data-table-CategoryInfo";
  addressPoupFlag = false;
  addressPoupFlagEdit = "none";
  contactInformationPoupFlag = false;
  contactInformationPoupFlagEdit = "none";
  AttachmentsPopupFlag = "none";
  serverMessage: any = "";
  empty = "__";
  YesNo = YesNo;
  typeEffectiveDateValidation = false;
  typeExpiryeDateValidation = false;

  vendorAddress: VendorAddress = new VendorAddress();
  vendorAddressEdit: VendorAddress = new VendorAddress();
  deletevendorAddresses: Array<VendorAddress> = [];

  vendorContact: VendorContact = new VendorContact();
  vendorContactEdit: VendorContact = new VendorContact();
  deletedContacts: Array<VendorContact> = [];

  lookups: VendorProfileLookup = new VendorProfileLookup();
  dependentLookups: VendorProfileLookup = new VendorProfileLookup();
  vendorAddressRowIndex: number;
  contactInfoRowIndex: number;
  deletedAttachment: Array<Attachment> = [];
  vendorProfileBtnDisable = false;
  vatTypes = [];

  dateDifference = 0;
  unSavedChanges = false;
  isFormSubmitted = false;

  gridInitialized = false;

  left = "0px";
  top = "0px";
  display = "inline";

  // multi select things
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  currencySetting: any;
  languageSetting: any;
  VatSetting: any;
  countrySetting: any;
  citySetting: any = this.singleSelectSetting("Select City", false);
  zipCodeSetting: any;
  typeSetting: any;
  logisticTypeSetting: any;
  purposeSetting = this.mutliselectSetting("Select Purpose");

  countries = [];
  selectedCountry = [];

  cities = [];
  selectedCity = [];

  zipCodes = [];
  selectedZipCode = [];

  logisticTypes = [];
  SelectedLogisticType = [];

  uploadMeta = [];
  selectedUploadMeta = [];

  languages = [];
  selectedLanguage = [];

  currencies = [];
  selectedCurrency = [];

  vats = [];
  selectedVat = [];

  canMarkPrimayAddress = false;
  numberValidation = false;
  public myDatePickerOptions: IMyDpOptions = {};

  disclaimerText = "";
  vatTypeValidation = false;
  vatReasonValidation = false;
  vatNumberValidation = false;
  vatDisclaimerValidation = false;
  datePickerSyntax = "";

  public model1: any = { date: { year: Moment(), day: 28, month: 12 } };
  public model2: any = { date: { year: 2018, day: 1, month: 1 } };
  canGo: Subject<boolean> = new Subject<boolean>();
  constructor(
    private route: ActivatedRoute,
    private vendorProfileService: VendorProfileService,
    private attachmentService: AttachmentService,
    private lookupService: LookupService,
    private notificationsService: NotificationsService,
    private utiltiy: UtiltiyService,
    private location: Location,
    private appSettingSrvc: AppSettingService
  ) {
    this.profile.organizationDetail = new OrganizationDetail();
    this.myDatePickerOptions = {
      dateFormat: this.appSettingSrvc.settings.datePicker.toString(),
      showClearDateBtn: false,
      showTodayBtn: false,
      alignSelectorRight: true,
      allowSelectionOnlyInCurrentMonth: false
    };
    this.datePickerSyntax = this.appSettingSrvc.settings.datePicker;
  }

  ngOnInit() {
    this.vendorProfileService.termAndCondition().subscribe(
      res => {
        //!
        const resp = JSON.parse(res.data);
        this.disclaimerText = resp.VATDisclaimer;
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.message;
          this.notificationsService.error(message);
        }
      }
    );

    //!
    const attachConfig = this.attachmentService.getConfig();
    this.ngxCustomUploadOne.maxSize = attachConfig.fileSize;
    this.ngxCustomUploadOne.fileExt = attachConfig.fileTypes;
    this.ngxCustomUploadOne.charactersNotAllowedFileName =
      attachConfig.disallowedCharacters;
    this.ngxCustomUploadOne.charactersNotAllowedMessage =
      attachConfig.disallowedCharactersError;

    this.route.data.subscribe(
      (data: { profile: HttpResponse<VendorProfile> }) => {
        if (data.profile) {
          this.lookupService.getVendorProfileLookups().subscribe(
            value => {
              if (value) {
                this.lookups = value;
                this.fillCountries(this.lookups.countries);
                //!
                this.fillContactInfoTypes(this.lookups.logisticsTypes);
                this.fillLanguages(this.lookups.languages);
                this.fillMultiSelect(this.lookups.addressPurpose, []);
                this.fillCurrencies(this.lookups.currency);
                this.fillVateRegistered(this.lookups.vatRegistered);
                this.fillVateTypes(this.lookups.vatType);
                this.assignVatDropdown();
              }
            },
            err => {
              if (
                err instanceof HttpErrorResponse &&
                err.status === HttpStatusCode.badRequest
              ) {
                const message = err.error.error || err.error.message;
                this.notificationsService.error(message);
              }
            }
          );
          this.profile = data.profile.body;
          this.model1 = this.getFormattedDate(
            this.profile.organizationDetail.effectiveDate
          );
          this.model2 = this.getFormattedDate(
            this.profile.organizationDetail.expirationDate
          );
          this.bindGrid(this.profile);
        }
      }
    );
  }

  public fillMultiSelect(data, selected) {
    this.dropdownList = [];
    for (let i = 0; i < data.length; i++) {
      this.dropdownList.push({ id: data[i].value, itemName: data[i].name });
    }

    for (let i = 0; i < selected.length; i++) {
      this.selectedItems.push({
        id: selected[i].value,
        itemName: selected[i].name
      });
    }
  }

  public fillSelectedItem(selected) {
    for (let i = 0; i < selected.length; i++) {
      this.selectedItems.push({
        id: selected[i].value,
        itemName: selected[i].name
      });
    }
  }

  private bindGrid(profile) {
    const dId1: any = $(this.tableIdAdress);
    const dId2: any = $(this.tableIdContactInfo);
    const dId3: any = $(this.tableIdAttachment);
    const dId4: any = $(this.tableIdCategoryInfo);

    //!
    this.addressGrid(profile.address);
    this.contactInformationGrid(profile.contactInformation);
    this.categoryInformationGrid(profile.categoryInformation);
    //!
    this.attachmentsGrid(profile.attachments);
    //!
    // this.fillUploadMeta(profile.uploadAttachmentMeta);
    this.fillUploadMeta(this.vendorProfileService.getUploadMeta());
  }

  tableDataRefresh(table, model) {
    table.DataTable().clear();
    table.DataTable().rows.add(model);
    table.DataTable().draw();
  }

  setUpRequiredColumns(data) {
    for (let i = 0; i < data.length; i++) {
      data[i]["isUpdated"] = false;
    }
    return data;
  }

  private addressGrid(address) {
    const addresses = this.setUpRequiredColumns(address);
    const dtId1: any = $(this.tableIdAdress);
    const tableWidget1 = dtId1.DataTable({
      responsive: true,
      paging: false,
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: false,
      lengthChange: false,
      sortable: false,
      ordering: false,
      autoWidth: false,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      columnDefs: [
        {
          targets: [4, 5, 6, 7, 8, 9, 10, 13, 14, 15],
          visible: false
        },
        {
          className: "text-center",
          targets: 11,
          render: function(data, type, row, index) {
            return row.canEdit === "Yes"
              ? '<a id="editAddress" style="cursor: pointer" class="edit" data-index=' +
                  index.row +
                  '><i class="icon-edit"></i></a>'
              : "";
          }
        },
        {
          className: "text-center",
          targets: 12,
          render: function(data, type, row, index) {
            return row.canDelete === "Yes"
              ? '<a id="deleteAddress" style="cursor: pointer" class="delete" data-index=' +
                  index.row +
                  '><i class="icon-delete"></i></a>'
              : "";
          }
        },
        {
          className: "text-center",
          targets: 3,
          render: function(value, type, row, index) {
            return row.primary === "Yes"
              ? '<div class="styledCheckbox checkbox"><input type="checkbox" id="primarybox' +
                  index.row +
                  '" name="primary" value="primary" disabled checked><label for="primarybox' +
                  index.row +
                  '"><span></span></label></div>'
              : '<div class="styledCheckbox checkbox"><input type="checkbox" id="primarybox' +
                  index.row +
                  '" name="primary" value="primary" disabled><label for="primarybox' +
                  index.row +
                  '"><span></span></label></div>';
          }
        }
      ],
      columns: [
        {
          data: "nameorDescription",
          title: "Name or Description",
          class: "min-phone-l",
          width: "40%"
        },
        {
          data: "address",
          title: "Address",
          class: "min-tablet-p",
          width: "25%"
        },
        {
          data: "purpose[, ].name",
          title: "Purpose",
          class: "min-tablet-l",
          width: "20%"
        },
        {
          data: "primary",
          title: "Primary",
          orderable: false,
          class: "min-tablet-l text-center",
          width: "5%"
        },
        { data: "building", title: "building" },
        { data: "city", title: "city" },
        { data: "countryRegion", title: "countryRegion" },
        { data: "street", title: "street" },
        { data: "streetNo", title: "streetNo" },
        { data: "zipPostalCode", title: "zipPostalCode" },
        { data: "transactionId", title: "transactionId" },
        {
          data: "Edit",
          title: "Edit",
          class: "min-tablet-l text-center",
          width: "5%"
        },
        {
          data: "Delete",
          title: "Delete",
          class: "min-tablet-l text-center",
          width: "5%"
        },
        { data: "canEdit", title: "" },
        { data: "canDelete", title: "" },
        { data: "isUpdated", title: "" }
      ],
      data: addresses
    });

    dtId1.on("click", "#editAddress", e => {
      this.canMarkPrimayAddress = false;
      e.preventDefault();
      this.vendorAddressEdit = new VendorAddress();
      this.selectedItems = [];
      this.selectedCountry = [];
      this.selectedCity = [];
      this.selectedZipCode = [];
      const index = e.currentTarget.attributes["data-index"].nodeValue;
      const data = this.findDataByIndex(index, this.tableIdAdress);
      const copiedData = JSON.parse(JSON.stringify(data));
      if (copiedData.primary === "No") {
        this.canMarkPrimayAddress = true;
      }
      // make country selected;
      const currentCountry = this.lookups.countries.filter(
        x => x.value === data.countryRegion
      );
      this.selectedCountry = this.getSelectedItemOnEdit(currentCountry);
      this.countrySelection(data.countryRegion, "edit");

      this.vendorAddressEdit = copiedData as VendorAddress;
      const value = { event: e, model: data };
      this.EditPopUpPosition(value);
      this.addressPoupFlagEdit = "block";
      this.vendorAddressRowIndex = index;
      this.fillSelectedItem(this.vendorAddressEdit.purpose);
    });

    dtId1.on("click", "#deleteAddress", e => {
      e.preventDefault();
      const index = e.currentTarget.attributes["data-index"].nodeValue;
      const dtId: any = $(this.tableIdAdress);
      const widget = dtId.DataTable();
      const data = this.findDataByIndex(index, this.tableIdAdress);
      data["isUpdated"] = true;
      data["isDeleted"] = true;
      this.addressBin(data);
      widget.row(index).remove();
      widget
        .rows()
        .invalidate("data")
        .draw(false);
      this.unSavedChanges = true;
    });
  }

  private addressBin(data) {
    const address = data as VendorAddress;
    if (address.transactionId > 0) {
      address.isDeleted = true;
      address.isUpdated = true;
      this.deletevendorAddresses.push(address);
    }
  }

  private contactInformationGrid(contactInfo) {
    const contactInfoes = this.setUpRequiredColumns(contactInfo);
    const dtId2: any = $(this.tableIdContactInfo);
    const tableWidget1 = dtId2.DataTable({
      sorting: [],
      responsive: true,
      paging: false,
      length: this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: false,
      lengthChange: false,
      sortable: false,
      ordering: false,
      autoWidth: false,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      columnDefs: [
        {
          className: "text-center",
          targets: 3,
          render: function(value, type, row, index) {
            return row.primary === "Yes"
              ? '<div class="styledCheckbox checkbox"><input type="checkbox" id="primarybox' +
                  index.row +
                  '" name="primary" value="primary" disabled checked><label for="primarybox' +
                  index.row +
                  '"><span></span></label></div>'
              : '<div class="styledCheckbox checkbox"><input type="checkbox" id="primarybox' +
                  index.row +
                  '" name="primary" value="primary" disabled><label for="primarybox' +
                  index.row +
                  '"><span></span></label></div>';
          }
        },
        {
          targets: [6, 7, 8, 9],
          visible: false
        },
        {
          className: "text-center",
          targets: 4,
          render: function(data, type, row, index) {
            return (
              '<a id="editContactInfo" style="cursor: pointer" class="edit" data-index=' +
              index.row +
              '><i class="icon-edit"></i></a>'
            );
          }
        },
        {
          className: "text-center",
          targets: 5,
          render: function(data, type, row, index) {
            return (
              '<a id="deleteContactInfo" style="cursor: pointer"  class="delete" data-index=' +
              index.row +
              '><i class="icon-delete"></i></a>'
            );
          }
        }
      ],
      columns: [
        {
          data: "description",
          title: "Description",
          class: "min-phone-l",
          width: "40%"
        },
        { data: "type", title: "Type", class: "min-tablet-p", width: "25%" },
        {
          data: "contactNumberAddress",
          title: "Contact Number/Address",
          class: "min-tablet-l",
          width: "20%"
        },
        {
          data: "primary",
          title: "Primary",
          orderable: false,
          class: "min-tablet-l text-center",
          width: "5%"
        },
        {
          data: "Edit",
          title: "Edit",
          class: "min-tablet-l text-center",
          width: "5%"
        },
        {
          data: "Delete",
          title: "Delete",
          class: "min-tablet-l text-center",
          width: "5%"
        },
        { data: "transactionId", title: "transactionId" },
        { data: "canEdit", title: "" },
        { data: "canDelete", title: "" },
        { data: "isUpdated", title: "" }
      ],
      data: contactInfoes
    });

    dtId2.on("click", "#editContactInfo", e => {
      e.preventDefault();
      this.SelectedLogisticType = [];
      this.vendorContactEdit = new VendorContact();
      const index = e.currentTarget.attributes["data-index"].nodeValue;
      const data = this.findDataByIndex(index, this.tableIdContactInfo);
      const copiedData = JSON.parse(JSON.stringify(data));
      this.vendorContactEdit = copiedData as VendorContact;

      // make country selected;
      //!
      const currentType = this.lookups.logisticsTypes.filter(
        x => x.value === data.type
      );
      this.SelectedLogisticType = this.getSelectedItemOnEdit(currentType);

      const value = { event: e, model: data };
      this.EditPopUpPosition(value);
      this.contactInformationPoupFlagEdit = "block";
      this.contactInfoRowIndex = index;
    });

    dtId2.on("click", "#deleteContactInfo", e => {
      e.preventDefault();
      const index = e.currentTarget.attributes["data-index"].nodeValue;
      const dtId1: any = $(this.tableIdContactInfo);
      const widget = dtId1.DataTable();
      const data = this.findDataByIndex(index, this.tableIdContactInfo);
      data["isUpdated"] = true;
      data["isDeleted"] = true;
      this.contactInfoBin(data);
      widget.row(index).remove();
      widget
        .rows()
        .invalidate("data")
        .draw(false);
      this.unSavedChanges = true;
    });
  }

  private contactInfoBin(data) {
    const contact = data as VendorContact;
    if (contact.transactionId > 0) {
      contact["isUpdated"] = true;
      contact["isDeleted"] = true;
      this.deletedContacts.push(contact);
    }
  }

  private attachmentsGrid(attachments) {
    const attachedFiles = this.setUpRequiredColumns(attachments);
    const dtId3: any = $(this.tableIdAttachment);
    const tableWidget1 = dtId3.DataTable({
      sorting: [],
      responsive: true,
      paging: false,
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: false,
      lengthChange: false,
      sortable: false,
      ordering: false,
      autoWidth: false,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      columnDefs: [
        {
          targets: 0,
          render: function(data, type, row, index) {
            //!
            const result = data || row.filename;
            return (
              '<span id="downloadBtn" data-index=' +
              index.row +
              ' data-toggle="tooltip" data-placement="bottom" title="download" style="cursor: pointer">' +
              result +
              "</span>"
            );
          }
        },
        {
          //
          targets: [2],
          render: (data, type, row) => {
            return Moment(data).format(
              this.appSettingSrvc.settings.dateTimeFormate
            );
          }
        },
        {
          targets: [3, 4],
          visible: false
        },
        {
          targets: 5,
          render: function(data, type, row, index) {
            const result = data || row.id;
            return (
              ' <a id="deleteAttachment" style="cursor: pointer" class="delete" data-index=' +
              index.row +
              '><i class="icon-delete"></i></a>'
            );
          }
        }
      ],
      columns: [
        { data: "name", title: "Name", class: "min-phone-l", width: "39%" },
        { data: "docType", title: "Type", class: "min-tablet-p", width: "24%" },
        {
          data: "createdDateTime",
          title: "Upload Date",
          class: "min-tablet-l",
          width: "32%"
        },
        { data: "id", title: "" },
        { data: "isUpdated", title: "" },
        {
          data: "Delete",
          title: "Delete",
          class: "min-tablet-l text-center",
          width: "5%"
        }
      ],
      data: attachedFiles
    });

    dtId3.on("click", "#downloadBtn", e => {
      e.preventDefault();
      const index = e.currentTarget.attributes["data-index"].nodeValue;
      const data = this.findDataByIndex(index, this.tableIdAttachment);
      this.downloadAttachment(data);
    });

    dtId3.on("click", "#deleteAttachment", e => {
      e.preventDefault();
      //!
      this.deleteAttachment(e);
    });
  }

  private attachmentBin(data) {
    const attachment = data as Attachment;
    attachment["isDeleted"] = true;
    this.deletedAttachment.push(attachment);
  }

  //!
  public downloadAttachment(item) {
    const payload = { Id: item.id };
    this.attachmentService.downloadAttachment(payload).subscribe(
      resp => {
        const blob = new Blob([resp.content], { type: resp.content.type });
        //!
        const fileName = item.filename;
        const objectUrl = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement(
          "a"
        ) as HTMLAnchorElement;

        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.message;
          this.notificationsService.error(message);
        } else if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.notFound
        ) {
          this.notificationsService.error(MessageText.fileNotAvailable);
        }
      }
    );
  }

  //!
  public deleteAttachment(e) {
    const dtId1: any = $(this.tableIdAttachment);
    const widget = dtId1.DataTable();
    const index = e.currentTarget.attributes["data-index"].nodeValue;
    const data = this.findDataByIndex(index, this.tableIdAttachment);

    swal({
      title: MessageText.documentsDeleteTitle,
      text: MessageText.documentsDeleteText,
      // type: 'warning',
      showCancelButton: true,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      confirmButtonClass: "btn btn-default",
      cancelButtonClass: "btn btn-info",
      buttonsStyling: false,
      reverseButtons: true
    }).then(discardChanges => {
      if (discardChanges.value) {
        const payload = { Id: data.id };
        this.attachmentService.deleteAttachment(payload).subscribe(
          resp => {
            //!
            this.attachmentBin(data);
            widget.row(index).remove();
            widget
              .rows()
              .invalidate("data")
              .draw(false);
            this.unSavedChanges = true;
          },
          err => {
            if (
              err instanceof HttpErrorResponse &&
              err.status === HttpStatusCode.badRequest
            ) {
              const message = err.error.error || err.error.Message;
              this.notificationsService.error(message);
            }
          }
        );
      } else if (discardChanges.dismiss.toString() === "cancel") {
        //
      }
    });
  }

  private categoryInformationGrid(categoryInfo) {
    const dtId1: any = $(this.tableIdCategoryInfo);
    const tableWidget1 = dtId1.DataTable({
      sorting: [],
      responsive: true,
      autoWidth: false,
      paging:
        categoryInfo.length > this.appSettingSrvc.settings.pagingAppearence,
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: categoryInfo.length > this.appSettingSrvc.settings.pagingAppearence,
      lengthChange: false,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      columnDefs: [
        {
          targets: [2, 3],
          render: (data, type, row) => {
            return Moment(data).format(
              this.appSettingSrvc.settings.dateFormate
            );
          }
        }
      ],
      columns: [
        {
          data: "categoryName",
          title: "Category Name",
          class: "min-phone-l",
          width: "25%"
        },
        {
          data: "categoryStatus",
          title: "Category Status",
          class: "min-tablet-p",
          width: "25%"
        },
        {
          data: "effectiveDate",
          title: "Effective Date",
          class: "min-tablet-l",
          width: "25%"
        },
        {
          data: "expirationDate",
          title: "Expiration Date",
          class: "min-tablet-l",
          width: "25%"
        }
      ],
      data: categoryInfo
    });

    tableWidget1.on("draw", () => {
      if (tableWidget1.page.info().pages < 2) {
        $(tableWidget1.table().container())
          .find("a.previous, a.next, #list-table_paginate span")
          .css("display", tableWidget1.page.info().pages < 2 ? "none" : "");
      } else {
        $(tableWidget1.table().container())
          .find("a.previous")
          .css("display", tableWidget1.page.info().page === 0 ? "none" : "");

        $(tableWidget1.table().container())
          .find("a.next")
          .css(
            "display",
            tableWidget1.page.info().page === tableWidget1.page.info().pages - 1
              ? "none"
              : ""
          );
      }
    });
    tableWidget1.rows().draw();
  }

  private getFormattedDate(data) {
    if (data) {
    } else {
      data = Moment();
    }
    const mdate = Moment(data);
    const date = mdate.date();
    const month = mdate.month() + 1;
    return {
      date: {
        year: mdate.year(),
        day: date,
        month: month
      }
    };
  }

  ngOnDestroy() {
    const dId1: any = $(this.tableIdAdress);
    if (dId1.hasClass("dataTable")) {
      dId1.DataTable().destroy();
    }
    const dId2: any = $(this.tableIdContactInfo);
    if (dId2.hasClass("dataTable")) {
      dId2.DataTable().destroy();
    }
    const dId3: any = $(this.tableIdAttachment);
    if (dId3.hasClass("dataTable")) {
      dId3.DataTable().destroy();
    }
    const dId4: any = $(this.tableIdCategoryInfo);
    if (dId4.hasClass("dataTable")) {
      dId4.DataTable().destroy();
    }
  }

  public redirectback() {
    this.location.back();
  }

  public findDataByIndex(index, grid) {
    const dtId: any = $(grid);
    const data = dtId
      .DataTable()
      .row(index)
      .data();
    return data;
  }

  public foucsValidation() {
    const el = $(".ng-invalid:not(#globalSearch):not(form):first");
    if (el) {
      $("html,body").animate(
        {
          scrollTop:
            el.offset().top -
            this.appSettingSrvc.settings.layoutValidationScrollThreashold
        },
        "slow",
        () => {
          el.focus();
        }
      );
    }
  }
  public removeValidation() {
    this.numberValidation = false;
    const ele = document.getElementById("yearsCompanyOperatesinUAE");
    ele.classList.remove("ng-invalid");
  }

  public updateVendorProfile(valid) {
    if (this.profile.organizationDetail.yearsCompanyOperatesinUAE) {
      if (
        this.isNormalInteger(
          this.profile.organizationDetail.yearsCompanyOperatesinUAE
        )
      ) {
      } else {
        this.numberValidation = true;
        this.initiateFocus("yearsCompanyOperatesinUAE");
        return false;
      }
    }
    let vatflag = true;
    if (this.profile.organizationDetail.vatRegistered !== YesNo.none) {
      if (this.profile.organizationDetail.vatRegistered === YesNo.yes) {
        if (this.profile.organizationDetail.vatNumber) {
          this.vatNumberValidation = false;
        } else {
          vatflag = false;
          this.vatNumberValidation = true;
          this.initiateFocus("vatNumber");
        }
      } else if (this.profile.organizationDetail.vatRegistered === YesNo.no) {
        if (
          this.profile.organizationDetail.vatNotRegisteredType &&
          this.profile.organizationDetail.vatNotRegisteredType !== YesNo.none
        ) {
          this.vatTypeValidation = false;
        } else {
          vatflag = false;
          this.vatTypeValidation = true;
          this.initiateFocus("vatType");
        }

        if (this.profile.organizationDetail.vatReason) {
          this.vatReasonValidation = false;
        } else {
          vatflag = false;
          this.vatReasonValidation = true;
          this.initiateFocus("vatReason");
        }
      }
      if (this.profile.organizationDetail.vatDisclaimer === YesNo.yes) {
        this.vatDisclaimerValidation = false;
      } else {
        vatflag = false;
        this.vatDisclaimerValidation = true;
        this.initiateFocus("disclaimerLabel");
      }
    }

    if (vatflag === false) {
      return vatflag;
    }

    if (!valid || this.dateDifference < 0) {
      this.foucsValidation();
      return false;
    }

    if (
      this.unSavedChanges === false &&
      this.vendorProfileForm.dirty === false
    ) {
      swal(MessageText.FormUnsaveChangesText);
      return false;
    }

    if (this.vendorProfileBtnDisable === true) {
      return false;
    }
    this.vendorProfileBtnDisable = true;

    const data = this.constructPostData();
    this.vendorProfileService.saveProfile(data).subscribe(
      resp => {
        this.isFormSubmitted = true;
        this.vendorProfileForm.control.markAsDirty();
        this.notificationsService.success(MessageText.profileUpdated);
        this.redirectback();
        this.vendorProfileBtnDisable = false;
      },
      err => {
        if (err.status === HttpStatusCode.badRequest) {
          const errorText = this.utiltiy.errorHandle(err);
          this.notificationsService.error(errorText);
          this.serverMessage = errorText;
        }
        this.vendorProfileBtnDisable = false;
      }
    );
  }

  public constructPostData() {
    const postObject = new VendorProfile();
    postObject.organizationDetail = this.profile.organizationDetail;
    //!
    postObject.address = this.getAddressData();
    postObject.contactInformation = this.getContactData();

    if (
      this.profile.organizationDetail.vatRegistered.toLowerCase() === "yes" &&
      this.profile.organizationDetail.vatReason
    ) {
      this.profile.organizationDetail.vatReason = "";
    } else if (
      this.profile.organizationDetail.vatRegistered.toLowerCase() === "no" &&
      this.profile.organizationDetail.vatNumber
    ) {
      this.profile.organizationDetail.vatNumber = "";
    }

    //!
    return postObject;
  }

  public getAddressData() {
    const dtId: any = $(this.tableIdAdress);
    const data = dtId.DataTable().data();
    let postAddress: Array<VendorAddress> = [];

    if (data.length > 0) {
      for (let i = 0; i < data.length; i++) {
        const cItem = data[i] as VendorAddress;
        cItem.isDeleted = false;
        postAddress.push(cItem);
      }
    }

    if (this.deletevendorAddresses.length > 0) {
      for (let i = 0; i < this.deletevendorAddresses.length; i++) {
        this.deletevendorAddresses[i].isDeleted = true;
        postAddress.push(this.deletevendorAddresses[i]);
      }
    }
    postAddress = postAddress.filter(x => x.isUpdated === true);
    return postAddress;
  }

  public getContactData() {
    const dtId: any = $(this.tableIdContactInfo);
    const data = dtId.DataTable().data();
    let postContact: Array<VendorContact> = [];

    if (data.length > 0) {
      for (let i = 0; i < data.length; i++) {
        const cItem = data[i] as VendorContact;
        cItem["isDeleted"] = false;
        postContact.push(cItem);
      }
    }
    if (this.deletedContacts.length > 0) {
      for (let i = 0; i < this.deletedContacts.length; i++) {
        this.deletedContacts[i]["isDeleted"] = true;
        postContact.push(this.deletedContacts[i]);
      }
    }
    postContact = postContact.filter(x => x.isUpdated === true);
    return postContact;
  }

  public addAddress(addressForm: any) {
    if (!addressForm.valid) {
      return false;
    }
    const dtId1: any = $(this.tableIdAdress);
    const tableWidget1 = dtId1.DataTable();
    this.vendorAddress.transactionId = 0;
    this.vendorAddress.isUpdated = true;
    this.vendorAddress.canEdit = "Yes";
    this.vendorAddress.canDelete = "Yes";
    this.vendorAddress.zipPostalCode = this.vendorAddress.zipPostalCode || "";
    this.vendorAddress.address =
      this.vendorAddress.street +
      ", " +
      this.vendorAddress.city +
      ", " +
      this.vendorAddress.countryRegion;
    if (this.vendorAddress.primary === "Yes") {
      this.removePrimaryIfExist(tableWidget1);
    }
    tableWidget1.row.add(this.vendorAddress).draw(false);
    this.addressPoupFlag = false;
    this.vendorAddress = new VendorAddress();
    this.unSavedChanges = true;
  }

  public addAddressEdit(valid) {
    if (!valid) {
      return false;
    }
    const dtId1: any = $(this.tableIdAdress);
    const tableWidget1 = dtId1.DataTable();
    this.vendorAddressEdit["isUpdated"] = true;
    this.vendorAddressEdit.zipPostalCode =
      this.vendorAddressEdit.zipPostalCode || "";
    this.vendorAddressEdit.address =
      this.vendorAddressEdit.street +
      ", " +
      this.vendorAddressEdit.city +
      ", " +
      this.vendorAddressEdit.countryRegion;
    if (this.vendorAddressEdit.primary === "Yes") {
      this.removePrimaryIfExist(tableWidget1);
    }
    tableWidget1
      .row(this.vendorAddressRowIndex)
      .data(this.vendorAddressEdit)
      .draw(false);
    this.addressPoupFlagEdit = "none";
    this.unSavedChanges = true;
  }

  public removePrimaryIfExist(tableWidget1) {
    const data = tableWidget1.data();
    const itemsToUpdate = data.filter(x => x.primary === "Yes");
    for (let i = 0; i < itemsToUpdate.length; i++) {
      const index = data.indexOf(itemsToUpdate[i]);
      itemsToUpdate[i].primary = "No";
      itemsToUpdate[i]["isUpdated"] = true;
      tableWidget1
        .row(index)
        .data(itemsToUpdate[i])
        .draw(false);
    }
  }

  public removePrimaryContactInfoIfExist(tableWidget1, type) {
    const data = tableWidget1.data();
    const itemsToUpdate = data.filter(
      x => x.primary === "Yes" && x.type === type
    );
    for (let i = 0; i < itemsToUpdate.length; i++) {
      const index = data.indexOf(itemsToUpdate[i]);
      itemsToUpdate[i].primary = "No";
      itemsToUpdate[i]["isUpdated"] = true;
      tableWidget1
        .row(index)
        .data(itemsToUpdate[i])
        .draw(false);
    }
  }

  public addContactInfo(contactInfoForm: any) {
    if (!contactInfoForm.valid) {
      return false;
    }
    const dtId1: any = $(this.tableIdContactInfo);
    const tableWidget1 = dtId1.DataTable();
    this.vendorContact.transactionId = 0;
    this.vendorContact.isUpdated = true;
    if (this.vendorContact.primary === "Yes") {
      this.removePrimaryContactInfoIfExist(
        tableWidget1,
        this.vendorContact.type
      );
    }
    tableWidget1.row.add(this.vendorContact).draw(false);
    this.contactInformationPoupFlag = false;
    this.vendorContact = new VendorContact();
    this.unSavedChanges = true;
  }

  public editContactInfo(valid) {
    if (!valid) {
      return false;
    }
    const dtId1: any = $(this.tableIdContactInfo);
    const tableWidget1 = dtId1.DataTable();
    this.vendorContactEdit["isUpdated"] = true;
    if (this.vendorContactEdit.primary === "Yes") {
      this.removePrimaryContactInfoIfExist(
        tableWidget1,
        this.vendorContactEdit.type
      );
    }
    tableWidget1
      .row(this.contactInfoRowIndex)
      .data(this.vendorContactEdit)
      .draw(false);
    this.contactInformationPoupFlagEdit = "none";
    this.unSavedChanges = true;
  }

  // poup events
  public toggleAddressPopup(flag, addressForm) {
    if (flag === true) {
      addressForm.resetForm();
      this.selectedItems = [];
      this.selectedCountry = [];
      this.selectedCity = [];
      this.selectedZipCode = [];
      this.vendorAddress.building = "";
      this.vendorAddress.nameorDescription = "";
      this.vendorAddress.street = "";
      this.vendorAddress.streetNo = "";
      this.vendorAddress.primary = "No";
      this.vendorAddress.countryRegion = "";
    }
    this.addressPoupFlag = flag;
  }

  public toggleAddressPopupEdit(flag) {
    if (flag === "none") {
      this.addAddressEditForm.resetForm();
    }
    this.addressPoupFlagEdit = flag;
  }

  public toggleContactInfoPopup(flag, ContactInfo) {
    if (flag === true) {
      ContactInfo.resetForm();
      this.SelectedLogisticType = [];
      this.vendorContact.contactNumberAddress = "";
      this.vendorContact.description = "";
      this.vendorContact.primary = "No";
      this.vendorContact.type = "No";
    }
    this.contactInformationPoupFlag = flag;
  }

  public toggleContactInfoPopupEdit(flag) {
    if (flag === "none") {
      this.ContactInfoEditForm.resetForm();
    }
    this.contactInformationPoupFlagEdit = flag;
  }

  public toggleAttachmentPopup(flag) {
    this.AttachmentsPopupFlag = flag;
  }

  public onCountryChange($event, value) {
    this.lookupService.VPCountryDependent(value).subscribe(res => {
      this.dependentLookups = res;
    });
  }

  public onChangeAddressPrimaryAdd(value) {
    if (value === true) {
      this.vendorAddress.primary = "Yes";
    } else {
      this.vendorAddress.primary = "No";
    }
  }

  public onChangeAddressPrimary(value) {
    this.addAddressEditForm.control.markAsDirty();
    if (value === true) {
      this.vendorAddressEdit.primary = "Yes";
    } else {
      this.vendorAddressEdit.primary = "No";
    }
  }

  public onChangeContactPrimaryAdd(value) {
    if (value === true) {
      this.vendorContact.primary = "Yes";
    } else {
      this.vendorContact.primary = "No";
    }
  }
  public onChangeContactPrimary(value) {
    this.ContactInfoEditForm.control.markAsDirty();
    if (value === true) {
      this.vendorContactEdit.primary = "Yes";
    } else {
      this.vendorContactEdit.primary = "No";
    }
  }

  public onChangevatRegistered(value) {
    if (value === true) {
      this.profile.organizationDetail.vatRegistered = "Yes";
    } else {
      this.profile.organizationDetail.vatRegistered = "No";
    }
  }

  public onChangeDisclaimer(value) {
    if (value === true) {
      this.profile.organizationDetail.vatDisclaimer = "Yes";
    } else {
      this.profile.organizationDetail.vatDisclaimer = "No";
    }
  }

  public onItemSelect(item: any) {
    this.updateLookup("edit");
  }

  public OnItemDeSelect(item: any) {
    this.updateLookup("edit");
  }

  public onSelectAll(items: any) {
    this.updateLookup("edit");
  }

  public onDeSelectAll(items: any) {
    this.updateLookup("edit");
  }

  public onItemSelectAdd(item: any) {
    this.updateLookup("");
  }

  public OnItemDeSelectAdd(item: any) {
    this.updateLookup("");
  }

  public onSelectAllAdd(items: any) {
    this.updateLookup("");
  }

  public onDeSelectAllAdd(items: any) {
    this.updateLookup("");
  }

  public onVatSelect(data) {
    this.vendorProfileForm.control.markAsDirty();
    this.profile.organizationDetail.vatRegistered = data.id;
  }

  private updateLookup(cmd) {
    const lookup: Array<Lookup> = [];
    for (let i = 0; i < this.selectedItems.length; i++) {
      const item = new Lookup();
      item.name = this.selectedItems[i].itemName;
      item.value = this.selectedItems[i].id;
      lookup.push(item);
    }
    if (cmd === "edit") {
      this.vendorAddressEdit.purpose = lookup;
    } else {
      this.vendorAddress.purpose = lookup;
    }
  }

  public uploaderStatus(data) {
    const dtId1: any = $(this.tableIdAttachment);
    const tableWidgetAttachment = dtId1.DataTable();
    const currentDate = new Date();
    for (let i = 0; i < data.length; i++) {
      const attach = new Attachment();
      attach.id = data[i].Id;
      attach.name = data[i].FileName;
      attach.docType = data[i].FileType;
      attach.createdDateTime = currentDate;
      attach.isUpdated = true;
      tableWidgetAttachment.row.add(attach).draw();
    }
    this.ngxCustomUploadOne.progress = 0;
    this.ngxCustomUploadOne.isCompleted = false;
  }

  onEffectiveDateChanged(event) {
    this.profile.organizationDetail.effectiveDate = Moment(event.jsdate).format(
      this.appSettingSrvc.settings.dateFormateForAx
    ); // event.jsdate.toISOString();
    this.verifyDate();
  }

  onExpirationDateChanged(event) {
    this.profile.organizationDetail.expirationDate = Moment(
      event.jsdate
    ).format(this.appSettingSrvc.settings.dateFormateForAx); // event.jsdate.toISOString();
    this.verifyDate();
  }

  public verifyDate() {
    if (
      this.profile.organizationDetail.effectiveDate &&
      this.profile.organizationDetail.expirationDate
    ) {
      if (
        Date.parse(this.profile.organizationDetail.expirationDate) <=
        Date.parse(this.profile.organizationDetail.effectiveDate)
      ) {
        this.dateDifference = -1;
        return;
      }
      this.dateDifference = 1;
    }
    this.dateDifference = 1;
  }

  public EditPopUpPosition(data) {
    // Calculate pageX/Y if missing and clientX/Y available
    if (data.event.pageX == null && data.event.clientX != null) {
      const doc = document.documentElement,
        body = document.body;
      data.event.pageX =
        data.event.clientX +
        ((doc && doc.scrollLeft) || (body && body.scrollLeft) || 0) -
        ((doc && doc.clientLeft) || (body && body.clientLeft) || 0);
      data.event.pageY =
        data.event.clientY +
        ((doc && doc.scrollTop) || (body && body.scrollTop) || 0) -
        ((doc && doc.clientTop) || (body && body.clientTop) || 0);
    }
    this.left = data.event.pageX - 500 + "px";
    this.top = data.event.pageY - 60 + "px";

    const bodyWidth = document.body.offsetWidth;
    if (bodyWidth < 768) {
      this.left = "auto";
      this.top = data.event.pageY + 15 + "px";
    }
  }

  // public deletDocument(data) {
  // }

  public deleteDocument(item) {}

  public onAttachmentMetaChange(data) {
    //!
    const Metas = this.vendorProfileService.getUploadMeta();
    let item = Metas.filter(value => value.DocType === data.id);
    this.ngxCustomUploadOne.updateMeta(item[0]);
  }

  canDeactivate(): boolean | Observable<boolean> | Promise<boolean> {
    this.canGo = new Subject<boolean>();
    // if form is submitted
    if (this.isFormSubmitted === true) {
      return true;
    }

    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    if (
      this.unSavedChanges === false &&
      this.vendorProfileForm.dirty === false
    ) {
      return true;
    }

    // Otherwise ask the user with the dialog service and return its
    // observable which resolves to true or false when the user decides
    swal({
      title: MessageText.FormLeavingTitle,
      text: MessageText.FormLeavingText,
      // type: 'warning',
      showCancelButton: true,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      confirmButtonClass: "btn btn-default",
      cancelButtonClass: "btn btn-info",
      buttonsStyling: false,
      reverseButtons: true
    }).then(discardChanges => {
      if (discardChanges.value) {
        this.canGo.next(true);
      } else if (discardChanges.dismiss.toString() === "cancel") {
        if (window.history.state) {
          const url = window.history.state.url;
          window.history.pushState({ url: url }, "", url);
        } else {
          const url = "vp/home";
          window.history.pushState({ url: url }, "", url);
        }
        this.canGo.next(false);
      }
    });
    return this.canGo;
  }

  // select control for adding contact
  public fillCountries(countries) {
    this.countries = [];
    for (let i = 0; i < countries.length; i++) {
      this.countries.push({
        id: countries[i].value,
        itemName: countries[i].name
      });
    }
    this.countrySetting = this.singleSelectSetting(
      "Select Country/Region",
      this.countries.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public onCountrySelect(value, type) {
    this.countrySelection(value.id, type);
  }

  //!
  public countrySelection(value, type) {
    this.lookupService.VPCountryDependent(value).subscribe(res => {
      this.dependentLookups = res;
      this.fillCities(res);
      if (type === "edit") {
        //!
        const currentCity = this.cities.filter(
          x => x.value === this.vendorAddressEdit.city
        );
        this.selectedCity = this.getSelectedItemOnEdit(currentCity);
      }
    });
    if (type === "edit") {
      this.vendorAddressEdit.countryRegion = value;
    } else {
      this.selectedCity = [];
      this.selectedZipCode = [];
      this.vendorAddress.countryRegion = value;
    }
  }

  public fillCities(cities) {
    this.cities = [];
    for (let i = 0; i < cities.length; i++) {
      this.cities.push({ id: cities[i].value, itemName: cities[i].name });
    }
    this.citySetting = this.singleSelectSetting(
      "Select City",
      this.cities.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public onCitySelect(value, type) {
    if (type === "edit") {
      this.vendorAddressEdit.city = value.id;
    } else {
      this.vendorAddress.city = value.id;
    }
  }

  public getSelectedItemOnEdit(data) {
    const selected = [];
    for (let i = 0; i < data.length; i++) {
      selected.push({ id: data[i].value, itemName: data[i].name });
    }
    return selected;
  }

  public fillContactInfoTypes(logisticTypes) {
    this.logisticTypes = [];
    for (let i = 0; i < logisticTypes.length; i++) {
      if (logisticTypes[i].name && logisticTypes[i].value) {
        this.logisticTypes.push({
          id: logisticTypes[i].value,
          itemName: logisticTypes[i].name
        });
      }
    }
    this.logisticTypeSetting = this.singleSelectSetting(
      "Select Type",
      this.logisticTypes.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public onLogisticTypeSelect(value, type) {
    if (type === "edit") {
      this.vendorContactEdit.contactNumberAddress = "";
      this.vendorContactEdit.type = value.id;
    } else {
      this.vendorContact.contactNumberAddress = "";
      this.vendorContact.type = value.id;
    }
  }

  public fillUploadMeta(meta) {
    this.uploadMeta = [];
    for (let i = 0; i < meta.length; i++) {
      //!
      meta[i].Email = this.profile.organizationDetail.email;
      //!
      this.uploadMeta.push({ id: meta[i].DocType, itemName: meta[i].DocType });
    }
  }

  public fillLanguages(languages) {
    this.languages = [];
    for (let i = 0; i < languages.length; i++) {
      this.languages.push({
        id: languages[i].value,
        itemName: languages[i].name
      });
    }
    this.languageSetting = this.singleSelectSetting(
      "Select Language",
      this.languages.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
    this.makeLanguageSelected(
      this.profile.organizationDetail.languagePreference
    );
  }

  public makeLanguageSelected(selected) {
    if (selected) {
      this.selectedLanguage = [];
      const language = this.lookups.languages.filter(
        x => x.value === selected.toLowerCase()
      );
      for (let i = 0; i < language.length; i++) {
        this.selectedLanguage.push({
          id: language[i].value,
          itemName: language[i].name
        });
      }
    }
  }

  public onLanguageChange(value) {
    this.profile.organizationDetail.languagePreference = value.id;
  }

  public fillCurrencies(currencies) {
    this.currencies = [];
    for (let i = 0; i < currencies.length; i++) {
      this.currencies.push({
        id: currencies[i].value,
        itemName: currencies[i].name
      });
    }
    this.currencySetting = this.singleSelectSetting(
      "Select Currency",
      currencies.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
    this.makeCurrencySelected(this.profile.organizationDetail.currency);
  }

  public fillVateRegistered(vats) {
    this.vats = [];
    for (let i = 0; i < vats.length; i++) {
      // if (vats[i].value && vats[i].name) {
      this.vats.push({ id: vats[i].value, itemName: vats[i].name });
      // }
    }
    this.VatSetting = this.singleSelectSetting(
      "Select VAT",
      this.vats.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public fillVateTypes(types) {
    const result = [];
    for (let i = 0; i < types.length; i++) {
      if (types[i].value && types[i].name) {
        result.push(types[i]);
      }
    }
    this.vatTypes = result;
    this.typeSetting = this.singleSelectSetting(
      "Select Type",
      this.vatTypes.length > this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public makeCurrencySelected(selected) {
    this.selectedCurrency = [];
    const currency = this.lookups.currency.filter(x => x.value === selected);
    for (let i = 0; i < currency.length; i++) {
      this.selectedCurrency.push({
        id: currency[i].value,
        itemName: currency[i].name
      });
    }
  }

  public onCurrencyChange(value) {
    this.profile.organizationDetail.currency = value.id;
  }

  public isNormalInteger(str) {
    const n = Math.floor(Number(str));
    return Number.isInteger(n) && n >= 0;
  }

  public assignVatDropdown() {
    this.selectedVat = this.vats.filter(
      x => x.id === this.profile.organizationDetail.vatRegistered
    );
  }

  public initiateFocus(id) {
    id = "." + id;
    const ele = $(id); // document.getElementById(id);
    if (ele) {
      $("html,body").animate(
        {
          scrollTop:
            ele.offset().top -
            this.appSettingSrvc.settings.layoutValidationScrollThreashold
        },
        "slow",
        () => {
          ele.focus();
        }
      );
    }
  }

  public mutliselectSetting(placeholder) {
    return {
      singleSelection: false,
      text: placeholder,
      classes: "form-control custom-select custom-control",
      enableSearchFilter: true,
      searchAutofocus: true,
      badgeShowLimit: 2,
      searchPlaceholderText: "Search"
    };
  }

  public singleSelectSetting(placeholder, search = true) {
    return {
      singleSelection: true,
      text: placeholder,
      showCheckbox: false,
      classes: "form-control custom-select custom-control",
      enableSearchFilter: search,
      searchAutofocus: search,
      searchPlaceholderText: "Search"
    };
  }
}
