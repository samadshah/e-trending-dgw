import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
// Modules
import { SharedModule } from '../../shared/shared.module';
import { VendorProfileComponent } from './vendor-profile.component';
import { VendorProfileRoutingModule } from './vendor-profile-routing.module';
import { VendorProfileDetailComponent } from './vendor-profile-detail/vendor-profile-detail.component';
import { VendorProfileService } from './vendor-profile.service';
import { VendorProfileEditComponent } from './vendor-profile-edit/vendor-profile-edit.component';
import { VendorProfileDetailResolve } from './vendor-profile-detail/vendor-profile-detail.resovle';
import { VendorProfileEditResolve } from './vendor-profile-edit/vendor-profile-edit.resovle';
import { NgxCustomUploadComponent } from '../../shared/components/ngx-custom-upload/ngx-custom-upload.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    SharedModule,
    VendorProfileRoutingModule,
  ],
  providers: [VendorProfileService, VendorProfileDetailResolve, VendorProfileEditResolve ],
  declarations: [VendorProfileComponent, VendorProfileDetailComponent, VendorProfileEditComponent]
})
export class VendorProfileModule { }
