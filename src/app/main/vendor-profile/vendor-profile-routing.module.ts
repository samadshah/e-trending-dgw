import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorProfileComponent } from './vendor-profile.component';
import { VendorProfileDetailComponent } from './vendor-profile-detail/vendor-profile-detail.component';
import { VendorProfileEditComponent } from './vendor-profile-edit/vendor-profile-edit.component';
import { VendorProfileDetailResolve } from './vendor-profile-detail/vendor-profile-detail.resovle';
import { VendorProfileEditResolve } from './vendor-profile-edit/vendor-profile-edit.resovle';
import { CanDeactivateGuard } from '../../core/guards/can-deactivate-guard.service';

const routes: Routes = [{
  path: '',
  component: VendorProfileComponent,
  children: [
    {
      path: '',
      component: VendorProfileDetailComponent,
      resolve: {
        profile: VendorProfileDetailResolve
      },
      data: {
        breadcrumb: 'Vendor Profile'
      }
    },
    {
      path: 'edit',
      component: VendorProfileEditComponent,
      canDeactivate: [CanDeactivateGuard],
      resolve: {
        profile: VendorProfileEditResolve
      },
      data: {
        breadcrumb: 'Edit'
      }
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CanDeactivateGuard]
})
export class VendorProfileRoutingModule { }
