import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { VendorProfile } from './models/vendor-profile';

//!
enum DocTypes {
  certificate = 'Certificate',
  companyProfile = 'Company profile',
  cv = 'CV',
  emiratesId = 'EmiratesId',
  financialDetails = 'Financial Details',
  incorporationDate = 'Incorporation Date',
  passportNo = 'Passport No',
  tradeLicenseCopy = 'Trade License Copy',
  visaNo = 'Visa No',
}

@Injectable()
export class VendorProfileService {
  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;

  //!
  public DocumentAttachmemts = [
    {
      Email: '',
      DocType: DocTypes.certificate
    },
    {
      Email: '',
      DocType: DocTypes.companyProfile
    },
    {
      Email: '',
      DocType: DocTypes.cv
    },
    {
      Email: '',
      DocType: DocTypes.emiratesId
    },
    {
      Email: '',
      DocType: DocTypes.financialDetails
    },
    {
      Email: '',
      DocType: DocTypes.incorporationDate
    },
    {
      Email: '',
      DocType: DocTypes.passportNo
    },
    {
      Email: '',
      DocType: DocTypes.tradeLicenseCopy
    },
    {
      Email: '',
      DocType: DocTypes.visaNo
    }
  ];

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/vendors';
    this.servicePath2 = '/api';
  }

  getVendorProfile(): Observable<HttpResponse<VendorProfile>> {
    return this.http.get<VendorProfile>(environment.base_api_url + this.servicePath2 + '/GetVendorProfile', { observe: 'response' });
  }

  saveProfile(data): Observable<any> {
    return this.http.post(environment.base_api_url + this.servicePath2 + '/PostVendorProfile', data);
  }

  termAndCondition(): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/termsandconditions');
  }

  //!!
  getUploadMeta(){
    return this.DocumentAttachmemts;
  }
}
