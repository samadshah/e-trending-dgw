import { Location } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications/dist';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { first } from 'rxjs/operators';

import { MessageText, RedirectUrl } from '../../../core/configuration/config';
import { VendorProfile } from '../models/vendor-profile';
import { VendorProfileService } from '../vendor-profile.service';

@Injectable()
export class VendorProfileDetailResolve implements Resolve<HttpResponse<VendorProfile>> {

  constructor(private vendorProfileService: VendorProfileService,
    private router: Router,
    private location: Location,
    private notificationsService: NotificationsService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<HttpResponse<VendorProfile>> {
    return this.vendorProfileService.getVendorProfile()
      // .pipe(first())
      .catch(() => {
        this.notificationsService.error(MessageText.unableToFetchData);
        this.router.navigate([RedirectUrl.home]);
        return of(null);
      });
  }
}
