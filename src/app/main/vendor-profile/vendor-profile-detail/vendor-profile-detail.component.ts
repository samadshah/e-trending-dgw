// tslint:disable
import {
  HttpErrorResponse,
  HttpHeaders,
  HttpResponse
} from "@angular/common/http";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationsService } from "angular2-notifications/dist";
import * as Moment from "moment";
import { last } from "rxjs/operators";
import { AppSettingService } from "../../../core/services/app-setting.service";
import {
  HttpStatusCode,
  MessageText,
  RedirectUrl
} from "../../../core/configuration/config";
import { AttachmentService } from "../../../shared/services/attachment-service";
import { OrganizationDetail, VendorProfile } from "../models/vendor-profile";
import { VendorProfileService } from "../vendor-profile.service";
import { LookupService } from "../../../shared/services/lookup-service";

@Component({
  selector: "app-vendor-profile-detail",
  templateUrl: "./vendor-profile-detail.component.html",
  styleUrls: ["./vendor-profile-detail.component.css"]
})
export class VendorProfileDetailComponent implements OnInit, OnDestroy {
  profile: VendorProfile = new VendorProfile();
  tableId1 = "#data-table-Adress";
  tableId2 = "#data-table-ContactInfo";
  tableId3 = "#data-table-Attachment";
  tableId4 = "#data-table-CategoryInfo";
  gridInitialized = false;
  empty = "__";
  vatTypes = [];
  checkRadio = true;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private attachmentService: AttachmentService,
    private vendorProfileService: VendorProfileService,
    private notificationsService: NotificationsService,
    private lookupService: LookupService,
    private appSettingSrvc: AppSettingService
  ) {
    this.profile.organizationDetail = new OrganizationDetail();
  }

  ngOnInit() {
    //!
    this.lookupService.vatTypes().subscribe(
      value => {
        this.fillVatType(value);
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.message;
          this.notificationsService.error(message);
        }
      }
    );

    this.route.data.subscribe(
      (data: { profile: HttpResponse<VendorProfile> }) => {
        if (data && data.profile) {
          this.refreshUI(data.profile);
        }

        if (this.isCached(data.profile)) {
          this.refreshData();
        }
      },
      error => {
        if (
          error instanceof HttpErrorResponse &&
          error.status === HttpStatusCode.badRequest
        ) {
          const message = error.error.error || error.error.message;
          this.notificationsService.error(message);
        }
        this.router.navigate([RedirectUrl.home]);
      }
    );
  }

  refreshUI(data: HttpResponse<VendorProfile>) {
    this.profile = data.body;
    //!! Seperate address with commas
    let address: any = this.profile.organizationDetail.address.split("\n");
    let newAddress = "";
    address.forEach((ele, ind) => {
      newAddress += ele;
      if (ind !== address.length - 1) {
        newAddress += ", ";
      }
    });
    this.profile.organizationDetail.address = newAddress;

    //!! Seperate address with commas
    this.profile.address.forEach(element => {
      let address: any = element.address.split("\n");
      let newAddress = "";
      address.forEach((ele, ind) => {
        newAddress += ele;
        if (ind !== address.length - 1) {
          newAddress += ", ";
        }
      });
      element.address = newAddress;
    });

    this.bindGrid(this.profile);
  }

  refreshData() {
    this.vendorProfileService
      .getVendorProfile()
      .pipe(last())
      .subscribe(
        data => {
          if (data) {
            this.refreshUI(data);
          }
        },
        err => {
          if (
            err instanceof HttpErrorResponse &&
            err.status === HttpStatusCode.badRequest
          ) {
            const message = err.error.error || err.error.message;
            this.notificationsService.error(message);
          }
        }
      );
  }

  isCached(res: HttpResponse<VendorProfile>): boolean {
    if (!res || res.headers instanceof HttpHeaders) {
      return false;
    }
    return true;
  }

  private bindGrid(profile) {
    const dtId1: any = $(this.tableId1);
    const dtId2: any = $(this.tableId2);
    const dtId3: any = $(this.tableId3);
    const dtId4: any = $(this.tableId4);

    if (this.gridInitialized) {
      //!
      this.tableDataRefresh(dtId1, profile.address);
      this.tableDataRefresh(dtId2, profile.contactInformation);
      //!
      this.tableDataRefresh(dtId3, profile.attachments);
      this.tableDataRefresh(dtId4, profile.categoryInformation);
    } else {
      this.gridInitialized = true;
      this.addressGrid(profile.address);
      this.contactInformationGrid(profile.contactInformation);
      this.categoryInformationGrid(profile.categoryInformation);
      //!
      this.attachmentsGrid(profile.attachments);
    }
  }

  tableDataRefresh(table, model) {
    table.DataTable().clear();
    table.DataTable().rows.add(model);
    table.DataTable().draw();
  }

  private addressGrid(address) {
    const dtId1: any = $(this.tableId1);
    const tableWidget1 = dtId1.DataTable({
      sorting: [],
      responsive: true,
      paging: address.length > this.appSettingSrvc.settings.pagingAppearence,
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: address.length > this.appSettingSrvc.settings.pagingAppearence,
      lengthChange: false,
      autoWidth: false,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      columnDefs: [
        //!
        {
          className: "text-center",
          targets: 2,
          render: function(value, type, row, index) {
            return row.primary === "1"
              ? '<div class="styledCheckbox checkbox viewMode"><input type="checkbox" id="primarybox' +
                  index.row +
                  '" name="primary" value="primary" disabled checked><label for="primarybox' +
                  index.row +
                  '"><span></span></label></div>'
              : '<div class="styledCheckbox checkbox viewMode"><input type="checkbox" id="primarybox' +
                  index.row +
                  '" name="primary" value="primary" disabled><label for="primarybox' +
                  index.row +
                  '"><span></span></label></div>';
          }
        }
      ],
      columns: [
        {
          data: "nameorDescription",
          title: "Name or Description",
          orderable: false,
          class: "min-phone-l",
          width: "40%"
        },
        {
          data: "address",
          title: "Address",
          orderable: false,
          class: "min-tablet-p",
          width: "40%"
        },
        {
          data: "primary",
          title: "Primary",
          orderable: false,
          class: "min-tablet-l text-center",
          width: "20%"
        }
      ],
      data: address
    });

    tableWidget1.on("draw", () => {
      if (tableWidget1.page.info().pages < 2) {
        $(tableWidget1.table().container())
          .find("a.previous, a.next, #list-table_paginate span")
          .css("display", tableWidget1.page.info().pages < 2 ? "none" : "");
      } else {
        $(tableWidget1.table().container())
          .find("a.previous")
          .css("display", tableWidget1.page.info().page === 0 ? "none" : "");

        $(tableWidget1.table().container())
          .find("a.next")
          .css(
            "display",
            tableWidget1.page.info().page === tableWidget1.page.info().pages - 1
              ? "none"
              : ""
          );
      }
    });
  }

  private contactInformationGrid(contactInfo) {
    const dtId1: any = $(this.tableId2);
    const tableWidget1 = dtId1.DataTable({
      sorting: [],
      responsive: true,
      paging:
        contactInfo.length > this.appSettingSrvc.settings.pagingAppearence,
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: contactInfo.length > this.appSettingSrvc.settings.pagingAppearence,
      lengthChange: false,
      autoWidth: false,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      columnDefs: [
        {
          className: "text-center",
          targets: 2,
          render: function(value, type, row, index) {
            //!
            return row.primary === "1"
              ? '<div class="styledCheckbox checkbox viewMode"><input type="checkbox" id="primarybox' +
                  index.row +
                  '" name="primary" value="primary" disabled checked><label for="primarybox' +
                  index.row +
                  '"><span></span></label></div>'
              : '<div class="styledCheckbox checkbox viewMode"><input type="checkbox" id="primarybox' +
                  index.row +
                  '" name="primary" value="primary" disabled><label for="primarybox' +
                  index.row +
                  '"><span></span></label></div>';
          }
        }
      ],
      columns: [
        {
          data: "type",
          title: "Type",
          orderable: false,
          class: "min-tablet-p",
          width: "40%"
        },
        {
          data: "contactNumberAddress",
          orderable: false,
          title: "Contact Number/Address",
          class: "min-tablet-l",
          width: "40%"
        },
        {
          data: "primary",
          title: "Primary",
          orderable: false,
          class: "min-tablet-l text-center",
          width: "20%"
        }
      ],
      data: contactInfo
    });

    tableWidget1.on("draw", () => {
      if (tableWidget1.page.info().pages < 2) {
        $(tableWidget1.table().container())
          .find("a.previous, a.next, #list-table_paginate span")
          .css("display", tableWidget1.page.info().pages < 2 ? "none" : "");
      } else {
        $(tableWidget1.table().container())
          .find("a.previous")
          .css("display", tableWidget1.page.info().page === 0 ? "none" : "");

        $(tableWidget1.table().container())
          .find("a.next")
          .css(
            "display",
            tableWidget1.page.info().page === tableWidget1.page.info().pages - 1
              ? "none"
              : ""
          );
      }
    });
  }

  private attachmentsGrid(attachments) {
    const dtId1: any = $(this.tableId3);
    const tableWidget1 = dtId1.DataTable({
      sorting: [],
      responsive: true,
      paging:
        attachments.length > this.appSettingSrvc.settings.pagingAppearence,
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: attachments.length > this.appSettingSrvc.settings.pagingAppearence,
      lengthChange: false,
      autoWidth: false,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      columnDefs: [
        {
          targets: 0,
          render: function(data, type, row, index) {
            const result = data || row.filename;
            return (
              '<span id="downloadBtn" class="downloadDocLink" data-index=' +
              index.row +
              ' data-toggle="tooltip" data-placement="bottom" title="download" style="cursor: pointer">' +
              result +
              "</span>"
            );
          }
        },
        {
          targets: [2],
          render: (data, type, row) => {
            return Moment(data).format(
              this.appSettingSrvc.settings.dateTimeFormate
            );
          }
        },
        {
          targets: 3,
          visible: false
        }
      ],
      columns: [
        { data: "name", title: "Name", class: "min-phone-l", width: "33.33%" },
        {
          data: "docType",
          title: "Type",
          class: "min-tablet-p",
          width: "33.33%"
        },
        {
          data: "createdDateTime",
          title: "Upload Date",
          orderable: false,
          class: "min-phone-l",
          width: "33.33%"
        },
        { data: "id", title: "" }
      ],
      data: attachments
    });

    dtId1.on("click", "#downloadBtn", e => {
      e.preventDefault();
      const index = e.currentTarget.attributes["data-index"].nodeValue;
      const data = this.findDataByIndex(index, this.tableId3);
      this.downloadAttachment(data);
    });

    tableWidget1.on("draw", () => {
      if (tableWidget1.page.info().pages < 2) {
        $(tableWidget1.table().container())
          .find("a.previous, a.next, #list-table_paginate span")
          .css("display", tableWidget1.page.info().pages < 2 ? "none" : "");
      } else {
        $(tableWidget1.table().container())
          .find("a.previous")
          .css("display", tableWidget1.page.info().page === 0 ? "none" : "");

        $(tableWidget1.table().container())
          .find("a.next")
          .css(
            "display",
            tableWidget1.page.info().page === tableWidget1.page.info().pages - 1
              ? "none"
              : ""
          );
      }
    });
  }

  public downloadAttachment(item) {
    const payload = {
      Id: item.id,
      Purpose: "Register"
    };

    this.attachmentService.downloadAttachment(payload).subscribe(
      resp => {
        const blob = new Blob([resp.content], { type: resp.content.type });
        const fileName = item.filename;
        const objectUrl = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement(
          "a"
        ) as HTMLAnchorElement;

        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.message;
          this.notificationsService.error(message);
        } else if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.notFound
        ) {
          this.notificationsService.error(MessageText.fileNotAvailable);
        }
      }
    );
  }

  //!
  private categoryInformationGrid(categoryInfo) {
    const dtId1: any = $(this.tableId4);
    const tableWidget1 = dtId1.DataTable({
      sorting: [],
      responsive: true,
      paging:
        categoryInfo.length > this.appSettingSrvc.settings.pagingAppearence,
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: categoryInfo.length > this.appSettingSrvc.settings.pagingAppearence,
      lengthChange: false,
      autoWidth: false,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      columnDefs: [
        {
          targets: [1],
          render: (data, type, row) => {
            return Moment(data).format(
              this.appSettingSrvc.settings.dateFormate
            );
          }
        }
      ],
      columns: [
        {
          data: "categoryName",
          title: "Category Name",
          class: "min-phone-l",
          width: "50%"
        },
        {
          data: "effectiveDate",
          title: "Effective Date",
          orderable: false,
          class: "min-tablet-p",
          width: "50%"
        }
      ],
      data: categoryInfo
    });
    tableWidget1.on("draw", () => {
      if (tableWidget1.page.info().pages < 2) {
        $(tableWidget1.table().container())
          .find("a.previous, a.next, #list-table_paginate span")
          .css("display", tableWidget1.page.info().pages < 2 ? "none" : "");
      } else {
        $(tableWidget1.table().container())
          .find("a.previous")
          .css("display", tableWidget1.page.info().page === 0 ? "none" : "");

        $(tableWidget1.table().container())
          .find("a.next")
          .css(
            "display",
            tableWidget1.page.info().page === tableWidget1.page.info().pages - 1
              ? "none"
              : ""
          );
      }
    });
  }

  public getFormattedDate(data) {
    return "" + Moment(data).format(this.appSettingSrvc.settings.dateFormate);
  }

  ngOnDestroy() {
    const dId1: any = $(this.tableId1);
    dId1.DataTable().destroy();
    const dId2: any = $(this.tableId2);
    dId2.DataTable().destroy();
    const dId3: any = $(this.tableId3);
    dId3.DataTable().destroy();
    const dId4: any = $(this.tableId4);
    dId4.DataTable().destroy();
  }

  public redirectToEdit() {
    this.router.navigate([RedirectUrl.profileEdit]);
  }

  public findDataByIndex(index, grid) {
    const dtId: any = $(grid);
    const data = dtId
      .DataTable()
      .row(index)
      .data();
    return data;
  }

  //!
  private fillVatType(value) {
    const result = [];
    const Vats = value.vatTypeList;
    for (let i = 0; i < Vats.length; i++) {
      if (Vats[i].value && Vats[i].name) {
        result.push(Vats[i]);
      }
    }
    this.vatTypes = result;
  }
}
