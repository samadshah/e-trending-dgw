
import { Location } from "@angular/common";
import {
  HttpErrorResponse,
  HttpHeaders,
  HttpResponse
} from "@angular/common/http";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationsService } from "angular2-notifications/dist";
import * as Moment from "moment";
import { last } from "rxjs/operators";
import { AppSettingService } from "../../../core/services/app-setting.service";
import {
  HttpStatusCode,
  MessageText,
  RedirectUrl
} from "../../../core/configuration/config";
import {
  AttachmentMetaType,
  Meta,
  MetaPostModel
} from "../../../core/models/AttachmentMeta";
import { UtiltiyService } from "../../../core/services/utility";
import { AttachmentService } from "../../../shared/services/attachment-service";
import { BlanketAgreementResponse } from "../model/blanket-agreement-detail";
import { BlanketAgreementService } from "./blanket-agreement-detial.service";

@Component({
  selector: "app-blanket-agreement-detail",
  templateUrl: "./blanket-agreement-detail.component.html",
  styleUrls: ["./blanket-agreement-detail.component.css"]
})
export class BlanketAgreementDetailComponent implements OnInit, OnDestroy {
  private id: any;
  private sub: any;
  model: BlanketAgreementResponse = new BlanketAgreementResponse();
  postAttachmentMeta: MetaPostModel = new MetaPostModel();
  tableId = "#data-table";
  gridInitialized = false;
  constructor(
    private detailService: BlanketAgreementService,
    private route: ActivatedRoute,
    private attachmentService: AttachmentService,
    private utiltiy: UtiltiyService,
    private notificationsService: NotificationsService,
    private router: Router,
    private location: Location,
    private appSettingSrvc: AppSettingService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(
      (data: { blanket: HttpResponse<BlanketAgreementResponse> }) => {
        if (data && data.blanket) {
          this.refreshUI(data.blanket);
        }

        if (this.isCached(data.blanket)) {
          this.refreshData();
        }
      },
      error => {
        if (
          error instanceof HttpErrorResponse &&
          error.status === HttpStatusCode.badRequest
        ) {
          const message = error.error.error || error.error.message;
          this.notificationsService.error(message);
        }
        this.router.navigate([RedirectUrl.bAgreement]);
      }
    );
  }

  refreshUI(data: HttpResponse<BlanketAgreementResponse>) {
    this.model = data.body;
    this.model.endDate = Moment(this.model.endDate).format(
      this.appSettingSrvc.settings.dateTimeFormate
    );
    this.model.startDate = Moment(this.model.startDate).format(
      this.appSettingSrvc.settings.dateTimeFormate
    );
    this.model.total = this.setTwoNumberDecimal(this.model.total, 2);
    this.InitComponent();
  }

  refreshData() {
    this.route.queryParams.subscribe(result => {
      const id = result["id"];

      if (id) {
        this.detailService
          .getDetailData(id)
          .pipe(last())
          .subscribe(
            data => {
              if (data) {
                this.refreshUI(data);
              }
            },
            err => {
              if (
                err instanceof HttpErrorResponse &&
                err.status === HttpStatusCode.badRequest
              ) {
                const message = err.error.error || err.error.message;
                this.notificationsService.error(message);
              }
            }
          );
      } else {
        this.notificationsService.alert(MessageText.invaliedId);
        this.router.navigate([RedirectUrl.bAgreement]);
      }
    });
  }

  isCached(res: HttpResponse<BlanketAgreementResponse>): boolean {
    if (!res || res.headers instanceof HttpHeaders) {
      return false;
    }
    return true;
  }

  InitComponent() {
    const dtId1: any = $(this.tableId);
    if (this.gridInitialized) {
      this.tableDataRefresh(dtId1, this.model.lines);
    } else {
      this.gridInitialized = true;
      this.bindGrid(this.model.lines);
    }
  }

  tableDataRefresh(table, model) {
    table.DataTable().clear();
    table.DataTable().rows.add(model);
    table.DataTable().draw();
  }
  ngOnDestroy() {
    const dtId: any = $(this.tableId);
    dtId.DataTable().destroy();
  }

  public downloadAttachment(item) {
    this.postAttachmentMeta.id = item.id;
    this.postAttachmentMeta.metaTypes = new Array<AttachmentMetaType>();
    this.postAttachmentMeta.metaTags = new Array<Meta>();
    for (const entry of this.model.metaTagTypeList) {
      if (entry.docType === item.docType) {
        this.postAttachmentMeta.metaTypes.push(entry);
      }
    }
    for (const entry of this.model.metaTags) {
      if (entry.docType === item.docType) {
        this.postAttachmentMeta.metaTags.push(entry);
      }
    }

    this.attachmentService
      .downloadAttachment(this.postAttachmentMeta)
      .subscribe(resp => {
        const blob = new Blob([resp.content], { type: resp.content.type });
        const fileName = item.name;

        if (this.utiltiy.isBrowserIe() === false) {
          const objectUrl = URL.createObjectURL(blob);
          const a: HTMLAnchorElement = document.createElement(
            "a"
          ) as HTMLAnchorElement;

          a.href = objectUrl;
          a.download = fileName;
          document.body.appendChild(a);
          a.click();

          document.body.removeChild(a);
          URL.revokeObjectURL(objectUrl);
        } else {
          window.navigator.msSaveBlob(blob, fileName);
        }
      });
  }

  public bindGrid(data) {
    const dtId: any = $(this.tableId);
    const tableWidget = dtId.DataTable({
      responsive: true,
      paging: data.length > this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: false,
      sDom: '<"clear">t<"F"p>',
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      order: [],
      columnDefs: [
        {
          className: "text-center",
          targets: 2,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(value, 2);
          }
        },
        {
          className: "text-center",
          targets: 3,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(value, 2);
          }
        },
        {
          className: "text-center",
          targets: 7,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(value, 2);
          }
        }
      ],
      columns: [
        { data: "itemName", title: "Item Name" },
        { data: "quantity", title: "Quantity" },
        { data: "unitPrice", title: "Unit Price" },
        { data: "discountAmount", title: "Discount Amount" },
        { data: "discountPercent", title: "Discount Percentage" },
        { data: "type", title: "Type" },
        { data: "amendmentNumber", title: "Amendment Number" },
        { data: "lineTotal", title: "Line Total" }
      ],
      data: data
    });
  }

  public getBackLink() {
    this.location.back();
  }

  public setTwoNumberDecimal(value, param) {
    value = value.toFixed(param);
    const data = value.split(".");
    value = parseFloat(data[0]).toLocaleString() + "." + data[1];
    return value;
  }
}
