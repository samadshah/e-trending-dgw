import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';

import { environment } from '../../../../environments/environment';
import { BlanketAgreementResponse } from '../model/blanket-agreement-detail';

@Injectable()
export class BlanketAgreementService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/Documents';

  }

  getDetailData(id): Observable<HttpResponse<BlanketAgreementResponse>> {
    return this.http.get<BlanketAgreementResponse>(environment.base_api_url
      + this.servicePath + '/BlanketAgreements/' + id, { observe: 'response' });
  }
}
