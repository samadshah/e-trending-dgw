import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";

import {
  RedirectUrl
} from "../../../core/configuration/config";
import { AppSettingService } from "../../../core/services/app-setting.service";
import { Router } from "@angular/router";
import { PlatformLocation } from "@angular/common";
import * as Moment from "moment";

import { catchError, map } from "rxjs/operators";

@Component({
  selector: "app-blanket-agreement-list",
  templateUrl: "./blanket-agreement-list.component.html",
  styleUrls: ["./blanket-agreement-list.component.css"]
})
export class BlanketAgreementListComponent implements OnInit {
  @ViewChild("blanketList")
  blanketList: any;
  columnsDefination: Array<any>;
  columns: Array<any>;
  serviceUrl = "Documents/BlanketAgreements";
  gridType = 4;
  title = "Blanket Agreements";
  appbase;

  constructor(
    private router: Router,
    private platformLocation: PlatformLocation,
    private appSettingSrvc: AppSettingService
  ) {}

  ngOnInit() {
    this.appbase = (this.platformLocation as any).location.pathname;
    this.columnsDefination = [
      {
        targets: 2,
        render: (data, type, row) => {
          return Moment(data).format(this.appSettingSrvc.settings.dateFormate);
        }
      },
      {
        targets: 3,
        render: (data, type, row) => {
          return Moment(data).format(this.appSettingSrvc.settings.dateFormate);
        }
      },
      {
        className: "text-center",
        targets: 5,
        render: (data, type, row) => {
          // tslint:disable-next-line:max-line-length
          return (
            `<a href="` +
            this.appbase +
            "/view?id=" +
            encodeURIComponent(row.blanketAgreement) +
            `"data-Id="` +
            encodeURIComponent(row.blanketAgreement) +
            `"data-url="` +
            RedirectUrl.bAgreement +
            "/view" +
            // tslint:disable-next-line:max-line-length
            `" class="cursor-pointer actionView"  data-prop="blanketAgreement" data-toggle="tooltip" data-placement="bottom" title="View attached file"><i class="icon-view"></i></a>`
          );
        }
      }
    ];
    this.columns = [
      { data: "blanketAgreement", title: "Blanket Agreements" },
      { data: "documentTitle", title: "Document Title" },
      { data: "startDate", title: "Start Date" },
      { data: "endDate", title: "End Date" },
      { data: "status", title: "Status" },
      { data: "view", title: "View", orderable: false }
    ];
  }

  public viewDetail(data) {
    this.router.navigate([RedirectUrl.bAgreement, data.blanketAgreement]);
  }

  public loadTooltip() {
    $('[data-toggle="tooltip"]').tooltip({
      trigger: "hover"
    });
  }
}
