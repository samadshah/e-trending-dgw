import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { BlanketAgreementRoutingModule } from './blanket-agreement-routing.module';
import { BlanketAgreementListComponent } from './blanket-agreement-list/blanket-agreement-list.component';
import { BlanketAgreementDetailComponent } from './blanket-agreement-detail/blanket-agreement-detail.component';
import { BlanketAgreementComponent } from './blanket-agreement.component';
import { BlanketAgreementService } from './blanket-agreement-detail/blanket-agreement-detial.service';
import { BlanketAgreementDetailResolve } from './blanket-agreement-detail/blanket-agreement-detail.resolve';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BlanketAgreementRoutingModule
  ],
  declarations: [BlanketAgreementListComponent, BlanketAgreementDetailComponent, BlanketAgreementComponent],
  providers: [BlanketAgreementService, BlanketAgreementDetailResolve]
})
export class BlanketAgreementModule { }
