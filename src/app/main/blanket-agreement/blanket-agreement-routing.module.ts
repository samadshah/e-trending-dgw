import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlanketAgreementListComponent } from './blanket-agreement-list/blanket-agreement-list.component';
import { BlanketAgreementDetailComponent } from './blanket-agreement-detail/blanket-agreement-detail.component';
import { BlanketAgreementComponent } from './blanket-agreement.component';
import { BlanketAgreementDetailResolve } from './blanket-agreement-detail/blanket-agreement-detail.resolve';
import { CanDeactivateGuard } from '../../core/guards/can-deactivate-guard.service';

const routes: Routes = [
  {
    path: '',
    component: BlanketAgreementComponent,
    children: [
      {
        path: '',
        component: BlanketAgreementListComponent,
        canDeactivate: [CanDeactivateGuard],
      },
      {
        path: 'view',
        component: BlanketAgreementDetailComponent,
        data: {
          breadcrumb: 'Blanket Agreement'
        },
        resolve: {
          blanket: BlanketAgreementDetailResolve
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlanketAgreementRoutingModule { }
