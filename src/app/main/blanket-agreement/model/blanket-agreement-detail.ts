
import { Attachments } from '../../../core/models/attachments';
import { AttachmentMetaType, MetaData, Meta } from '../../../core/models/AttachmentMeta';

export class BlanketAgreementResponse {

    startDate: string;
    endDate: string;
    currency: string;
    documentTitle: string;
    purchaseOrder: string;
    status: string;
    total: string;
    attachments: Array<Attachments>;
    metaTagTypeList: Array<AttachmentMetaType>;
    metaTags: Array<Meta>;
    lines: Array<Line>;
}


export class Line {
    public type: string;
    public discountAmount: number;
    public discountPercent: number;
    public itemName: string;
    public lineTotal: number;
    public quantity: number;
    public unitPrice: number;
    public amendmentNumber: number;
}
