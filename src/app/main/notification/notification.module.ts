import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { NotificationRoutingModule } from './notification-routing.module';
import { NotificationComponent } from './notification.component';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { NotificationDetailComponent } from './notification-detail/notification-detail.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NotificationRoutingModule
  ],
  declarations: [NotificationComponent, NotificationListComponent, NotificationDetailComponent]
})
export class NotificationModule { }
