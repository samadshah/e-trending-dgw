import { PlatformLocation } from "@angular/common";
import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { NotificationsService } from "angular2-notifications/dist";
import * as Moment from "moment";
import { finalize } from "rxjs/operators";
import { AppSettingService } from "../../../core/services/app-setting.service";
import {
  GridType,
  HttpStatusCode,
  MessageText
} from "../../../core/configuration/config";
import {
  AttachmentMetaType,
  MetaPostModel
} from "../../../core/models/AttachmentMeta";
import { AttachmentService } from "../../../shared/services/attachment-service";
import { Popup } from "../../notification/models/popup";
import { NavigationService } from "../../../layout/navigation/navigation.service";
import { NotificationCountSharingServiceService } from "../../../core/services/notification-count-sharing-service.service";

@Component({
  selector: "app-notification-list",
  templateUrl: "./notification-list.component.html",
  styleUrls: ["./notification-list.component.css"],
  providers: [NavigationService]
})
export class NotificationListComponent implements OnInit, AfterViewInit {
  @ViewChild("notificationList")
  notificationList: any;
  columnsDefination: Array<any>;
  columns: Array<any>;
  serviceUrl = "Documents/Notifications";
  gridType = GridType.NotificationList;
  title = "Inbox";
  notificationSubmitDisable = false;
  show = false;
  left = "0px";
  top = "0px";
  display = "inline";
  currentPopupItem = new Popup();
  metaTagsTypeList;
  metaTags;
  appbase;
  empty = "__";

  constructor(
    private attachmentService: AttachmentService,
    private notificationsService: NotificationsService,
    private platformLocation: PlatformLocation,
    private appSettingSrvc: AppSettingService,
    private navigationService: NavigationService,
    private notificationCountSharingServiceService: NotificationCountSharingServiceService
  ) {}

  ngOnInit() {
    this.appbase = (this.platformLocation as any).location.pathname;
    this.columnsDefination = [
      {
        targets: [4, 5],
        visible: false,
        sortable: false
      },
      {
        targets: 1,
        render: (data, type, row) => {
          return Moment(data).format(this.appSettingSrvc.settings.dateFormate);
        }
      },
      {
        className: "text-center",
        targets: 3,
        render: function(data, type, row, index) {
          return (
            '<a id="" class="popUp" data-id=' +
            index.row +
            ' data-toggle="tooltip" data-placement="bottom" title="View" style="cursor:pointer;"><i class="icon-view"></i></a>'
          );
        }
      }
    ];
    this.columns = [
      { data: "subject", title: "Subject", class: "min-phone-l", width: "40%" },
      {
        data: "dateCreated",
        title: "Notification Date",
        class: "min-phone-l",
        width: "25%"
      },
      { data: "sentBy", title: "Sent By", class: "min-tablet-p", width: "25%" },
      {
        data: "view",
        title: "View",
        orderable: false,
        class: "text-center min-tablet-p",
        width: "10%"
      },
      { data: "metaTags", title: "View", orderable: false },
      { data: "message", title: "", orderable: false }
    ];
  }

  ngAfterViewInit() {
    const date = Moment().toISOString();
    this.navigationService.dismiss(date).subscribe(
      resp => {
        this.notificationCountSharingServiceService.sendCount(0);
      },
      error => {}
    );
  }

  public viewDetail(data) {
    this.metaTagsTypeList = data.model.metaTypes;
    this.metaTags = data.model.metaTags;

    // Calculate pageX/Y if missing and clientX/Y available
    if (data.event.pageX == null && data.event.clientX != null) {
      const doc = document.documentElement,
        body = document.body;
      data.event.pageX =
        data.event.clientX +
        ((doc && doc.scrollLeft) || (body && body.scrollLeft) || 0) -
        ((doc && doc.clientLeft) || (body && body.clientLeft) || 0);
      data.event.pageY =
        data.event.clientY +
        ((doc && doc.scrollTop) || (body && body.scrollTop) || 0) -
        ((doc && doc.clientTop) || (body && body.clientTop) || 0);
    }
    this.left = data.event.pageX - 500 + "px";
    this.top = data.event.pageY - 20 + "px";
    const bodyWidth = document.body.offsetWidth;
    if (bodyWidth < 768) {
      this.left = "auto";
      this.top = data.event.pageY + 15 + "px";
    }
    const meta = new MetaPostModel();
    this.currentPopupItem.message = data.message;
    this.currentPopupItem.topic = data.subject;
    const type = data.model.metaTypes;
    const tags = data.model.metaTags;

    const post = new MetaPostModel();
    post.metaTags = data.model.metaTags;
    post.metaTypes = this.getRelaventMetaType(data.model);
    if (post.metaTypes.length > 0) {
      if (this.notificationSubmitDisable === true) {
        return false;
      }
      this.notificationSubmitDisable = true;
      this.attachmentService
        .get(post)
        .pipe(
          finalize(() => {
            this.show = true;
            this.notificationSubmitDisable = false;
          })
        )
        .subscribe(
          res => {
            this.currentPopupItem.attachment = res;
            this.notificationSubmitDisable = false;
          },
          err => {
            if (
              err instanceof HttpErrorResponse &&
              err.status === HttpStatusCode.badRequest
            ) {
              const message = err.error.error || err.error.message;
              this.notificationsService.error(message);
            }
            this.notificationSubmitDisable = false;
          }
        );
    } else {
      this.show = true;
    }
  }

  public closePopUp() {
    this.show = false;
  }

  private getRelaventMetaType(data) {
    const types = new Array<AttachmentMetaType>();
    for (let i = 0; i < data.metaTags.length; i++) {
      const cType = data.metaTypes.filter(
        x => x.docType === data.metaTags[i].docType
      );
      if (cType.length === 1) {
        types.push(cType[0]);
      }
    }
    return types;
  }

  public downloadAttachment(item) {
    const postAttachmentMeta = new MetaPostModel();
    postAttachmentMeta.id = item.id;
    for (const entry of this.metaTagsTypeList) {
      if (entry.docType === item.docType) {
        postAttachmentMeta.metaTypes = new Array<AttachmentMetaType>();
        postAttachmentMeta.metaTypes.push(entry);
      }
    }
    for (const entry of this.metaTags) {
      if (entry.docType === item.docType) {
        postAttachmentMeta.metaTags = entry.metaTags;
      }
    }

    this.attachmentService.downloadAttachment(postAttachmentMeta).subscribe(
      resp => {
        const blob = new Blob([resp.content], { type: resp.content.type });
        const fileName = item.name;
        const objectUrl = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement(
          "a"
        ) as HTMLAnchorElement;

        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.message;
          this.notificationsService.error(message);
        } else if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.notFound
        ) {
          this.notificationsService.info(MessageText.fileNotAvailable);
        }
      }
    );
  }

  public loadTooltip() {
    $('[data-toggle="tooltip"]').tooltip({
      trigger: "hover"
    });
  }
}
