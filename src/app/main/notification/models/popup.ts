import {Attachment} from '../../../core/models/AttachmentMeta';

export class Popup {
        topic: string;
        attachment: Array<any>;
        message: string;
        rfqTechnicalDoc: Array<any>;
        rfqFunctionalDoc: Array<any>;
        rfqSupportingDoc: Array<any>;
        rfqDoc: Array<any>;
}
