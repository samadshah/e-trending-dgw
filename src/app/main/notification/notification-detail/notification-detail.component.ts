import { Component, OnInit } from '@angular/core';

import { GridType } from '../../../core/configuration/config';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.component.html',
  styleUrls: ['./notification-detail.component.css']
})
export class NotificationDetailComponent implements OnInit {

  columnsDefination: Array<any>;
  columns: Array<any>;
  serviceUrl = 'Documents/Notifications';
  gridType = GridType.NotificationList;
  title = 'Notifications';

  constructor() { }

  ngOnInit() {
    this.columnsDefination = [
      {
        targets: 0,
        visible: false
      },
      {
        targets: 2,
        render: function (data, type, row) {
          return '' + new Date(data).toLocaleDateString();
        }
      },
      {
        targets: 4,
        render: function (data, type, row) {
          return '' + new Date(data).toLocaleDateString();
        }
      },
      {
        targets: 5,
        render: function (data, type, row) {
          return '' + new Date(data).toLocaleDateString();
        }
      },
      {
        className: 'text-center',
        targets: 6,
        render: function (data, type, row) {
          return '<a id="ViewBtn" data-Id="' + row.notificationId +
            '" data-prop="notificationId" data-toggle="tooltip" data-placement="bottom" title="View" style="cursor:pointer;"><i class="icon-view"></i></a>';
        }
      },
    ];
    this.columns = [
      { data: 'message', title: '' },
      { data: 'subject', title: 'Subject' },
      { data: 'dateCreated', title: 'Date Created' },
      { data: 'sentBy', title: 'Sent by' },
      { data: 'dueDate', title: 'Due Date' },
      { data: 'expirationDate', title: 'Expiration Date' },
      { data: 'view', title: 'View', orderable: false },
    ];
  }

  public viewDetail(data) {

  }

}
