import { PlatformLocation } from "@angular/common";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Logger } from "angular2-logger/core";
import {
  SearchSharingService,
  SearchState
} from "../../core/services/search-sharing.service";
import {
  GlobalSearchType,
  RedirectUrl
} from "../../core/configuration/config";
import { environment } from "../../../environments/environment";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.css"]
})
export class SearchComponent implements OnInit, OnDestroy {
  columnsDefination: Array<any>;
  columns: Array<any>;
  serviceUrl = "dashboard/search";
  gridType = 9;
  title = "Gloab Search";
  hideFilter = true;
  appbase;
  host = "";
  subscription;
  searchTerm;
  readParamSubscription: any;
  constructor(
    private route: ActivatedRoute,
    private platformLocation: PlatformLocation,
    private logger: Logger,
    private searchSharingService: SearchSharingService
  ) {
    this.readParamSubscription = this.route.queryParams.subscribe(params => {
      this.logger.debug(
        "ng-sys-grid >> ngonInit >> this.route.queryParams.subscribe"
      );
      this.searchTerm = params["term"] || "Nothing";
    });
  }

  ngOnInit() {
    this.appbase = (this.platformLocation as any).location.pathname;
    this.host = (this.platformLocation as any).location.host;
    this.subscription = this.searchSharingService.searchState.subscribe(
      (state: SearchState) => {
        this.searchTerm = state.searchText;
      }
    );

    this.columnsDefination = [
      {
        targets: [1, 2],
        visible: false
      },
      {
        className: "text-center",
        targets: [0, 1, 2],
        render: (data, type, row) => {
          let url = this.getGlobalSearchUrl(row.documentType);
          let href =
            this.getGlobalSearchUrl(row.documentType) +
            encodeURIComponent(row.documentID);
          const dataUrl = this.getDataUrl(row);
          const dataId = encodeURIComponent(row.documentID);

          return (
            '<small><a class="actionView" href="' +
            href +
            '"  data-Id="' +
            dataId +
            '" data-url="' +
            dataUrl +
            '" >' +
            url +
            row.documentID +
            "</a></small>" +
            "<strong>" +
            row.documentType +
            " " +
            row.documentID +
            "</strong>" +
            "<p>" +
            row.documentTitle +
            "</p>"
          );
        }
      }
    ];

    this.columns = [
      { data: "documentID", title: "", className: "search-list-result" },
      {
        data: "documentTitle",
        title: "",
        className: "search-list-result",
        width: "0"
      },
      {
        data: "documentType",
        title: "",
        className: "search-list-result",
        width: "0"
      }
    ];
  }

  public getGlobalSearchUrl(type) {
    const sw = type.toLowerCase();
    let env = environment.root_path === "/" ? "" : environment.root_path;
    if (env) {
      env = env.toString().replace(/\//g, "");
      env = "/" + env;
    }

    switch (sw) {
      case GlobalSearchType.requestForQuotation:
        return this.host + env + RedirectUrl.rfqDetail + "?id=";
      case GlobalSearchType.purchaseOrder:
        return this.host + env + RedirectUrl.pOrders + "/view?id=";
    }
  }

  public getDataUrl(row) {
    const sw = row.documentType.toLowerCase();
    switch (sw) {
      case GlobalSearchType.requestForQuotation:
        return RedirectUrl.rfqs + "/view";
      case GlobalSearchType.purchaseOrder:
        return RedirectUrl.pOrders + "/view";
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.readParamSubscription.unsubscribe();
  }
}
