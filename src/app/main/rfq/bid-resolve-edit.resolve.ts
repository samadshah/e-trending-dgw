import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications/dist';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { MessageText, RedirectUrl } from '../../core/configuration/config';
import { BidDetail } from './models/bid';
import { BidDetailService } from './rfq-service';

@Injectable()
export class BidResolveEdit implements Resolve<HttpResponse<BidDetail>> {

    constructor(private bidDetailService: BidDetailService, private router: Router,
        private notificationsService: NotificationsService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<HttpResponse<BidDetail>> {
        const id = route.queryParams['id']; // route.paramMap.get('id');
        if (id) {
            return this.bidDetailService.get(id)
                .catch(() => {
                    this.notificationsService.error(MessageText.unableToFetchData);
                    this.router.navigate([RedirectUrl.rfqs]);
                    return of(null);
                });
        } else {
            this.notificationsService.error(MessageText.invaliedId);
            this.router.navigate([RedirectUrl.rfqs]);
        }

    }
}
