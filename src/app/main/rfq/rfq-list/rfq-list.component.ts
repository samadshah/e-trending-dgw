// tslint:disable
import { Component, OnInit, ViewChild } from "@angular/core";
import { AppSettingService } from "../../../core/services/app-setting.service";
import {
  MessageText,
  GridType,
  rfQListIndication
} from "../../../core/configuration/config";
import {
  RedirectUrl,
  HttpStatusCode
} from "../../../core/configuration/config";
import { PlatformLocation } from "@angular/common";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { NotificationsService } from "angular2-notifications/dist";
import * as Moment from "moment";
import { UtiltiyService } from "../../../core/services/utility";
import { AttachmentService } from "../../../shared/services/attachment-service";
import { RecallRequest } from "../models/recall-request.model";
import { BidDetailService } from "../rfq-service";
import { Popup } from "../models/popup";

@Component({
  selector: "app-rfq-list",
  templateUrl: "./rfq-list.component.html",
  styleUrls: ["./rfq-list.component.css"]
})
export class RfqListComponent implements OnInit {
  left = "0px";
  top = "0px";
  show = false;

  columnsDefination: Array<any>;
  columns: Array<any>;
  //!
  serviceUrl = "/RFQs/GetData";
  gridType = GridType.RFQList;
  title = "Request For Quotations";
  appbase;
  host = "";
  request = new RecallRequest();
  @ViewChild("rfqGrid") rfqGrid;
  // questionareFlowIds: any;
  // bidBtnDisable = false;
  bidRecallBtnDisable = false;
  // questionareSubmitBtnDisable = false;
  attachmentDownloadDisable = false;
  // canGo: Subject<boolean> = new Subject<boolean>();
  currentPopupItem = new Popup();
  currentMeta: any;

  constructor(
    private attachmentService: AttachmentService,
    private router: Router,
    // private route: ActivatedRoute,
    private notificationsService: NotificationsService,
    private platformLocation: PlatformLocation,
    private bidDetailService: BidDetailService,
    private utiltiy: UtiltiyService,
    private appSettingSrvc: AppSettingService
  ) {
    this.initModels();
  }

  //!
  initModels() {
    this.currentPopupItem.rfqDoc = [];
    this.currentPopupItem.rfqSupportingDoc = [];
  }

  ngOnInit() {
    this.appbase = (this.platformLocation as any).location.pathname;
    this.host = (this.platformLocation as any).location.host;
    this.columnsDefination = [
      {
        targets: [0, 10],
        visible: false
      },
      {
        className: "text-center1",
        targets: 3,
        render: (data, type, row) => {
          return data;
        }
      },
      {
        //!
        targets: 5,
        render: (data, type, row) => {
          const resp = this.createToolTip(row);
          //!! minus the difference of 5 hours from expiry date
          let newDate = new Date(data);
          newDate.setHours(newDate.getHours() - 5);

          if (resp["tooltipTitle"]) {
            return (
              '<span class="status" data-toggle="tooltip" data-placement="bottom" title="' +
              resp.tooltipTitle +
              '">' +
              Moment(newDate).format(
                this.appSettingSrvc.settings.dateTimeFormate
              ) +
              "</span>"
            );
          } else {
            return (
              "<span>" +
              Moment(newDate).format(
                this.appSettingSrvc.settings.dateTimeFormate
              ) +
              "</span>"
            );
          }
        }
      },
      {
        className: "text-center",
        targets: 6,
        render: (data, type, row) => {
          return data === "0"
            ? '<a  class="disabled" data-toggle="tooltip" data-placement="bottom" title="Bid">' +
                '<i class="icon-bid"></i></a>'
            : `<a id="bidBtn" data-rfq="` +
                encodeURIComponent(row.requestforquotation) +
                `"data-rfqId="` +
                encodeURIComponent(row.rfqId) +
                `" class="cursor-pointer actionView" data-toggle="tooltip" data-placement="bottom" title="Bid"><i class="icon-bid"></i></a>`;
        }
      },
      {
        className: "text-center",
        targets: 7,
        render: (data, type, row) => {
          return data === "0"
            ? '<a  class="disabled" data-toggle="tooltip" data-placement="bottom" title="Recall Bid">' +
                '<i class="icon-bid-recall"></i></a>'
            : `<a id="bidRecallBtn" data-Id="` +
                row.requestforquotation +
                `" class="cursor-pointer"  data-prop="requestforquotation" data-toggle="tooltip" data-placement="bottom" title="Recall Bid"><i class="icon-bid-recall"></i></a>`;
        }
      },
      {
        className: "text-center",
        targets: 8,
        render: (data, type, row) => {
          return (
            `<a href="` +
            this.appbase +
            "/view?id=" +
            row.requestforquotation +
            "&" +
            row.rfqStatus +
            `"data-Id="` +
            row.requestforquotation +
            "&" +
            row.rfqStatus +
            `"data-url="` +
            RedirectUrl.rfqs +
            "/view" +
            `" class="cursor-pointer actionView" data-prop="requestforquotation" data-toggle="tooltip" data-placement="bottom" title="View"><i class="icon-view"></i></a>`
          );
        }
      },
      {
        className: "",
        targets: 9,
        render: (data, type, row) => {
          return (
            '<a id="" class="popUpViewRfq" data-Id="' +
            row.rfqId +
            '" data-prop="rfqId" data-toggle="tooltip" style="cursor: pointer" data-placement="bottom" title="Attached">' +
            '<i class="icon-attachment"></i></a>'
          );
        }
      }
    ];

    this.columns = [
      { data: "status", title: "Status" },
      {
        data: "requestforquotation",
        title: "Request For Quotation",
        class: "rfq min-phone-l",
        width: "18%"
      },
      {
        data: "documentTitle",
        title: "Document Title",
        class: "doc-type min-phone-l",
        width: "20%"
      },
      {
        data: "rfqStatus",
        title: "Status",
        class: "min-tablet-p",
        width: "8%"
      },
      {
        data: "bidType",
        title: "Bid Type",
        class: "bid-type min-tablet-l",
        width: "8%"
      },
      {
        data: "expirationDateAndTime",
        title: "Expiration Date And Time",
        class: "exp-date min-tablet-l",
        width: "18%"
      },
      {
        data: "bid",
        title: "Bid",
        orderable: false,
        class: "text-center bid min-tablet-l",
        width: "8%"
      },
      {
        data: "bidRecall",
        title: "Recall Bid",
        orderable: false,
        class: "text-center recall-bid min-tablet-l",
        width: "8%"
      },
      {
        data: "view",
        title: "View",
        orderable: false,
        class: "text-center view desktop",
        width: "8%"
      },
      {
        data: "Attach",
        title: "Attachment",
        orderable: false,
        class: "text-center attached desktop",
        width: "8%"
      },
      { data: "rfqId", title: "" }
    ];
  }

  //!
  public bidRecall(data) {
    this.request.replyId = data.rfqId;

    if (this.bidRecallBtnDisable === true) {
      return false;
    }
    this.bidRecallBtnDisable = true;
    this.bidDetailService.recallBid(this.request).subscribe(
      res => {
        if (res.isSuccess) {
          this.notificationsService.success(MessageText.bidRecalled);
          this.bidRecallBtnDisable = false;
          this.rfqGrid.reloadGridWithOrWithoutFilters();
        } else {
          this.notificationsService.error(res.message);
        }
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.message;
          this.notificationsService.error(message);
        }
        this.bidRecallBtnDisable = false;
      }
    );
  }

  public bid(ids) {
    this.submitBidRedirect(ids.requestforquotation);
  }

  public submitBidRedirect(id) {
    this.router.navigate([RedirectUrl.rfqSubmit], { queryParams: { id: id } });
  }

  public viewDetail(data) {
    this.router.navigate([RedirectUrl.rfqs], {
      queryParams: { id: encodeURIComponent(data.rfqId) }
    });
  }

  public attachment(data: any) {
    if (this.attachmentDownloadDisable === true) {
      return false;
    }
    this.attachmentDownloadDisable = true;
    this.currentMeta = data.model;

    // Calculate pageX/Y if missing and clientX/Y available
    if (data.event.pageX == null && data.event.clientX != null) {
      const doc = document.documentElement,
        body = document.body;
      data.event.pageX =
        data.event.clientX +
        ((doc && doc.scrollLeft) || (body && body.scrollLeft) || 0) -
        ((doc && doc.clientLeft) || (body && body.clientLeft) || 0);
      data.event.pageY =
        data.event.clientY +
        ((doc && doc.scrollTop) || (body && body.scrollTop) || 0) -
        ((doc && doc.clientTop) || (body && body.clientTop) || 0);
    }
    this.left = data.event.pageX - 500 + "px";
    this.top = data.event.pageY - 20 + "px";
    const bodyWidth = document.body.offsetWidth;
    if (bodyWidth < 768) {
      this.left = "auto";
      this.top = data.event.pageY + 15 + "px";
    }

    //!
    this.attachmentService.get(data.model.id).subscribe(
      resp => {
        let attachments = resp.attachments;
        this.show = true;
        this.currentPopupItem.rfqDoc = attachments.filter(
          x => x.docType.toLowerCase() == "RFQPrintForm".toLowerCase()
        );
        this.currentPopupItem.rfqSupportingDoc = attachments.filter(
          x => x.docType.toLowerCase() == "RFQSupportDoc".toLowerCase()
        );
        this.attachmentDownloadDisable = false;
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.Message;
          this.notificationsService.error(message);
        } else if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.notFound
        ) {
          this.notificationsService.info(MessageText.fileNotAvailable);
        }
        this.attachmentDownloadDisable = false;
      }
    );
  }

  //!
  public downloadAttachment(item) {
    const payload = {
      Id: item.id,
      Purpose: "DOC"
    };

    this.attachmentService.downloadAttachment(payload).subscribe(
      resp => {
        const blob = new Blob([resp.content], { type: resp.content.type });
        const fileName = item.fileName;

        if (this.utiltiy.isBrowserIe() === false) {
          const objectUrl = URL.createObjectURL(blob);
          const a: HTMLAnchorElement = document.createElement(
            "a"
          ) as HTMLAnchorElement;

          a.href = objectUrl;
          a.download = fileName;
          document.body.appendChild(a);
          a.click();

          document.body.removeChild(a);
          URL.revokeObjectURL(objectUrl);
        } else {
          window.navigator.msSaveBlob(blob, fileName);
        }
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.Message;
          this.notificationsService.error(message);
        }
      }
    );
  }

  public loadTooltip() {
    $('[data-toggle="tooltip"]').tooltip({
      trigger: "hover"
    });
  }

  public closePopUp() {
    this.show = false;
  }

  public createToolTip(data): any {
    const toolTipObj: any = {};
    const Cdate = new Date();
    const date = Moment(data.expirationDateAndTime).toString();
    const expDate = new Date(date);
    const timeDiff = expDate.getTime() - Cdate.getTime();
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    if (
      diffDays > -1 &&
      diffDays < this.appSettingSrvc.settings.expiredNoDaysBeforeNotification &&
      data.rfqStatus.toLowerCase() === rfQListIndication.status
    ) {
      toolTipObj["class"] = "showBackground";
      toolTipObj["tooltipTitle"] = this.tooltipText(diffDays);
    }
    return toolTipObj;
  }

  private tooltipText(diffDays): string {
    return diffDays < 1
      ? "last day remaining to bid"
      : diffDays === 1
      ? diffDays + " day remaining to bid"
      : diffDays + " days remaining to bid";
  }
}
