import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { RfqRoutingModule } from './rfq-routing.module';
import { RfqComponent } from './rfq.component';
import { RfqListComponent } from './rfq-list/rfq-list.component';
import { RfqDetailComponent } from './rfq-detail/rfq-detail.component';
import { BidDetailService } from './rfq-service';
import { RfqBidComponent } from './rfq-bid/rfq-bid.component';
import { BidResolve } from './bid-resolve.resolve';
import { BidResolveEdit } from './bid-resolve-edit.resolve';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RfqRoutingModule
  ],
  declarations: [RfqComponent, RfqListComponent, RfqDetailComponent, RfqBidComponent],
  providers: [BidDetailService, BidResolve, BidResolveEdit]
})
export class RfqModule { }
