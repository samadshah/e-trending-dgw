import { Location } from "@angular/common";
import {
  HttpErrorResponse,
  HttpHeaders,
  HttpResponse
} from "@angular/common/http";
import {
  Component,
  OnDestroy,
  OnInit,
  AfterViewInit,
  Inject
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationsService } from "angular2-notifications/dist";
import * as Moment from "moment";
import { last } from "rxjs/operators";
import { AppSettingService } from "../../../core/services/app-setting.service";
import {
  HttpStatusCode,
  MessageText,
  RedirectUrl
} from "../../../core/configuration/config";
import { UtiltiyService } from "../../../core/services/utility";
import { AttachmentService } from "../../../shared/services/attachment-service";
import { BidDetail, Financial } from "../models/bid";
import { BidDetailService } from "../rfq-service";
import { APP_BASE_HREF } from "@angular/common";
import { PlatformLocation } from "@angular/common";

@Component({
  selector: "app-rfq-detail",
  templateUrl: "./rfq-detail.component.html",
  styleUrls: ["./rfq-detail.component.css"]
})
export class RfqDetailComponent implements OnInit, AfterViewInit, OnDestroy {
  queryParamSubscription: any;
  idParams: any;
  id: any;
  rfqStatus: any;
  currentItem: BidDetail = new BidDetail();
  tableId = "#data-table";
  gridInitialized = false;
  TechPropFiles = null;
  TechProp2Files = null;
  TechPropError = "";
  TechProp2Error = "";

  timer = "";
  empty = "__";
  urls = RedirectUrl;
  rfqID = "";
  appbase;
  host = "";

  constructor(
    private bidDetailService: BidDetailService,
    private router: Router,
    private route: ActivatedRoute,
    private attachmentService: AttachmentService,
    private utiltiy: UtiltiyService,
    private notificationsService: NotificationsService,
    private location: Location,
    private appSettingSrvc: AppSettingService,
    private platformLocation: PlatformLocation,
    @Inject(APP_BASE_HREF) private baseHref: string
  ) {
    this.appbase = (this.platformLocation as any).location.pathname;
    this.host = (this.platformLocation as any).location.host;
  }

  ngOnInit() {
    this.queryParamSubscription = this.route.queryParams.subscribe(params => {
      this.idParams = params["id"];
      this.idParams = this.idParams.split("&");
      this.id = this.idParams[0];
      this.rfqStatus = this.idParams[1];
    });

    this.currentItem.provideFinancials = new Array<Financial>();
    this.route.data.subscribe(
      (data: { bid: HttpResponse<BidDetail> }) => {
        if (data && data.bid) {
          this.refreshUI(data.bid);
        }
        if (this.isCached(data.bid)) {
          this.refreshData();
        }
      },
      error => {
        if (
          error instanceof HttpErrorResponse &&
          error.status === HttpStatusCode.badRequest
        ) {
          const message = error.error.error || error.error.message;
          this.notificationsService.error(message);
        }
        this.router.navigate([RedirectUrl.rfqs]);
      }
    );
  }

  ngAfterViewInit() {}

  InitComponent() {
    //!!
    if (this.rfqStatus !== "Awarded" && this.rfqStatus !== "Rejected") {
      this.InitTimer();
    }

    const dtId1: any = $(this.tableId);
    if (this.gridInitialized) {
      this.tableDataRefresh(dtId1, this.currentItem.provideFinancials);
    } else {
      this.gridInitialized = true;
      this.bindGrid(this.currentItem.provideFinancials);
    }
  }

  refreshUI(data: HttpResponse<BidDetail>) {
    this.currentItem = data.body;
    this.currentItem.attachments = data.body.attachments;

    //!! Minus difference of 5 hours from expiry date
    let expiryDate = new Date(this.currentItem.expirationDateAndTime);
    expiryDate.setHours(expiryDate.getHours() - 5);

    this.timer = expiryDate.toISOString();
    this.currentItem.expirationDateAndTime = Moment(expiryDate).format(
      this.appSettingSrvc.settings.dateTimeFormate
    );
    this.InitComponent();
    // this.loadPrintDoc();
  }

  refreshData() {
    if (this.id) {
      this.bidDetailService
        .get(this.id)
        .pipe(last())
        .subscribe(
          data => {
            if (data) {
              this.refreshUI(data);
            }
          },
          err => {
            if (
              err instanceof HttpErrorResponse &&
              err.status === HttpStatusCode.badRequest
            ) {
              const message = err.error.error || err.error.message;
              this.notificationsService.error(message);
            }
          }
        );
    } else {
      this.notificationsService.alert(MessageText.invaliedId);
      this.router.navigate([RedirectUrl.rfqs]);
    }
  }

  isCached(res: HttpResponse<BidDetail>): boolean {
    if (!res || res.headers instanceof HttpHeaders) {
      return false;
    }
    return true;
  }

  tableDataRefresh(table, model) {
    table.DataTable().clear();
    table.DataTable().rows.add(model);
    table.DataTable().draw();
  }

  public bindGrid(data) {
    const dtId: any = $(this.tableId);
    const tableWidget = dtId.DataTable({
      responsive: true,
      paging: data.length > this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: false,
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      sDom: '<"clear">t<"F"p>',
      order: [],
      columnDefs: [
        {
          className: "text-center",
          targets: 1,
          render: (value, type, row, index) => {
            return (
              '<input readonly type="text"  class="form-control custom-control text-center" value=" ' +
              row.quantity +
              '">'
            );
          }
        },
        {
          className: "text-center",
          targets: 2,
          render: (value, type, row, index) => {
            return (
              '<input readonly type="text"  class="form-control custom-control text-center" value=" ' +
              row.unit +
              '">'
            );
          }
        },
        {
          className: "text-center",
          targets: 3,
          render: (value, type, row, index) => {
            return (
              '<input readonly type="text"  class="form-control custom-control text-right" value=" ' +
              this.setTwoNumberDecimal(row.unitPrice, 6) +
              '">'
            );
          }
        },
        {
          className: "text-center",
          targets: 4,
          render: (value, type, row, index) => {
            return (
              '<input readonly type="text"  class="form-control custom-control text-right" value=" ' +
              this.setTwoNumberDecimal(row.discountAmount, 2) +
              '">'
            );
          }
        },
        {
          className: "text-center",
          targets: 5,
          render: (value, type, row, index) => {
            return (
              '<input readonly type="text"  class="form-control custom-control text-right" value=" ' +
              this.setTwoNumberDecimal(row.discountPercent, 2) +
              '">'
            );
          }
        },
        {
          className: "text-center",
          targets: 6,
          render: (value, type, row, index) => {
            return this.setTwoNumberDecimal(row.vatAmount, 2);
          }
        },
        {
          className: "text-center",
          targets: 7,
          render: (value, type, row, index) => {
            return (
              '<input readonly type="text"  class="form-control custom-control text-right" value=" ' +
              this.setTwoNumberDecimal(row.lineAmount, 2) +
              '">'
            );
          }
        }
      ],
      autoWidth: false,
      columns: [
        {
          data: "itemService",
          title: "Item/Service",
          class: "min-phone-l",
          width: "30%"
        },
        {
          data: "quantity",
          title: "Quantity",
          class: "columTxt-right min-phone-l",
          width: "10%"
        },
        { data: "unit", title: "Unit", class: "min-tablet-p", width: "10%" },
        {
          data: "unitPrice",
          title: "Unit Price",
          class: "columTxt-right min-tablet-l",
          width: "10%"
        },
        {
          data: "discountAmount",
          title: "Discount Amount",
          class: "columTxt-right min-tablet-l",
          width: "10%"
        },
        {
          data: "discountPercent",
          title: "Discount Percentage %",
          class: "columTxt-right min-tablet-l",
          width: "10%"
        },
        {
          data: "vatAmount",
          title: "VAT Amount",
          class: "columTxt-right min-tablet-l",
          width: "10%"
        },
        {
          data: "lineAmount",
          title: "Line Amount",
          class: "columTxt-right min-tablet-l",
          width: "10%"
        }
      ],
      data: data
    });
  }

  public setTwoNumberDecimal(value, param) {
    value = value.toFixed(param);
    const data = value.split(".");
    value = parseFloat(data[0]).toLocaleString() + "." + data[1];
    return value;
  }

  public InitTimer() {
    function getTimeRemaining(endtime) {
      endtime = new Date(endtime);
      let t = 0;
      let seconds = 0;
      let minutes = 0;
      let hours = 0;
      let days = 0;
      if (endtime > new Date()) {
        t = endtime.getTime() - new Date().getTime();
        seconds = Math.floor((t / 1000) % 60);
        minutes = Math.floor((t / 1000 / 60) % 60);
        hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        days = Math.floor(t / (1000 * 60 * 60 * 24));
      }
      return {
        total: t,
        days: days,
        hours: hours,
        minutes: minutes,
        seconds: seconds
      };
    }

    function initializeClock(id, endtime) {
      const clock = document.getElementById(id);
      const daysSpan = clock.querySelector(".days");
      const hoursSpan = clock.querySelector(".hours");
      const minutesSpan = clock.querySelector(".minutes");
      const secondsSpan = clock.querySelector(".seconds");

      function updateClock() {
        const t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days.toString();
        hoursSpan.innerHTML = ("0" + t.hours).slice(-2);
        minutesSpan.innerHTML = ("0" + t.minutes).slice(-2);
        secondsSpan.innerHTML = ("0" + t.seconds).slice(-2);

        if (t.total <= 0) {
          clearInterval(timeinterval);
        }
      }

      updateClock();
      const timeinterval = setInterval(updateClock, 1000);
    }

    initializeClock("clockdiv", this.timer);
  }

  public getAmount() {
    return this.currentItem.currency + " " + " ";
  }

  //!
  public downloadAttachment(item) {
    const payload = {
      Id: item.id,
      Purpose: "DOC"
    };

    this.attachmentService.downloadAttachment(payload).subscribe(
      resp => {
        const blob = new Blob([resp.content], { type: resp.content.type });
        const fileName = item.filename;

        if (this.utiltiy.isBrowserIe() === false) {
          const objectUrl = URL.createObjectURL(blob);
          const a: HTMLAnchorElement = document.createElement(
            "a"
          ) as HTMLAnchorElement;

          a.href = objectUrl;
          a.download = fileName;
          document.body.appendChild(a);
          a.click();

          document.body.removeChild(a);
          URL.revokeObjectURL(objectUrl);
        } else {
          window.navigator.msSaveBlob(blob, fileName);
        }
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.Message;
          this.notificationsService.error(message);
        }
      }
    );
  }

  //!
  public downloadAllAttachment() {
    // this.postAttachmentMeta.metaTypes = new Array<AttachmentMetaType>();
    // this.postAttachmentMeta.metaTags = new Array<Meta>();
    // for (const entry of this.currentItem.metaTagTypeList) {
    //   if (entry.docType === 'RFQSupportDoc') {
    //     this.postAttachmentMeta.metaTypes.push(entry);
    //   }
    // }
    // for (const entry of this.currentItem.metaTags) {
    //   if (entry.docType === 'RFQSupportDoc') {
    //     this.postAttachmentMeta.metaTags.push(entry);
    //   }
    // }
    // this.attachmentService.downloadAttachment(this.postAttachmentMeta).subscribe(
    //   resp => {
    //     const blob = new Blob([resp.content], { type: resp.content.type });
    //     const fileName = 'Supporting Documents';
    //     if (this.utiltiy.isBrowserIe() === false) {
    //       const objectUrl = URL.createObjectURL(blob);
    //       const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
    //       a.href = objectUrl;
    //       a.download = fileName;
    //       document.body.appendChild(a);
    //       a.click();
    //       document.body.removeChild(a);
    //       URL.revokeObjectURL(objectUrl);
    //     } else {
    //       window.navigator.msSaveBlob(blob, fileName);
    //     }
    //   });
  }

  showAttachError(list, value) {
    return !(list.filter(x => x.docType === value).length < 1);
  }

  public getBackLink() {
    this.location.back();
  }

  public cancel() {
    this.getBackLink();
  }

  ngOnDestroy() {
    const dtId: any = $(this.tableId);
    dtId.DataTable().destroy();
  }
}
