import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';

import { environment } from '../../../environments/environment';
import { BidDetail } from './models/bid';

@Injectable()
export class BidDetailService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/rfqs';
    this.servicePath2 = '/api';
  }

  get(rfqId): Observable<HttpResponse<BidDetail>> {
    return this.http.get<BidDetail>(environment.base_api_url
      + this.servicePath2 + '/RFQs/GetRfqDetail?RfqID=' + rfqId, { observe: 'response' });
  }

  save(obj): Observable<any> {
    return this.http.post(environment.base_api_url + this.servicePath2 + '/RFQs/SaveBid', obj);
  }

  recallBid(obj): Observable<any> {
    return this.http.post(environment.base_api_url + this.servicePath2 + '/RFQs/RecallBid', obj);
  }

  // getQuestionare(type, rfqId): Observable<QuestionareResponse> {
  //   const params = new HttpParams().set('type', type).set('rfqId', rfqId);
  //   return this.http.post<QuestionareResponse>(environment.base_api_url +
  //     this.servicePath + '/questionare?type=' + type + '&&rfqId=' + rfqId, '');
  // }

  // saveQuestionare(data) {
  //   return this.http.post(environment.base_api_url + this.servicePath + '/saveQuestionare/', data);
  // }

  declineBid(data): Observable<any> {
    return this.http.post(environment.base_api_url + this.servicePath2 + '/RFQs/DeclineBid', data);
  }

  getCurrency(): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/vendorCurrency');
  }
}
