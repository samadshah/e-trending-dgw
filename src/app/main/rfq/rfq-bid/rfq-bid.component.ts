//tslint:disable
import { Component, OnInit, OnDestroy, ViewChild, Inject } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BidDetail, Financial } from "../models/bid";
import {
  HttpStatusCode,
  RedirectUrl,
  MessageText
} from "../../../core/configuration/config";
import { AppSettingService } from "../../../core/services/app-setting.service";
import { BidDetailService } from "../rfq-service";
import { AttachmentService } from "../../../shared/services/attachment-service";
import { UtiltiyService } from "../../../core/services/utility";
import {
  MetaPostModel,
  AttachmentMetaType,
  Meta,
  Attachment
} from "../../../core/models/AttachmentMeta";
import { BidRequest } from "../models/bid-request.model";
import { NotificationsService } from "angular2-notifications/dist";
import { BidDecline } from "../models/rfq.model";
import { HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { Location } from "@angular/common";
import swal from "sweetalert2";
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";
import * as Moment from "moment";
import { Lookup } from "../../../core/models/lookup";
import { APP_BASE_HREF } from "@angular/common";
import { PlatformLocation } from "@angular/common";

@Component({
  selector: "app-rfq-bid",
  templateUrl: "./rfq-bid.component.html",
  styleUrls: ["./rfq-bid.component.css"]
})
export class RfqBidComponent implements OnInit, OnDestroy {
  left = "0%";
  top = "0%";

  queryParamSubscription: any;
  id: any;
  deliveryDates: any = [];
  currentItem: BidDetail = new BidDetail();
  request: BidRequest = new BidRequest();
  tableId = "#data-table";
  TechPropFiles = null;
  TechProp2Files = null;
  TechPropError = "";
  TechProp2Error = "";
 
  technicalDoc = true;

  tableValidation = "";
  table = true;
  showPopUp = true;
  bidDeclineComments = "";
  bidDeclinePopupFlag = false;
  unSavedChanges = false;
  isFormSubmitted = false;
  canGo: Subject<boolean> = new Subject<boolean>();
  submitDisable = false;
  timer = "";
  currencyList = new Array<Lookup>();
  currencyDropDownList = [];
  selectedCurrency = [];
  singleSelectSettings = {};
  empty = "__";

  poupPosition: any;
  urls = RedirectUrl;
  rfqID = "";
  appbase;
  host = "";
  bidDeclineCommentsRequired = false;
  bidDeclinedCancelLeavePage = false;

  //!!
  gridColumnsDefs = [];
  gridColumns = [];

  constructor(
    private bidDetailService: BidDetailService,
    private router: Router,
    private route: ActivatedRoute,
    private attachmentService: AttachmentService,
    private utiltiy: UtiltiyService,
    private notificationsService: NotificationsService,
    private location: Location,
    private appSettingSrvc: AppSettingService,
    private platformLocation: PlatformLocation,
    @Inject(APP_BASE_HREF) private baseHref: string
  ) {
    this.appbase = (this.platformLocation as any).location.pathname;
    this.host = (this.platformLocation as any).location.host;
  }

  ngOnInit() {
    this.queryParamSubscription = this.route.queryParams.subscribe(params => {
      this.id = params["id"];
    });
    this.singleSelectSettings = {
      singleSelection: true,
      text: "Select Currency",
      classes: "form-control custom-select custom-control",
      enableSearchFilter: true,
      showCheckbox: false,
      searchPlaceholderText: "Search"
    };
    this.currentItem.provideFinancials = new Array<Financial>();
    this.request.provideFinancials = new Array<Financial>();
    //!
    this.request.attachments = new Array<Attachment>();

    this.InitComponent();
  }

  InitComponent() {
    this.route.data.subscribe(
      (data: { bid: HttpResponse<BidDetail> }) => {
        //!
        // data is now an instance of type ItemsResponse, so you can do this:
        this.currentItem = data.bid.body;
        data.bid.body.attachments = data.bid.body.attachments;

        //!! Minus difference of 5 hours from expiry date
        let expiryDate = new Date(this.currentItem.expirationDateAndTime);
        expiryDate.setHours(expiryDate.getHours() - 5);

        this.timer = expiryDate.toISOString();
        this.currentItem.expirationDateAndTime = Moment(expiryDate).format(
          this.appSettingSrvc.settings.dateTimeFormate
        );

        this.InitTimer();
        this.initGrid();
        this.bindGrid(this.currentItem.provideFinancials);

        //!! sets all delivery dates in separate array
        this.currentItem.provideFinancials.forEach((element, index) => {
          this.deliveryDates.push(element.deliveryDate);
        });
      },
      error => {
        if (
          error instanceof HttpErrorResponse &&
          error.status === HttpStatusCode.badRequest
        ) {
          const message = error.error.error || error.error.message;
          this.notificationsService.error(message);
        }
        this.router.navigate([RedirectUrl.rfqs]);
      }
    );
  }

  tableDataRefresh(table, model, isBAFO) {
    table.DataTable().clear();
    table.DataTable().rows.add(model);
    table.DataTable().draw();
  }

  //!
  public initGrid() {
    const isBAFO: any = "No"; // param;
    if (this.currentItem.provideFinancials[0].alternateItemAllow) {
      this.gridColumnsDefs = [
        {
          targets: 1,
          render: (value, type, row, index) => {
            return (
              '<input type="text" class="form-control control-in-column" id="quantity" data-transaction=" ' +
              row.transactionID +
              ' " value="' +
              row.quantity +
              '" disabled>'
            );
          }
        },
        {
          targets: 3,
          render: (value, type, row, index) => {
            return (
              '<input type="text" class="form-control control-in-column" maxlength="30" id="itemName" data-transaction=" ' +
              row.transactionID +
              ' " value="' +
              row.vendorItemId +
              '" >'
            );
          }
        },
        {
          targets: 4,
          render: (value, type, row, index) => {
            return (
              '<input type="text" class="form-control control-in-column" maxlength="80" id="itemDescription" data-transaction=" ' +
              row.transactionID +
              ' " value="' +
              row.vendorItemDescription +
              '" >'
            );
          }
        },
        {
          targets: 5,
          render: (value, type, row, index) => {
            return (
              '<input type="date" class="form-control control-in-column" id="deliveryDate" data-transaction=" ' +
              row.transactionID +
              ' " value="' +
              Moment(row.deliveryDate).format("YYYY-MM-DD") +
              '" >'
            );
          }
        },
        {
          targets: 6,
          render: (value, type, row, index) => {
            return (row.validateCheck !== undefined &&
              row.validateCheck.startsWith("unitPrice")) ||
              (row.validateCheck !== undefined &&
                row.validateCheck.startsWith("line amount"))
              ? '<input  type="text" class="form-control control-in-column inline-control-error unitPrice" id="unitPrice" data-transaction=" ' +
                  row.transactionID +
                  ' " value=" ' +
                  row.unitPrice +
                  '" >'
              : isBAFO === "Yes"
              ? '<input type="text" maxLength="12" class="form-control control-in-column" id="unitPrice" data-transaction=" ' +
                row.transactionID +
                ' " value=" ' +
                row.unitPrice +
                '" disabled>'
              : '<input type="text" maxLength="12" class="form-control control-in-column unitPrice" id="unitPrice" data-transaction=" ' +
                row.transactionID +
                ' " value=" ' +
                row.unitPrice +
                '">';
          }
        },
        {
          targets: 7,
          render: function(value, type, row, index) {
            const dp = "DP-" + index.row;
            const da = "DA-" + index.row;
            return (row.validateCheck !== undefined &&
              row.validateCheck.startsWith("discountAmount")) ||
              (row.validateCheck !== undefined &&
                row.validateCheck.startsWith("line amount"))
              ? '<input type="text" maxLength="12" data-da="' +
                  da +
                  '" data-dp="' +
                  dp +
                  '" class="form-control control-in-column inline-control-error discountAmount ' +
                  da +
                  '" id="discountAmount" data-transaction=" ' +
                  row.transactionID +
                  ' " value=" ' +
                  row.discountAmount +
                  '" >'
              : '<input type="text" maxLength="12" data-da="' +
                  da +
                  '" data-dp="' +
                  dp +
                  '" class="form-control control-in-column discountAmount ' +
                  da +
                  '" id="discountAmount" data-transaction=" ' +
                  row.transactionID +
                  ' " value=" ' +
                  row.discountAmount +
                  '">';
          }
        },
        {
          targets: 8,
          render: function(value, type, row, index) {
            const dp = "DP-" + index.row;
            const da = "DA-" + index.row;
            return (row.validateCheck !== undefined &&
              row.validateCheck.startsWith("discountPercent")) ||
              (row.validateCheck !== undefined &&
                row.validateCheck.startsWith("line amount"))
              ? '<input type="text" maxLength="6" data-da="' +
                  da +
                  '" data-dp="' +
                  dp +
                  '" class="form-control control-in-column inline-control-error discountPercent ' +
                  dp +
                  '" id="discountPercent" data-transaction=" ' +
                  row.transactionID +
                  ' " value=" ' +
                  row.discountPercent +
                  '" >'
              : '<input type="text" maxLength="6" data-da="' +
                  da +
                  '" data-dp="' +
                  dp +
                  '"  class="form-control control-in-column discountPercent ' +
                  dp +
                  '" id="discountPercent" data-transaction=" ' +
                  row.transactionID +
                  ' " value=" ' +
                  row.discountPercent +
                  '" >';
          }
        },
        {
          targets: 9,
          render: (value, type, row, index) => {
            return (
              '<input type="text" class="form-control control-in-column text-right" id="" data-transaction=" ' +
              row.transactionID +
              ' " value=" ' +
              row.vatAmount +
              '" disabled>'
            );
          }
        },
        {
          targets: 10,
          render: (value, type, row, index) => {
            return (
              '<input type="text" class="form-control control-in-column text-right" id="lineAmount" data-transaction=" ' +
              row.transactionID +
              ' " value=" ' +
              this.setTwoNumberDecimal(row.lineAmount, 2) +
              '" disabled>'
            );
          }
        },
        {
          targets: 11,
          visible: false
        }
      ];

      this.gridColumns = [
        {
          data: "itemService",
          title: "Item/Service",
          class: "min-phone-l",
          width: "11%"
        },
        {
          data: "quantity",
          title: "Quantity",
          class: "columTxt-right controlWraper min-phone-l",
          width: "6%"
        },
        {
          data: "unit",
          title: "Unit",
          class: "controlWraper min-tablet-p",
          width: "4%"
        },
        //!
        {
          data: "itemName",
          title: "Item Name",
          class: "controlWraper desktop",
          width: "11%"
        },
        {
          data: "itemDescription",
          title: "Item Description",
          class: "controlWraper desktop",
          width: "13%"
        },
        {
          data: "deliveryDate",
          title: "New Delivery Date",
          class: "controlWraper desktop",
          width: "10%"
        },

        {
          data: "unitPrice",
          title: 'Unit Price <label class="required"></label>',
          class: "columTxt-right controlWraper min-tablet-l",
          width: "10%"
        },
        {
          data: "discountAmount",
          title: "Discount Amount",
          class: "columTxt-right controlWraper desktop",
          width: "9%"
        },
        {
          data: "discountPercent",
          title: "Discount Percentage %",
          class: "columTxt-right controlWraper desktop",
          width: "9%"
        },
        {
          data: "vatAmount",
          title: "VAT Amount",
          class: "columTxt-right controlWraper desktop",
          width: "7%"
        },
        {
          data: "lineAmount",
          title: "Line Amount",
          class: "columTxt-right controlWraper desktop",
          width: "10%"
        },
        { data: "vatPercent", title: "" }
      ];
    } else {
      this.gridColumnsDefs = [
        {
          targets: 1,
          render: (value, type, row, index) => {
            return (
              '<input type="text" class="form-control control-in-column" id="quantity" data-transaction=" ' +
              row.transactionID +
              ' " value=" ' +
              row.quantity +
              '" disabled>'
            );
          }
        },
        //!
        {
          targets: 3,
          render: (value, type, row, index) => {
            return (
              '<input type="date" class="form-control control-in-column" id="deliveryDate" data-transaction=" ' +
              row.transactionID +
              ' " value="' +
              Moment(row.deliveryDate).format("YYYY-MM-DD") +
              '">'
            );
          }
        },
        {
          targets: 4,
          render: (value, type, row, index) => {
            return (row.validateCheck !== undefined &&
              row.validateCheck.startsWith("unitPrice")) ||
              (row.validateCheck !== undefined &&
                row.validateCheck.startsWith("line amount"))
              ? '<input type="text" maxLength="12" class="form-control control-in-column inline-control-error unitPrice" id="unitPrice" data-transaction=" ' +
                  row.transactionID +
                  ' " value=" ' +
                  row.unitPrice +
                  '" >'
              : isBAFO === "Yes"
              ? '<input  type="text" maxLength="12" class="form-control control-in-column" id="unitPrice" data-transaction=" ' +
                row.transactionID +
                ' " value=" ' +
                row.unitPrice +
                '" disabled>'
              : '<input type="text" maxLength="12" class="form-control control-in-column unitPrice" id="unitPrice" data-transaction=" ' +
                row.transactionID +
                ' " value=" ' +
                row.unitPrice +
                '">';
          }
        },
        {
          targets: 5,
          render: function(value, type, row, index) {
            const dp = "DP-" + index.row;
            const da = "DA-" + index.row;
            return (row.validateCheck !== undefined &&
              row.validateCheck.startsWith("discountAmount")) ||
              (row.validateCheck !== undefined &&
                row.validateCheck.startsWith("line amount"))
              ? '<input  type="text" maxLength="12" data-da="' +
                  da +
                  '" data-dp="' +
                  dp +
                  '" class="form-control control-in-column inline-control-error discountAmount ' +
                  da +
                  '" id="discountAmount" data-transaction=" ' +
                  row.transactionID +
                  ' " value=" ' +
                  row.discountAmount +
                  '" >'
              : '<input type="text" maxLength="12" data-da="' +
                  da +
                  '" data-dp="' +
                  dp +
                  '" class="form-control control-in-column discountAmount ' +
                  da +
                  '" id="discountAmount" data-transaction=" ' +
                  row.transactionID +
                  ' " value=" ' +
                  row.discountAmount +
                  '">';
          }
        },
        {
          targets: 6,
          render: function(value, type, row, index) {
            const dp = "DP-" + index.row;
            const da = "DA-" + index.row;
            return (row.validateCheck !== undefined &&
              row.validateCheck.startsWith("discountPercent")) ||
              (row.validateCheck !== undefined &&
                row.validateCheck.startsWith("line amount"))
              ? '<input type="text" maxLength="6" data-da="' +
                  da +
                  '" data-dp="' +
                  dp +
                  '" class="form-control control-in-column inline-control-error discountPercent ' +
                  dp +
                  '" id="discountPercent" data-transaction=" ' +
                  row.transactionID +
                  ' " value=" ' +
                  row.discountPercent +
                  '" >'
              : '<input type="text" maxLength="6" data-da="' +
                  da +
                  '" data-dp="' +
                  dp +
                  '"  class="form-control control-in-column discountPercent ' +
                  dp +
                  '" id="discountPercent" data-transaction=" ' +
                  row.transactionID +
                  ' " value=" ' +
                  row.discountPercent +
                  '" >';
          }
        },
        {
          targets: 7,
          render: (value, type, row, index) => {
            return (
              '<input type="text" class="form-control control-in-column text-right" id="" data-transaction=" ' +
              row.transactionID +
              ' " value=" ' +
              row.vatAmount +
              '" disabled>'
            );
          }
        },
        {
          targets: 8,
          render: (value, type, row, index) => {
            return (
              '<input type="text" class="form-control control-in-column text-right" id="lineAmount" data-transaction=" ' +
              row.transactionID +
              ' " value=" ' +
              this.setTwoNumberDecimal(row.lineAmount, 2) +
              '" disabled>'
            );
          }
        },
        {
          targets: 9,
          visible: false
        }
      ];

      this.gridColumns = [
        {
          data: "itemService",
          title: "Item/Service",
          class: "min-phone-l",
          width: "15%"
        },
        {
          data: "quantity",
          title: "Quantity",
          class: "columTxt-right controlWraper min-phone-l",
          width: "6%"
        },
        {
          data: "unit",
          title: "Unit",
          class: "controlWraper min-tablet-p",
          width: "6%"
        },
        //!
        {
          data: "deliveryDate",
          title: "New Delivery Date",
          class: "controlWraper desktop",
          width: "15%"
        },

        {
          data: "unitPrice",
          title: 'Unit Price <label class="required"></label>',
          class: "columTxt-right controlWraper min-tablet-l",
          width: "15%"
        },
        {
          data: "discountAmount",
          title: "Discount Amount",
          class: "columTxt-right controlWraper desktop",
          width: "10%"
        },
        {
          data: "discountPercent",
          title: "Discount Percentage %",
          class: "columTxt-right controlWraper desktop",
          width: "10%"
        },
        {
          data: "vatAmount",
          title: "VAT Amount",
          class: "columTxt-right controlWraper desktop",
          width: "10%"
        },
        {
          data: "lineAmount",
          title: "Line Amount",
          class: "columTxt-right controlWraper desktop",
          width: "13%"
        },
        { data: "vatPercent", title: "" }
      ];
    }
  }

  //!
  public bindGrid(data) {
    const dtId: any = $(this.tableId);
    const tableWidget = dtId.DataTable({
      responsive: true,
      paging: false,
      searching: false,
      info: false,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      ordering: false,
      autoWidth: false,
      columnDefs: this.gridColumnsDefs,
      columns: this.gridColumns,
      data: data,
      createdRow: (row, rowData, index) => {
        setTimeout(() => {
          const da = "DA-" + index;
          const dp = "DP-" + index;
          if (rowData.discountAmount > 0) {
            $("." + da).removeAttr("disabled");
            $("." + dp).attr("disabled", "disabled");
          } else if (rowData.discountPercent > 0) {
            $("." + dp).removeAttr("disabled");
            $("." + da).attr("disabled", "disabled");
          } else {
            $("." + da).removeAttr("disabled");
            $("." + dp).removeAttr("disabled");
          }
        }, 1000);
      }
    });

    const unitPriceMask: any = $(".unitPrice");
    unitPriceMask.inputmask({
      alias: "numeric",
      prefix: "",
      autoGroup: true,
      digits: 2,
      digitsOptional: false,
      max: 100000000
    });

    const discountAmountMask: any = $(".discountAmount");
    discountAmountMask.inputmask({
      alias: "numeric",
      prefix: "",
      autoGroup: true,
      digits: 2,
      digitsOptional: false,
      max: 99999999
    });

    const discountPercentMask: any = $(".discountPercent");
    discountPercentMask.inputmask({
      alias: "numeric",
      prefix: "",
      autoGroup: true,
      digits: 2,
      digitsOptional: false,
      max: 100
    });

    //!!
    dtId.on("blur", "#itemName", e => {
      e.preventDefault();
      const value = e.currentTarget.value;
      const transactionID =
        e.currentTarget.attributes["data-transaction"].nodeValue;
      const index = this.findRowIndexByTransactionId(transactionID, dtId);
      dtId
        .DataTable()
        .row(index)
        .data().vendorItemId = value;
    });

    //!!
    dtId.on("blur", "#itemDescription", e => {
      e.preventDefault();
      const value = e.currentTarget.value;
      const transactionID =
        e.currentTarget.attributes["data-transaction"].nodeValue;
      const index = this.findRowIndexByTransactionId(transactionID, dtId);
      dtId
        .DataTable()
        .row(index)
        .data().vendorItemDescription = value;
    });

    //!!
    dtId.on("change", "#deliveryDate", e => {
      e.preventDefault();
      const value = e.currentTarget.value;
      const transactionID =
        e.currentTarget.attributes["data-transaction"].nodeValue;
      const index = this.findRowIndexByTransactionId(transactionID, dtId);

      dtId
        .DataTable()
        .row(index)
        .data().deliveryDate = value;
    });

    //!
    dtId.on("change", "#unitPrice", e => {
      e.preventDefault();
      const value = e.currentTarget.value;
      const transactionID =
        e.currentTarget.attributes["data-transaction"].nodeValue;
      const index = this.findRowIndexByTransactionId(transactionID, dtId);
      const validateCheck = this.validateInput(value, "unitPrice");
      const unitPrice = (dtId
        .DataTable()
        .row(index)
        .data().unitPrice = +value);
      const discountAmount = dtId
        .DataTable()
        .row(index)
        .data().discountAmount;
      let discountPercent = dtId
        .DataTable()
        .row(index)
        .data().discountPercent;
      discountPercent = discountPercent > 100 ? 100 : discountPercent;
      const quantity = dtId
        .DataTable()
        .row(index)
        .data().quantity;
      const lineAmount = this.calculateLineAmount(
        unitPrice,
        discountAmount,
        discountPercent,
        quantity
      );
      dtId
        .DataTable()
        .row(index)
        .data().lineAmount = lineAmount;
      dtId
        .DataTable()
        .row(index)
        .data().vatAmount = this.calculateVatAmount(
        dtId
          .DataTable()
          .row(index)
          .data().vatPercent,
        lineAmount
      );

      this.rebindTable(dtId, index, validateCheck);
      const unitPriceMask2: any = $(".unitPrice");
      unitPriceMask2.inputmask({
        alias: "numeric",
        prefix: "",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        max: 100000000
      });
      const discountAmountMask2: any = $(".discountAmount");
      discountAmountMask2.inputmask({
        alias: "numeric",
        prefix: "",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        max: 99999999
      });
      const discountPercentMask2: any = $(".discountPercent");
      discountPercentMask2.inputmask({
        alias: "numeric",
        prefix: "",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        max: 100
      });
    });

    dtId.on("change", "#discountAmount", e => {
      e.preventDefault();
      const value = e.currentTarget.value === "" ? 0 : e.currentTarget.value;
      const transactionID =
        e.currentTarget.attributes["data-transaction"].nodeValue;
      const index = this.findRowIndexByTransactionId(transactionID, dtId);
      const validateCheck = this.validateInput(value, "discountAmount");
      const unitPrice = dtId
        .DataTable()
        .row(index)
        .data().unitPrice;
      const discountAmount = (dtId
        .DataTable()
        .row(index)
        .data().discountAmount = +value);
      let discountPercent = dtId
        .DataTable()
        .row(index)
        .data().discountPercent;
      discountPercent = discountPercent > 100 ? 100 : discountPercent;
      const quantity = dtId
        .DataTable()
        .row(index)
        .data().quantity;
      const lineAmount = this.calculateLineAmount(
        unitPrice,
        discountAmount,
        discountPercent,
        quantity
      );
      dtId
        .DataTable()
        .row(index)
        .data().lineAmount = lineAmount;
      dtId
        .DataTable()
        .row(index)
        .data().vatAmount = this.calculateVatAmount(
        dtId
          .DataTable()
          .row(index)
          .data().vatPercent,
        lineAmount
      );

      this.rebindTable(dtId, index, validateCheck);
      const unitPriceMask2: any = $(".unitPrice");
      unitPriceMask2.inputmask({
        alias: "numeric",
        prefix: "",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        max: 100000000
      });
      const discountAmountMask2: any = $(".discountAmount");
      discountAmountMask2.inputmask({
        alias: "numeric",
        prefix: "",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        max: 99999999
      });
      const discountPercentMask2: any = $(".discountPercent");
      discountPercentMask2.inputmask({
        alias: "numeric",
        prefix: "",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        max: 100
      });
      const da = e.currentTarget.attributes["data-da"].nodeValue;
      const dp = e.currentTarget.attributes["data-dp"].nodeValue;
      if (discountAmount > 0) {
        $("." + da).removeAttr("disabled");
        $("." + dp).attr("disabled", "disabled");
      } else {
        $("." + da).removeAttr("disabled");
        $("." + dp).removeAttr("disabled");
      }
    });

    dtId.on("change", "#discountPercent", e => {
      e.preventDefault();
      const value = e.currentTarget.value === "" ? 0 : e.currentTarget.value;
      const transactionID =
        e.currentTarget.attributes["data-transaction"].nodeValue;
      const index = this.findRowIndexByTransactionId(transactionID, dtId);
      const validateCheck = this.validateInput(value, "discountPercent");
      const unitPrice = dtId
        .DataTable()
        .row(index)
        .data().unitPrice;
      const discountAmount = dtId
        .DataTable()
        .row(index)
        .data().discountAmount;
      let discountPercent = (dtId
        .DataTable()
        .row(index)
        .data().discountPercent = +value);
      discountPercent = discountPercent > 100 ? 100 : discountPercent;
      const quantity = dtId
        .DataTable()
        .row(index)
        .data().quantity;
      const lineAmount = this.calculateLineAmount(
        unitPrice,
        discountAmount,
        discountPercent,
        quantity
      );
      dtId
        .DataTable()
        .row(index)
        .data().lineAmount = lineAmount;
      dtId
        .DataTable()
        .row(index)
        .data().vatAmount = this.calculateVatAmount(
        dtId
          .DataTable()
          .row(index)
          .data().vatPercent,
        lineAmount
      );

      this.rebindTable(dtId, index, validateCheck);
      const unitPriceMask2: any = $(".unitPrice");
      unitPriceMask2.inputmask({
        alias: "numeric",
        prefix: "",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        max: 100000000
      });
      const discountAmountMask2: any = $(".discountAmount");
      discountAmountMask2.inputmask({
        alias: "numeric",
        prefix: "",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        max: 99999999
      });
      const discountPercentMask2: any = $(".discountPercent");
      discountPercentMask2.inputmask({
        alias: "numeric",
        prefix: "",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        max: 100
      });

      const da = e.currentTarget.attributes["data-da"].nodeValue;
      const dp = e.currentTarget.attributes["data-dp"].nodeValue;
      if (discountPercent > 0) {
        $("." + dp).removeAttr("disabled");
        $("." + da).attr("disabled", "disabled");
      } else {
        $("." + da).removeAttr("disabled");
        $("." + dp).removeAttr("disabled");
      }
    });

    dtId.on("click", "td", function(e) {
      if (e.currentTarget.getAttribute("tabindex") === "0") {
        const unitPriceMaskRes: any = $(".unitPrice");
        unitPriceMaskRes.inputmask({
          alias: "numeric",
          prefix: "",
          autoGroup: true,
          digits: 2,
          digitsOptional: false,
          max: 100000000
        });

        const discountAmountMaskRes: any = $(".discountAmount");
        discountAmountMaskRes.inputmask({
          alias: "numeric",
          prefix: "",
          autoGroup: true,
          digits: 2,
          digitsOptional: false,
          max: 99999999
        });

        const discountPercentMaskRes: any = $(".discountPercent");
        discountPercentMaskRes.inputmask({
          alias: "numeric",
          prefix: "",
          autoGroup: true,
          digits: 2,
          digitsOptional: false,
          max: 100
        });
      }
    });

    //!!
    this.calculateNetAmount(dtId);
  }

  public calculateVatAmount(percent, lineAmount) {
    return (percent / 100) * lineAmount;
  }

  public rebindTable(dtId, index, validateCheck) {
    const model = dtId
      .DataTable()
      .row(index)
      .data();
    if (model.lineAmount > 0 || validateCheck !== undefined) {
      model.validateCheck = validateCheck;
    } else {
      model.validateCheck = "line amount must be greater than zero";
    }
    if (validateCheck === undefined) {
      this.table = true;
    }
    dtId
      .DataTable()
      .row(index)
      .data(model)
      .draw();
    this.calculateNetAmount(dtId);
    this.unSavedChanges = true;
  }

  public calculateNetAmount(dtId) {
    const value = dtId.DataTable().data();
    let bidTotal = 0;
    let vatAmount = 0;
    for (let i = 0; i < value.length; i++) {
      bidTotal = bidTotal + value[i].lineAmount;
      vatAmount = vatAmount + value[i].vatAmount;
    }
    this.currentItem.bidTotal = bidTotal;
    this.currentItem.vatAmount = vatAmount;
    this.currentItem.netAmount = bidTotal + vatAmount;
    this.request.netAmount = bidTotal;
  }

  public calculateLineAmount(
    unitPrice,
    discountAmount,
    discountPercent,
    quantity
  ) {
    let lineAmount = 0;

    if (discountPercent > 0) {
      lineAmount =
        (unitPrice - discountAmount) *
        ((100 - discountPercent) / 100) *
        quantity;
    } else {
      lineAmount = (unitPrice - discountAmount) * quantity;
    }

    return lineAmount;
  }

  public validateInput(value, item) {
    if (value === "") {
      return item + "required";
    }
    value = +value;
    if (value > 0) {
      return undefined;
    }
    if (item === "unitPrice") {
      if (value < 1) {
        return "unitPrice mut be greater than zero";
      } else {
        return "unitPrice mut be a number";
      }
    }
    if (item === "discountPercent") {
      if (value === 0) {
        return undefined;
      } else if (value < 0) {
        return item + "mut be positive";
      } else if (value > 99) {
        return item + "can not be greater and equal to 100";
      } else {
        return item + "mut be a number";
      }
    }

    if (item === "discountAmount") {
      if (value === 0) {
        return undefined;
      } else if (value < 0) {
        return item + "mut be positive";
      } else {
        return item + "mut be a number";
      }
    }
  }

  public validateDeliveryDate(value) {
    if (
      Moment(value).format("YYYY-MM-DD") <
      Moment(new Date()).format("YYYY-MM-DD")
    ) {
      return "Delivery date can not be less than current date";
    } else {
      return undefined;
    }
  }

  public setTwoNumberDecimal(value, param) {
    value = value.toFixed(param);
    const data = value.split(".");
    value = parseFloat(data[0]).toLocaleString() + "." + data[1];
    return value;
  }

  public findRowIndexByTransactionId(transactionID, tableId) {
    const dtId: any = $(tableId);
    const data = dtId.DataTable().data();
    for (let i = 0; i < data.length; i++) {
      if (Number(data[i]["transactionID"]) === Number(transactionID)) {
        return i;
      }
    }
  }

  public InitTimer() {
    function getTimeRemaining(endtime) {
      endtime = new Date(endtime);
      let t = 0;
      let seconds = 0;
      let minutes = 0;
      let hours = 0;
      let days = 0;
      if (endtime > new Date()) {
        t = endtime.getTime() - new Date().getTime();
        seconds = Math.floor((t / 1000) % 60);
        minutes = Math.floor((t / 1000 / 60) % 60);
        hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        days = Math.floor(t / (1000 * 60 * 60 * 24));
      }
      return {
        total: t,
        days: days,
        hours: hours,
        minutes: minutes,
        seconds: seconds
      };
    }

    function initializeClock(id, endtime) {
      const clock = document.getElementById(id);
      const daysSpan = clock.querySelector(".days");
      const hoursSpan = clock.querySelector(".hours");
      const minutesSpan = clock.querySelector(".minutes");
      const secondsSpan = clock.querySelector(".seconds");

      function updateClock() {
        const t = getTimeRemaining(endtime);
        daysSpan.innerHTML = t.days.toString();
        hoursSpan.innerHTML = ("0" + t.hours).slice(-2);
        minutesSpan.innerHTML = ("0" + t.minutes).slice(-2);
        secondsSpan.innerHTML = ("0" + t.seconds).slice(-2);
        if (t.total <= 0) {
          clearInterval(timeinterval);
        }
      }

      updateClock();
      const timeinterval = setInterval(updateClock, 1000);
    }

    initializeClock("clockdiv", this.timer);
  }

  public getAmount() {
    return this.currentItem.currency + " " + " ";
  }

  //!
  public downloadAttachment(item) {
    const payload = {
      Id: item.id,
      Purpose: "DOC"
    };

    this.attachmentService.downloadAttachment(payload).subscribe(
      resp => {
        const blob = new Blob([resp.content], { type: resp.content.type });
        const fileName = item.filename;

        if (this.utiltiy.isBrowserIe() === false) {
          const objectUrl = URL.createObjectURL(blob);
          const a: HTMLAnchorElement = document.createElement(
            "a"
          ) as HTMLAnchorElement;

          a.href = objectUrl;
          a.download = fileName;
          document.body.appendChild(a);
          a.click();

          document.body.removeChild(a);
          URL.revokeObjectURL(objectUrl);
        } else {
          window.navigator.msSaveBlob(blob, fileName);
        }
      },
      err => {
        if (
          err instanceof HttpErrorResponse &&
          err.status === HttpStatusCode.badRequest
        ) {
          const message = err.error.error || err.error.Message;
          this.notificationsService.error(message);
        }
      }
    );
  }

  //!
  public downloadAllAttachment() {
    // this.postAttachmentMeta.metaTypes = new Array<AttachmentMetaType>();
    // this.postAttachmentMeta.metaTags = new Array<Meta>();
    // for (const entry of this.currentItem.metaTagTypeList) {
    //   if (entry.docType === 'RFQSupportDoc') {
    //     this.postAttachmentMeta.metaTypes.push(entry);
    //   }
    // }
    // for (const entry of this.currentItem.metaTags) {
    //   if (entry.docType === 'RFQSupportDoc') {
    //     this.postAttachmentMeta.metaTags.push(entry);
    //   }
    // }
    // const value = this.currentItem.attachments.find(x => x.docType === 'RFQSupportDoc');
    // if (value) {
    //   this.attachmentService.downloadAttachment(this.postAttachmentMeta).subscribe(
    //     resp => {
    //       const blob = new Blob([resp.content], { type: resp.content.type });
    //       const fileName = 'Supporting Documents';
    //       if (this.utiltiy.isBrowserIe() === false) {
    //         const objectUrl = URL.createObjectURL(blob);
    //         const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
    //         a.href = objectUrl;
    //         a.download = fileName;
    //         document.body.appendChild(a);
    //         a.click();
    //         document.body.removeChild(a);
    //         URL.revokeObjectURL(objectUrl);
    //       } else {
    //         window.navigator.msSaveBlob(blob, fileName);
    //       }
    //     },
    //     err => {
    //       if (err instanceof HttpErrorResponse
    //         && (err.status === HttpStatusCode.badRequest)) {
    //         const message = err.error.error || err.error.message;
    //         this.notificationsService.error(message);
    //       }
    //     });
    // }
  }

  //!
  public deleteAttachment(item) {
    // const result = this.currentItem.attachments.find(x => x.id === item.id);
    // const index = this.request.attachments.findIndex(x => x.id === item.id);
    // if (result) {
    //   this.request.attachments[index].isDeleted = true;
    // } else {
    //   this.request.attachments.splice(index, 1);
    // }
  }

  public getBackLink() {
    this.location.back();
  }

  public cancel() {
    this.getBackLink();
  }

  //!
  public validate() {

    this.table = true;

    for (let i = 0; i < this.request.provideFinancials.length; i++) {
      if (this.request.provideFinancials[i].lineAmount == 0) {
        this.table = false;
        this.tableValidation = "Line amount can not be 0";
      }
      if (this.request.provideFinancials[i].unitPrice == 0) {
        this.table = false;
        this.tableValidation = "Unit Price can not be 0";
      }
      if (this.request.provideFinancials[i].unitPrice < 0) {
        this.table = false;
        this.tableValidation = "Unit Price can not be less than 0";
      }
      if (this.request.provideFinancials[i].discountAmount < 0) {
        this.table = false;
        this.tableValidation = "Discount amount can not be less than 0";
      }
      if (this.request.provideFinancials[i].discountPercent < 0) {
        this.table = false;
        this.tableValidation = "Discount Percentage can not be less than 0";
      }
      let validateDate = this.validateDeliveryDate(
        this.request.provideFinancials[i].deliveryDate
      );
      if (validateDate != undefined) {
        this.table = false;
        this.tableValidation = validateDate;
      } else if (this.request.provideFinancials[i].deliveryDate == "") {
        //!! set original delivery date if selected empty
        this.request.provideFinancials[i].deliveryDate = this.deliveryDates[i];
      }
    }

    //!
    // if (this.technicalDoc && this.financialDoc && this.table) {
    if (this.table) {
      return true;
    } else {
      return false;
    }
  }

  openPopUp() {
    this.showPopUp = false;
    $('<div class="modal-backdrop fade in"></div>').appendTo(document.body);
    $("#submit").addClass("modal fade in block");
  }
  closePopUp() {
    this.showPopUp = true;
    $("#submit").removeClass("modal fade in block");
    $(".modal-backdrop").remove();
  }

  //!
  public Save(value) {
    const dtId: any = $(this.tableId);
    const data = dtId.DataTable().data();
    this.calculateNetAmount(dtId);
    this.request.provideFinancials = new Array<Financial>();
    for (let i = 0; i < data.length; i++) {
      const financial = new Financial();
      financial.discountAmount = data[i].discountAmount;
      financial.discountPercent = data[i].discountPercent;
      financial.itemService = data[i].itemService;
      financial.lineAmount = data[i].lineAmount;
      financial.quantity = data[i].quantity;
      financial.transactionID = data[i].transactionID;
      financial.unit = data[i].unit;
      financial.unitPrice = data[i].unitPrice;
      //!!
      if (data[i].deliveryDate != "") {
        financial.deliveryDate = new Date(data[i].deliveryDate).toISOString();
      } else {
        financial.deliveryDate = "";
      }

      if (this.currentItem.provideFinancials[0].alternateItemAllow) {
        financial.vendorItemId = data[i].vendorItemId;
        financial.vendorItemDescription = data[i].vendorItemDescription;
      }

      this.request.provideFinancials.push(financial);
    }

    this.request.transactionID = this.currentItem.transactionID;
    this.request.replyId = this.currentItem.replyId;
    this.request.submissionType = value;
    this.request.currency = this.currentItem.currency;

    // if (value === 0) {
    //     this.call();
    // } else {
    if (this.validate()) {
      this.call();
    }
    // }
  }

  //!
  public call() {

    if (this.submitDisable === true) {
      return false;
    }
    this.submitDisable = true;

    this.bidDetailService.save(this.request).subscribe(
      res => {
        if (res.isSuccess) {
          this.isFormSubmitted = true;
          if (this.request.submissionType === 0) {
            this.notificationsService.success(MessageText.bidSaved);
          } else {
            this.notificationsService.success(MessageText.bidSubmitted);
          }
          this.getBackLink();
          this.submitDisable = false;
        } else {
          this.notificationsService.error(res.message);
          this.submitDisable = false;
        }
      },
      err => {
        if (err.status === HttpStatusCode.badRequest) {
          this.notificationsService.error(err.error.Message);
        }
        this.submitDisable = false;
      }
    );
  }

  //!!
  public closeBid() {
    this.router.navigate([RedirectUrl.rfqs]);
  }

  //!
  public uploaderStatusTech(data) {
    for (let i = 0; i < data.length; i++) {
      const attach = new Attachment();
      attach.id = data[i].id;
      attach.name = data[i].fileName;
      attach.docType = data[i].docType;

      this.request.attachments.push(attach);
      this.technicalDoc = true;
      this.unSavedChanges = true;
    }
  }

  //!
  // public uploaderStatusFin(data) {

  //   for (let i = 0; i < data.length; i++) {
  //     const attach = new Attachment();
  //     attach.id = data[i].id;
  //     attach.name = data[i].fileName;
  //     attach.docType = data[i].docType;
  //     if (this.currentItem.isBAFO === 'Yes') {
  //       attach.isNew = true;
  //     }
  //     this.request.attachments.push(attach);
  //     this.financialDoc = true;
  //     this.unSavedChanges = true;
  //   }
  // }

  onCurrencySelect(item, model) {
    model.currency = item.id;
  }

  onCurrencyDeSelect(item, model) {
    model.currency = "";
  }

  ngOnDestroy() {
    const dtId: any = $(this.tableId);
    dtId.DataTable().destroy();
  }

  //!
  public bidDeclineSubmit() {
    if (this.bidDeclineComments) {
      this.bidDeclineCommentsRequired = false;
    } else {
      this.bidDeclineCommentsRequired = true;
      // return false;
    }
    if (!this.bidDeclineCommentsRequired) {
      const decline = new BidDecline();
      decline.declineComments = this.bidDeclineComments;
      decline.replyId = this.currentItem.replyId;
      decline.transactionId = this.currentItem.transactionID;

      this.bidDetailService.declineBid(decline).subscribe(
        data => {
          this.bidDeclinedCancelLeavePage = true;
          if (data.isSuccess) {
            this.closePopUp();
            this.getBackLink();
          } else {
            this.closePopUp();
            this.notificationsService.error(data.message);
          }
        },
        err => {}
      );
    }
  }

  showAttachError(list, value) {
    return !(list.filter(x => x.docType === value).length < 1);
  }

  canDeactivate(): Observable<boolean> | boolean {
    this.canGo = new Subject<boolean>();

    if (this.bidDeclinedCancelLeavePage === true) {
      return true;
    }

    // if form is submitted
    if (this.isFormSubmitted === true) {
      return true;
    }

    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    if (this.unSavedChanges === false) {
      return true;
    }
    // Otherwise ask the user with the dialog service and return its
    // observable which resolves to true or false when the user decides
    swal({
      title: MessageText.FormLeavingTitle,
      text: MessageText.FormLeavingText,
      // type: 'warning',
      showCancelButton: true,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      confirmButtonClass: "btn btn-default",
      cancelButtonClass: "btn btn-info",
      buttonsStyling: false,
      reverseButtons: true,
      width: 360
    }).then(discardChanges => {
      if (discardChanges.value) {
        this.canGo.next(true);
      } else if (discardChanges.dismiss.toString() === "cancel") {
        if (window.history.state) {
          const url = window.history.state.url;
          window.history.pushState({ url: url }, "", url);
        } else {
          const url = "vp/home";
          window.history.pushState({ url: url }, "", url);
        }
        this.canGo.next(false);
      }
    });
    return this.canGo;
  }
}
