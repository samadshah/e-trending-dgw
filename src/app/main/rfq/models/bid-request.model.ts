import { AttachmentMetaType, Meta, Attachment } from '../../../core/models/AttachmentMeta';

//!
export class BidRequest {
    public comments: string;
    public currency: string;
    public netAmount: number;
    public replyId: string;
    public submissionType: number;
    public transactionID: string;
    public vatAmount: number;
    public bidTotal: number;
    public provideFinancials: Array<Financial>;
    //!
    // public metaTagTypeList: Array<AttachmentMetaType>;
    // public metaTags: Array<Meta>;
    public attachments: Array<Attachment>;
}

export class Financial {
    public discountAmount: number;
    public discountPercent: number;
    public itemService: string;
    public lineAmount: number;
    public quantity: number;
    public transactionID: number;
    public unit: string;
    public unitPrice: number;
    public vatPercent: number;
    public vatAmount: number;
    //!!
    public alternateItemAllow: boolean;
    public deliveryDate: string;
    public vendorItemId: string;
    public vendorItemDescription: string;
}
