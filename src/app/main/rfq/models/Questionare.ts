    export class QuestionareResponse {
        public  questionnaireForRFQId: string;
        public  notes: string;
        public  qaList: Array<Questionare>;
    }

    export class Questionare {
        public questions: Array<QuestionContract>;
        public answers: Array<AnswerContract>;

    }

    export class QuestionContract {
        public  isMandatory: string;
        public  question: string;
        public  questionId: string;
        public  questionType: string;
        public  sequence: number;
    }

    export class AnswerContract {
        public  answer: string;
        public  sequence: string;
    }



export class Question {
    public isMandatory: string;
    public question: string;
    public  questionId: number;
    public questionType: string;
    public sequenceNo: number;
    public options: Array<Option>;
    public answer: Answer;
 }

 export class Option {
    public  text: string;
    public  sequenceNo: string;
    public  questionId: number;
    public  selected: boolean;
 }

 export class Answer {
    public  questionId: number;
    public  answer: string;
 }
 export class PostQuestionare {
    public questionnaireType: string;
    public rfqId: string;
    public vendorId: string;
    public  answers: Array<Answer>;
 }
