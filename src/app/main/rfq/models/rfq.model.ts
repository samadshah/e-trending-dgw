// import { AttachmentMeta } from '../../../core/models/AttachmentMeta';

export class Quotation {
    columns: ColumnsModel[];
    pagination: PaginationModel;
    rfqList: RFQModel[];
}

export class ColumnsModel {
    name: string;
    value: string;
    dataType: string;
    allowFilterSort: boolean;
}

export class PaginationModel {
    pageNo: number;
    pageSize: number;
    pageCount: number;
}

export class RFQModel {
    bid: string;
    bidRecall: string;
    bidType: string;
    caseId: string;
    documentTitle: string;
    expiryDateTime: string;
    status: string;
    rfqId: string;
}


export class IQuotation {
    columns: ColumnsModel[];
    pagination: PaginationModel;
    rfqList: RFQModel[];
}


export class IColumnsModel {
    name: string;
    value: string;
}

export class IPaginationModel {
    pageNo: number;
    pageSize: number;
    pageCount: number;
}

export class IRFQModel {
    bid: string;
    bidRecall: string;
    bidType: string;
    caseId: string;
    documentTitle: string;
    expiryDateTime: string;
    status: string;
    rfqId: string;
}

//!
export class BidDecline {
    declineComments: string;
    replyId: string;
    transactionId: string;
    // vendorId: string;
}
