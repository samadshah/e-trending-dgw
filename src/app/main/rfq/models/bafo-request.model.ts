import { AttachmentMetaType, Meta, Attachment } from '../../../core/models/AttachmentMeta';

export class BafoRequest {
    public comments: string;
    public currency: string;
    public netAmount: number;
    public replyId: string;
    public submissionType: number;
    public transactionID: string;
    public vatAmount: number;
    public bidTotal: number;
    public provideFinancials: Array<Financial>;
    public metaTagTypeList: Array<AttachmentMetaType>;
    public metaTags: Array<Meta>;
    public attachmentList: Array<Attachment>;
}

export class Financial {
    public discountAmount: number;
    public discountPercent: number;
    public transactionID: number;
    public vatPercent : number;
    public vatAmount : number;
}
