import { AttachmentMetaType, Meta, Attachment } from '../../../core/models/AttachmentMeta';


export class BidDetail {
    public canDecline: string;
    public canRecall: string;
    //!
    public isBAFO: string;
    public replyId: string;
    public currency: string;
    public documentTitle: string;
    public expirationDateAndTime: string;
    public notes: string;
    public rfqid: string;
    public rfqTitle: string;
    public transactionID: string;
    public provideFinancials: Array<Financial>;
    public netAmount: number;
    public vatAmount: number;
    public bidTotal: number;
    public attachments: Array<Attachment>;
    //!
    public metaTagTypeList: Array<AttachmentMetaType>;
    public metaTags: Array<Meta>;
}

export class Financial {
    public discountAmount: number;
    public discountPercent: number;
    public itemService: string;
    public lineAmount: number;
    public lineNum: number;
    public quantity: number;
    public transactionID: number;
    public unit: string;
    public unitPrice: number;
    public vatPercent: number;
    public vatAmount: number;
    //!
    public alternateItemAllow: boolean;
    public deliveryDate: string;
    public vendorItemId: string;
    public vendorItemDescription: string;
}
