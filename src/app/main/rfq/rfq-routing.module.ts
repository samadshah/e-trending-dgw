import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RfqComponent } from './rfq.component';
import { RfqListComponent } from './rfq-list/rfq-list.component';
import { RfqDetailComponent } from './rfq-detail/rfq-detail.component';
import { RfqBidComponent } from './rfq-bid/rfq-bid.component';
import { BidResolve } from './bid-resolve.resolve';
import { BidResolveEdit } from './bid-resolve-edit.resolve';
import { CanDeactivateGuard } from '../../core/guards/can-deactivate-guard.service';


const routes: Routes = [
  {
    path: '',
    component: RfqComponent,
    children: [
      {
        path: '',
        component: RfqListComponent,
        canDeactivate: [CanDeactivateGuard],
      },
      {
        path: 'bid',
        component: RfqBidComponent,
        data: {
          breadcrumb: 'Bid'
        },
        resolve: {
          bid: BidResolveEdit
        },
        canDeactivate: [CanDeactivateGuard],
      },
      {
        path: 'view',
        component: RfqDetailComponent,
        data: {
          breadcrumb: 'Quotation'
        },
        resolve: {
          bid: BidResolve
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RfqRoutingModule { }
