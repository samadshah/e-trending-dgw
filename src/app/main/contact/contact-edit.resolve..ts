import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications/dist';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { MessageText, RedirectUrl } from '../../core/configuration/config';
import { ContactDataSharingService } from './contact-data-sharing.service';
import { ContactService } from './contact.service';
import { ContactResponse } from './model/contact-detail';
import { last } from 'rxjs/operators';


@Injectable()
export class ContactEditResolve implements Resolve<HttpResponse<ContactResponse>> {

  constructor(private contactService: ContactService, private router: Router,
    private dataSharingService: ContactDataSharingService, private notificationsService: NotificationsService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<HttpResponse<ContactResponse>> {
    // if (this.dataSharingService.getConfig().contactDetails === undefined) {
      const id = route.queryParams['id']; // route.paramMap.get('id');
      if (id) {
        return this.contactService.getDetailData(id)
          // .pipe(last())
          .catch(() => {
            this.notificationsService.error(MessageText.unableToFetchData);
            this.router.navigate([RedirectUrl.contacts]);
            return of(null);
          });
      } else {
        this.notificationsService.error(MessageText.invaliedId);
        this.router.navigate([RedirectUrl.contacts]);
      }
    // } else {
    //   this.notificationsService.error(MessageText.unableToFetchData);
    //   this.router.navigate([RedirectUrl.contacts]);
    // }
  }
}
