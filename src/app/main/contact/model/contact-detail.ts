import { Lookup } from '../../../core/models/lookup';

export class ContactResponse {
    address: Array<Address>;
    contactDetails: ContactDetails;
    contactInformation: Array<ContactInformation>;
}

export class Address {
    public address: string;
    public building: string;
    public city: string;
    public countryRegion: string;
    public county: string;
    public nameorDescription: string;
    public primary: string;
    public purpose: Array<Lookup>;
    public state: string;
    public street: string;
    public streetNo: string;
    public transactionId: number;
    public zipPostalCode: string;
    public canDelete: string;
    public canEdit: string;
}

export class ContactDetails {
    public language: string;
    public arabicFirstName: string;
    public arabicLastName: string;
    public contactPersonId: string;
    public firstName: string;
    public gender: string;
    public jobTitle: string;
    public lastName: string;
    public maritalStatus: string;
    public otherJobTitle: string;
    public transactionId: number;
}


export class ContactInformation {
    public contactNumberAddress: string;
    public description: string;
    public primary: string;
    public type: string;
    public transactionId: number;
}
