import { Lookup } from '../../../core/models/lookup';

export class ContactEditRequest {
    public contactPersonId: string;
    public address: Array<AddressRequest>;
    public contactDetails: ContactDetailsRequest;
    public contactInformation: Array<ContactInformationRequest>;
}

export class AddressRequest {
    public address: string;
    public building: string;
    public city: string;
    public countryRegion: string;
    public county: string;
    public nameorDescription: string;
    public primary: any;
    public purpose: Array<Lookup>;
    public state: string;
    public street: string;
    public streetNo: string;
    public transactionId: number;
    public zipPostalCode: string;
    public isDeleted: boolean;
    public index: string;
    public canDelete: string;
    public canEdit: string;
}

export class ContactDetailsRequest {
    public arabicFirstName: string;
    public arabicLastName: string;
    public contactPersonId: string;
    public firstName: string;
    public gender: string;
    public jobTitle: string;
    public lastName: string;
    public maritalStatus: string;
    public middleName: string;
    public otherJobTitle: string;
    public language: string;
    public languageId: string;
    public transactionId: number;
}


export class ContactInformationRequest {
    public contactNumberAddress: string;
    public description: string;
    public primary: any;
    public type: string;
    public transactionId: number;
    public isDeleted: boolean;
    public index: string;
}
