import { Injectable } from '@angular/core';
import { ContactResponse } from './model/contact-detail';

@Injectable()
export class ContactDataSharingService {
    private values = new ContactResponse();

    setOption(value) {
        this.values = value;
    }

    getConfig() {
        return this.values;
    }
}
