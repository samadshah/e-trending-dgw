// tslint:disable
import { Location } from "@angular/common";
import {
  HttpErrorResponse,
  HttpHeaders,
  HttpResponse
} from "@angular/common/http";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationsService } from "angular2-notifications/dist";
import { last } from "rxjs/operators";
import { AppSettingService } from "../../../core/services/app-setting.service";
import {
  HttpStatusCode,
  MessageText,
  RedirectUrl
} from "../../../core/configuration/config";
import { ContactDataSharingService } from "../contact-data-sharing.service";
import { ContactService } from "../contact.service";
import {
  Address,
  ContactDetails,
  ContactInformation,
  ContactResponse
} from "../model/contact-detail";

@Component({
  selector: "app-contact-detail",
  templateUrl: "./contact-detail.component.html",
  styleUrls: ["./contact-detail.component.css"]
})
export class ContactDetailComponent implements OnInit, OnDestroy {
  public empty = "__";
  model: ContactResponse = new ContactResponse();
  tableContactId = "#data-table-contact";
  tableAddressId = "#data-table-address";
  gridInitialized = false;
  //!!
  primaryEmail: any;

  constructor(
    private detailService: ContactService,
    private route: ActivatedRoute,
    private dataSharingService: ContactDataSharingService,
    private notificationsService: NotificationsService,
    private location: Location,
    private router: Router,
    private appSettingSrvc: AppSettingService
  ) {}

  ngOnInit() {
    this.model.address = new Array<Address>();
    this.model.contactDetails = new ContactDetails();
    this.model.contactInformation = new Array<ContactInformation>();

    this.route.data.subscribe(
      (data: { contact: HttpResponse<ContactResponse> }) => {
        if (data && data.contact) {
          this.refreshUI(data.contact);
        }

        if (this.isCached(data.contact)) {
          this.refreshData();
        }
      },
      error => {
        if (
          error instanceof HttpErrorResponse &&
          error.status === HttpStatusCode.badRequest
        ) {
          const message = error.error.error || error.error.message;
          this.notificationsService.error(message);
        }
        this.router.navigate([RedirectUrl.contacts]);
      }
    );
  }

  //!!
  fetchPrimaryEmailResponse() {
    let primaryEmailIndex;
    this.model.contactInformation.forEach((cInfo, ind) => {
      if (cInfo.type === "Email" && cInfo.primary === "Yes") {
        primaryEmailIndex = ind;
        this.primaryEmail = cInfo.contactNumberAddress;
      }
    });
    //!! deletes primary email from contact info grid
    this.model.contactInformation.splice(primaryEmailIndex, 1);
  }

  isCached(res: HttpResponse<ContactResponse>): boolean {
    if (!res || res.headers instanceof HttpHeaders) {
      return false;
    }
    return true;
  }

  refreshUI(data: HttpResponse<ContactResponse>) {
    this.model = data.body;
    //!!
    this.fetchPrimaryEmailResponse();

    this.InitComponent();
  }

  refreshData() {
    this.route.queryParams.subscribe(result => {
      const id = result["id"];

      if (id) {
        this.detailService
          .getDetailData(id)
          .pipe(last())
          .subscribe(
            data => {
              if (data) {
                this.refreshUI(data);
              }
            },
            error => {
              this.notificationsService.error(MessageText.unableToFetchData);
            }
          );
      } else {
        this.notificationsService.alert(MessageText.invaliedId);
        this.router.navigate([RedirectUrl.contacts]);
      }
    });
  }

  InitComponent() {
    this.dataSharingService.setOption(this.model);

    //!! Seperate address with commas
    this.model.address.forEach(element => {
      let address: any = element.address.split("\n");
      let newAddress = "";
      address.forEach((ele, ind) => {
        newAddress += ele;
        if (ind !== address.length - 1) {
          newAddress += ", ";
        }
      });
      element.address = newAddress;
    });

    const dtId1: any = $(this.tableContactId);
    const dtId2: any = $(this.tableAddressId);
    if (this.gridInitialized) {
      this.tableDataRefresh(dtId1, this.model.contactInformation);
      this.tableDataRefresh(dtId2, this.model.address);
    } else {
      this.gridInitialized = true;
      this.bindGridContact(this.model.contactInformation);
      this.bindGridAddress(this.model.address);
    }
  }

  tableDataRefresh(table, model) {
    table.DataTable().clear();
    table.DataTable().rows.add(model);
    table.DataTable().draw();
  }

  public bindGridContact(data) {
    const dtContactId: any = $(this.tableContactId);
    const tableWidget = dtContactId.DataTable({
      responsive: true,
      paging: data.length > this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: false,
      sDom: '<"clear">t<"F"p>',
      pageLength: 5,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      order: [],
      autoWidth: false,
      columnDefs: [
        {
          className: "text-center",
          targets: 3,
          render: function(value, type, row) {
            return row.primary === "Yes"
              ? '<div class="styledCheckbox checkbox"><input type="checkbox" id="primarybox1" name="primary" value="primary" disabled checked><label for="primarybox1"><span></span></label></div>'
              : '<div class="styledCheckbox checkbox"><input type="checkbox" id="primarybox2" name="primary" value="primary" disabled><label for="primarybox2"><span></span></label></div>';
          }
        }
      ],
      columns: [
        {
          data: "description",
          title: "Description",
          class: "min-phone-l",
          width: "40%"
        },
        { data: "type", title: "Type", class: "min-phone-l", width: "25%" },
        {
          data: "contactNumberAddress",
          title: "Particulars",
          class: "min-tablet-l",
          width: "25%"
        },
        {
          data: "primary",
          title: "Primary",
          orderable: false,
          class: "min-tablet-l text-center",
          width: "10%"
        }
      ],
      data: data
    });
  }

  public bindGridAddress(data) {
    const dtAddressId: any = $(this.tableAddressId);
    const tableWidget = dtAddressId.DataTable({
      responsive: true,
      paging: data.length > this.appSettingSrvc.settings.pagingAppearence,
      searching: false,
      info: false,
      sDom: '<"clear">t<"F"p>',
      pageLength: this.appSettingSrvc.settings.pagingAppearence,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      order: [],
      autoWidth: false,
      columnDefs: [
        {
          className: "text-center",
          targets: 3,
          render: function(value, type, row) {
            //!
            return row.primary === "1"
              ? '<div class="styledCheckbox checkbox"><input type="checkbox"  disabled checked><label for="primarybox1"><span></span></label></div>'
              : '<div class="styledCheckbox checkbox"><input type="checkbox"  disabled><label for="primarybox2"><span></span></label></div>';
          }
        }
      ],
      columns: [
        {
          data: "nameorDescription",
          title: "Name or Description",
          class: "min-phone-l",
          width: "40%"
        },
        {
          data: "address",
          title: "Address",
          class: "min-phone-l",
          width: "25%"
        },
        {
          data: "purpose[, ].name",
          title: "Purpose",
          class: "min-tablet-l",
          width: "25%"
        },
        {
          data: "primary",
          title: "Primary",
          orderable: false,
          class: "min-tablet-l text-center",
          width: "10%"
        }
      ],
      data: data
    });
  }

  ngOnDestroy() {
    const dtContactId: any = $(this.tableContactId);
    dtContactId.DataTable().destroy();
    const dtAddressId: any = $(this.tableAddressId);
    dtAddressId.DataTable().destroy();
  }

  public getBackLink() {
    this.location.back();
  }
}
