import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactService } from './contact.service';
import { ContactResolve } from './contact.resolve';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactDataSharingService } from './contact-data-sharing.service';
import { ContactAddComponent } from './contact-add/contact-add.component';
import { ContactEditResolve } from './contact-edit.resolve.';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ContactRoutingModule,
  ],
  declarations: [ContactComponent, ContactDetailComponent, ContactListComponent, ContactEditComponent, ContactAddComponent],
  providers: [ContactService, ContactResolve, ContactDataSharingService, ContactEditResolve]
})
export class ContactModule { }
