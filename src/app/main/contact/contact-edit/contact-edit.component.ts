// tslint:disable
import { Location } from "@angular/common";
import { HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationsService } from "angular2-notifications/dist";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import swal from "sweetalert2";
import { AppSettingService } from "../../../core/services/app-setting.service";
import {
  HttpStatusCode,
  MessageText,
  RedirectUrl
} from "../../../core/configuration/config";
import { Lookup } from "../../../core/models/lookup";
import { UtiltiyService } from "../../../core/services/utility";
import { ContactDataSharingService } from "../contact-data-sharing.service";
import { ContactService } from "../contact.service";
import {
  Address,
  ContactDetails,
  ContactInformation,
  ContactResponse
} from "../model/contact-detail";
import {
  AddressRequest,
  ContactDetailsRequest,
  ContactEditRequest,
  ContactInformationRequest
} from "../model/contact-edit";

@Component({
  selector: "app-contact-edit",
  templateUrl: "./contact-edit.component.html",
  styleUrls: ["./contact-edit.component.css"]
})
export class ContactEditComponent implements OnInit, OnDestroy {
  private id: any;
  private sub: any;
  model: ContactResponse = new ContactResponse();
  tableContactId = "#data-table-contact";
  tableAddressId = "#data-table-address";
  request: ContactEditRequest = new ContactEditRequest();
  showAddressPopup = false;
  showContactPopup = false;
  left = "0px";
  top = "0px";
  display = "inline";
  addressList = new Array<AddressRequest>();
  contactList = new Array<ContactInformationRequest>();
  currentAddressPopUp = new AddressRequest();
  currentContactPopUp = new ContactInformationRequest();
  genderList = new Array<Lookup>();
  genderDropDownList = [];
  selectedGender = [];
  maritalStatusList = new Array<Lookup>();
  selectedMarital = [];
  maritalDropDownList = [];
  jobTitleList = new Array<AddressRequest>();
  jobTitleDropDownList = [];
  selectedjobTitle = undefined;
  countryList = new Array<Lookup>();
  countryDropDownList = [];
  selectedCountry = [];
  cityList = new Array<Lookup>();
  cityDropDownList = [];
  selectedCity = [];
  zipCodeList = new Array<Lookup>();
  zipcodeDropDownList = [];
  selectedzipcode = [];
  addressPurposeList = new Array<Lookup>();
  addressTypeList = new Array<Lookup>();
  typeDropDownList = [];
  selectedType = [];
  serverMessage: any;
  popup = false;
  dropdownSettings = {};
  singleSelectSettings = {};
  dropdownList = [];
  selectedItems = [];
  unSavedChanges = false;
  isFormSubmitted = false;
  gridInitialized = false;
  canGo: Subject<boolean> = new Subject<boolean>();
  contactSubmitDisable = false;
  @ViewChild("formModel") formModel;

  languageList = new Array<Lookup>();
  selectedLanguage = [];
  languagedropdownList = [];

  genderSetting: any;
  jobTitleSetting: any;
  maritalStatusSetting: any;
  languageSetting: any;
  countrySetting: any;
  citySetting: any;
  zipSetting: any;
  typeSetting: any;
  purposeSetting = this.mutliselectSetting("Select Purpose");
  //!!
  primaryEmail: any;

  constructor(
    private detailService: ContactService,
    private route: ActivatedRoute,
    private dataSharingService: ContactDataSharingService,
    private servcie: ContactService,
    private location: Location,
    private notificationsService: NotificationsService,
    private router: Router,
    private utiltiy: UtiltiyService,
    private appSettingSrvc: AppSettingService
  ) {}

  ngOnInit() {
    this.getAddressPurpose();
    this.getAddressType();
    this.getCountry();
    this.request.address = new Array<AddressRequest>();
    this.request.contactDetails = new ContactDetailsRequest();
    this.request.contactInformation = new Array<ContactInformationRequest>();

    this.model.address = new Array<Address>();
    this.model.contactDetails = new ContactDetails();
    this.model.contactInformation = new Array<ContactInformation>();

    this.dropdownSettings = {
      singleSelection: false,
      text: "Select",
      classes: "form-control custom-select custom-control",
      enableSearchFilter: true,
      searchPlaceholderText: "Search",
      badgeShowLimit: 2
    };

    this.singleSelectSettings = {
      singleSelection: true,
      text: "Select",
      classes: "form-control custom-select custom-control",
      enableSearchFilter: true,
      showCheckbox: false,
      searchPlaceholderText: "Search"
    };
    this.route.data.subscribe(
      (data: { contact: HttpResponse<ContactResponse> }) => {
        this.model = data.contact.body;
        //!!
        this.fetchPrimaryEmailResponse();

        this.bindGridContact(this.model.contactInformation);
        this.bindGridAddress(this.model.address);
        this.dataSharingService.setOption(this.model);
        this.getJobTitle(this.model.contactDetails);
        this.getMaritalStatus(this.model.contactDetails);
        this.getGender(this.model.contactDetails);
        this.getLanguages(this.model.contactDetails);
      },
      error => {
        if (
          error instanceof HttpErrorResponse &&
          error.status === HttpStatusCode.badRequest
        ) {
          const message = error.error.error || error.error.message;
          this.notificationsService.error(message);
        }
        this.router.navigate([RedirectUrl.contacts]);
      }
    );
  }

  //!!
  fetchPrimaryEmailResponse() {
    let primaryEmailIndex;
    this.model.contactInformation.forEach((cInfo, ind) => {
      if (cInfo.type === "Email" && cInfo.primary === "Yes") {
        primaryEmailIndex = ind;
        this.primaryEmail = cInfo.contactNumberAddress;
      }
    });
    //!! deletes primary email from contact info grid
    this.model.contactInformation.splice(primaryEmailIndex, 1);
  }

  public bindGridContact(data) {
    const dtContactId: any = $(this.tableContactId);

    dtContactId.DataTable({
      data: data,
      responsive: true,
      paging: false,
      searching: false,
      info: false,
      ordering: false,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      autoWidth: false,
      columnDefs: [
        {
          className: "text-center",
          targets: 3,
          render: function(value, type, row) {
            return row.primary === "Yes" || row.primary === 1
              ? '<div class="styledCheckbox checkbox viewMode"><input type="checkbox" id="primarybox3" name="primary" value="primary" disabled checked><label for="primarybox3"><span></span></label></div>'
              : '<div class="styledCheckbox checkbox viewMode"><input type="checkbox" id="primarybox4" name="primary" value="primary" disabled><label for="primarybox4"><span></span></label></div>';
          }
        },
        {
          className: "text-center",
          targets: 4,
          render: function(value, type, row, index) {
            //!!
            return row.primary !== "Yes" || row.type !== "Email"
              ? '<a id="editBtn" class="popUp" data-id=' +
                  row.transactionId +
                  ' data-prop="transactionId" data-index="' +
                  index.row +
                  '" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="icon-edit"></i></a>'
              : '<a class="disabled" disabled id="" class="popUp" data-id=' +
                  row.transactionId +
                  ' data-prop="transactionId" data-index="' +
                  index.row +
                  '" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="icon-edit"></i></a>';
          }
        },
        {
          className: "text-center",
          targets: 5,
          render: function(value, type, row, index) {
            row["index"] = index.row;
            //!!
            return row.primary !== "Yes" || row.type !== "Email"
              ? '<a id="deleteBtn" class="popUp" data-id=' +
                  row.transactionId +
                  ' data-prop="transactionId" data-index="' +
                  index.row +
                  '" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="icon-delete"></i></a>'
              : '<a class="disabled" id="" class="popUp" data-id=' +
                  row.transactionId +
                  ' data-prop="transactionId" data-index="' +
                  index.row +
                  '" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="icon-delete"></i></a>';
          }
        }
      ],
      columns: [
        {
          data: "description",
          title: "Description",
          class: "min-phone-l",
          width: "20%"
        },
        { data: "type", title: "Type", class: "min-phone-l", width: "20%" },
        {
          data: "contactNumberAddress",
          title: "Particulars",
          class: "min-tablet-p",
          width: "45%"
        },
        {
          data: "primary",
          title: "Primary",
          orderable: false,
          class: "min-tablet-l text-center",
          width: "5%"
        },
        {
          data: "edit",
          title: "Edit",
          class: "min-tablet-l text-center",
          width: "5%"
        },
        {
          data: "delete",
          title: "Delete",
          class: "min-tablet-l text-center",
          width: "5%"
        }
      ]
    });

    dtContactId.on("click", "#editBtn", e => {
      e.preventDefault();
      const result = e.currentTarget.attributes["data-id"].nodeValue;
      const prop = e.currentTarget.attributes["data-prop"].nodeValue;
      const index = e.currentTarget.attributes["data-index"].nodeValue;
      const row = this.findDataByModelId(index, this.tableContactId);
      const rowIndex = this.findRowIndexByModelId(index, this.tableContactId);
      row.primary =
        row.primary === "Yes" || row.primary === true || row.primary === 1
          ? true
          : false;
      this.editContact(e, row, index, rowIndex);
    });

    dtContactId.on("click", "#deleteBtn", e => {
      e.preventDefault();
      const index = e.currentTarget.attributes["data-index"].nodeValue;
      const row = this.findDataByModelId(index, this.tableContactId);
      const rowIndex = this.findRowIndexByModelId(index, this.tableContactId);
      this.deleteContact(row, e, rowIndex);
    });
  }

  public bindGridAddress(data) {
    //!! Seperate address with commas
    data.forEach(element => {
      let address: any = element.address.split("\n");
      let newAddress = "";
      address.forEach((ele, ind) => {
        newAddress += ele;
        if (ind !== address.length - 1) {
          newAddress += ", ";
        }
      });
      element.address = newAddress;
    });

    const dtAddressId: any = $(this.tableAddressId);
    const tableWidget = dtAddressId.DataTable({
      responsive: true,
      paging: false,
      searching: false,
      info: false,
      pageLength: 5,
      lengthChange: false,
      sortable: false,
      ordering: false,
      autoWidth: false,
      language: {
        emptyTable: MessageText.gridEmpty
      },
      columnDefs: [
        {
          className: "text-center",
          targets: 3,
          render: function(value, type, row) {
            //!
            return row.primary === "1" || row.primary === 1
              ? '<div class="styledCheckbox checkbox viewMode"><input id="primarybox1" type="checkbox" name="primary" value="primary" disabled checked><label for="primarybox1"><span></span></label></div>'
              : '<div class="styledCheckbox checkbox viewMode"><input id="primarybox2" type="checkbox" name="primary" value="primary" disabled><label for="primarybox2"><span></span></label></div>';
          }
        },
        {
          className: "text-center",
          targets: 4,
          render: function(value, type, row, index) {
            row["index"] = index.row;
            //!
            return row.canEdit === "1" || row.canEdit === "Yes"
              ? '<a id="editBtn" class="popUp" data-id=' +
                  row.transactionId +
                  ' data-prop="transactionId" data-index="' +
                  index.row +
                  '" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="icon-edit"></i></a>'
              : '<a class="disabled" disabled id="" class="popUp" data-id=' +
                  row.transactionId +
                  ' data-prop="transactionId" data-index="' +
                  index.row +
                  '" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="icon-edit"></i></a>';
          }
        },
        {
          className: "text-center",
          targets: 5,
          render: function(value, type, row, index) {
            //!
            return row.canDelete === "1" || row.canDelete === "Yes"
              ? '<a id="deleteBtn" class="popUp" data-id=' +
                  row.transactionId +
                  ' data-prop="transactionId" data-index="' +
                  index.row +
                  '" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="icon-delete"></i></a>'
              : '<a class="disabled" id="" class="popUp" data-id=' +
                  row.transactionId +
                  ' data-prop="transactionId" data-index="' +
                  index.row +
                  '" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="icon-delete"></i></a>';
          }
        }
      ],
      columns: [
        {
          data: "nameorDescription",
          title: "Name or Description",
          class: "min-phone-l",
          width: "20%"
        },
        {
          data: "address",
          title: "Address",
          class: "min-phone-l",
          width: "45%"
        },
        {
          data: "purpose[, ].name",
          title: "Purpose",
          class: "min-tablet-p",
          width: "20%"
        },
        {
          data: "primary",
          title: "Primary",
          orderable: false,
          class: "min-tablet-l text-center",
          width: "5%"
        },
        {
          data: "edit",
          title: "Edit",
          class: "min-tablet-l text-center",
          width: "5%"
        },
        {
          data: "delete",
          title: "Delete",
          class: "min-tablet-l text-center",
          width: "5%"
        }
      ],
      data: data
    });

    dtAddressId.on("click", "#editBtn", e => {
      e.preventDefault();
      const result = e.currentTarget.attributes["data-id"].nodeValue;
      const prop = e.currentTarget.attributes["data-prop"].nodeValue;
      const index = e.currentTarget.attributes["data-index"].nodeValue;
      const row = this.findDataByModelId(index, this.tableAddressId);
      const rowIndex = this.findRowIndexByModelId(index, this.tableAddressId);
      row.primary =
        row.primary === "Yes" || row.primary === true || row.primary === 1
          ? true
          : false;
      this.editAddress(e, row, index, rowIndex);
    });

    dtAddressId.on("click", "#deleteBtn", e => {
      e.preventDefault();
      const index = e.currentTarget.attributes["data-index"].nodeValue;
      const row = this.findDataByModelId(index, this.tableAddressId);
      const rowIndex = this.findRowIndexByModelId(index, this.tableAddressId);
      this.deleteAddress(row, e, rowIndex);
    });
  }

  public findDataByModelId(index, tableId) {
    const dtId: any = $(tableId);
    const data = dtId.DataTable().data();
    for (let i = 0; i < data.length; i++) {
      if (Number(data[i]["index"]) === Number(index)) {
        return data[i];
      }
    }
  }

  public findRowIndexByModelId(index, tableId) {
    const dtId: any = $(tableId);
    const data = dtId.DataTable().data();
    for (let i = 0; i < data.length; i++) {
      if (Number(data[i]["index"]) === Number(index)) {
        return i;
      }
    }
  }

  public editAddress(event, row, index, rowIndex) {
    this.countryDropDownList = [];
    this.selectedCountry = [];
    this.cityDropDownList = [];
    this.selectedCity = [];
    this.zipcodeDropDownList = [];
    this.selectedzipcode = [];
    this.openPopUp(event);
    this.showAddressPopup = true;
    if (row != null) {
      this.currentAddressPopUp = Object.assign({}, row);
      this.getCityZipCode(
        this.currentAddressPopUp.countryRegion,
        this.currentAddressPopUp
      );
    } else {
      this.currentAddressPopUp = new AddressRequest();
      (this.currentAddressPopUp.canDelete = "Yes"),
        (this.currentAddressPopUp.canEdit = "Yes");
    }
    this.currentAddressPopUp.index = index;
    this.fillMultiSelect(
      this.addressPurposeList,
      row ? row.purpose : undefined
    );
    this.fillSingleSelectCountry(
      this.countryList,
      row ? row.countryRegion : undefined
    );
  }

  public editContact(event, row, index, rowIndex) {
    this.selectedType = [];
    this.openPopUp(event);
    this.showContactPopup = true;
    if (row != null) {
      row.type = row.type === "None" ? undefined : row.type;
      this.currentContactPopUp = Object.assign({}, row);
    } else {
      this.currentContactPopUp = new ContactInformationRequest();
    }
    this.currentContactPopUp.index = index;
    this.fillSingleSelectType(this.addressTypeList, row ? row.type : undefined);
  }

  public openPopUp(event) {
    // Calculate pageX/Y if missing and clientX/Y available
    if (event.pageX == null && event.clientX != null) {
      const doc = document.documentElement,
        body = document.body;
      event.pageX =
        event.clientX +
        ((doc && doc.scrollLeft) || (body && body.scrollLeft) || 0) -
        ((doc && doc.clientLeft) || (body && body.clientLeft) || 0);
      event.pageY =
        event.clientY +
        ((doc && doc.scrollTop) || (body && body.scrollTop) || 0) -
        ((doc && doc.clientTop) || (body && body.clientTop) || 0);
    }
    this.left = event.pageX - 500 + "px";
    this.top = event.pageY - 60 + "px";

    const bodyWidth = document.body.offsetWidth;
    if (bodyWidth < 768) {
      this.left = "auto";
      this.top = event.pageY + 15 + "px";
    }
  }

  public closeAddressPopUp() {
    this.showAddressPopup = false;
  }

  public closeContactPopUp() {
    this.showContactPopup = false;
  }

  public overWritePrimary() {
    for (let index = 0; index < this.request.address.length; index++) {
      this.request.address[index].primary =
        this.request.address[index].primary === true ||
        this.request.address[index].primary.toString() === "Yes" ||
        this.request.address[index].primary === 1
          ? 1
          : 0;
    }
    for (
      let index = 0;
      index < this.request.contactInformation.length;
      index++
    ) {
      this.request.contactInformation[index].primary =
        this.request.contactInformation[index].primary === true ||
        this.request.contactInformation[index].primary.toString() === "Yes" ||
        this.request.contactInformation[index].primary === 1
          ? 1
          : 0;
    }
  }

  public onSubmit(model, valid) {
    if (valid === true) {
      this.request.contactDetails = model;
      this.request.contactPersonId = this.model.contactDetails.contactPersonId;
      this.request.contactDetails.transactionId = this.model.contactDetails.transactionId;
      this.request.contactDetails.language =
        this.model.contactDetails.language === undefined ||
        this.model.contactDetails.language === "undefined"
          ? undefined
          : this.model.contactDetails.language;
      this.request.contactDetails.gender =
        this.model.contactDetails.gender === undefined ||
        this.model.contactDetails.gender === "undefined"
          ? undefined
          : this.model.contactDetails.gender;
      this.request.contactDetails.maritalStatus =
        this.model.contactDetails.maritalStatus === undefined ||
        this.model.contactDetails.maritalStatus === "undefined"
          ? undefined
          : this.model.contactDetails.maritalStatus;
      this.request.contactDetails.jobTitle =
        this.model.contactDetails.jobTitle === undefined ||
        this.model.contactDetails.jobTitle === "undefined"
          ? undefined
          : this.model.contactDetails.jobTitle;
      if (this.contactSubmitDisable === true) {
        return false;
      }
      this.contactSubmitDisable = true;
      this.overWritePrimary();
      this.servcie.saveDetailData(this.request).subscribe(
        res => {
          if (res.isSuccess) {
            this.isFormSubmitted = true;
            this.notificationsService.success(MessageText.contactUpdated);
            this.getBackLink();
            this.contactSubmitDisable = false;
          } else {
            this.contactSubmitDisable = false;
            this.notificationsService.error(res.message);
          }
        },
        err => {
          if (err.status === HttpStatusCode.badRequest) {
            const errorText = this.utiltiy.errorHandle(err);
            this.notificationsService.error(errorText);
          }
          this.contactSubmitDisable = false;
        }
      );
    } else {
      const el = $(".ng-invalid:not(#globalSearch):not(form):first");
      if (el && el.length > 0) {
        $("html,body").animate(
          {
            scrollTop:
              el.offset().top -
              this.appSettingSrvc.settings.layoutValidationScrollThreashold
          },
          "slow",
          () => {
            el.focus();
          }
        );
      }
      return false;
    }
  }

  public changePurposeModel(purpose) {
    if (purpose != null) {
      for (let i = 0; i < purpose.length; i++) {
        purpose[i].name = purpose[i].itemName;
        purpose[i].value = purpose[i].id;
        delete purpose[i].itemName;
        delete purpose[i].id;
      }
    }
    return purpose;
  }

  public onAddressEdit(model, transactionId, index, valid) {
    if (valid === true) {
      const dtAddressId: any = $(this.tableAddressId);
      if (transactionId) {
        model.transactionId = transactionId;
      }
      if (index) {
        model.index = parseInt(index);
        index = parseInt(index);
      }
      model.primary = model.primary === true || model.primary === "Yes" ? 1 : 0;
      //!! make 'business' default purpose
      model.purpose = [{ id: "5637144583", itemName: "Business" }];

      model.purpose = this.changePurposeModel(model.purpose);

      model.countryRegion =
        model.countryRegion.length > 0 ? model.countryRegion[0].id : "";
      model.city = model.city.length > 0 ? model.city[0].id : "";
      model.zipPostalCode = model.zipPostalCode; // .length > 0 ? model.zipPostalCode[0].id : '';

      model.street = model.street === undefined ? "" : model.street;
      model.address =
        model.street + ", " + model.city + ", " + model.countryRegion;

      //!
      // if (model.primary === true || model.primary === 'Yes'  || model.primary === 1) {
      //   this.removePrimaryIfExist(dtAddressId);
      // }

      if (this.request.address.length > 0 && index != null) {
        // edit on local
        const arrIndex = this.request.address.findIndex(x => x.index == index);
        const rowIndex = this.findRowIndexByModelId(index, this.tableAddressId);
        if (arrIndex > -1) {
          this.request.address[arrIndex] = model;
          dtAddressId
            .DataTable()
            .row(rowIndex)
            .data(model)
            .draw();
        } else {
          this.request.address.push(model);
          dtAddressId
            .DataTable()
            .row(rowIndex)
            .data(model)
            .draw();
        }
      } else {
        // new add
        const rowIndex = this.findRowIndexByModelId(index, this.tableAddressId);
        this.request.address.push(model);
        if (index != null) {
          dtAddressId
            .DataTable()
            .row(rowIndex)
            .data(model)
            .draw();
        } else {
          dtAddressId
            .DataTable()
            .row.add(model)
            .draw();
        }
      }
      this.showAddressPopup = false;
      this.popup = true;
      this.unSavedChanges = true;
    } else {
      return false;
    }
  }

  public onContactEdit(model, transactionId, index, valid) {
    if (valid === true) {
      const dtContactId: any = $(this.tableContactId);
      if (transactionId) {
        model.transactionId = transactionId;
      }
      if (index) {
        model.index = index;
      }
      model.primary = model.primary === true || model.primary === "Yes" ? 1 : 0;
      model.description =
        model.description === undefined ? "" : model.description;
      model.type = model.type.length > 0 ? model.type[0].id : "";
      model.contactNumberAddress =
        model.contactNumberAddress === undefined
          ? ""
          : model.contactNumberAddress;

      //!
      // if(model.primary === true || model.primary === 'Yes'  || model.primary === 1) {
      //   this.removePrimaryContactIfExist(dtContactId, model.type);
      // }

      if (this.request.contactInformation.length > 0 && index != null) {
        // edit on local
        const arrIndex = this.request.contactInformation.findIndex(
          x => x.index == index
        );
        const rowIndex = this.findRowIndexByModelId(index, this.tableContactId);
        if (arrIndex > -1) {
          this.request.contactInformation[arrIndex] = model;
          dtContactId
            .DataTable()
            .row(rowIndex)
            .data(model)
            .draw();
        } else {
          this.request.contactInformation.push(model);
          dtContactId
            .DataTable()
            .row(rowIndex)
            .data(model)
            .draw();
        }
      } else {
        // new add
        const rowIndex = this.findRowIndexByModelId(index, this.tableContactId);
        this.request.contactInformation.push(model);
        if (index != null) {
          dtContactId
            .DataTable()
            .row(rowIndex)
            .data(model)
            .draw();
        } else {
          dtContactId
            .DataTable()
            .row.add(model)
            .draw();
        }
      }
      this.showContactPopup = false;
      this.popup = true;
      this.unSavedChanges = true;
    } else {
      return false;
    }
  }

  public deleteContact(row, e, rowIndex) {
    const dtContactId: any = $(this.tableContactId);
    dtContactId
      .DataTable()
      .row(rowIndex)
      .remove()
      .draw();
    if (this.request.contactInformation.length > 0) {
      // edit on local
      const arrIndex = this.request.contactInformation.findIndex(
        x => x.index === row.index
      );
      //!!
      if (arrIndex !== -1) {
        if (row.transactionId != null) {
          this.request.contactInformation[arrIndex].isDeleted = true;
        } else {
          this.request.contactInformation.splice(arrIndex, 1);
        }
      } else {
        row["isDeleted"] = true;
        this.request.contactInformation.push(row);
        this.popup = true;
      }
    } else if (row.transactionId != null) {
      row.primary = row.primary === "Yes" ? 1 : 0;
      //!
      row["isDeleted"] = true;
      this.request.contactInformation.push(row);
      this.popup = true;
    }
  }

  public deleteAddress(row, e, rowIndex) {
    const dtAddressId: any = $(this.tableAddressId);
    dtAddressId
      .DataTable()
      .row(rowIndex)
      .remove()
      .draw();
    if (this.request.address.length > 0) {
      // edit on local
      const arrIndex = this.request.address.findIndex(
        x => x.index === row.index
      );
      //!!
      if (arrIndex !== -1) {
        if (row.transactionId != null) {
          this.request.address[arrIndex].isDeleted = true;
        } else {
          this.request.address.splice(arrIndex, 1);
        }
      } else {
        row["isDeleted"] = true;
        this.request.address.push(row);
        this.popup = true;
      }
    } else if (row.transactionId != null) {
      row.primary = row.primary === "Yes" ? 1 : 0;
      //!
      // row.isDeleted = true;
      row["isDeleted"] = true;
      this.request.address.push(row);
      this.popup = true;
    }
  }

  public getAddressPurpose() {
    this.servcie.getAddressPurpose().subscribe(
      res => {
        this.addressPurposeList = res;
      },
      err => {
        if (err.status === HttpStatusCode.badRequest) {
        }
      }
    );
  }

  public getGender(contactdetail) {
    this.servcie.getGender().subscribe(
      res => {
        this.genderList = res;
        this.fillSingleSelectGender(
          this.genderList,
          contactdetail ? contactdetail.gender : ""
        );
      },
      err => {
        if (err.status === HttpStatusCode.badRequest) {
        }
      }
    );
  }

  public getJobTitle(contactdetail) {
    this.servcie.getJobTitle().subscribe(
      res => {
        this.jobTitleList = res;
        this.fillSingleSelectJobTitle(
          this.jobTitleList,
          contactdetail ? contactdetail.jobTitle : ""
        );
      },
      err => {
        if (err.status === HttpStatusCode.badRequest) {
        }
      }
    );
  }

  onJobSelect(item, modelContactDetails) {
    modelContactDetails.jobTitle = item.id;
  }

  onJobDeSelect(item, modelContactDetails) {
    modelContactDetails.jobTitle = "";
  }

  onLangSelect(item, modelContactDetails) {
    modelContactDetails.language = item.id;
  }

  onLangDeSelect(item, modelContactDetails) {
    modelContactDetails.language = "";
  }

  public getLanguages(data) {
    this.servcie.getLanguage().subscribe(
      res => {
        this.languageList = res;
        this.fillSingleSelectlanguages(
          this.languageList,
          data ? data.languageId : ""
        );
      },
      err => {
        if (err.status === HttpStatusCode.badRequest) {
        }
      }
    );
  }

  public getMaritalStatus(contactdetail) {
    this.servcie.getMaritalStatus().subscribe(
      res => {
        this.maritalStatusList = res;
        this.fillSingleSelectMarital(
          this.maritalStatusList,
          contactdetail ? contactdetail.maritalStatus : ""
        );
      },
      err => {
        if (err.status === HttpStatusCode.badRequest) {
        }
      }
    );
  }

  public getCountry() {
    this.servcie.getCountry().subscribe(
      res => {
        //!
        this.countryList = res.countries;
      },
      err => {
        if (err.status === HttpStatusCode.badRequest) {
        }
      }
    );
  }

  public getCity(id, row) {
    this.servcie.getCity(id).subscribe(
      res => {
        this.cityList = res;
        this.fillSingleSelectCity(this.cityList, row ? row.city : undefined);
      },
      err => {
        if (err.status === HttpStatusCode.badRequest) {
        }
      }
    );
  }

  public getAddressType() {
    this.servcie.getAddressType().subscribe(
      res => {
        this.addressTypeList = res.filter(x => x.value !== "None");
      },
      err => {
        if (err.status === HttpStatusCode.badRequest) {
        }
      }
    );
  }

  public getCityZipCode(id, item) {
    this.getCity(id, item);
  }

  public fillMultiSelect(data, selected) {
    this.dropdownList = [];
    this.selectedItems = [];
    for (let i = 0; i < data.length; i++) {
      this.dropdownList.push({ id: data[i].value, itemName: data[i].name });
    }
    if (selected != null) {
      for (let i = 0; i < selected.length; i++) {
        this.selectedItems.push({
          id: selected[i].value,
          itemName: selected[i].name
        });
      }
    }
  }

  public fillSingleSelectCountry(data, selected) {
    this.countryDropDownList = [];
    this.selectedCountry = [];
    for (let i = 0; i < data.length; i++) {
      this.countryDropDownList.push({
        id: data[i].value,
        itemName: data[i].name
      });
    }
    if (selected !== undefined && selected !== "") {
      const countryName = this.countryDropDownList.filter(
        x => x.id === selected
      );
      this.selectedCountry.push({
        id: selected,
        itemName: countryName[0].itemName
      });
    }
    this.countrySetting = this.singleSelectSetting(
      "Select Country/Region",
      this.countryDropDownList.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
    // !!
    this.cityDropDownList = [];
    this.citySetting = this.singleSelectSetting(
      "Select City",
      this.cityDropDownList.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public fillSingleSelectCity(data, selected) {
    this.cityDropDownList = [];
    this.selectedCity = [];
    for (let i = 0; i < data.length; i++) {
      this.cityDropDownList.push({ id: data[i].value, itemName: data[i].name });
    }
    if (selected !== undefined && selected !== "") {
      const cityName = this.cityDropDownList.filter(x => x.id === selected);
      this.selectedCity.push({ id: selected, itemName: cityName[0].itemName });
    }
    this.citySetting = this.singleSelectSetting(
      "Select City",
      this.cityDropDownList.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public fillSingleSelectZipcode(data, selected) {
    this.zipcodeDropDownList = [];
    this.selectedzipcode = [];
    for (let i = 0; i < data.length; i++) {
      this.zipcodeDropDownList.push({
        id: data[i].value,
        itemName: data[i].name
      });
    }
    if (selected !== undefined && selected !== "") {
      const zipcodeName = this.zipcodeDropDownList.filter(
        x => x.id === selected
      );
      if (zipcodeName.length > 0) {
        this.selectedzipcode.push({
          id: selected,
          itemName: zipcodeName[0].itemName
        });
      }
    }
    this.zipSetting = this.singleSelectSetting(
      "Select Zip/Postal Code",
      this.zipcodeDropDownList.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public fillSingleSelectJobTitle(data, selected) {
    this.jobTitleDropDownList = [];
    for (let i = 0; i < data.length; i++) {
      this.jobTitleDropDownList.push({
        id: data[i].value,
        itemName: data[i].name
      });
    }
    if (selected !== undefined && selected !== "") {
      this.selectedjobTitle = [];
      const jobName = this.jobTitleDropDownList.filter(x => x.id === selected);
      if (jobName.length > 0) {
        this.selectedjobTitle.push({
          id: selected,
          itemName: jobName[0].itemName
        });
      }
    }
    this.jobTitleSetting = this.singleSelectSetting(
      "Select Job Title",
      this.jobTitleDropDownList.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public fillSingleSelectGender(data, selected) {
    this.genderDropDownList = [];
    for (let i = 0; i < data.length; i++) {
      if (data[i].value && data[i].name) {
        this.genderDropDownList.push({
          id: data[i].value,
          itemName: data[i].name
        });
      }
    }
    if (selected !== undefined && selected !== "") {
      this.selectedGender = [];
      const name = this.genderDropDownList.filter(x => x.id === selected);
      if (name.length > 0) {
        this.selectedGender.push({ id: selected, itemName: name[0].itemName });
      }
    }
    this.genderSetting = this.singleSelectSetting(
      "Select Gender",
      this.genderDropDownList.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public fillSingleSelectlanguages(data, selected) {
    this.languagedropdownList = [];
    for (let i = 0; i < data.length; i++) {
      this.languagedropdownList.push({
        id: data[i].value,
        itemName: data[i].name
      });
    }
    if (selected !== undefined && selected !== "") {
      this.selectedLanguage = [];
      const name = this.languagedropdownList.filter(
        x => x.id === selected.toLowerCase()
      );
      if (name.length > 0) {
        this.selectedLanguage.push({
          id: selected,
          itemName: name[0].itemName
        });
        //!!
        this.model.contactDetails.language = selected;
      } else {
        this.model.contactDetails.language = "";
      }
    }
    this.languageSetting = this.singleSelectSetting(
      "Select Language",
      this.languagedropdownList.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public fillSingleSelectMarital(data, selected) {
    this.maritalDropDownList = [];
    for (let i = 0; i < data.length; i++) {
      this.maritalDropDownList.push({
        id: data[i].value,
        itemName: data[i].name
      });
    }
    if (selected !== undefined && selected !== "") {
      this.selectedMarital = [];
      const name = this.maritalDropDownList.filter(x => x.id === selected);
      this.selectedMarital.push({ id: selected, itemName: name[0].itemName });
    }
    this.maritalStatusSetting = this.singleSelectSetting(
      "Select Marital Status",
      this.maritalDropDownList.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public fillSingleSelectType(data, selected) {
    this.typeDropDownList = [];
    for (let i = 0; i < data.length; i++) {
      this.typeDropDownList.push({
        id: data[i].value,
        itemName: data[i].value
      });
    }
    if (selected !== undefined && selected !== "") {
      this.selectedType = [];
      const name = this.typeDropDownList.filter(x => x.id === selected);
      this.selectedType.push({ id: selected, itemName: name[0].itemName });
    }
    this.typeSetting = this.singleSelectSetting(
      "Select Type",
      this.typeDropDownList.length >
        this.appSettingSrvc.settings.searchEnableOnItem
    );
  }

  public onContrySelect(item: any) {
    this.getCityZipCode(item.id, "");
  }

  onGenderSelect(item, modelContactDetails) {
    modelContactDetails.gender = item.id;
  }

  onGenderDeSelect(item, modelContactDetails) {
    modelContactDetails.gender = "";
  }

  onMaritalSelect(item, modelContactDetails) {
    modelContactDetails.maritalStatus = item.id;
  }

  onMaritalDeSelect(item, modelContactDetails) {
    modelContactDetails.maritalStatus = "";
  }

  onTypeSelect(item, currentContactPopUp) {
    currentContactPopUp.contactNumberAddress = "";
    currentContactPopUp.type = item.id;
  }

  onTypeDeSelect(item, currentContactPopUp) {
    currentContactPopUp.type = "";
  }

  public Cancel(formModel) {
    this.getBackLink();
  }

  public getBackLink() {
    this.location.back();
  }

  canDeactivate(): Observable<boolean> | boolean {
    this.canGo = new Subject<boolean>();
    // if form is submitted
    if (this.isFormSubmitted === true) {
      this.dataSharingService.setOption(new ContactResponse());
      return true;
    }

    // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    if (this.unSavedChanges === false && this.formModel.dirty === false) {
      this.dataSharingService.setOption(new ContactResponse());
      return true;
    }
    // Otherwise ask the user with the dialog service and return its
    // observable which resolves to true or false when the user decides
    swal({
      title: MessageText.FormLeavingTitle,
      text: MessageText.FormLeavingText,
      // type: 'warning',
      showCancelButton: true,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      confirmButtonClass: "btn btn-default",
      cancelButtonClass: "btn btn-info",
      buttonsStyling: false,
      reverseButtons: true,
      width: 360
    }).then(discardChanges => {
      if (discardChanges.value) {
        this.canGo.next(true);
      } else if (discardChanges.dismiss.toString() === "cancel") {
        if (window.history.state) {
          const url = window.history.state.url;
          window.history.pushState({ url: url }, "", url);
        } else {
          const url = "vp/home";
          window.history.pushState({ url: url }, "", url);
        }
        this.canGo.next(false);
      }
    });
    return this.canGo;
  }

  ngOnDestroy() {
    const dtContactId: any = $(this.tableContactId);
    dtContactId.DataTable().destroy();
    const dtAddressId: any = $(this.tableAddressId);
    dtAddressId.DataTable().destroy();
  }
  public mutliselectSetting(placeholder) {
    return {
      singleSelection: false,
      text: placeholder,
      classes: "form-control custom-select custom-control",
      enableSearchFilter: true,
      searchAutofocus: true,
      badgeShowLimit: 2,
      searchPlaceholderText: "Search"
    };
  }

  public singleSelectSetting(placeholder, search = true) {
    return {
      singleSelection: true,
      text: placeholder,
      showCheckbox: false,
      classes: "form-control custom-select custom-control",
      enableSearchFilter: search,
      searchAutofocus: search,
      searchPlaceholderText: "Search"
    };
  }

  public removePrimaryIfExist(tableWidget1) {
    const data = tableWidget1.DataTable().data();
    if (data.length === undefined || data.length < 1) {
    } else {
      const itemsToUpdate = data.filter(
        x => x.primary === "Yes" || x.primary === true || x.primary === 1
      );
      for (let i = 0; i < itemsToUpdate.length; i++) {
        const index = data.indexOf(itemsToUpdate[i]);
        itemsToUpdate[i].primary = 0;
        itemsToUpdate[i].index = itemsToUpdate[i].index;
        const updateditem = itemsToUpdate[i];
        data
          .row(index)
          .data(updateditem)
          .draw();
      }
    }
  }

  public removePrimaryContactIfExist(tableWidget1, type) {
    const data = tableWidget1.DataTable().data();
    if (data.length === undefined || data.length < 1) {
    } else {
      const itemsToUpdate = data.filter(
        x =>
          x.type === type &&
          (x.primary === "Yes" || x.primary === true || x.primary === 1)
      );
      for (let i = 0; i < itemsToUpdate.length; i++) {
        const index = data.indexOf(itemsToUpdate[i]);
        itemsToUpdate[i].primary = 0;
        itemsToUpdate[i].index = itemsToUpdate[i].index;
        const updateditem = itemsToUpdate[i];
        data
          .row(index)
          .data(updateditem)
          .draw();
      }
    }
  }

  //!!
  onTextToNumberChanged(e) {
    if (
      [46, 8, 9, 27, 13, 110, 16].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode == 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+V
      (e.keyCode == 86 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode == 88 && e.ctrlKey === true) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)
    ) {
      return;
    }

    // Ensure that it is a number and stop the keypress
    if (
      (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) &&
      (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }
  }
}
