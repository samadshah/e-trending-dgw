import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactResolve } from './contact.resolve';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactAddComponent } from './contact-add/contact-add.component';
import { CanDeactivateGuard } from '../../core/guards/can-deactivate-guard.service';
import { ContactEditResolve } from './contact-edit.resolve.';

const routes: Routes = [{
  path: '',
  component: ContactComponent,
  children: [
    {
      path: '',
      component: ContactListComponent,
      canDeactivate: [CanDeactivateGuard],
    },
    {
      path: 'view',
      component: ContactDetailComponent,
      data: {
        breadcrumb: 'View Contact'
      },
      resolve: {
        contact: ContactResolve
      }
    },
    {
      path: 'edit',
      component: ContactEditComponent,
      canDeactivate: [CanDeactivateGuard],
      data: {
        breadcrumb: 'Edit Contact',
      },
      resolve: {
        contact: ContactEditResolve
      }
    },
    {
      path: 'add',
      component: ContactAddComponent,
      canDeactivate: [CanDeactivateGuard],
      data: {
        breadcrumb: 'Add Contact',
      }
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactRoutingModule { }
