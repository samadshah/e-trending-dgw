import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ContactResponse } from './model/contact-detail';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class ContactService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;
  private servicePathForLookups: string;

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = '/api/vendors';
    this.servicePath2 = '/api';
    this.servicePathForLookups = '/api/lookups';
  }

  getDetailData(id): Observable<HttpResponse<ContactResponse>> {
    return this.http.get<ContactResponse>(environment.base_api_url
      + this.servicePath2 + '/Contacts/Detail?ContactID=' + id, { observe: 'response' });
  }

  getAddressPurpose(): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/ContactLookups/AddressPurpose');
  }

  getGender(): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/ContactLookups/Gender');
  }

  getMaritalStatus(): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/ContactLookups/MaritalStatus');
  }

  getCountry(): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/vendorCountry');
  }

  getCity(Id): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/vendorlookup/city/' + Id);
  }

  getAddressType(): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/ContactLookups/LogisticElectronicAddressType');
  }

  getLanguage(): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/ContactLookups/Language');
  }

  getJobTitle(): Observable<any> {
    return this.http.get(environment.base_api_url + this.servicePath2 + '/ContactLookups/PersonTitle');
  }

  saveDetailData(obj): Observable<any> {
    return this.http.post(environment.base_api_url + this.servicePath2 + '/Contacts/Save', obj);
  }

  //!
  // deleteContactData(id): Observable<any> {
  //   return this.http.delete(environment.base_api_url + this.servicePath + '/Contacts/delete/' + id);
  // }

}
