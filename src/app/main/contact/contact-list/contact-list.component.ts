// tslint:disable
import { Component, OnInit, ViewChild } from "@angular/core";
import { GridType, RedirectUrl } from "../../../core/configuration/config";
import { PlatformLocation } from "@angular/common";
import { ContactDataSharingService } from "../contact-data-sharing.service";
import { ContactResponse } from "../model/contact-detail";

@Component({
  selector: "app-contact-list",
  templateUrl: "./contact-list.component.html",
  styleUrls: ["./contact-list.component.css"]
})
export class ContactListComponent implements OnInit {
  columnsDefination: Array<any>;
  columns: Array<any>;
  //!
  serviceUrl = "/Contacts/List";

  gridType = GridType.ContactList;
  title = "Contacts";
  addUrl = "/vp/contacts/add";
  appbase;
  @ViewChild("gridChild")
  gridChild: any;
  constructor(
    private platformLocation: PlatformLocation,
    private dataSharingService: ContactDataSharingService
  ) // private servcie: ContactService,
  // private notificationsService: NotificationsService
  {}

  ngOnInit() {
    this.appbase = (this.platformLocation as any).location.pathname;
    this.columnsDefination = [
      {
        className: "text-center",
        targets: 4,
        render: (data, type, row) => {
          return (
            `<a href="` +
            this.appbase +
            "/edit?id=" +
            encodeURIComponent(row.contactPersonId) +
            `"data-Id="` +
            encodeURIComponent(row.contactPersonId) +
            `"data-url="` +
            RedirectUrl.contacts +
            "/edit/" +
            `" class="cursor-pointer actionView"  data-prop="contactPersonId" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="icon-edit"></i></a>`
          );
        }
      },
      {
        className: "text-center",
        targets: 5,
        render: (data, type, row) => {
          return (
            `<a href="` +
            this.appbase +
            "/view?id=" +
            encodeURIComponent(row.contactPersonId) +
            `"data-Id="` +
            encodeURIComponent(row.contactPersonId) +
            `"data-url="` +
            RedirectUrl.contacts +
            "/view/" +
            `" class="cursor-pointer actionView"  data-prop="contactPersonId" data-toggle="tooltip" data-placement="bottom" title="View"><i class="icon-view"></i></a>`
          );
        }
      }
    ];
    //!
    this.columns = [
      {
        data: "contactPersonId",
        title: "Contact Id",
        class: "min-phone-l",
        width: "13%"
      },
      {
        data: "firstName",
        title: "First Name",
        class: "min-phone-l",
        width: "15%"
      },
      {
        data: "lastName",
        title: "Last Name",
        class: "min-tablet-p",
        width: "15%"
      },
      {
        data: "jobTitle",
        title: "Job Title ",
        class: "min-tablet-l",
        width: "22%"
      },
      {
        data: "edit",
        title: "Edit",
        orderable: false,
        class: "text-center min-tablet-l",
        width: "10%"
      },
      //!
      // { data: 'delete', title: 'Delete', orderable: false, class:'text-center min-tablet-l', width: '10%' },
      {
        data: "view",
        title: "View",
        orderable: false,
        class: "text-center min-tablet-l",
        width: "10%"
      }
    ];
    this.dataSharingService.setOption(new ContactResponse());
  }

  public loadTooltip() {
    $('[data-toggle="tooltip"]').tooltip({
      trigger: "hover"
    });
  }
}
