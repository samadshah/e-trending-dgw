import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartModule } from 'angular-highcharts';
import { SharedModule } from '../../shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { RfqGraphComponent } from './components/rfq-graph/rfq-graph.component';
import { DocumentGraphComponent } from './components/document-graph/document-graph.component';
import { ActivitySeedComponent } from './components/activity-seed/activity-seed.component';
import { SupplierHelpGuidesComponent } from './components/supplier-help-guides/supplier-help-guides.component';
import { NeedAssistanceComponent } from './components/need-assistance/need-assistance.component';
import { DashboardComponent } from './dashboard.component';
import { ActivityFeedService } from './components/activity-seed/activity-seed.service';
import { DashBoardDataService } from './dashboard-data.service';
import { DashboardResolve } from './dashboard.resolve';
import { RfqGraphService } from './components/rfq-graph/rfq-graph.service';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule,
    ChartModule,
  ],
  declarations: [
    DashboardComponent,
    RfqGraphComponent,
    DocumentGraphComponent,
    ActivitySeedComponent,
    SupplierHelpGuidesComponent,
    NeedAssistanceComponent
  ],
  providers: [ActivityFeedService, DashBoardDataService, DashboardResolve, RfqGraphService]
})
export class DashboardModule { }
