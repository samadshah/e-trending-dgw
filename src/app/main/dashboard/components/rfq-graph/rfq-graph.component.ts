// tslint:disable
import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges
} from "@angular/core";
import { Chart } from "angular-highcharts";
import {
  ChartData,
  RequestBids,
  ChartDataColumn
} from "../../models/dashboard-data.model";
import { Router } from "@angular/router";
import { RedirectUrl } from "../../../../core/configuration/config";
import { PageQuery } from "../../../../core/models/page";
import { RfqGraphService } from "./rfq-graph.service";
import { Utils } from "../../../../core/Helpers/utils";

@Component({
  selector: "app-rfq-graph",
  templateUrl: "./rfq-graph.component.html",
  styleUrls: ["./rfq-graph.component.css"]
})
export class RfqGraphComponent implements OnInit, OnChanges {
  @Input()
  rfq: Array<ChartData> = new Array();
  totalQuotatiosReq = 0;
  config: any[];
  chart: any;
  pageQueryModel: PageQuery = new PageQuery();
  list = false;
  page = false;
  requestforquotationId = "";
  requestBids: RequestBids = new RequestBids();
  animate = true;

  @Input()
  set chartConfig(conf) {
    this.config = conf;
  }

  get chartConfig() {
    return this.config;
  }

  constructor(
    private router: Router,
    private rfqGraphService: RfqGraphService
  ) {}

  ngOnInit() {
    this.rfq = this.rfq || this.getEmptyObject();
    this.initComponent(this.rfq);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.rfq.firstChange) {
      if (!Utils.isEqual(changes.rfq.currentValue, changes.rfq.previousValue)) {
        this.initChart(changes.rfq.currentValue);
      }
    }
  }

  private initComponent(rfq) {
    this.initChart(rfq);
  }

  private initChart(rfq) {
    if (this.chart) {
      this.animate = false;
    }
    const total = rfq ? rfq.reduce((sum, item) => sum + item.count, 0) || 0 : 0;
    this.chart = new Chart({
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        style: {
          fontFamily: "RobotoRegular"
        },
        events: {
          load: function() {
            this.series[0].data.map(el => {
              if (
                el.percentage.toFixed(2) <= 3.99 &&
                el.percentage.toFixed(2) > 0.0
              ) {
                el.update({
                  dataLabels: {
                    distance: 20
                  }
                });
                el.dataLabel.text.attr({
                  style: "color:black;font-size:11px;"
                });
              }
            });
          }
        }
      },
      title: {
        text: total,
        align: "center",
        verticalAlign: "middle",
        y: -25,
        style: {
          fontSize: "40px"
        }
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: true,
            distance: -18,
            formatter: function() {
              return this.point.percentage.toFixed(2) == 0.0
                ? ""
                : this.point.percentage.toFixed(0) > 0
                ? this.point.percentage.toFixed(0) + "%"
                : this.point.percentage.toFixed(1) + "%";
            },
            style: {
              fontWeight: "normal",
              color: "white",
              textOutline: false,
              fontSize: "11px"
            }
          },
          point: {
            events: {
              legendItemClick: e => {
                e.preventDefault(true);
                const result = e.target.name.substr(
                  e.target.name.indexOf(" ") + 1
                );
                setTimeout(() => {
                  this.goToRfq(result);
                }, 0);
                return false;
              }
            }
          },
          showInLegend: true,
          size: 180
        }
      },
      legend: {
        width: 345,
        height: 150,
        itemWidth: 172,
        itemMarginBottom: 5,
        align: "center",
        itemStyle: {
          textTransform: "capitalize"
        },
        labelFormatter: function() {
          const words = this.name.split(/[\s]+/);
          const numWordsPerLine = 3;
          const str = [];
          const totalLength = words.length - 1;
          for (let word = 0; word < words.length; word++) {
            if (
              word > 0 &&
              word < totalLength &&
              word % numWordsPerLine === 0
            ) {
              str.push("<br>");
            }
            str.push(words[word]);
          }
          const result = str.join(" ");
          return result;
        }
      },
      series: [
        {
          animation: this.animate,
          colors: [
            "#04a15c",
            "#c038f3",
            "#5e9bfc",
            "#f39938",
            "#7c4ce9",
            "#e94c6b",
            "#afaf0a"
          ],
          type: "pie",
          name: "Requests for quotations",
          innerSize: "60%",
          cursor: "pointer",
          point: {
            events: {
              click: e => {
                e.preventDefault(true);
                const result = e.point.name.substr(
                  e.point.name.indexOf(" ") + 1
                );
                this.goToRfq(result);
                return true;
              }
            }
          },
          data: this.chartData
        }
      ],
      responsive: {
        rules: [
          {
            condition: {
              maxWidth: 330
            },
            chartOptions: {
              legend: {
                width: 255,
                itemWidth: 125,
                align: "center",
                verticalAlign: "bottom",
                layout: "horizontal"
              }
            }
          },
          {
            condition: {
              maxWidth: 240
            },
            chartOptions: {
              legend: {
                width: 240,
                itemWidth: 120,
                align: "center",
                layout: "horizontal"
              }
            }
          }
        ]
      }
    });
  }

  private goToRfq(status) {
    const filter = "RFQStatus=" + status;
    this.router.navigate([RedirectUrl.rfqs], {
      queryParams: { filter: filter }
    });
  }

  get chartData() {
    const mappedData: Array<any> = [];
    for (let i = 0; i < this.rfq.length; i++) {
      const currData = this.rfq[i];
      const currCol = this.rfq[i].filterColumns[0].value;
      const arr: Array<any> = [];
      const val = currData.count + " " + currCol;
      arr.push(val);
      arr.push(currData.count);
      mappedData.push(arr);
    }
    return mappedData;
  }

  public redirect() {
    this.router.navigate([RedirectUrl.rfqs]);
  }

  //!
  public bidSubmit() {
    const filter = "RFQStatus=New";
    this.router.navigate([RedirectUrl.rfqs], {
      queryParams: { filter: filter }
    });
  }

  getEmptyObject(): Array<ChartData> {
    const emptyRfqList = new Array<ChartData>();
    const emptyRfq = new ChartData();
    const emptyFilterCol: Array<ChartDataColumn> = [];
    const ob = new ChartDataColumn();
    ob.value = "";
    emptyFilterCol.push(ob);
    emptyRfq.filterColumns = emptyFilterCol;
    emptyRfq.count = 0;
    emptyRfqList.push(emptyRfq);
    return emptyRfqList;
  }
}
