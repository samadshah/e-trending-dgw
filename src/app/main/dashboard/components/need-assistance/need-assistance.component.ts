import { Component, OnInit } from "@angular/core";
import { DashBoardDataService } from "../../dashboard-data.service";
import { Observable } from "rxjs/Observable";
import { CompanyMeta } from "../../models/company-meta.model";

@Component({
  selector: "app-need-assistance",
  templateUrl: "./need-assistance.component.html",
  styleUrls: ["./need-assistance.component.css"]
})
export class NeedAssistanceComponent implements OnInit {
  companyData: CompanyMeta = new CompanyMeta();
  constructor(private dashBoardDataService: DashBoardDataService) {}

  ngOnInit() {
    this.initComponent();
  }

  private initComponent() {
    this.dashBoardDataService.getCompanyMeta().subscribe(data => {
      this.companyData = data;
    });
  }
}
