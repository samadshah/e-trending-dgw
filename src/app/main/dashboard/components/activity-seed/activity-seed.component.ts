import { HttpErrorResponse } from "@angular/common/http";
import { Component, EventEmitter, NgZone, OnInit, Output } from "@angular/core";
import { NotificationsService } from "angular2-notifications/dist";
import * as Moment from "moment";
import { last } from "rxjs/operators";
import { AppSettingService } from "../../../../core/services/app-setting.service";
import {
  HttpStatusCode,
  CssClass,
  MessageText
} from "../../../../core/configuration/config";
import {
  ActivityFeed,
  ActivityTypeIcon
} from "../../models/activity-feed.model";
import { ActivityFeedService } from "./activity-seed.service";
import { PageQuery } from "../../../../core/models/page";

@Component({
  selector: "app-activity-seed",
  templateUrl: "./activity-seed.component.html",
  styleUrls: ["./activity-seed.component.css"]
})
export class ActivitySeedComponent implements OnInit {
  @Output() applyScrollBar = new EventEmitter<any>();
  feeds: ActivityFeed[] = [];
  activityTypes: ActivityTypeIcon[] = [];
  // pageNo = 1;
  pageModel = new PageQuery();
  firstLoad = false;
  waitToComplete = false;
  recordCount = 0;
  acitivityFeedPaging = 0;
  loading = "";
  moreRecords = true;
  isRefresh = false;
  messageText = "Loading...";
  subscription: any;

  constructor(
    private activityFeedService: ActivityFeedService,
    private ngZone: NgZone,
    private notificationsService: NotificationsService,
    private appSettingSrvc: AppSettingService
  ) {
    this.pageModel.pageNo = 1;
    this.pageModel.pageSize = this.appSettingSrvc.settings.activityFeeds;
    this.acitivityFeedPaging = 1; //this.appSettingSrvc.settings.activityFeeds;
  }

  ngOnInit() {
    this.loadAcivityTypes();
    this.getActivityFeed();
    this.apply();
  }

  public refreshActivityFeed() {
    if (this.waitToComplete === false) {
      this.waitToComplete = true;
      this.loading = CssClass.loading;
      this.pageModel.pageNo = 1;
      this.isRefresh = true;
      this.moreRecords = true;
      this.getActivityFeed();
    }
  }

  public getActivityFeed() {
    this.subscription = this.activityFeedService
      .getAllFeeds(this.pageModel)
      .pipe(last())
      .subscribe(
        data => {
          if (this.pageModel.pageNo > 1 && data.activityFeed.length === 0) {
            this.pageModel.pageNo = this.pageModel.pageNo - 1;
            this.moreRecords = false;
          }
          this.recordCount = data.pagination.pageCount;
          if (this.isRefresh === true) {
            this.feeds = [];
            this.isRefresh = false;
          }
          this.mappData(data.activityFeed);
          this.waitToComplete = false;
          this.loading = "";
          this.messageText = MessageText.gridEmpty;
        },
        err => {
          if (
            err instanceof HttpErrorResponse &&
            err.status === HttpStatusCode.badRequest
          ) {
            const message = err.error.error || err.error.Message;
            this.notificationsService.error(message);
          }
          if (this.pageModel.pageNo > 1) {
            this.pageModel.pageNo = this.pageModel.pageNo - 1;
          }
          this.waitToComplete = false;
          this.loading = "";
        }
      );
  }

  public loadAcivityTypes() {
    this.activityTypes.push(
      new ActivityTypeIcon("bid submitted", "submit-bid")
    );
    this.activityTypes.push(
      new ActivityTypeIcon("bid declined", "decline-bid")
    );
    this.activityTypes.push(new ActivityTypeIcon("bid recalled", "recall-bid"));
    this.activityTypes.push(
      new ActivityTypeIcon("vendor profile updated", "update-profile")
    );
    this.activityTypes.push(
      new ActivityTypeIcon("new address added", "vendor-address-add")
    );
    this.activityTypes.push(
      new ActivityTypeIcon("address deleted", "contact-delete")
    );
    this.activityTypes.push(
      new ActivityTypeIcon("new contact information added", "vendor-info-add")
    );
    this.activityTypes.push(
      new ActivityTypeIcon("contact information deleted", "vendor-info-delete")
    );
    this.activityTypes.push(
      new ActivityTypeIcon("new contact created", "new-contact")
    );
    this.activityTypes.push(
      new ActivityTypeIcon("contact deleted", "vendor-address-delete")
    );
    this.activityTypes.push(
      new ActivityTypeIcon("invoice submitted", "submit-invoice")
    );
    this.activityTypes.push(
      new ActivityTypeIcon("invoice deleted", "decline-invoice")
    );
  }

  public mappData(data) {
    const dataToUpdate = data;
    for (let i = 0; i < dataToUpdate.length; i++) {
      const itemExist = this.activityTypes.filter(
        x => x.type === dataToUpdate[i].actionType.toLowerCase()
      );
      dataToUpdate[i].icon =
        itemExist.length > 0 ? itemExist[0].icon : "update-profile";
      dataToUpdate[i].timeElapsed = Moment(
        dataToUpdate[i].activityAt
      ).fromNow();
      this.feeds.push(dataToUpdate[i]);
    }
  }

  public apply() {
    const customScrollBar: any = $(".mCustomScrollbar");
    customScrollBar.mCustomScrollbar({
      axis: "y", // horizontal scrollbar
      theme: "dark",
      scrollInertia: 300,
      scrollbarPosition: "inside"
    });
  }

  public loadActivityAtScrollEnd() {
    if (this.waitToComplete === false) {
      this.waitToComplete = true;
      this.loading = CssClass.loading;
      this.pageModel.pageNo = this.pageModel.pageNo + 1;
      this.getActivityFeed();
    }
  }

  public loadMore() {
    this.pageModel.pageNo = this.pageModel.pageNo + 1;
    this.getActivityFeed();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
