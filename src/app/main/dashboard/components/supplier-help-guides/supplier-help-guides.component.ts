import { Component, OnInit, Inject } from "@angular/core";
import { APP_BASE_HREF } from "@angular/common";
import { Guide, RedirectUrl } from "../../../../core/configuration/config";

@Component({
  selector: "app-supplier-help-guides",
  templateUrl: "./supplier-help-guides.component.html",
  styleUrls: ["./supplier-help-guides.component.css"]
})
export class SupplierHelpGuidesComponent implements OnInit {
  guide = Guide;
  constructor(@Inject(APP_BASE_HREF) private baseHref: string) {}

  regOrganisationPath = "";
  regPersonPath = "";
  invoicePath = "";
  profilePath = "";
  biddingPath = "";
  urls = RedirectUrl;

  ngOnInit() {
    this.regOrganisationPath =
      this.baseHref +
      this.urls.guideView +
      "?doc=" +
      this.guide.registerOrganisation;
    this.regPersonPath =
      this.baseHref + this.urls.guideView + "?doc=" + this.guide.registerPerson;
    this.biddingPath =
      this.baseHref + this.urls.guideView + "?doc=" + this.guide.bidding;
  }
}
