// tslint:disable
import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/observable/throw';
import { environment } from '../../../environments/environment';
import { DashBoardData } from './models/dashboard-data.model';
import { CompanyMeta } from './models/company-meta.model';

@Injectable()
export class DashBoardDataService {

  request$: EventEmitter<any>;
  private headers: HttpHeaders;
  private servicePath: string;
  private servicePath2: string;

  private handleError(error: any) {
    this.request$.emit('finished');
    if (error instanceof Response) {
      return _throw(error.json()['error'] || 'backend server error');
    }
    return _throw(error || 'backend server error');
  }

  constructor(private http: HttpClient) {
    this.request$ = new EventEmitter();
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.servicePath = environment.base_api_url + '/api/dashboard';
    this.servicePath2 = '/api';
  }

  getAllData(): Observable<HttpResponse<DashBoardData>> {
    return this.http.get<DashBoardData>(environment.base_api_url + this.servicePath2 + '/DashBoardData/GetDashBoard', { observe: 'response' });
  }

  getCompanyMeta(): Observable<CompanyMeta> {
    return this.http.get<CompanyMeta>(environment.base_api_url + this.servicePath2 + '/CompanyMeta');
  }

}
