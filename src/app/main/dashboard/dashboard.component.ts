// tslint:disable
import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashBoardDataService } from './dashboard-data.service';
import { DashBoardData } from './models/dashboard-data.model';
import { ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications/dist';
import { HttpStatusCode } from '../../core/configuration/config';
import { HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {

  data: DashBoardData = new DashBoardData();
  show = true;
  subscription: any;
  constructor(private route: ActivatedRoute,
    private dashBoardDataService: DashBoardDataService,
    private notificationsService: NotificationsService) { }

  ngOnInit() {
    this.initComponent();
  }

  private initComponent() {

    this.route.data
      .subscribe((data: { dasboard: HttpResponse<DashBoardData> }) => {
        if (data && data.dasboard) {
          // this.refreshUI(data.dasboard);
          this.data = data.dasboard.body;
        }

        if (this.isCached(data.dasboard)) {
          this.refreshData();
        }
      },
      error => {
        if (error instanceof HttpErrorResponse
          && (error.status === HttpStatusCode.badRequest)) {
          const message = error.error.error || error.error.message;
          this.notificationsService.error(message);
        }
      });
  }

  refreshData() {
    this.subscription = this.dashBoardDataService.getAllData()
      .subscribe(data => {
        if (data) {
          this.data = data.body;
        }
      },
      err => {
        if (err instanceof HttpErrorResponse
          && (err.status === 400)) {
          const message = err.error.error || err.error.message;
          this.notificationsService.error(message);
        }
      }
      );
  }

  isCached(res: HttpResponse<DashBoardData>): boolean {
    if (!res || res.headers instanceof HttpHeaders) {
      return false;
    }
    return true;
  }

  public applyScrollBar() {
    const customScrollBar: any = $('.mCustomScrollbar');
    customScrollBar.mCustomScrollbar({
      axis: 'y', // horizontal scrollbar
      theme: 'dark'
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
