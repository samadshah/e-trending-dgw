export class ActivityFeed {
    constructor(public message: string,
        public actionType: string,
        public activityAt: Date,
        public activityBy: string,
        public icon: string,
        public timeElapsed: string) {
    }
}

export interface IActivityFeed {
    message: string;
    actionType: string;
    activityAt: Date;
    activityBy: string;
    icon: string;
    timeElapsed: string;
}


export class ActivityTypeIcon {
    constructor(type, icon) {
        this.type = type;
        this.icon = icon;
    }
    public type: string;
    public icon: string;
}

export class ActivityFeedWrapper {
    public activityFeed: Array<ActivityFeed>;
    public pagination: LazyLoadingPageModel;
}

export class LazyLoadingPageModel {
    public pageNo: number;
    public pageSize: number;
    public pageCount: number;
}

