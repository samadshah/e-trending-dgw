export class DashBoardData {
    public documents: Array<ChartData>;
    public rfQs: Array<ChartData>;
}

export class ChartData {
    public filterColumns: Array<ChartDataColumn>;
    public count: number;
    public Status: string;
}

export class ChartDataColumn {
    public name: string;
    public value: string;
}

export class RequestBids {
    public status: string;
}
