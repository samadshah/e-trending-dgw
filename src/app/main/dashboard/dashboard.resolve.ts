import { Location } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications/dist';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { first } from 'rxjs/operators';

import { MessageText } from '../../core/configuration/config';
import { DashBoardDataService } from './dashboard-data.service';
import { DashBoardData } from './models/dashboard-data.model';

@Injectable()
export class DashboardResolve implements Resolve<HttpResponse<DashBoardData>> {

    constructor(private dashBoardDataService: DashBoardDataService,
        private router: Router,
        private location: Location,
        private notificationsService: NotificationsService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<HttpResponse<DashBoardData>> {
        return this.dashBoardDataService.getAllData()
            .pipe(
            first()
            )
            .catch(() => {
                this.notificationsService.error(MessageText.unableToFetchData);
                return of(null);
            });
    }
}
