import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { MainComponent  } from './main.component';
import { environment } from '../../environments/environment';

// Modules
import { LayoutModule } from '../layout/layout.module';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { MainRoutingModule  } from './main-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    LayoutModule,
    MainRoutingModule
  ],
  declarations: [
    MainComponent,
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: environment.root_path }
  ],
})
export class MainModule { }
