//!
var captcha = (function() {
  return {
    realPerson: function(selector, hashSelector) {
      $(selector).realperson({
        chars: $.realperson.alphanumeric,
        hashName: '{n}Hash'
      });
    },
    getHash: function(selector) {
      return $(selector).realperson('getHash');
    }
  };
})(captcha | {});


$(document).ready(function () {

    // $('#list-table').addClass('nowrap').dataTable({
    //     responsive: true
    //     , "pageLength": 50
    //     , "searching": false
    //     , "sDom": 'Rfrtlip'
    //     , "language": {
    //         "info": "_START_ - _END_ of _TOTAL_ records"
    //     }
    //     , "sDom": '<"clear">t<"F"p><"F"ilr>'
    //         //,  "pageLength": 19
    //     ,   "order": [[ 0, "desc" ]]
    // });
    // $('#data-table').addClass('nowrap').dataTable({
    //     responsive: true
    //     , "searching": false
    //     , paging: false
    //     , "info": false
    // });
    // $('#billing-table').addClass('nowrap').dataTable({
    //     responsive: true
    //     , "searching": false
    //     , paging: true
    //     , "info": false
    // });
    // $(".dropdown-menu").click(function () {
    //     event.stopPropagation();
    // });
    // $('[data-toggle="tooltip"]').tooltip();
    // $('[data-toggle="dropdown"][title]').tooltip();
    // $(".mCustomScrollbar").mCustomScrollbar({
    //     axis: "y", // horizontal scrollbar
    //     theme: "dark"
    // });
    // $(".navbar-header .navbar-toggle").on("click",function () {
    //     $(".left-navigation").css("width", "300px");
    // });

    var wid = window.innerWidth;
    $(document).on('click', function (e) {
        if (wid < 768) {
            var container = $(".left-navigation");
            var openingBtn =$(".mbile-toggle");
            var innerLink = $(".left-navigation a");
            // event.stopPropagation();
            // if the target of the click isn't the container nor a descendant of the container
            if ((!container.is(e.target) && container.has(e.target).length === 0 && !openingBtn.is(e.target)) || innerLink.is(e.target))
            {
                $(".left-navigation").css("width", "0");
            }

            innerLink.click(function(){
              container.css('width',0);
            });
        }


    });



    $('body').on('click', '.navbar-header .navbar-toggle', function () {
        $(".left-navigation").css("width", "300px");
    });

    $('body').on('click', '.close-menu', function () {
        $(".left-navigation").css("width", "0");
    });

    // $(".search-link").click(function () {
    //     $(".search-holder").slideToggle();
    // });
    // $(".close-popup").click(function () {
    //     $(this).closest(".open").removeClass("open");
    // });
    // $(".filter-btn").click(function () {
    //     $(".filter-box .filter-box-holder.filter-result").slideToggle("fast");
    // });
    // $(".next-step").click(function () {
    //     $(".bid-02").show();
    //     $(".bid-01").hide();
    // });
    // $(".prev-step").click(function () {
    //     $(".bid-01").show();
    //     $(".bid-02").hide();
    // });
    // $(".data-btn-holder.data-actions .edit-button").click(function () {
    //     $(this).addClass("active");
    //     $(".main-title .edit-options , .checkbox-box .edit-options").show();
    //     $(".custom-control.form-control").attr("readonly", false);
    // });
    // $(".end-step").click(function () {
    //     $(this).closest(".open").removeClass("open");
    //     $('#decline').modal();
    // })
    // setTimeout(function(){
    //     $('[data-toggle="tooltip"]').tooltip();
    // },0);

});
/*$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});*/
