----------------------------------------
Upgarde from 4 to 5
----------------------------------------

npm install
npm install typescript@2.4.2 --save-exact

----------------------------------------
Upgarde from 5 to 5.1
----------------------------------------
Step 0 = Upgrade NodeJs to 8.9.3
Step 1 = Angular CLI Upgrade
Step 2 = npm install
Step 3 = npm install typescript@2.5.3 --save-exact


-----------------------------------------
Angular CLI Upgrade
-----------------------------------------

Global package:
-------------------
npm uninstall -g @angular/cli
npm cache clean
# if npm version is > 5 then use `npm cache verify` to avoid errors (or to avoid using --force)
npm install -g @angular/cli@latest


Local project package:
-----------------------
delete node_modules folder
npm install --save-dev @angular/cli@latest
npm install


Local ServeTC document that need to run during the UAT 
------------------------
ng serve --prod --sourcemaps --build-optimizer -e desi -o
ng serve --prod --sourcemaps --aot=false -e desi -o
ng serve --prod --sourcemaps -e desi -o


npm install --save @angular/animations@5.2.8 @angular/common@5.2.8 @angular/compiler@5.2.8 @angular/core@5.2.8 @angular/forms@5.2.8 @angular/http@5.2.8 @angular/platform-browser@5.2.8 @angular/platform-browser-dynamic@5.2.8 @angular/router@5.2.8

npm install --save-dev @angular/compiler-cli@latest @angular/language-service@latest